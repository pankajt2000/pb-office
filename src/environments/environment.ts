// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  app_title: 'PB Office Dev',

  logo_title: 'PB Office Dev',
  logo_background: 'black',

  session_id: '42c4858ca04b0c43b2aa60f24bd0bd15',
  app_auth_header: 'x9jUtZNpfAnLaSvs-GtZ83xskNfRP9g9L',

  gmap_api_key: 'AIzaSyCQxAxmdLKypd6PAxQ8P4jNwqymfdPT5uk',
  google_client_id: '12676953707-e2kdrg1lq5utpun03jfltbr92vh75h59.apps.googleusercontent.com',

  web_base_url: 'https://stage.pocketbroker.com',
  auth_base_url: 'https://auth-api-stage.pocketbroker.com/v1/',
  app_base_url: 'https://app-api.pocketbroker.com',
  agent_base_url: 'https://agent-api-stage.pocketbroker.com/v1/',
  media_base_url: 'https://media-aws-stage.pocketbroker.com/media/',
  transcode_media_base_url: 'https://media-aws-stage.pocketbroker.com/transcode/',
  _360_base_url: 'https://agent-api-stage.pocketbroker.com/v1/three60',
  admin_base_url: 'https://admin-stage.pocketbroker.com',
};
