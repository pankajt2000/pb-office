export const environment = {
  production: true,

  app_title: 'PB Office',

  logo_title: 'PB Office',
  logo_background: '#4a89dc',

  session_id: '42c4858ca04b0c43b2aa60f24bd0bd15',
  app_auth_header: 'x9jUtZNpfAnLaSvs-GtZ83xskNfRP9g9L',
  
  gmap_api_key: 'AIzaSyCQxAxmdLKypd6PAxQ8P4jNwqymfdPT5uk',
  google_client_id: '129109542624-frh5u0k3njpsodnhdic2rh1djsgusvbj.apps.googleusercontent.com',
  
  web_base_url: 'https://pocketbroker.com',
  auth_base_url: 'https://auth-api.pocketbroker.com/v1/',
  app_base_url: 'https://app-api.pocketbroker.com',
  agent_base_url: 'https://agent-api.pocketbroker.com/v1/',
  media_base_url: 'https://media-aws.pocketbroker.com/media/',
  transcode_media_base_url: 'https://media-aws.pocketbroker.com/transcode/',
  _360_base_url: 'https://agent-api.pocketbroker.com/v1/three60',
  admin_base_url: 'https://admin.pocketbroker.com',
  login_page_url: '/app/login',
  access_token_local_storage: 'access_token',
  user_local_storage: 'user_data',
  default_content_type: 'application/json',
};
