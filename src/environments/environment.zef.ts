// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  app_title: 'PB Office Dev',

  logo_title: 'PB Office Dev',
  logo_background: 'black',

  session_id: '0e8e880b408f5767a2fe40b69fe29dfb',
  app_auth_header: 'lhy63ivDt1PE4XdK-kbgaMo2Ce9qtACgA',

  gmap_api_key: 'AIzaSyCQxAxmdLKypd6PAxQ8P4jNwqymfdPT5uk',
  google_client_id: '12676953707-e2kdrg1lq5utpun03jfltbr92vh75h59.apps.googleusercontent.com',

  web_base_url: 'http://pocketbroker.local',
  auth_base_url: 'http://auth-api.pocketbroker.local/v1/',
  app_base_url: 'http://app-api.pocketbroker.local',
  agent_base_url: 'http://agent-api.pocketbroker.local/v1/',
  media_base_url: 'http://media.pocketbroker.local/media/',
  transcode_media_base_url: 'http://media.pocketbroker.local/transcode/',
  _360_base_url: 'http://agent-api.pocketbroker.local/v1/three60',
  admin_base_url: 'http://admin.pocketbroker.local',
};
