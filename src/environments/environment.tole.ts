// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  app_title: 'PB Office Dev',

  logo_title: 'PB Office Dev',
  logo_background: 'black',

  session_id: '42c4858ca04b0c43b2aa60f24bd0bd15',
  app_auth_header: 'x9jUtZNpfAnLaSvs-GtZ83xskNfRP9g9L',

  gmap_api_key: 'AIzaSyCQxAxmdLKypd6PAxQ8P4jNwqymfdPT5uk',
  google_client_id: '108629568286-mgnsf0e6rr76rarealdgsovnlsivt2u1.apps.googleusercontent.com',

  web_base_url: 'http://adomee.lokal.com',
  auth_base_url: 'http://auth.api.adomee.lokal.com/v1/',
  app_base_url: 'http://app.api.adomee.lokal.com',
  agent_base_url: 'http://agent.api.adomee.lokal.com/v1/',
  media_base_url: 'http://adomee.media.lokal.com/media/',
  transcode_media_base_url: 'http://adomee.media.lokal.com/transcode/',
  _360_base_url: 'http://agent.api.adomee.lokal.com/v1/three60',
  admin_base_url: 'http://admin.adomee.lokal.com',
};
