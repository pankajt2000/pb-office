export const environment = {
  production: true,

  app_title: 'Stage Office',

  logo_title: 'Stage Office',
  logo_background: 'red',

  session_id: '42c4858ca04b0c43b2aa60f24bd0bd15',
  app_auth_header: 'x9jUtZNpfAnLaSvs-GtZ83xskNfRP9g9L',

  gmap_api_key: 'AIzaSyCQxAxmdLKypd6PAxQ8P4jNwqymfdPT5uk',
  google_client_id: '891789703844-snedk0aos645iap9cl9celpiaj8dp8h5.apps.googleusercontent.com',
  
  web_base_url: 'https://stage.pocketbroker.com',
  auth_base_url: 'https://auth-api-stage.pocketbroker.com/v1/',
  app_base_url: 'https://app-api.pocketbroker.com',
  agent_base_url: 'https://agent-api-stage.pocketbroker.com/v1/',
  media_base_url: 'https://media-aws-stage.pocketbroker.com/media/',
  transcode_media_base_url: 'https://media-aws-stage.pocketbroker.com/transcode/',
  _360_base_url: 'https://agent-api-stage.pocketbroker.com/v1/three60',
  admin_base_url: 'https://admin-stage.pocketbroker.com',
  login_page_url: '/app/login',
  access_token_local_storage: 'access_token',
  user_local_storage: 'user_data',
  default_content_type: 'application/json',
};
