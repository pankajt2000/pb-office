import { DeserializableInterface } from '../../shared/models/deserializable.interface';

export class DosFormListModel implements DeserializableInterface<DosFormListModel> {

  form_id: string;

  agent_name: string;

  status: string;

  date: string;

  client_name: string;
  client_email: string;

  constructor() {}

  deserialize(input: any): DosFormListModel {
    Object.assign(this, input);
    return this;
  }

}
