import { DeserializableInterface } from '../../shared/models/deserializable.interface';

export class DosFormDetailsModel implements DeserializableInterface<DosFormDetailsModel> {

  form_id: string;

  status: string;

  time_created: string;

  client_id: string;
  client_name: string;

  data: {
    broker_in_interest_of: string,
    date: string,
    dual_agent: boolean,
    advance_informed_consent: boolean,
    represents_landlord: boolean,
    represents_tenant: boolean,
    common_signature_value: string,
    signature_tenants: boolean,
    signature_landlords: boolean,
    client_data_email: string,
    client_data_signature: {
      value: string,
      url: string,
      filename: string
    },
    agent_data_email: string,
    agent_data_signature: {
      value: string,
      url: string,
      filename: string
    }
  };

  constructor() {}

  deserialize(input: any): DosFormDetailsModel {
    Object.assign(this, input);
    return this;
  }

}
