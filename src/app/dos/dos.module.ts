import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DosListComponent } from './dos-list/dos-list.component';
import { DosService } from './services/dos.service';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { NewDosFormComponent } from './new-dos-form/new-dos-form.component';
import { EditDosComponent } from './edit-dos/edit-dos.component';
import { DosDetailsComponent } from './dos-details/dos-details.component';
import { BsDropdownModule } from 'ngx-bootstrap';



@NgModule({
  providers: [
    DosService
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    BsDropdownModule.forRoot(),

    SharedModule
  ],
  declarations: [
    DosListComponent,
    NewDosFormComponent,
    EditDosComponent,
    DosDetailsComponent,
  ],
  exports: [
    DosListComponent
  ]
})
export class DosModule { }
