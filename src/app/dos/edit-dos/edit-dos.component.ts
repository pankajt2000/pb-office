import { Component, OnInit } from '@angular/core';
import { DosFormDetailsModel } from '../models/dos-form-details.model';
import { DosService } from '../services/dos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { AgentService } from '../../agent/services/agent.service';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { AgentModel } from '../../agent/models/agent.model';
import { environment } from '../../../environments/environment';
import { SendModalComponent } from '../../listings/send-modal/send-modal.component';
import { BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-edit-dos',
  templateUrl: './edit-dos.component.html',
  styleUrls: ['./edit-dos.component.less']
})
export class EditDosComponent implements OnInit {

  dos_form: DosFormDetailsModel;
  agents: AgentModel[];

  form: FormGroup;

  loading: boolean;

  typeaheads = {
    represents_landlord: {
      values: [],
      typeahead: new Subject<String>(),
      loading: false,
      display_prop: 'name',
      value_prop: 'user_id',
      is_complex: false,
      switch_map: (term) => this.agentService.getAgents(0, 0, term, (a, b) => {
        return a.name.charCodeAt(0) - b.name.charCodeAt(0);
      })
    },
    represents_tenant: {
      values: [],
      typeahead: new Subject<String>(),
      loading: false,
      display_prop: 'name',
      value_prop: 'user_id',
      is_complex: false,
      switch_map: (term) => this.agentService.getAgents(0, 0, term, (a, b) => {
        return a.name.charCodeAt(0) - b.name.charCodeAt(0);
      })
    }
  };

  constructor(
    private dosService: DosService,
    private agentService: AgentService,
    private alertService: BootstrapAlertService,
    private modalService: BsModalService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.params.subscribe(params => {
      this.loading = true;

      const dos_form_id = params['dos_form_id'] || 0;
      window.scroll(0, 0);

      this.initDosForm(dos_form_id);
    })
  }

  ngOnInit() {

  }

  initDosForm(dos_form_id: string) {
    this.agentService.getAgents()
      .mergeMap(data => {
        this.agents = data[0];

        return this.dosService.getDosForm(dos_form_id);
      })
      .map(form => this.dos_form = form)
      .subscribe(() => this.initForm());
  }

  initForm() {
    this.form = this.fb.group({
      status: [this.dos_form.status, Validators.required],
      data: this.fb.group({
        broker_in_interest_of: [this.dos_form.data.broker_in_interest_of, Validators.required],
        dual_agent: [this.dos_form.data.dual_agent],
        advance_informed_consent: [this.dos_form.data.advance_informed_consent],
        represents_landlord: [this.dos_form.data.represents_landlord],
        represents_tenant: [this.dos_form.data.represents_tenant]
      })
    });

    this.loading = false;

    this.initTypeahead();
  }

  initTypeahead() {
    for (const item in this.typeaheads) {
      let current = this.typeaheads[item];

      const existing = this.dos_form.data[item];

      if (existing) {
        const to_push = {
          name: this.agents.find(a => a.user_id === existing)[current.display_prop],
          value: this.agents.find(a => a.user_id === existing)[current.value_prop]
        };

        current.values = [...current.values, to_push];
      }


      current.typeahead.pipe(
        tap(() => current.loading = true),
        distinctUntilChanged(),
        debounceTime(200),
        switchMap(term => current.switch_map(term))
      ).subscribe(item => {
        const it = item.length === 2 && Array.isArray(item[0]) ? item[0] : item;

        current.values = it.map(object => {
          const name = current.display_prop ? object[current.display_prop] : object;
          const value = current.value_prop ? object[current.value_prop] : object;

          return { name, value };
        });

        current.loading = false;
      }, () => {
        current.values = [];
        current.loading = false;
      })
    }
  }

  save(redirect: boolean = true) {
    if (this.form.invalid) return;

    const agency = this.form.get('data.broker_in_interest_of').value;

    if (agency === 'da' || agency === 'dadsa') {
      this.form.get('data.dual_agent').patchValue(true);
    } else {
      this.form.get('data.advance_informed_consent').patchValue(false);
      this.form.get('data.represents_tenant').patchValue(null);
      this.form.get('data.represents_landlord').patchValue(null);
    }

    this.dosService.updateDosForm(this.dos_form.form_id, this.form.value).subscribe(() => {
      this.alertService.showSucccess('Form successfully updated.');

      if (redirect) {
        this.router.navigate(['/client', 'dos', 'list']);
      } else {
        this.initDosForm(this.dos_form.form_id);
      }
    }, () => {
      this.alertService.showError('An error occurred while updating the form.');
    });
  }

  submit(send_type: string) {
    if (this.form.invalid) return;

    if (this.form.invalid) return;

    const agency = this.form.get('data.broker_in_interest_of').value;

    if (agency === 'da' || agency === 'dadsa') {
      this.form.get('data.dual_agent').patchValue(true);
    } else {
      this.form.get('data.advance_informed_consent').patchValue(false);
      this.form.get('data.represents_tenant').patchValue(null);
      this.form.get('data.represents_landlord').patchValue(null);
    }

    this.dosService.updateDosForm(this.dos_form.form_id, this.form.value).subscribe((form: DosFormDetailsModel) => {
        const subject = 'Your DOS form';
        const cbf = send_type === 'web' ? `here: ${environment.web_base_url}/dos/${this.dos_form.form_id}` : 'attached to this email';
        const message = `Hi ${form.client_name}!

Your DOS form can be found ${cbf}`;

        let initialState = {
          email: form.data.client_data_email, subject, message
        };

        if (send_type === 'pdf') initialState['dosFormId'] = form.form_id;

        const modal = this.modalService.show(SendModalComponent, { initialState });
    });
  }

  cancel() {
    this.router.navigate(['/client', 'dos', 'list']);
  }

}
