import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { DosFormListModel } from '../models/dos-form-list.model';
import { DosFormDetailsModel } from '../models/dos-form-details.model';

@Injectable()
export class DosService {

  constructor(private api: AgentApiService) {}

  getDosForms(offset = 0, limit = 10, agent_id?: string) {
    return this.api.get(`dos?offset=${offset}&limit=${limit}&agent_id=${agent_id}`, { observe: 'response' }).map(response => {
      return [
        response.body.map(dos => new DosFormListModel().deserialize(dos)),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

  getDosForm(dos_form_id: string) {
    return this.api.get(`dos/${dos_form_id}`).map(form => new DosFormDetailsModel().deserialize(form));
  }

  createDosForm(client_id: string) {
    return this.api.post(`dos`, { client_id }).map(res => res.new_form_id);
  }

  updateDosForm(dos_form_id: string, form_data) {
    return this.api.put(`dos/${dos_form_id}`, form_data);
  }

  deleteDosForm(form_id: string) {
    return this.api.delete(`dos/${form_id}`);
  }

}
