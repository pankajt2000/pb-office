import { Component, OnInit } from '@angular/core';
import { DosService } from '../services/dos.service';
import { AgentService } from '../../agent/services/agent.service';
import { ActivatedRoute } from '@angular/router';
import { DosFormDetailsModel } from '../models/dos-form-details.model';
import { environment } from '../../../environments/environment';
import { SendModalComponent } from '../../listings/send-modal/send-modal.component';
import { BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-dos-details',
  templateUrl: './dos-details.component.html',
  styleUrls: ['./dos-details.component.less']
})
export class DosDetailsComponent implements OnInit {

  loading = false;

  dos_form: DosFormDetailsModel;

  represents_tenant: string = null;
  represents_landlord: string = null;

  constructor(
    private dosService: DosService,
    private agentService: AgentService,
    private modalService: BsModalService,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {
      const dos_form_id = params['dos_form_id'];

      this.initForm(dos_form_id);
    })
  }

  ngOnInit() {
  }

  initForm(dos_form_id) {
    this.loading = true;

    this.dosService.getDosForm(dos_form_id).subscribe(form => {
      this.dos_form = form;

      if (this.dos_form.data.represents_tenant) {
        this.agentService.getAgents().subscribe(res => {
          const agents = res[0];

          this.represents_tenant = agents.find(a => a.user_id === this.dos_form.data.represents_tenant).name;
        })
      }

      if (this.dos_form.data.represents_landlord) {
        this.agentService.getAgents().subscribe(res => {
          const agents = res[0];

          this.represents_landlord = agents.find(a => a.user_id === this.dos_form.data.represents_landlord).name;
        })
      }

      this.loading = false;
    })
  }

  send(send_type: string) {
    const subject = 'Your DOS form';
    const cbf = send_type === 'web' ? `here: ${environment.web_base_url}/dos/${this.dos_form.form_id}` : 'attached to this email';
    const message = `Hi ${this.dos_form.client_name}!

Your DOS form can be found ${cbf}`;

    let initialState = {
      email: this.dos_form.data.client_data_email, subject, message
    };

    if (send_type === 'pdf') initialState['dosFormId'] = this.dos_form.form_id;

    const modal = this.modalService.show(SendModalComponent, { initialState });
  }
}
