import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DosListComponent } from './dos-list.component';

describe('DosListComponent', () => {
  let component: DosListComponent;
  let fixture: ComponentFixture<DosListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DosListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
