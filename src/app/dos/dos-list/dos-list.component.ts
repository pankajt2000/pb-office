import { Component, OnInit } from '@angular/core';
import { AgentModel } from '../../agent/models/agent.model';
import { BsModalService } from 'ngx-bootstrap';
import { AuthService } from '../../authentication/services/auth.service';
import { AgentService } from '../../agent/services/agent.service';
import { Router } from '@angular/router';
import { DosFormListModel } from '../models/dos-form-list.model';
import { DosService } from '../services/dos.service';
import { SendModalComponent } from '../../listings/send-modal/send-modal.component';
import { environment } from '../../../environments/environment';
import { ConfirmPopupComponent } from '../../shared/components/confirm-popup/confirm-popup.component';

@Component({
  selector: 'app-dos-list',
  templateUrl: './dos-list.component.html',
  styleUrls: ['./dos-list.component.less']
})
export class DosListComponent implements OnInit {

  limit = 15;
  offset = 0;
  loaded = false;
  loading = false;
  total: number;
  dos_forms: DosFormListModel[] = [];

  is_supervisor: boolean = false;

  agent_list: AgentModel[];
  selected_agent_id: any = null;

  constructor(
    private dosService: DosService,
    private modalService: BsModalService,
    private authService: AuthService,
    private agentService: AgentService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initDosForms();
  }

  initDosForms() {
    this.loading = true;
    this.loaded = false;

    this.authService.sessionInfo().subscribe((info) => {
      this.is_supervisor = info.user.roles.map(r => r.toLowerCase()).includes('supervisor');

      if (this.is_supervisor) {
        this.selected_agent_id = "-1";

        this.agentService.getAgents().subscribe((agents) => {
          this.agent_list = agents[0].filter(a => !a.is_test_user && a.active).map(a => { return { user_id: a.user_id, name: a.name }});
        });
      }

      this.loadDosForms();
    })
  }

  loadDosForms(force_refresh: boolean = false) {
    this.loaded = false;
    this.loading = true;

    this.dosService.getDosForms(this.offset, this.limit, this.selected_agent_id).subscribe(data => {
      if (!force_refresh) {
        this.dos_forms = this.dos_forms.concat(data[0]);
      } else {
        this.dos_forms = data[0];
      }
      this.total = data[1];

      this.loaded = true;
      this.loading = false;
    })
  }

  sendViaWeb(form: DosFormListModel) {
    const subject = 'Your DOS form';

    const message = `Hi ${form.client_name}!

Your DOS form can be found here: ${environment.web_base_url}/dos/${form.form_id}`;

    const modal = this.modalService.show(SendModalComponent, {
      initialState: {
        email: form.client_email, subject, message
      }
    })
  }

  sendAsPdf(form: DosFormListModel) {
    const subject = 'Your DOS form';

    const message = `Hi ${form.client_name}!

Your DOS form can be found attached to this message.`;

    const modal = this.modalService.show(SendModalComponent, {
      initialState: {
        email: form.client_email, subject, message, dosFormId: form.form_id
      }
    })
  }

  createNewDosForm() {
    this.router.navigate(['/client', 'dos', 'new']);
  }

  deleteDosForm(form: DosFormListModel) {
    const modal = this.modalService.show(ConfirmPopupComponent, {
      class: 'modal-sm m-t-10', initialState: {
        message: 'Are you sure you wish to delete this form?'
      }
    });

    modal.content.confirm.subscribe(() => {
      this.loaded = false;
      this.loading = true;

      this.dosService.deleteDosForm(form.form_id).subscribe(res => {
        this.loadDosForms(true);
      });
    });
  }

  getFormattedStatus(dos_form: DosFormListModel) {
      return dos_form.status.charAt(0).toUpperCase() + dos_form.status.slice(1);
  }

  loadMore() {
    this.offset = this.dos_forms.length;

    this.loadDosForms();
  }

  isSupervisor() {
    return this.is_supervisor;
  }

}
