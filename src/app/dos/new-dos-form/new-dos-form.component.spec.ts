import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDosFormComponent } from './new-dos-form.component';

describe('NewDosFormComponent', () => {
  let component: NewDosFormComponent;
  let fixture: ComponentFixture<NewDosFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDosFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDosFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
