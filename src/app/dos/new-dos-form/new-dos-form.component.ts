import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService } from '../../client/services/client.service';
import { Subject } from 'rxjs/Subject';
import { debounceTime, distinctUntilChanged, switchMap, tap, concat } from 'rxjs/operators';
import { ClientModel } from '../../client/models/client.model';
import { ClientDetailsModel } from '../../client/models/client-details.model';
import { of } from 'rxjs/observable/of';
import { BsModalService } from 'ngx-bootstrap';
import { CreateClientComponent } from '../../client/create-client/create-client.component';
import { DosService } from '../services/dos.service';
import { AuthService } from '../../authentication/services/auth.service';
import { UserService } from '../../authentication/services/user.service';

@Component({
  selector: 'app-new-dos-form',
  templateUrl: './new-dos-form.component.html',
  styleUrls: ['./new-dos-form.component.less']
})
export class NewDosFormComponent implements OnInit {

  loading = false;
  checking_source = false;

  client_source: string;

  has_signature: boolean;
  is_supervisor: boolean;

  signature_data = null;

  client_data = {
    values: [],
    typeahead: new Subject<string>(),
    loading: false,
    selected: null,
    switch_map: (term) => this.clientService.getClients( 10, 0, 'CURRENT', term),
    on_change: (data) => {
      this.checking_source = true;

      if (!data || !data.value) {
        this.checking_source = false;
        return;
      }

      this.clientService.getClientDetails(data.value).subscribe(client => {
        this.client_source = client.source_value;

        this.checking_source = false;
      })
    },
    create: (term) => {
      const first_name = term.split(' ').shift();
      const last_name = term.split(' ').slice(1).join(' ');

      const modal = this.modalService.show(CreateClientComponent, { initialState: { first_name, last_name } });
      modal.content.client_data = { first_name, last_name };

      modal.content.clientCreated.subscribe(id => {
        this.client_data.values = [{ name: term, value: id }];
        this.client_data.selected = id;
        this.client_data.on_change({ name: term, value: id });
      });
    }
  };

  constructor(
    private dosService: DosService,
    private authService: AuthService,
    private userService: UserService,
    private clientService: ClientService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.loading = true;

    this.route.queryParams
      .map(params => params.client_id)
      .switchMap(id => !!id ? this.clientService.getClientDetails(id) : of({}))
      .subscribe((client: ClientDetailsModel) => {
        this.client_data.selected = client.user_id;
        this.client_data.values.push({ name: client.name, value: client.user_id });

        this.initTypeahead();

        this.authService.sessionInfo().subscribe(session => {
          if (!session.user) return;

          this.has_signature = !!(session.user.preferences && session.user.preferences.agent_signature_id);
          this.is_supervisor = session.user.roles.map(r => r.toLowerCase()).includes('supervisor');

          this.loading = false;
        })
      });
  }

  ngOnInit() {
  }

  onDraw(data_url) {
    const blob = this._dataURItoBlob(data_url);

    const sd = new FormData();
    sd.append('agent-signature', blob, 'agent-signature.png');

    this.signature_data = sd;
  }

  initTypeahead() {
    this.client_data.typeahead.pipe(
      tap(() => this.client_data.loading = true),
      distinctUntilChanged(),
      debounceTime(200),
      switchMap(term => this.client_data.switch_map(term))
    ).subscribe(x => {
      this.client_data.values = x[0].map((client: ClientModel) => {
        return { name: client.name, value: client.user_id }
      });

      this.client_data.loading = false;
    }, () => {
      this.client_data.values = [];
      this.client_data.loading = false;
    });
  }

  submit() {
    if (!this.has_signature && !this.signature_data) {
      return;
    }

    this.loading = true;

    if (this.signature_data !== null) {
        this.userService.updateAgentSignature(this.signature_data).subscribe(() => {
          this.dosService.createDosForm(this.client_data.selected).subscribe(form_id => this.router.navigate(['/client', 'dos', form_id, 'edit']));
        });
    } else {
      this.dosService.createDosForm(this.client_data.selected).subscribe(form_id => this.router.navigate(['/client', 'dos', form_id, 'edit']));
    }
  }

  cancel() {
    this.router.navigate(['/client', 'dos', 'list']);
  }

  private _dataURItoBlob(data_url: string): Blob {

    if (data_url === '') {
      return null;
    }

    const byteString = atob(data_url.split(',')[1]);

    const mimeString = data_url.split(',')[0].split(':')[1].split(';')[0];

    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ab], { type: mimeString });
  }

}
