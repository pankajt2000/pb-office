import { DeserializableInterface } from "../../../../shared/models/deserializable.interface";
import { UserModel } from "../../../../authentication/models/user.model";

export class InquiryDetailsModel implements DeserializableInterface<InquiryDetailsModel> {

  inquiry_id: string;
  src_type: string;
  ad_type?: string;
  subject: string;
  status: 'NEW' | 'PROGRESS' | 'MADE_APPOINTMENT' | 'CLOSED' | 'DEAD_LEAD' | 'IGNORED' | 'CANCELED';
  in_error: boolean;
  errors: {
    account: string,
    error_message: string,
    message: string,
    from: string,
    to: string,
    time: string
  }[];
  reminders: {
    account: string,
    time: string
  }[];
  listing: {
    ad_type: string,
    advertisement_id: string,
    address: string,
    unit: string
  };
  assigned?: UserModel;
  data: {
    parse_result: {
      email: string,
      name: string,
      phone: string,
      address: string,
      unit: string,
      neighborhood: string
    };
    email: string;
    is_broker: boolean;
    client_id: string;
    listing_id: string;
    needs_response: boolean;
    watchers: string[];
    message_count: string;
    messages: {
      account: string,
      type: "INBOUND" | "OUTBOUND",
      id: string,
      from: string,
      first_name?: string;
      to: string,
      ccs?: string[],
      message: string,
      error_message?: string,
      is_reminder?: number,
      time: string,
      notices?: string[]
    }[]
  };
  date: string;

  constructor() { }

  deserialize(input: any): InquiryDetailsModel {
    if (input.assigned) {
      input.assigned = new UserModel().deserialize(input.assigned);
    }
    Object.assign(this, input);
    return this;
  }
}
