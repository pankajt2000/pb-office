import { DeserializableInterface } from "../../../../shared/models/deserializable.interface";
import { UserModel } from "../../../../authentication/models/user.model";

export class InquiryShortModel implements DeserializableInterface<InquiryShortModel> {

  inquiry_id: string;
  status: 'NEW' | 'PROGRESS' | 'MADE_APPOINTMENT' | 'CLOSED' | 'DEAD_LEAD' | 'IGNORED' | 'CANCELED';

  assigned?: UserModel;

  listing_id: string;
  client_id: string;

  subject: string;

  address: string;
  unit: string;
  name: string;
  email: string;

  alarm: boolean;

  needs_response: boolean;
  has_error: boolean;
  has_reminder: boolean;
  is_broker: boolean;

  message_count: number;

  src_type: string;
  ad_type?: string;

  date: string;
  last_message_time: string;
  
  constructor() { }

  deserialize(input: any): InquiryShortModel {
    if (input.assigned) {
      input.assigned = new UserModel().deserialize(input.assigned);
    }

    Object.assign(this, input);
    return this;
  }
}
