import { Component, OnInit } from '@angular/core';
import { I18nPluralPipe } from '@angular/common';
import { InquiryShortModel } from '../inquiries/models/inquiry-short.model';
import { InquiriesService } from '../../../inquiries/services/inquiries.service';
import { InquiryStatusComponent } from "../../../inquiries/inquiry-status/inquiry-status.component";
import { BootstrapAlertService } from "ngx-bootstrap-alert-service";

@Component({
  selector: 'app-inquiries',
  templateUrl: './inquiries.component.html',
  styleUrls: ['./inquiries.component.less']
})
export class InquiriesComponent implements OnInit {

  msgPlural = {
    '=1': 'message',
    'other': 'messages'
  };

  loading = true;
  inquiries: InquiryShortModel[];

  offset: number = 0;
  limit: number = 1;

  constructor(
    private inquiriesService: InquiriesService,
    private alertService: BootstrapAlertService
  ) { }

  ngOnInit() {
    this.initInquiries();
  }

  initInquiries() {
    this.loading = true;
    console.log(this.limit);
    this.inquiriesService.getInquiries(this.offset, this.limit).subscribe(data => {
      this.inquiries = data[0];
      console.log(data);
      this.loading = false;
    }, (_) => {
      this.loading = false;

      this.alertService.showError('Could not fetch inquiries.');
    });
  }

  getBadgeText(inquiry_src: string) {
    switch (inquiry_src) {
      case 'street_easy_inquiry':
        return 'StreetEasy';
      case 'rent_hop_inquiry':
        return 'RentHop';
      default:
        return 'Unknown';
    }
  }

  getDateFromTimestamp(ts: string) {
    return new Date(+ts * 1000);
  }

  needsResponse(inquiry: InquiryShortModel): boolean {
    return inquiry.needs_response && !inquiry.is_broker && !inquiry.has_error && inquiry.status === 'PROGRESS';
  }

}
