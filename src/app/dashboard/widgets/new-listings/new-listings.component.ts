import { Component, OnInit, TemplateRef } from '@angular/core';
import { ListingService } from '../../../listings/services/listing.service';
import { ListingModel } from '../../../listings/models/listing.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import 'rxjs/add/observable/combineLatest';
import { SendModalComponent } from '../../../listings/send-modal/send-modal.component';
import { SavedListingService } from '../../../listings/services/saved-listing.service';
import { ClientService } from '../../../client/services/client.service';
import { ClientModel } from '../../../client/models/client.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-new-listings',
  templateUrl: './new-listings.component.html',
  styleUrls: ['./new-listings.component.less']
})
export class NewListingsComponent implements OnInit {

  modalPhotosRef: BsModalRef;
  modalLoadMoreRef: BsModalRef;
  modalItem: ListingModel = null;
  modalItemPhotos: string[] = [];
  data: { client: any, listings: ListingModel[] }[] = []; //ListingDetailsModel
  client: ClientModel;

  modalConfig = {
    animated: true,
    backdrop: true,
  };

  slickConfig = {
    dots: true,
  };
  loading = true;

  constructor(
    public router: Router,
    public listingService: ListingService,
    public savedListingService: SavedListingService,
    private clientService: ClientService,
    private modalService: BsModalService) {
    this.clientService.getCurrentClient().subscribe(client => this.client = client);
  }

  openPhotos(template: TemplateRef<any>, item: ListingModel) {
    this.modalItem = item;

    this.modalService.onShown.subscribe((reason: string) => {
      window.dispatchEvent(new Event('resize'));
    });

    this.modalPhotosRef = this.modalService.show(template, this.modalConfig);
    this.modalItemPhotos = this.modalItem.getPhotoUrls();
  }

  openLoadMore(template) {
    this.modalLoadMoreRef = this.modalService.show(template, this.modalConfig);
  }

  goToSearch(clientId) {
      this.router.navigate(['/search/clients-new-listings/' + clientId]);
  }

  ngOnInit() {
    this.listingService.getNewListings(0, 3)
      .subscribe((data) => {
        this.data = data;
        this.loading = false;
      });
  }

  sendListing(item: ListingModel) {
    this.modalService.show(SendModalComponent, { class: 'modal-lg m-t-10', initialState: { listings: [item] } });
  }

}
