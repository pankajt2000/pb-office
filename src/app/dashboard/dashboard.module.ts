import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { ClientsComponent } from './widgets/clients/clients.component'
import { CompsComponent } from './widgets/comps/comps.component'
import { NewListingsComponent } from './widgets/new-listings/new-listings.component';
import { NewestApartmentsComponent } from './widgets/newestapartments/newestapartments.component'
import { RentalSalesStatesComponent } from './widgets/rentalsalesstates/rentalsalesstates.component'
import { ApiService } from '../services/api/api.service';
import { ModalModule, TabsModule } from 'ngx-bootstrap';
import { SlickModule } from 'ngx-slick';
import { RouterModule } from '@angular/router';
import { SavedListingService } from '../listings/services/saved-listing.service';
import { SharedModule } from '../shared/shared.module';
import { InquiriesComponent } from './widgets/inquiries/inquiries.component';
import { InquiriesModule } from '../inquiries/inquiries.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    SlickModule.forRoot(),
    SharedModule,
    InquiriesModule
  ],
  providers:[
    ApiService,
    SavedListingService,
  ],
  declarations: [
    DashboardComponent,
    NewListingsComponent,
    InquiriesComponent,
    ClientsComponent,
    CompsComponent,
    NewestApartmentsComponent,
    RentalSalesStatesComponent
  ]
})
export class DashboardModule { }
