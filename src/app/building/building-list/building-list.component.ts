import { Component, EventEmitter, OnInit } from '@angular/core';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { BuildingService } from '../services/building.service';
import { BuildingModel } from '../models/building.model';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-building-list',
  templateUrl: './building-list.component.html',
  styleUrls: ['./building-list.component.less']
})
export class BuildingListComponent implements OnInit {

  limit = 10;
  offset = 0;
  loaded = false;
  loading = false;
  searching = false;
  total: number;
  result: BuildingModel[] = [];
  search_text = '';
  searchTerm = new EventEmitter<string>();


  constructor(private buildingService: BuildingService, private alertService: BootstrapAlertService) {
  }

  ngOnInit() {
    this.updateBuildings();

    this.searchTerm
      .debounceTime(300)
      .distinctUntilChanged()
      .subscribe(text => {
        this.search_text = text;
        this.offset = 0;
        this.searching = true;
        this.updateBuildings();
      });
  }

  updateBuildings() {
    this.loading = true;
    this.buildingService.getBuildings(this.search_text, this.offset, this.limit).subscribe(data => {
      this.result = this.offset ? this.result.concat(data[0]) : data[0];
      this.total = data[1];
      this.loaded = true;
      this.loading = false;
      this.searching = false;
    }, (errorData) => {
      this.alertService.showError('Error: Cannot load buildings');
      this.loading = false;
    });
  }

  loadMore() {
    this.offset = this.result.length;

    this.updateBuildings();
  }

}
