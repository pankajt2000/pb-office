import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { ListingModel } from '../../listings/models/listing.model';

export class BuildingModel implements DeserializableInterface<BuildingModel> {

  address: string;
  address2: string;
  borough: string;
  building_id: string;
  building_type: string;
  city: string;
  neighborhood: string;
  neighborhood_id: string;
  ownership: string;
  pet_policy: string;
  rentals: string | ListingModel[];
  sales: string | ListingModel[];
  source_id: string;
  state: string;
  year_built: string;
  zip: string;


  deserialize(input: any): BuildingModel {
    Object.assign(this, input);
    return this;
  }

}
