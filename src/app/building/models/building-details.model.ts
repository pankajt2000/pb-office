import { BuildingModel } from './building.model';
import { ListingModel } from '../../listings/models/listing.model';

export class BuildingDetailsModel extends BuildingModel {
  amenities: string[];
  building_info: string;
  building_name: string;
  units_total: number;
  description: string;
  featured_photo: string;
  floors: string;
  latitude: number;
  longitude: number;
  qualifications: string[];
  qualifications2: string[];
  rentals: ListingModel[];
  sale_requirements: string;
  sales: ListingModel[];
  street_view_available: 0 | 1;
  streets: string;
  time_created: number;
  management_company: { name: string, address: string, phone: string, email: string, fax: string, website: string };

  deserialize(input: any): BuildingDetailsModel {
    input['rentals'] = input['rentals'].map((listing) => Object.assign(new ListingModel(), listing));
    input['sales'] = input['sales'].map((listing) => Object.assign(new ListingModel(), listing));
    Object.assign(this, input);
    return this;
  }
}
