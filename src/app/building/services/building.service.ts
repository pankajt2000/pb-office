import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { BuildingModel } from '../models/building.model';
import { BuildingDetailsModel } from '../models/building-details.model';

@Injectable()
export class BuildingService {

  constructor(private api: AgentApiService) {

  }

  getBuildings(search: string = '', offset: number = 0, limit: number = 10) {
    return this.api.get(`search-buildings?search=${search}&limit=${limit}&offset=${offset}`, { observe: 'response' }).map(response => {
      return [
        response.body.map(building => new BuildingModel().deserialize(building)),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

  getDetails(building_id: string, inactive_too = 1) {
    return this.api.get(`search-buildings/${building_id}?inactive_too=${inactive_too}`)
      .map(building => new BuildingDetailsModel().deserialize(building));
  }


}
