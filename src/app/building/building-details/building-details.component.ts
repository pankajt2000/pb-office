import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BuildingService } from '../services/building.service';
import { BuildingDetailsModel } from '../models/building-details.model';
import { SearchService } from '../../search/services/search.service';
import { ListingService } from '../../listings/services/listing.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';

@Component({
  selector: 'app-building-details',
  templateUrl: './building-details.component.html',
  styleUrls: ['./building-details.component.less']
})
export class BuildingDetailsComponent implements OnInit {

  building: BuildingDetailsModel;
  loading = true;
  rental_active_listings = 1;
  sale_active_listings = 1;
  delete_reasons: { id: string; label: string; type: string }[] = [];

  constructor(private route: ActivatedRoute,
              private buildingService: BuildingService,
              private listingService: ListingService,
              private alertService: BootstrapAlertService,
              private searchService: SearchService) {
    this.route.params.subscribe(params => {
      if (params['building_id']) {
        this.initBuilding(params['building_id']);
      }
    });
  }

  ngOnInit() {
  }

  initBuilding(building_id) {
    this.loading = true;

    this.buildingService.getDetails(building_id).subscribe((building) => {
      this.building = building;
      this.loading = false;
    });
  }

  rentListingsCount() {
    return this.building.rentals.filter(l => l.active === this.rental_active_listings).length;
  }

  saleListingsCount() {
    return this.building.sales.filter(l => l.active === this.sale_active_listings).length;
  }

  listingsCount(type: string, active: number) {
    switch (type.toLowerCase()) {
      case 'rentals':
        return this.building.rentals.filter(l => l.active === active).length;
      case 'sales':
        return this.building.sales.filter(l => l.active === active).length;
      default:
        throw new Error(`Unsupported type ${type}`);
    }
  }

  loadingDeleteReason(type) {
    if (!this.delete_reasons.length) {
      this.searchService.getFormOptions().subscribe((data) => {
        this.delete_reasons = data.status_change_reasons[type];
      });
    }
  }

  deleteListing(listing, reason) {
    listing.deleting = true;

    this.listingService.deleteListing(listing, reason.id).subscribe(() => {

      this.initBuilding(this.building.building_id);

    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError('Listing not found');
      } else {
        this.alertService.showError('Error: Cannot delete the listing');
      }

      listing.deleting = false;
    });
  }

}
