import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuildingListComponent } from './building-list/building-list.component';
import { BuildingDetailsComponent } from './building-details/building-details.component';
import { BuildingService } from './services/building.service';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { TabsModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    TabsModule.forRoot(),
  ],
  providers: [
    BuildingService,
  ],
  declarations: [BuildingListComponent, BuildingDetailsComponent]
})
export class BuildingModule {
}
