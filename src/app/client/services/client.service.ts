import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ClientModel } from '../models/client.model';
import { Observable } from 'rxjs/Observable';
import { ListingModel } from '../../listings/models/listing.model';
import { ClientDetailsModel } from '../models/client-details.model';

@Injectable()
export class ClientService {

  private suggest_clients: BehaviorSubject<ClientModel[]> = new BehaviorSubject<ClientModel[]>([]);
  private current_client: BehaviorSubject<ClientModel> = new BehaviorSubject<ClientModel>(null);
  private sidebar_colapsed: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(public api: AgentApiService) {
  }

  setSidebarColapsed(value: boolean) {
    this.sidebar_colapsed.next(value);
  }

  getSidebarColapsed() {
    return this.sidebar_colapsed;
  }

  initCurrentClient() {
    const client_id = localStorage.getItem('current_client');
    if (client_id && !this.current_client.getValue()) {
      this.setCurrentClientById(client_id);
    }
  }

  setCurrentClientById(client_id: string) {
    this.suggest_clients.getValue().forEach(client => {
      if (client.user_id.toString() === client_id) {
        this.setCurrentClient(client);
      }
    });
  }

  setSuggestClients(value: ClientModel[]) {
    this.suggest_clients.next(value);
    this.initCurrentClient();
  }

  getCurrentClient() {
    return this.current_client;
  }

  setCurrentClient(value: ClientModel) {
    localStorage.setItem('current_client', value ? value.user_id.toString() : '');
    this.current_client.next(value);
  }

  getSuggestClients() {
    return this.suggest_clients;
  }

  getAllSuggestClients(): BehaviorSubject<ClientModel[]> | Observable<ClientModel[]> {
    if (this.suggest_clients.value && this.suggest_clients.value.length) {
      return this.suggest_clients;
    } else {
      return this.getSuggest();
    }
  }

  getSuggest(search: string = '', limit: number = 10, offset: number = 0, search_by: string = '', search_dir: string = '',
             get_more: string = 'saved_searches', active_only: number = 1) {
    return this.api.get(`suggest-clients?search=${search}&limit=${limit}&offset=${offset}&search_by=${search_by}&
        search_dir=${search_dir}&get_more=${get_more}&active_only=${active_only}`)
      .map(data => {
        this.setSuggestClients(data.map((item: ClientModel) => new ClientModel().deserialize(item)));
        return this.suggest_clients.value;
      });
  }

  getListingsInfo(client: ClientModel, ids: string[] = []) {
    return this.api.get(`clients-listings-info/${client.user_id}?listing_ids=${ids.join(',')}`);
  }

  getClients(limit: number = 10, offset: number = 0, mode: string = 'CURRENT', search?: string, sort?: (a, b) => number) {
    let url = `clients?limit=${limit}&offset=${offset}&mode=${mode}`;

    if (search) {
      url += `&search=${search}`;
    }

    sort = sort || ((a, b) => 0);

    return this.api.get(url, { observe: 'response' })
      .map(response => {
        return [
          response.body.map((item: ClientModel) => new ClientModel().deserialize(item)).sort(sort),
          response.headers.get('Adm-Result-Total-Count'),
        ];
      });
  }

  delete(client: ClientModel | ClientDetailsModel) {
    return this.api.delete(`clients/${client.user_id}`);
  }

  createClient(data) {
    return this.api.post('clients', data);
  }

  getClientDetails(client_id: string) {
    return this.api.get(`clients/${client_id}`).map(data => new ClientDetailsModel().deserialize(data));
  }

  getClientsNewListings(client_id: string, offset: number = 0, limit: number = 10, sort_by: string = '', sort_dir: string = '', days: number | '' = '') {
    return this.api.get(`clients-new-listings/${client_id}?days=${days}&offset=${offset}&limit=${limit}&sort_by=${sort_by}&sort_dir=${sort_dir}`, { observe: 'response' })
      .map(response => {
        return [
          response.body.map(listing => new ListingModel().deserialize(listing)),
          response.headers.get('Adm-Result-Total-Count'),
        ];
      });
  }

  updateClient(client_id: string, data) {
    return this.api.put(`clients/${client_id}`, data);
  }

}
