import { Injectable } from "@angular/core";
import { AgentApiService } from "../../services/api/agent-api.service";
import { FreeAgentRequestModel } from "../models/free-agent-request.model";

@Injectable()
export class FindFreeAgentService {

  constructor(public api: AgentApiService) {
  }

  getCurrentFreeAgentRequest(client_id: string) {
    return this.api.get(`clients-free-agents/${client_id}`).map(res => new FreeAgentRequestModel().deserialize(res));
  }

  sendFreeAgentsMessage(client_id: string, agents: string[], subject: string, message: string, date: number, time: number) {
    return this.api.post(`clients-free-agents/${client_id}`, { agents, subject, message, date, time });
  }

}
