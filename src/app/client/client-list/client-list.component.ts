import { Component, OnDestroy, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { ClientService } from '../services/client.service';
import { SearchService } from "../../search/services/search.service";
import { QuickviewService } from './../../layout/quickview/quickview.service';
import { ClientModel } from '../models/client.model';
import { ConfirmPopupComponent } from '../../shared/components/confirm-popup/confirm-popup.component';
import { DealsService } from '../../deals/services/deals.service';

declare var $: any;

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.less']
})
export class ClientListComponent implements OnInit, OnDestroy {

  loading = true;
  clients: ClientModel[] = [];
  total: number;
  page = 1;
  limit = 15;

  constructor(
    private clientService: ClientService,
    private searchService: SearchService,
    private dealsService: DealsService,
    private quickviewService: QuickviewService,
    private modalService: BsModalService,
    private router: Router)
  {
    this.getClients();
  }

  getClients() {
    this.loading = true;
    this.clientService.getClients(this.limit, this.limit * (this.page - 1)).subscribe((data: [ClientModel[], number]) => {
      this.clients = this.page > 1 ? this.clients.concat(data[0]) : data[0];
      this.total = data[1];
      this.loading = false;
    });
  }

  loadMore() {
    this.page += 1;
    this.getClients();
  }

  performSearch(client: ClientModel) {
    this.router.navigate(['search', 'criteria', client.search_id]);
  }

  suspendSearch(client: ClientModel, e: Event) {
    e.stopPropagation();

    this.searchService.suspendSearch(client.user_id, client.search_id).subscribe((data: any) => {
      this.getClients();
    });
  }

  unsuspendSearch(client: ClientModel, e: Event) {
    e.stopPropagation();

    this.searchService.removeSuspend(client.user_id, client.search_id).subscribe((data: any) => {
      this.getClients();
    });
  }

  deleteClient(client: ClientModel, e: Event) {
    const modal = this.modalService.show(ConfirmPopupComponent, {
      class: 'modal-sm m-t-10', initialState: {
        message: 'Do you really want to delete this client?'
      }
    });

    modal.content.confirm.subscribe(() => {
      this.clientService.delete(client).subscribe((data: any) => {
        this.getClients();
      });
    });

    e.stopPropagation();
  }

  createSearch(c: ClientModel, e: Event) {
    e.stopPropagation();

    this.clientService.setCurrentClient(c);

    this.quickviewService.setState('saved_searches');
    this.quickviewService.setOpened(true);
  }

  createNewDeal(c: ClientModel) {
    this.router.navigate(['/deals', 'new'], { queryParams: { client_id: c.user_id }});
  }

  getActiveClients() {
    return this.clients.filter(c => !c.deleted);
  }

  ngOnInit() {
    $('.page-content').addClass('clientspro');
  }

  ngOnDestroy(): void {
    $('.page-content').removeClass('clientspro');
  }

}
