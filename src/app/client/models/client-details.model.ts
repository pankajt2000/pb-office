import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { SavedListingModel } from '../../listings/models/saved-listing.model';
import { SearchQueryModel } from '../../search/models/search-query.model';
import { environment } from '../../../environments/environment';
import { ListingModel } from '../../listings/models/listing.model';
import { InquiryDetailsModel } from '../../dashboard/widgets/inquiries/models/inquiry-details.model';
import { SigninDetailsModel } from '../../signin/models/signin-details.model';
import { FreeAgentRequestModel } from "./free-agent-request.model";

export class ClientDetailsModel implements DeserializableInterface<ClientDetailsModel> {

  user_id: string;
  enabled: '0' | '1';
  type: string;
  first_name: string;
  last_name: string;
  phone: string;
  email: string;
  client_type: string;
  time_created: number;
  last_activity: number;
  name: string;
  photo: string;
  year_born:string;
  gender:string;
  deleted: 0 | 1;
  source_value?: string;
  status: string;
  status_color: string;
  searches: {
    search_id: string;
    results_count: number;
    last_modified: number;
    title: string;
    created_by: string;
    parent_id: number
    suspended: 0 | 1;
    search: SearchQueryModel;
  }[];
  message_threads: any[];
  appointments: any[];
  client_saved: SavedListingModel[];
  agent_saved: SavedListingModel[];
  inquiries: InquiryDetailsModel[];
  signin: SigninDetailsModel;
  free_agent_request?: FreeAgentRequestModel;
  default_search: {
    search_id: string;
    results_count: number;
    last_modified: number;
    title: string;
    created_by: string;
    parent_id: number;
    suspended: 0 | 1;
    search: {
      has_photos: 0 | 1;
      zd_type: string;
      search: string;
      other: string[];
      ratings: string;
      pets: string;
      amenities: string[];
      building_type: string[];
      ownership: string[];
      bathrooms: string;
      bedrooms_mode: string;
      bedrooms: string;
      listing_ids: number[];
      neighborhood_id: string;
      neighborhood: string[];
      max_price: string;
      min_price: string;
      company_name: string;
      furnished: string;
      exposures: string[];
      has_concession: 0 | 1;
      fee_structure: string
    }
  };

  getAvatarUrl() {
    return environment.media_base_url + this.photo;
  }

  deserialize(input: any): ClientDetailsModel {
    input.agent_saved = input.agent_saved.map(listing => new SavedListingModel().deserialize(listing));
    input.inquiries = input.inquiries.map(inquiry => new InquiryDetailsModel().deserialize(inquiry));

    if (input.signin) {
      input.signin = new SigninDetailsModel().deserialize(input.signin);
    }

    if (input.free_agent_request) {
      input.free_agent_request = new FreeAgentRequestModel().deserialize(input.free_agent_request);
    }

    Object.assign(this, input);
    return this;
  }

}
