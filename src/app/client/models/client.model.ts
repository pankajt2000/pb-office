import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { environment } from '../../../environments/environment';
import { config } from '../../config';

export class ClientModel implements DeserializableInterface<ClientModel> {

  user_id: number;
  enabled: 0 | 1;
  deleted: 0 | 1;
  type: string;
  first_name: string;
  last_name: string;
  phone: number;
  email: string;
  client_type: string;
  time_created: number;
  last_activity: number;
  status: string;
  name: string;
  photo: string;
  search_info: string;
  search_id?: string;

  search_suspended: 0 | 1;

  search_counts: {
    total: number,
    '3': number,
    '7': number
  };

  searches: {
    created_by: string,
    last_modified: number;
    parent_id: number | null;
    results_count: number;
    search: any,
    search_id: string,
    suspended: 0 | 1,
    title: string,
  }[];

  deleting: boolean;
  suspended = false;
  suspending = false;

  getAvatarUrl() {
    return environment.media_base_url + this.photo;
  }

  getSmallAvatarUrl() {
    return environment.transcode_media_base_url + config.media_sizes.wlist + '/' + this.photo;
  }

  hasSuspendedSearch() {
    return this.searches.filter(s => s.suspended).length > 0;
  }

  deserialize(input: any): ClientModel {
    Object.assign(this, input);
    return this;
  }

}
