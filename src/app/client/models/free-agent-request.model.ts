import { DeserializableInterface } from "../../shared/models/deserializable.interface";

export class FreeAgentRequestModel implements DeserializableInterface<FreeAgentRequestModel> {

  free_agent_request_id: string;
  status: 'NEW' | 'OPEN' | 'CLOSED' | 'ERROR' | 'DELETED' | 'ARCHIVED';
  status_message: string;
  time_created: Date;
  time_updated: Date;
  agents: Array<{
    user_id: string,
    name: string,
    email: string,
    status: string,
    status_message: string,
    clients: number,
    listings: number,
    last_active: string & number,
    time_updated: string & number
  }>;

  constructor() {}

  deserialize(input: any): FreeAgentRequestModel {
    input.time_created = new Date(input.time_created * 1000);
    input.time_updated = new Date(input.time_updated * 1000);
    Object.assign(this, input);
    return this;
  }

}
