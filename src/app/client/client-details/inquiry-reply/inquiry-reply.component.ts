import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { InquiryDetailsModel } from '../../../dashboard/widgets/inquiries/models/inquiry-details.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { ContactModel } from '../../../mailbox/models/contact.model';
import { ContactService } from '../../../mailbox/services/contact.service';
import { InquiriesService } from '../../../inquiries/services/inquiries.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';

@Component({
  selector: 'app-inquiry-reply',
  templateUrl: './inquiry-reply.component.html',
  styleUrls: ['./inquiry-reply.component.less']
})
export class InquiryReplyComponent implements OnInit {

  searchTerm = new Subject<string>();
  contacts: ContactModel[] = [];
  search_contacts: ContactModel[] = [];

  contacts_cc_loading: boolean = false;
  contactsCcTypeahead = new Subject<string>();

  per_page = 10;
  page = 0;

  loading: boolean = false;

  search: string;

  sortField = 'time_created';
  sortDirection: 'desc' | 'asc' = 'desc';
  sortData: { label: string, field: string }[] = [
    { label: 'Recent', field: 'time_created' },
    { label: 'Alphabetically', field: 'first_name' },
  ];

  messageForm: FormGroup;

  @Input('inquiry') inquiry: InquiryDetailsModel;
  @Output() replySent: EventEmitter<any> = new EventEmitter();

  constructor(
    private contactService: ContactService,
    private inquiriesService: InquiriesService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private alertService: BootstrapAlertService
  ) { }

  ngOnInit() {
    this.searchTerm
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(text => this._updateContactList(text))
      .subscribe(res => this.contacts = res);

    this._updateContactList().subscribe(data => {
      this.contacts = data;
      this.search_contacts = data.slice(0);
    });

    this.initMessageForm();
    this.loadCcSearchContacts();
  }

  replyToInquiry(inquiry_id: string) {
    this.loading = true;

    if (this.messageForm.invalid) {
      this.alertService.showError('Please make sure you filled out the message and try again.');
      this.loading = false;
      return;
    }

    const message = this.messageForm.get('message').value;
    let ccs = this.messageForm.get('ccs').value;

    if (ccs === '') {
      ccs = [];
    }

    this.inquiriesService.replyToInquiry(inquiry_id, message, ccs).subscribe((inquiry: InquiryDetailsModel) => {
      this.initMessageForm();
      this.replySent.emit(null);
      this.loading = false;
    }, (err) => {
      this.alertService.showError('Could not reply to this inquiry. Please try again later.');

      this.loading = false;
    })
  }

  initMessageForm() {
    this.messageForm = this.fb.group({
      ccs: [''],
      message: ['', Validators.required]
    });
  }

  loadCcSearchContacts() {
    this.contactsCcTypeahead.pipe(
      tap(() => this.contacts_cc_loading = true),
      distinctUntilChanged(),
      debounceTime(100),
      switchMap(term => this.contactService.getContacts(term)),
    ).subscribe(x => {
      this.search_contacts = x;
      this.contacts_cc_loading = false;
      this.cd.markForCheck();
    }, () => {
      this.search_contacts = [];
    });
  }

  private _updateContactList(search_text: string = null) {
    if (search_text !== null) {
      this.search = search_text;
    }
    return this.contactService.getContacts(this.search, this.per_page, this.per_page * this.page, this.sortField, this.sortDirection);
  }

}
