import { Component, OnInit } from '@angular/core';
import { ClientService } from '../services/client.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ClientDetailsModel } from '../models/client-details.model';
import { ListingModel } from '../../listings/models/listing.model';
import { SavedListingService } from '../../listings/services/saved-listing.service';
import { SendModalComponent } from '../../listings/send-modal/send-modal.component';
import { BsModalService } from 'ngx-bootstrap';
import { SearchService } from '../../search/services/search.service';
import { SearchQueryModel } from '../../search/models/search-query.model';
import { QuickviewService } from '../../layout/quickview/quickview.service';
import { ConfirmPopupComponent } from '../../shared/components/confirm-popup/confirm-popup.component';
import { InquiriesService } from "../../inquiries/services/inquiries.service";
import { BootstrapAlertService } from "ngx-bootstrap-alert-service";
import { AuthService } from "../../authentication/services/auth.service";
import { InquiryDetailsModel } from '../../dashboard/widgets/inquiries/models/inquiry-details.model';
import { CurrencyMaskConfig } from 'ngx-currency/src/currency-mask.config';
import { CompsListingModel } from '../../comps/models/comps-listing.model';
import { CompsService } from '../../comps/services/comps.service';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.less']
})
export class ClientDetailsComponent implements OnInit {

  loading = true;
  client: ClientDetailsModel;

  agentId: string;

  new_listings: ListingModel[];
  new_listings_total: number = null;
  new_listings_page = 1;
  new_listings_limit = 10;
  loading_new_listing = false;

  search;
  search_query: SearchQueryModel;
  state = 'view';

  opened_tab: string;

  comps: CompsListingModel[];

  is_supervisor: boolean;

  msgPlural = {
    '=1': 'message',
    'other': 'messages'
  };

  priceMaskConfig: CurrencyMaskConfig = {
    align: 'left',
    allowNegative: false,
    allowZero: true,
    decimal: '.',
    precision: 0,
    prefix: '$ ',
    suffix: '',
    thousands: ',',
    nullable: true
  };
  
  action: string = 'reply';

  constructor(private clientService: ClientService,
              private router: Router,
              private route: ActivatedRoute,
              private searchService: SearchService,
              private quickviewService: QuickviewService,
              private inquiriesService: InquiriesService,
              private compsService: CompsService,
              private authService: AuthService,
              public savedListingService: SavedListingService,
              private alertService: BootstrapAlertService,
              private modalService: BsModalService) {
    this.route.params.subscribe(params => {
      if (params['client_id']) {
        this.loading = true;
        this.initClient(params['client_id']);
        this.updateNewListings(params['client_id']);
        this.initSearch(params['client_id']);
      }
    });
  }

  initClient(client_id) {
    if (!this.loading) this.loading = true;

    this.clientService.getClientDetails(client_id).subscribe((client: ClientDetailsModel) => {
      this.client = client;
      this.loading = false;
     
      this.clientService.setCurrentClientById(client.user_id);

      if (this.compsTabEnabled()) {
        const comps_obs = 
          this.client.inquiries.filter(i => !!i.data.listing_id)
          .map(i => this.compsService.getListingComps(i.data.listing_id));

        forkJoin(comps_obs).subscribe(res => {
          // r is an object with "keyed" indices, i.e. it has a key "0" instead of being an array w/ index 0, so this is
          // needed to reduce it to an array of arrays. We need to make sure it deserializes properly.
          const nested = res.map(r => Object.keys(r).map(x => new CompsListingModel().deserialize(r[x])));
          this.comps = [].concat(...nested); // quick-n-easy way of flattening an array of arrays
        });
      }
    });
  }

  initSearch(client_id) {
    this.searchService.getSavedSearches(client_id).subscribe((searches) => {
      if (searches.length) {
        this.search = searches[0];
        this.search_query = searches[0].search;
      }
    });
  }

  loadMoreNewListings() {
    this.new_listings_page += 1;
    this.updateNewListings(this.client.user_id);
  }

  updateNewListings(client_id) {
    this.loading_new_listing = true;
    const ofset = (this.new_listings_page - 1) * this.new_listings_limit;
    this.clientService.getClientsNewListings(client_id, ofset, this.new_listings_limit).subscribe((data) => {
      this.new_listings = this.new_listings_page === 1 ? data[0] : this.new_listings.concat(data[0]);
      this.new_listings_total = data[1];
      this.loading_new_listing = false;
    });
  }

  hasSelectedListings() {
    return this.client.agent_saved.filter(listing => listing.checked).length;
  }

  getBedrooms() {
    return this.search_query.bedrooms === parseFloat(this.search_query.bedrooms).toString() ? (this.search_query.bedrooms + ' br') : this.search_query.bedrooms;
  }

  sendListings() {
    const modalWindow = this.modalService.show(SendModalComponent, {
      class: 'modal-lg m-t-10',
      initialState: { listings: this.client.agent_saved.filter(listing => listing.checked) }
    });
    modalWindow.content.sentEvent.subscribe(() => {
      this.client.agent_saved.forEach(item => item.checked = false);
      this.initClient(this.client.user_id);
    });
  }

  editSearchCriteria() {
    this.clientService.setCurrentClientById(this.client.user_id);
    this.quickviewService.setOpened(true);
    this.quickviewService.setState('saved_searches');
  }

  deleteClient() {
    const modal = this.modalService.show(ConfirmPopupComponent, {
      class: 'modal-sm m-t-10', initialState: {
        message: 'Do you really want to delete this client?'
      }
    });

    modal.content.confirm.subscribe(() => {
      this.clientService.delete(this.client).subscribe((res: any) => {
        this.router.navigateByUrl('/client/list');
      });
    });
  }

  ngOnInit() {
    this.authService.sessionInfo().subscribe(info => {
      this.is_supervisor = info.user.roles.map(r => r.toLowerCase()).includes('supervisor');

      this.agentId = info.user.user_id;
    });

    this.route.queryParams.subscribe(params => {
      this.opened_tab = params['opened_tab'] || null;
    });
  }

  FFATabEnabled() {
    const has_open = this.client.inquiries.some((inquiry: InquiryDetailsModel) => {
      return inquiry.status === 'PROGRESS';
    });

    const has_assigned = this.client.inquiries.some((inquiry: InquiryDetailsModel) => {
      return inquiry.assigned.user_id === this.agentId || inquiry.data.watchers.includes(this.agentId);
    });

    return has_open && has_assigned;
  }

  compsTabEnabled() {
    return this.client.inquiries.some((inquiry: InquiryDetailsModel) =>  !!inquiry.data.listing_id);
  }

  getFFATabHeading() {
    const ffa_request = this.client.free_agent_request;

    switch (ffa_request.status) {
      case "NEW":
        return 'Find Free Agent';
      case "OPEN":
        return 'Finding Agent';
      case "CLOSED": // boys
        return 'Agent Found';
      default:
        return 'Find Free Agent';
    }
  }

  refreshInquiry(inquiry_id: string) {
    this.loading = true;

    this.inquiriesService.getListingInquiryById(inquiry_id).subscribe(inquiry => {
      const inquiry_index = this.client.inquiries.findIndex(i => i.inquiry_id === inquiry_id);

      if (inquiry_index > -1) {
        this.client.inquiries[inquiry_index] = inquiry;
      } else {
        this.alertService.showError('Couldn\'t refresh inquiry. Please try again later.');
      }

      this.loading = false;
    }, (_) => {
      this.alertService.showError('Something went wrong while refreshing inquiry.');
      this.loading = false;
    })
  }

  getBadgeText(inquiry_src: string) {
    switch (inquiry_src) {
      case 'street_easy_inquiry':
        return 'StreetEasy';
      case 'rent_hop_inquiry':
        return 'RentHop';
      default:
        return 'Unknown';
    }
  }

}
