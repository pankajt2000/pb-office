import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { FindFreeAgentService } from "../../services/find-free-agent.service";
import { BootstrapAlertService } from "ngx-bootstrap-alert-service";
import { FreeAgentRequestModel } from '../../models/free-agent-request.model';

type FFAgent = {
  user_id: string,
  name: string,
  status: string,
  status_message: string,
  time_updated?: Date,
  selected: boolean
};

@Component({
  selector: 'app-find-free-agent',
  templateUrl: './find-free-agent.component.html',
  styleUrls: ['./find-free-agent.component.less']
})
export class FindFreeAgentComponent implements OnInit {

  @Input('clientId') clientId: string;

  loading: boolean = false;

  far: FreeAgentRequestModel;
  agents: FFAgent[] = [];

  messageForm: FormGroup;

  params: {[key: string]: any};

  constructor(
    private findFreeAgentService: FindFreeAgentService,
    private alertService: BootstrapAlertService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.initFAR();
  }

  initFAR() {
    this.loading = true;

    this.findFreeAgentService.getCurrentFreeAgentRequest(this.clientId).subscribe(res => {
      this.far = res;

      this.initForm();

      this.loading = false;
    });
  }

  initForm() {
    this.agents = this.far.agents.map(agent => {
      return {
        user_id: agent.user_id,
        name: agent.name,
        status_message: agent.status_message,
        status: agent.status,
        time_updated: agent.time_updated !== 0 ? new Date(agent.time_updated * 1000) : null,
        selected: false
      };
    });

    const d = new Date();
    const date = d.toISOString().split('T')[0];
    const time = d.toTimeString().split(':').slice(0, 2).join(':');

    this.messageForm = this.fb.group({
      date: [date],
      time: [time],
      subject: ['', Validators.required],
      message: ['', Validators.required]
    });

    this.params = { date_time: d.getTime() };
  }

  sendRequest() {
    this.loading = true;

    if (this.messageForm.invalid) {
      this.alertService.showError('Please make sure you filled out the message and try again.');
      this.loading = false;
      return;
    }

    const agent_ids = this.agents.filter(agent => agent.selected).map(agent => `${agent.user_id}`);

    const subject = this.messageForm.get('subject').value;
    const message = this.messageForm.get('message').value;

    const date = (new Date(this.messageForm.get('date').value)).getTime();
    const time = this._timeToTimestamp(this.messageForm.get('time').value);

    this.findFreeAgentService.sendFreeAgentsMessage(this.clientId, agent_ids, subject, message, date, time).subscribe(_ => {
      this.initFAR();
    }, _ => {
      this.alertService.showError('Could not send find free agent request. Please try again later.');

      this.loading = false;
    });
  }

  sendDisabled(): boolean {
    return !this.agents.some(a => a.selected) || this.messageForm.invalid || this.loading;
  }

  getBadgeClass<T extends { status: string }>(entity: T) {
    switch (entity.status) {
      case 'ERROR':
        return 'badge badge-danger';
      case 'OPEN':
      case 'PENDING':
        return 'badge badge-secondary';
      case 'SUCCESS':
      case 'ACCEPTED':
      case 'CLOSED':
        return 'badge badge-success';
      default:
        return 'badge badge-info';
    }
  }

  private _timeToTimestamp(time: string) {
    const [hours, minutes] = time.split(':').map(x => +x);

    const d = new Date();
    d.setHours(hours, minutes, 0, 0);

    return d.getTime();
  }
}
