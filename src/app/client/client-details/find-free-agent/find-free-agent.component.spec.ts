import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindFreeAgentComponent } from './find-free-agent.component';

describe('FindFreeAgentComponent', () => {
  let component: FindFreeAgentComponent;
  let fixture: ComponentFixture<FindFreeAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindFreeAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindFreeAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
