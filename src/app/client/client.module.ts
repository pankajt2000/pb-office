import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from '../services/api/api.service';
import { ButtonsModule, ModalModule, TabsModule, AlertModule, AccordionModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { ClientService } from './services/client.service';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import { EditClientComponent } from './edit-client/edit-client.component';
import { InquiriesModule } from "../inquiries/inquiries.module";
import { MailboxModule } from "../mailbox/mailbox.module";
import { FindFreeAgentComponent } from './client-details/find-free-agent/find-free-agent.component';
import { FindFreeAgentService } from "./services/find-free-agent.service";
import { InquiryReplyComponent } from './client-details/inquiry-reply/inquiry-reply.component';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ModalModule.forRoot(),
    SharedModule,
    TabsModule.forRoot(),
    BrowserModule,
    NgxDnDModule,
    NgxCurrencyModule,
    ButtonsModule.forRoot(),
    AlertModule.forRoot(),
    AccordionModule.forRoot(),
    InquiriesModule,
    MailboxModule
  ],
  providers: [
    ApiService,
    ClientService,
    FindFreeAgentService
  ],
  declarations: [
    ClientListComponent,
    ClientDetailsComponent,
    EditClientComponent,
    FindFreeAgentComponent,
    InquiryReplyComponent
  ],
  entryComponents: []
})
export class ClientModule {
}
