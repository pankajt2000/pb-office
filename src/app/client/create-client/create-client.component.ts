import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef, ModalOptions } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientService } from '../services/client.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';

@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.component.html',
  styleUrls: ['./create-client.component.less']
})
export class CreateClientComponent implements OnInit {

  clientForm: FormGroup;
  submitted = false;
  submitting = false;
  errorMsg = '';

  first_name: string;
  last_name: string;

  @Output() clientCreated = new EventEmitter();

  constructor(
    public bsModalRef: BsModalRef,
    public options: ModalOptions,
    private fb: FormBuilder,
    private clientService: ClientService,
    private alertService: BootstrapAlertService
  ) {

    // required to silence ts compiler
    const init: Object & any = this.options.initialState;

    this.first_name = init.first_name || '';
    this.last_name = init.last_name || '';

    this.clientForm = this.fb.group({
      email: [[], [Validators.required, Validators.email]],
      first_name: [this.first_name],
      last_name: [this.last_name],
      phone: [''],
    });
  }

  ngOnInit() {
  }

  createClient() {

    this.submitted = true;

    if (this.clientForm.invalid) return;

    this.errorMsg = '';
    this.submitting = true;
    this.clientService.createClient(this.clientForm.value).subscribe(client => {

      this.submitting = false;

      this.clientCreated.emit(client.user_id);

      this.bsModalRef.hide();

      this.alertService.showSucccess('Client successfully created');

      this.clientService.getSuggest().subscribe(() => {
      });

    }, (errorData) => {

      this.submitting = false;

      if (errorData.error) {
        this.errorMsg = errorData.error.errorMsg;
      }
    });

  }

}
