import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ClientDetailsModel } from '../models/client-details.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientService } from '../services/client.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.less']
})
export class EditClientComponent implements OnInit {

  @Input() client: ClientDetailsModel;
  @Input() state: string;

  @Output() afterSaving: EventEmitter<string> = new EventEmitter<string>();

  clientForm: FormGroup;

  submitted = false;
  submitting = false;
  errorMsg = '';

  constructor(private fb: FormBuilder, private clientService: ClientService, private alertService: BootstrapAlertService) {
    this.clientForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.pattern(/^[0-9()+-]+$/)]],
      year_born: ['', [Validators.pattern(/^[0-9]{4}$/)]],
      gender: ['', [Validators.pattern(/^[mf]{1}$/)]],
    });
  }

  ngOnInit() {
    this.clientForm.patchValue(this.client);
  }

  submitClient() {
    this.submitted = true;

    if (this.clientForm.invalid) {
      return;
    }

    this.submitting = true;

    this.clientService.updateClient(this.client.user_id, this.clientForm.value).subscribe((data) => {

      this.client = Object.assign(this.client, data);

      this.afterSaving.emit('view');
      this.submitting = false;

    }, (data) => {
      if (data.status === 404) {
        this.alertService.showError('Client not found');
      } else {
        this.alertService.showError('Oops something went wrong');
      }
    });
  }

}
