import { BrowserModule } from '@angular/platform-browser';
import { Injector, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { LayoutModule } from './layout/layout.module';
import { ApiService } from './services/api/api.service';
import { HttpClientModule } from '@angular/common/http';
import { AgentApiService } from './services/api/agent-api.service';
import { ListingsModule } from './listings/listings.module';
import { MailboxModule } from './mailbox/mailbox.module';
import { SharedModule } from './shared/shared.module';
import { MessageService } from './mailbox/services/message.service';
import { AuthenticationModule } from './authentication/authentication.module';
import { AuthGuardService } from './authentication/services/auth-guard.service';
import { SearchModule } from './search/search.module';
import { StatsModule } from './stats/stats.module';
import { UserModule } from './user/user.module';
import { ClientModule } from './client/client.module';
import { ApplicationModule } from './application/application.module';
import { BuildingModule } from './building/building.module';
import { RoleGuardService } from './authentication/services/role-guard.service';
import { AgentModule } from './agent/agent.module';
import { PaymentsModule } from './payments/payments.module';
import { DealsModule } from './deals/deals.module';
import { DosModule } from './dos/dos.module';
import { InvoicesModule } from "./invoices/invoices.module";
import { AppLoadModule } from './app-load.module';
import { PerformanceModule } from "./performance/performance.module";
import { CompsModule } from "./comps/comps.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,

    SharedModule,
    AppLoadModule,
    AppRoutingModule,
    DashboardModule,
    ListingsModule,
    LayoutModule,
    MailboxModule,
    AuthenticationModule,
    SearchModule,
    StatsModule,
    UserModule,
    ClientModule,
    ApplicationModule,
    PaymentsModule,
    DealsModule,
    BuildingModule,
    AgentModule,
    DosModule,
    InvoicesModule,
    PerformanceModule,
    CompsModule
  ],
  providers: [
    ApiService,
    AgentApiService,
    MessageService,
    AuthGuardService,
    RoleGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  /**
   * Allows for retrieving singletons using `AppModule.injector.get(MyService)`
   * This is good to prevent injecting the service as constructor parameter.
   */
  static injector: Injector;

  constructor(injector: Injector) {
    AppModule.injector = injector;
  }
}
