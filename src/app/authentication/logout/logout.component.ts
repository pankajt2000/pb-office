import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { config } from '../../config';
import { AuthService } from '../services/auth.service';
import { ClientService } from '../../client/services/client.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-logout',
  template: ''
})
export class LogoutComponent implements OnInit {

  constructor(private clientService: ClientService,
    private authService: AuthService,
    private userService: UserService,
    private router: Router) {
  }

  ngOnInit() {
    this.authService.logout().subscribe(_ => {
      this.clientService.setSuggestClients(null);
      this.clientService.setCurrentClient(null);
      this.userService.setUser(null);

      this.router.navigate([config.login_page_url]);
    });
  }
}
