import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ClientService } from '../../client/services/client.service';
import { UserService } from '../services/user.service';
import { PushMessagesService } from '../../layout/header/alerts/push-messages.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { ActivatedRoute, Router } from '@angular/router';
import { config } from '../../config';

@Component({
  selector: 'app-restore-session',
  templateUrl: './restore-session.component.html',
  styleUrls: ['./restore-session.component.less']
})
export class RestoreSessionComponent implements OnInit {

  loading: boolean;

  constructor(protected authService: AuthService,
    protected clientService: ClientService,
    protected userService: UserService,
    protected pushMessagesService: PushMessagesService,
    protected alertService: BootstrapAlertService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected ngZone: NgZone) { }

  ngOnInit() {
    this.authService.restoreSession().subscribe(data => { this.setUpSession(data); });
  }

  setUpSession(data) {
    localStorage.setItem(config.access_token_local_storage, data.session.user_session_id);

    this.clientService.getSuggest().subscribe((response) => response);

    this.pushMessagesService.rotatePushMessages();

    this.authService.sessionInfo().subscribe((userData) => {
      this.loading = false;

      this.userService.setUser(userData.user);

      this.ngZone.run(() => { this.router.navigate(['/agent', 'list']); })
    })
  }
}
