import { Injectable } from '@angular/core';
import { AuthApiService } from '../../services/api/auth-api.service';
import { config } from '../../config';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs/Observable';
import { SessionModel } from '../models/session.model';

@Injectable()
export class AuthService {

  private cached_session: SessionModel = null;

  constructor(
    protected authApi: AuthApiService,
    private cookieService: CookieService) {
  }

  get stored_session() {
    if (this.cached_session) {
      return this.cached_session;
    }

    let session = this.cookieService.get('adm_office_session');

    if (session !== '') {
      this.cached_session = new SessionModel().deserialize(JSON.parse(session));
      return this.cached_session;
    }

    return null;
  }

  set stored_session(session) {
    this.cookieService.set(
      'adm_office_session',
      JSON.stringify(session),
      365,
      '/', window.location.hostname,
      window.location.protocol.includes('https')
    );
  }

  isAuthenticated() {
    if (this.stored_session) {
      localStorage.setItem(config.access_token_local_storage, this.stored_session.session.user_session_id);
    }

    const token = localStorage.getItem(config.access_token_local_storage);
    return !!token;
  }

  loginGoogle(code: string) {
    return this.authApi.post('login-google', { code });
  }

  loginAs(user_id: string) {
    return this.authApi.post(`login-as/${user_id}`, {});
  }

  restoreSession() {
    return this.authApi.delete('login-as');
  }

  logout() {
    this.cookieService.delete('adm_office_session', '/', window.location.hostname);

    localStorage.removeItem(config.access_token_local_storage);

    const auth2: gapi.auth2.GoogleAuth = (window as any).$$auth2;

    if (auth2 && auth2.isSignedIn.get()) {
      return Observable.from(auth2.signOut());
    }

    return Observable.of({});
  }

  reset_password(data: { email: string }) {
    return this.authApi.post('reset-password', data);
  }

  sessionInfo(force_refresh: boolean = true): Observable<SessionModel> {
    let session = this.stored_session;

    if (!force_refresh && session) {
      return Observable.of(session);
    } else {
      return this.authApi.get('session-info').map(data => new SessionModel().deserialize(data));
    }
  }

}
