import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { config } from '../../config';
import { UserModel } from '../models/user.model';
import { AgentApiService } from '../../services/api/agent-api.service';

@Injectable()
export class UserService {
  private user = new BehaviorSubject<UserModel>(null);
  cast = this.user.asObservable();

  constructor(private api: AgentApiService) {
  }

  getUser() {
    return this.user;
  }

  setUser(newUser: UserModel) {
    this.user.next(newUser);
  }

  getAgent() {
    return this.api.get('agent-profile').map(response => new UserModel().deserialize(response));
  }

  updateAgent(data) {
    return this.api.put('agent-profile', data).map(response => new UserModel().deserialize(response));
  }

  changePassword(data) {
    return this.api.put('agent-profile-password', data).map(response => new UserModel().deserialize(response));
  }

  changePreferences(data) {
    return this.api.put('agent-profile-preferences', data);
  }

  getAgentSignature(type: string = 'image') {
    return this.api.get(`agent-profile-signature?type=${type}`).map(response => response.signature);
  }

  updateAgentSignature(data) {
    return this.api.post('agent-profile-signature', data).map(response => new UserModel().deserialize(response));
  }

  hasRole(role: string): boolean {
    return this.user.getValue() && this.user.getValue().roles.includes(role);
  }

}
