import { Injectable } from '@angular/core';
import { Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { AuthService } from './auth.service';
import { UserService } from './user.service';

@Injectable()
export class RoleGuardService implements CanActivate {
  constructor(public auth: AuthService, protected route: ActivatedRoute, public userService: UserService) {
  }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.auth.isAuthenticated()) {
      // logged in so return true
      if (this.route.snapshot.data.expectedRole) {
        return this.userService.hasRole(this.route.snapshot.data.expectedRole);
      }
      return true;
    }

    return false;
  }

}
