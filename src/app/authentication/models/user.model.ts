import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { config } from '../../config';
import { environment } from '../../../environments/environment';

export class UserModel implements DeserializableInterface<UserModel> {

  user_id: string;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  photo: string;
  name: string;
  gender: string;
  year_born: string;
  time_created: number;
  description: string;
  position: string;
  roles: string[];
  preferences: {
    agent_signature_id: string,
    agent_notification_email: 0 | 1,
    agent_notification_push: 0 | 1,
    agent_publish_ad_se: 0 | 1,
    agent_publish_ad_na: 0 | 1,
    agent_publish_ad_rh: 0 | 1
  };

  getPhotoUrl(size: string = config.media_sizes.jango_contact) {

    if (!this.photo) {
      return '/images/avatars/user1.png';
    }

    if (size === null) {
      return environment.media_base_url + this.photo;
    }
    return environment.transcode_media_base_url + size + '/' + this.photo;
  }

  deserialize(input: any): UserModel {
    Object.assign(this, input);
    return this;
  }

}
