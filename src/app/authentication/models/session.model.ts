import { DeserializableInterface } from "../../shared/models/deserializable.interface";
import { UserModel } from "./user.model";

type Session = {
  user_session_id: string,
  parent_session_id?: string,
  time_created: Date,
  is_fb: 1 | 0;
}

export class SessionModel implements DeserializableInterface<SessionModel> {
  
  user: UserModel;
  session: Session;

  constructor() { }

  deserialize(input: any): SessionModel {
    if (!(input.user instanceof UserModel)) {
      input.user = new UserModel().deserialize(input.user);
    }

    input.session.time_created = new Date(input.session.time_created);

    Object.assign(this, input);
    return this;
  }
}