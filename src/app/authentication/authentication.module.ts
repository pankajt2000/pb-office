import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticationRoutingModule } from './authentication.routing.module';
import { LoginComponent } from './login/login.component';
import { AuthLayoutComponent } from './auth-layout/auth-layout.component';
import { SharedModule } from '../shared/shared.module';
import { AuthApiService } from '../services/api/auth-api.service';
import { AuthService } from './services/auth.service';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { LogoutComponent } from './logout/logout.component';
import { RestoreSessionComponent } from './restore-session/restore-session.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AuthenticationRoutingModule,
  ],
  providers: [
    AuthService,
    AuthApiService,
  ],
  declarations: [
    AuthLayoutComponent,
    LoginComponent,
    ResetPasswordComponent,
    LogoutComponent,
    RestoreSessionComponent
  ]
})
export class AuthenticationModule {
}
