import { Component, OnInit } from '@angular/core';
import { config } from '../../config';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less']
})
export class ResetPasswordComponent implements OnInit {

  model: { email: string } = { email: '' };
  loading = false;
  errorMsg: boolean;
  suceessMsg: boolean;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
  }

  reset_password() {
    this.loading = true;
    this.authService.reset_password(this.model).subscribe(
      (data) => {
        this.suceessMsg =true;
        this.loading = false;
      },
      (data) => {
        this.loading = false;
        this.errorMsg = true;
      });
  }

}
