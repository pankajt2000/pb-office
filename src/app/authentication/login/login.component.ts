import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { config } from '../../config';
import { ClientService } from '../../client/services/client.service';
import { UserService } from '../services/user.service';
import { PushMessagesService } from '../../layout/header/alerts/push-messages.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { Observable } from 'rxjs/Observable';
import { UserModel } from "../models/user.model";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  loading = false;
  returnUrl: string;
  errorMsg: string;

  loginAsId: string;

  constructor(protected authService: AuthService,
    protected clientService: ClientService,
    protected userService: UserService,
    protected pushMessagesService: PushMessagesService,
    protected alertService: BootstrapAlertService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected ngZone: NgZone) {
  }

  ngOnInit() {
    this.loading = true;

    this.loginAsId = this.route.snapshot.queryParams['loginAsId'] || '';

    if (this.loginAsId !== '') {
      this.loginAs(this.loginAsId);
    }

    this.authService.logout().subscribe(_ => {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

      this.loading = false;
    });
  }

  googleLogin() {
    this.loading = true;

    const auth2: gapi.auth2.GoogleAuth = (window as any).$$auth2;

    try {
      const auth2promise = auth2.grantOfflineAccess({ prompt: 'select_account' });

      Observable.from(auth2promise)
        .flatMap((auth_data: { code: string }) => {
          if (!auth_data || !auth_data.code) {
            this.loading = false;
            this.alertService.showError('Error while trying to log in with Google.');
            return;
          }

          return Observable.from([auth_data.code]);
        })
        .flatMap((code) => this.authService.loginGoogle(code))
        .subscribe((data) => this.setUpSession(data), (err) => {
          this.alertService.showError('Something went wrong while logging in.');
          this.loading = false;
        });
    } catch (err) {
      this.alertService.showError('Something went wrong while logging in.');
      return;
    }
  }

  private loginAs(user_id: string) {
    this.authService.loginAs(user_id).subscribe((data) => this.setUpSession(data, true, false));
  }

  setUpSession(data, force_reload: boolean = false, store_session_data: boolean = true) {
    localStorage.setItem(config.access_token_local_storage, data.session.user_session_id);

    this.clientService.getSuggest().subscribe((response) => response);

    this.pushMessagesService.rotatePushMessages();

    this.userService.setUser(new UserModel().deserialize(data.user));

    if (store_session_data) {
      this.authService.stored_session = data;
    }

    this.ngZone.run(() => {
      this.loading = false;

      if (force_reload) {
        location.href = this.returnUrl;
      } else {
        this.router.navigate([this.returnUrl]);
      }
    })
  }
}
