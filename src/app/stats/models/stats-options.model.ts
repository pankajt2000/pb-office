import { DeserializableInterface } from '../../shared/models/deserializable.interface';

export class StatsOptionsModel implements DeserializableInterface<StatsOptionsModel> {

  period_options: {
    duration: number;
    interval: string;
    name: string;
    period_type: string;
  }[];
  trends: {
    distributions: {
      id: string;
      label: string;
      editor: string;
      selected?: boolean;
    }[];
    segments: {
      id: string;
      label: string;
      editor: string;
      selected?: boolean;
    }[];
  }

  deserialize(input: any): StatsOptionsModel {
    Object.assign(this, input);
    return this;
  }

}
