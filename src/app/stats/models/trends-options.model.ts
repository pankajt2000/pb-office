import { DeserializableInterface } from '../../shared/models/deserializable.interface';

export class TrendsOptionsModel implements DeserializableInterface<TrendsOptionsModel> {

  ad_type: string[] = ['rental', 'sale'];
  amenities: string[];
  bathrooms: number[];
  bedrooms: number | string[];
  boroughs: {
    abbreviation: string;
    name: string;
    neighborhoods: {
      neighborhood_id: number;
      abbreviation: string;
      name: string;
      selected?: boolean;
    }[];
  }[];
  building_type: string[];
  furnished: string[];
  max_saved_searches: number;
  other: string[];
  outdoor_spaces: any;
  ownership: {
    rental: string[];
    sale: string[];
  };
  pets: string[];
  price_defaults: {
    rental: { max_price: number; min_price: number };
    sale: { max_price: number; min_price: number };
  };
  price_options: {
    rental: number[];
    sale: number[];
  };
  status_change_reasons: {
    rental: { id: string; label: string; type: string }[];
    sale: { id: string; label: string; type: string }[];
  };
  fee_structure: string[];
  exposures: string[];

  deserialize(input: any): TrendsOptionsModel {
    Object.assign(this, input);
    return this;
  }

}
