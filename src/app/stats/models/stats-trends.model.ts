export class StatsTrendsModel {

  [distribution: string]: {
    name: string;
    data: {
      columns: {
        id: string;
        label: string;
        description?: string;
        format: string;
        id_field?: string;
      }[];
      rows: {
        id: string;
        label: string;
        description?: string;
      }[];
      data: {
        [segment: string]: {
          [date: string]: {
            value: number;
            list: string;
          }
        }
      }
    }
  }

}
