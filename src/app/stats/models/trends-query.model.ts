export class TrendsQueryModel {

  ad_type: string;

  ratings: number | '' | null;

  min_price: number;

  max_price: number;

  has_photos: number;

  borough: string;

  neighborhood: string[] = [];

  neighborhood_id: number[];

  building_type: string[];

  amenities: string[];

  ownership: string[];

  furnished: string;

  other: string[];

  pets: string;

  bathrooms: string;

  bedrooms: string;

  search: string = '';

  listing_ids: string = '';

  company_name: string[];

  has_concession: 0 | 1;

  advertising_mode: 0 | 1;

  fee_structure: string;

  exposures: string[];

  annotation_mode: 0 | 1 = 0;

  pbcv_status: string[];

  distributions: string[];

  segments: string[];

  period_type: string;

  selectedCompanies: string[];

  selectedNeighborhoods: number[];

}
