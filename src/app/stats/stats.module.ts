import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { TrendsRentalsComponent } from './trends-rentals/trends-rentals.component';
import { TrendsSalesComponent } from './trends-sales/trends-sales.component';
import { StatsService } from './services/stats.service';
import { ModalModule, RatingModule, TabsModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { NgxCurrencyModule } from 'ngx-currency';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  imports: [
    NgxChartsModule,
    CommonModule,
    RouterModule,
    SharedModule,
    RatingModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    NgxCurrencyModule,
  ],
  providers: [
    StatsService
  ],
  declarations: [
    TrendsRentalsComponent,
    TrendsSalesComponent
  ]
})
export class StatsModule {
}
