import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendsSalesComponent } from './trends-sales.component';

describe('TrendsSalesComponent', () => {
  let component: TrendsSalesComponent;
  let fixture: ComponentFixture<TrendsSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendsSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendsSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
