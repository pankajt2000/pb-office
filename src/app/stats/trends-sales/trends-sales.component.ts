import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { TrendsRentalsComponent } from '../trends-rentals/trends-rentals.component';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService } from '../../client/services/client.service';
import { StatsService } from '../services/stats.service';

@Component({
  selector: 'app-trends-sales',
  templateUrl: './../trends-rentals/trends-rentals.component.html',
  styleUrls: ['./../trends-rentals/trends-rentals.component.less']
})
export class TrendsSalesComponent extends TrendsRentalsComponent {

  constructor(protected searchService: StatsService,
              protected route: ActivatedRoute,
              protected router: Router,
              protected clientService: ClientService,
              protected cd: ChangeDetectorRef) {
    super(searchService, route, router, clientService, cd);
  }

}
