import { ChangeDetectorRef, Component, EventEmitter, HostListener, OnDestroy, OnInit, AfterViewInit } from '@angular/core';
import { StatsService } from '../services/stats.service';
import { TrendsQueryModel } from '../models/trends-query.model';
import { ListingModel } from '../../listings/models/listing.model';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ClientService } from '../../client/services/client.service';
import { ClientModel } from '../../client/models/client.model';
import { TrendsOptionsModel } from '../models/trends-options.model';
import { StatsOptionsModel } from '../models/stats-options.model';
import { StatsTrendsModel } from '../models/stats-trends.model';
import { isEqual, differenceWith } from 'lodash';
import { CurrencyMaskConfig } from 'ngx-currency/src/currency-mask.config';
import { debounceTime, distinctUntilChanged, switchMap, tap, filter, multicast } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { Observable } from "rxjs/Observable"
import { CurrencyPipe } from '@angular/common';

declare var $: any;

@Component({
  selector: 'app-trends-rentals',
  templateUrl: './trends-rentals.component.html',
  styleUrls: ['./trends-rentals.component.less']
})
export class TrendsRentalsComponent implements OnInit, OnDestroy, AfterViewInit {

  colorScheme = {
    domain: ['#5899DA', '#E8743B', '#19A979', '#ED4A7B', '#945ECF', '#13A4B4', '#525DF4', '#BF399E', '#6C8893', '#EE6868', '#2F6497']
  };

  scrollEvent: EventEmitter<any> = new EventEmitter<any>();
  changeFormEvent: EventEmitter<any> = new EventEmitter<any>();
  ad_type: string = this.route.snapshot.data.ad_type;
  model: TrendsQueryModel = new TrendsQueryModel();
  searching = false;
  client: ClientModel;
  formOptions: TrendsOptionsModel;
  statsOptions: StatsOptionsModel;
  trends: StatsTrendsModel;
  scrolling = false;
  modelDiff = null;
  highlight: boolean;
  search_link: string;
  loaded = false;
  clientSubscription: any;
  selected_borough = null;
  companies_to_loading = false;
  selected_companies_to_loading = false;
  selected_neighborhoods_to_loading = false;
  companiesToTypeahead = new Subject<string>();
  selectedCompaniesToTypeahead = new Subject<string>();
  selectedNeighborhoodsToTypeahead = new Subject<string>();
  search_companies = [];
  selected_search_companies = [];
  selected_neighborhoods = [];
  priceMaskConfig: CurrencyMaskConfig = {
    align: 'left',
    allowNegative: false,
    allowZero: true,
    decimal: ',',
    precision: 0,
    prefix: '$ ',
    suffix: '',
    thousands: '.',
    nullable: true
  };
  neighborhoods = [];

  pbcv_statuses = [
    { label: 'Not Available', value: 'NA', selected: false },
    { label: 'Automatic', value: 'AUTO', selected: true },
    { label: 'Partial', value: 'PARTIAL', selected: false },
    { label: 'Manual', value: 'MANUAL', selected: false },
  ];

  fee_structure_sort_order = ['All', 'CYOF', 'Co-Broke'];
  manhattan_sort_order = ['16', '3', '14', '5', '1', '9', '2', '19', '7', '20', '4', '18', '25', '6', '24', '10', '26', '15', '22', '23', '12', '11','13', '21', '17', '27', '28', '29'];

  chartGroups = [];
  tables = [];
  show_companies_filter = false;
  show_neighborhoods_filter = false;

  constructor(protected statsService: StatsService,
              protected route: ActivatedRoute,
              protected router: Router,
              protected clientService: ClientService,
              protected cd: ChangeDetectorRef) {
    this.initModel();

    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
    ).subscribe(x => {
      if (!Object.keys(this.route.snapshot.queryParams).length) {
        this.resetFilter();
      }
    });

    this.changeFormEvent.debounceTime(100).subscribe(() => {
      this.prepareFilters();
      let is_equal = true;
      if (this.modelDiff) {
        ['ad_type', 'borough', 'neighborhood_id', 'bedrooms', 'bathrooms', 'pets', 'min_price', 'max_price', 'building_type', 'ownership',
          'amenities', 'other', 'furnished', 'ratings', 'has_photos', 'company_name', 'has_concession', 'advertising_mode',
          'fee_structure', 'exposures', 'annotation_mode', 'pbcv_status', 'distributions', 'segments',
          'period_type', 'selectedCompanies', 'selectedNeighborhoods'
        ]
          .forEach((filed) => {
            if (!isEqual(this.modelDiff[filed], this.model[filed])) {
              is_equal = false;
            }
          });
      } else {
        is_equal = false;
      }
      if (!is_equal) {
        this.prepareFilters();
        this.modelDiff = (JSON.parse(JSON.stringify(this.model)));
      }
      this.changeUrl(true);
    });
  }

  initModel(more_data = {}) {
    this.model = Object.assign(this.model, {
      ad_type: this.ad_type,
      ratings: '',
      min_price: null,
      max_price: null,
      has_photos: 0,
      borough: '',
      neighborhood_id: [],
      building_type: [],
      amenities: [],
      ownership: [],
      other: [],
      furnished: 'Unfurnished',
      pets: '',
      bathrooms: '',
      bedrooms: '',
      search: '',
      listing_ids: '',
      fee_structure: 'All',
      company_name: '',
      has_concession: '',
      advertising_mode: '',
      exposures: [],
      annotation_mode: 0,
      pbcv_status: [],
      distributions: [],
      segments: [],
      period_type: 'last_3_months',
      selectedCompanies: '',
      selectedNeighborhoods: ''
    }, more_data);
  }

  ngAfterViewInit() {
    this.statsService.getFormOptions().subscribe((data) => Promise.resolve(null).then(() => {
      data.boroughs = data.boroughs.filter(borough => borough.neighborhoods.length);
      data.boroughs.forEach(borough => {
        borough.neighborhoods.forEach(neighborhood => {
          this.neighborhoods.push({
            value: neighborhood.neighborhood_id,
            name: neighborhood.name
          })
        })
      });

      this.formOptions = this.resortProperies(data);
      this.initForm();

      this.clientSubscription = this.clientService.getCurrentClient().subscribe((client) => {
        this.client = client;
      });

      this.statsService.getStatsOptions().subscribe((statsData) => {
        this.statsOptions = statsData;

        this.route.queryParams.distinctUntilChanged().subscribe((qParams) => {
          const params = Object.assign({}, qParams);
          this.modelDiff = (JSON.parse(JSON.stringify(this.model)));
      
          if (Object.keys(params).length) {
            params.has_photos = parseInt(params.has_photos);
            params.annotation_mode = parseInt(params.annotation_mode);
            params.advertising_mode = params.advertising_mode && !isNaN(params.advertising_mode) ? parseInt(params.advertising_mode) : null;
            params.pbcv_status = typeof params.pbcv_status === 'string' ? params.pbcv_status.split(',') : params.pbcv_status;
          
            params.neighborhood_id = typeof params.neighborhood_id === 'string' ? [params.neighborhood_id] : params.neighborhood_id;
            this.formOptions.boroughs.forEach((borough) => {
              borough.neighborhoods.forEach((neighborhood) => {
                neighborhood.selected = params.neighborhood_id && params.neighborhood_id.indexOf(neighborhood.neighborhood_id) > -1;
              });
            });
  
            params.distributions = typeof params.distributions === 'string' ? [params.distributions] : params.distributions;
            this.statsOptions.trends.distributions.forEach((distribution) => {
              distribution.selected = params.distributions && params.distributions.indexOf(distribution.id) > -1;
            });
  
            params.segments = typeof params.segments === 'string' ? [params.segments] : params.segments;
            this.statsOptions.trends.segments.forEach((segment) => {
              segment.selected = params.segments && params.segments.indexOf(segment.id) > -1;
            });
      
            this.pbcv_statuses.forEach(status => {
              status.selected = params.pbcv_status && params.pbcv_status.indexOf(status.value) > -1;
            });

            if (typeof params.company_name === 'string' && params.company_name.length) {
              params.company_name = params.company_name.slice(1, -1).split('","');
            }
            if (params.company_name.length > 0) {
              this.search_companies = params.company_name.map(c => {
                return { name: c, value: c };
              });
            }

            if (params.segments && params.segments.indexOf('company') > -1) {    
              if (params.selectedCompanies) {  
                if (typeof params.selectedCompanies === 'string' && params.selectedCompanies.length) {
                  params.selectedCompanies = params.selectedCompanies.slice(1, -1).split('","');
                }
                if (params.selectedCompanies.length > 0) {
                  this.selected_search_companies = params.selectedCompanies.map(c => {
                    return { name: c, value: c };
                  });
                }
              }
              this.show_companies_filter = true;
            } else {
              params.selectedCompanies = [];
              this.selected_search_companies = [];
              this.show_companies_filter = false;
            }

            if (params.segments && params.segments.indexOf('neighborhood') > -1) {    
              if (params.selectedNeighborhoods) {  
                if (typeof params.selectedNeighborhoods === 'string' && params.selectedNeighborhoods.length) {
                  params.selectedNeighborhoods = params.selectedNeighborhoods.slice(1, -1).split('","');
                }
                if (params.selectedNeighborhoods.length > 0) {
                  this.selected_neighborhoods = this.neighborhoods.filter(x => params.selectedNeighborhoods.includes(x.value));
                }
              }
              this.show_neighborhoods_filter = true;
            } else {
              params.selectedNeighborhoods = [];
              this.selected_neighborhoods = [];
              this.show_neighborhoods_filter = false;
            }
      
            this.model = Object.assign(this.model, params);
          } else {
            this.initModel();
            this.initForm();
      
            this.scrolling = true;
            $('html, body').animate({ scrollTop: 0 }, 200, () => {
              this.scrolling = false;
            });
          }
      
          this.changeForm();
      
          this.loaded = true;
        });
      });
    }));
  }

  ngOnInit() {
    this.scrollEvent.debounceTime(50).distinctUntilChanged().subscribe(($event) => {

      if (this.scrolling || !this.loaded) {
        return;
      }

      this.prepareFilters();

      this.router.navigate(['/stats/' + this.model.ad_type], {
        queryParams: this.model,
        replaceUrl: true
      });
    });

    this.initSearchCompanies();

    this.statsService.triggerSearch.subscribe(() => {
      this.search();
    });

  }

  initSearchCompanies() {
    this.companiesToTypeahead.pipe(
      tap(() => { 
        this.companies_to_loading = true;
      }),
      distinctUntilChanged(),
      debounceTime(200),
      switchMap(term => this.statsService.getCompanies(term)),
    ).subscribe(x => {
      this.search_companies = x.map(c => {
        return { name: c, value: c };
      });
      this.companies_to_loading = false;
      this.cd.markForCheck();
    }, () => {
      this.search_companies = [];
    });

    this.selectedCompaniesToTypeahead.pipe(
      tap(() => { 
        this.selected_companies_to_loading = true;
      }),
      distinctUntilChanged(),
      debounceTime(200),
      switchMap(term => this.statsService.getCompanies(term)),
    ).subscribe(x => {
      this.selected_search_companies = x.map(c => {
        return { name: c, value: c };
      });
      this.selected_companies_to_loading = false;
      this.cd.markForCheck();
    }, () => {
      this.selected_search_companies = [];
    });

    this.selectedNeighborhoodsToTypeahead.pipe(
      tap(() => { 
        this.selected_neighborhoods_to_loading = true;
      }),
      distinctUntilChanged(),
      debounceTime(200),
      switchMap(term => this.getNeighborhoods(term)),
    ).subscribe(x => {
      this.selected_neighborhoods = x.map(c => {
        return { name: c.name, value: c.value };
      });
      this.selected_neighborhoods_to_loading = false;
      this.cd.markForCheck();
    }, () => {
      this.selected_neighborhoods = [];
    });
  }

  getNeighborhoods(val) {
    return new Observable<any>((observer) => {
      const search = val.toLowerCase();
      const filteredNeighborhoods = this.neighborhoods.filter(x => x.name.toLowerCase().includes(search));
      observer.next(filteredNeighborhoods)
      observer.complete()
    })
  }

  initForm() {
    if (this.formOptions) {
      this.formOptions.boroughs.forEach((borough) => {
        borough.neighborhoods.forEach((neighborhood) => {
          neighborhood.selected = false;
        });
      });
    }
    if (this.statsOptions) {
      this.statsOptions.trends.distributions.forEach((distribution) => {
          distribution.selected = false;
      });
      this.statsOptions.trends.segments.forEach((segment) => {
        segment.selected = false;
    });
    }
  }

  @HostListener('window:scroll', ['$event']) onScrollEvent($event) {

    this.scrollEvent.emit($event);
  }

  changeUrl(replace_url: boolean = false) {
    if (!this.loaded) {
      return;
    }

    this.prepareFilters();

    const filter_query = this.model;

    this.search_link = this.router.createUrlTree(['/stats/' + this.model.ad_type], { queryParams: filter_query }).toString();

    this.router.navigate(['/stats/' + this.model.ad_type], {
      queryParams: filter_query,
      replaceUrl: replace_url
    });
  }

  enterEvent(event) {
    if ($(event.srcElement).attr('role') !== 'combobox') {
      this.search();
    }
  }

  search(event?) {
    this.scrolling = true;

    if (event && event.ctrlKey) {
      window.open(event.toElement.href);
      return;
    }

    if (this.searching) {
      return;
    }

    this.searching = true;
    this.trends = null;
    this.chartGroups = [];
    this.tables = [];

    let search_criteria = Object.assign({}, this.model);
    search_criteria = Object.assign(search_criteria, {
      company_name: this.model.company_name.length ? ('"' + this.model.company_name.join('","') + '"') : '',
      selectedCompanies: this.model.selectedCompanies.length ? ('"' + this.model.selectedCompanies.join('","') + '"') : '',
      pbcv_status: this.model.pbcv_status.length ? this.model.pbcv_status.join(',') : ''
    });

    let rows = null;
    let columns = null;
    let distributions = [];

    this.statsService.getTrends(search_criteria).subscribe((data) => {
      this.trends = data;
      const combinedChartGroup = {
        title: "All segments combined",
        charts: []
      }
      for (let distribution in this.trends) {
        distributions.push({
          id: distribution,
          name: this.trends[distribution].name
        });
        
        const cols = this.trends[distribution].data.columns.filter(x => !['volume_total', 'price_total'].includes(x.id));
        if (!columns) {
          columns = cols.slice();
        }
        if (!rows) {
          rows = this.trends[distribution].data.rows.slice();
        }

        if (cols && cols.length && rows && rows.length) {
          const chart = {
            currencyFormatting: cols[0].format === "nfcurrency",
            legend: true,
            name: this.trends[distribution].name,
            data: []
          };

          for (let segment in this.trends[distribution].data.data) {
            const segmentData = this.trends[distribution].data.data[segment];
            const column = columns.find(x => x.id === segment);
            if (column) {
              const line = {
                name: column.label,
                series: rows.map(x => ({
                  name: x.label,
                  value: segmentData[x.id] ? segmentData[x.id].value : 0
                }))
              };
              chart.data.push(line);
            }
          }
          if (chart.data.length) {
            combinedChartGroup.charts.push(chart);
          }
        }
      }
      if (combinedChartGroup.charts.length > 0) {
        this.chartGroups.push(combinedChartGroup);
      }

      if (columns) {
        columns.forEach(column => {
          const segmentChartGroup = {
            title: column.label,
            charts: []
          }

          distributions.forEach(distribution => {
            const segmentData = this.trends[distribution.id].data.data[column.id];
            const cols = this.trends[distribution.id].data.columns;
            
            const chart = {
              currencyFormatting: cols[0].format === "nfcurrency",
              legend: false,
              name: distribution.name,
              data: [{
                  name: column.label,
                  series: rows.map(x => ({
                    name: x.label,
                    value: segmentData[x.id] ? segmentData[x.id].value : 0
                  }))
              }]
            };
            segmentChartGroup.charts.push(chart);
          });
          this.chartGroups.push(segmentChartGroup);
        });
      }

      if (columns) {
        distributions.forEach(distribution => {
          const cols = this.trends[distribution.id].data.columns;
          if (cols.length) {
            const table = {
              name: distribution.name,
              columns: cols.map(x => x.label),
              rows: []
            }

            rows.forEach(r => {
              const row = [{label: r.label, list: ''}];
              cols.forEach(c => {
                const rowData = this.trends[distribution.id].data.data[c.id][r.id];
                const value = rowData ? rowData.value : 0;
                const list = rowData && c.id_field === "advertisement_id" ? rowData.list : '';
                if (c.format === "nfcurrency") {
                  row.push({label: this.formatPriceValue(value), list});
                } else 
                  row.push({label: value, list});
              });
              table.rows.push(row);
            });
            this.tables.push(table);
          }
        });
      }

      this.searching = false;

      setTimeout(() => {
        const offset = $("#trendsData").offset();
        if (offset) {
          $('html, body').animate({ scrollTop: offset.top - 50 }, 0, () => {
            this.scrolling = false;
          });
        }
      }, 1000);
    }, () => {
      this.searching = false;
    });

    return false;
  }

  changeForm() {
    this.changeFormEvent.emit();
  }

  resetFilter() {

    /** @todo find a possibility to uncheck without using query */
    $('[name="neighborhoods"]').iCheck('uncheck');
    $('[name="distributions"]').iCheck('uncheck');
    $('[name="segments"]').iCheck('uncheck');

    this.initModel();
    this.initForm();
    this.changeForm();

    this.changeUrl(true);
  }

  ngOnDestroy() {
    if (this.clientSubscription) {
      this.clientSubscription.unsubscribe();
    }
  }

  prepareFilters() {
    this.preparePBCVStatus();
    this.prepareNeighborhoods();
    this.prepareDisplayOptions();
  }

  preparePBCVStatus() {
    this.model.pbcv_status = [];
    this.pbcv_statuses.forEach(status => {
      if (status.selected) {
        this.model.pbcv_status = [status.value];
      }
    });
  }

  setAnotationMode() {
    this.model.annotation_mode = 1;
    this.model.advertising_mode = 0;
    if (this.pbcv_statuses.length === 0) {
      this.pbcv_statuses.find(status => status.value === 'AUTO').selected = true;
    }
  }

  setAdvertisingMode() {
    this.model.advertising_mode = 1;
    this.model.annotation_mode = 0;
  }

  resortProperies(data) {
    data.fee_structure.sort((a, b) => {
      return this.fee_structure_sort_order.indexOf(a) - this.fee_structure_sort_order.indexOf(b);
    });

    data.boroughs[0].neighborhoods.sort((a, b) => {
      return this.manhattan_sort_order.indexOf(a.neighborhood_id) - this.manhattan_sort_order.indexOf(b.neighborhood_id);
    });

    return data;
  }

  setAmenitities(amenity) {
    const index = this.model.amenities.indexOf(amenity);

    if (index === -1) {
      this.model.amenities.push(amenity);
    } else {
      this.model.amenities.splice(index, 1);
    }
  }

  setBuildingType(building_type) {
    if (building_type) {
      const index = this.model.building_type.indexOf(building_type);
      if (index === -1) {
        this.model.building_type.push(building_type);
        const all_index = this.model.building_type.indexOf('');
        if (all_index > -1) {
          this.model.building_type.splice(all_index, 1);
        }
      } else {
        this.model.building_type.splice(index, 1);
      }
    }

    if (!building_type || this.model.building_type.length === 0) {
      this.model.building_type = [''];
    }
  }

  setBorough(borough) {

    if (borough) {
      this.model.borough = borough.abbreviation;
      this.selected_borough = borough;
    } else {
      this.model.borough = '';
      this.model.neighborhood_id = [];
      this.selected_borough = null;
    }
  }

  setOther(other) {
    const index = this.model.other.indexOf(other);

    if (index === -1) {
      this.model.other.push(other);
    } else {
      this.model.other.splice(index, 1);
    }
  }

  setExposure(exposure) {
    const index = this.model.exposures.indexOf(exposure);

    if (index === -1) {
      this.model.exposures.push(exposure);
    } else {
      this.model.exposures.splice(index, 1);
    }
  }

  setOwnership(ownership) {
    const index = this.model.ownership.indexOf(ownership);

    if (index === -1) {
      this.model.ownership.push(ownership);
    } else {
      this.model.ownership.splice(index, 1);
    }
  }

  prepareNeighborhoods() {
    this.model.neighborhood = [];
    this.model.neighborhood_id = [];
    if (this.formOptions) {
      this.formOptions.boroughs.forEach(borough => {

        borough.neighborhoods.forEach((neighborhood) => {
          if (neighborhood.selected) {
            this.model.neighborhood_id.push(neighborhood.neighborhood_id);
          }
        });
      });
    }
  }

  prepareDisplayOptions() {
    this.model.segments = [];
    this.model.distributions = [];
    if (this.statsOptions) {
      this.statsOptions.trends.segments.forEach(segment => {
        if (segment.selected) {
          this.model.segments.push(segment.id);
        }
      });
      this.statsOptions.trends.distributions.forEach(distribution => {
        if (distribution.selected) {
          this.model.distributions.push(distribution.id);
        }
      });
    }
  }

  generateLink(list) {
    return `/search/${this.ad_type}?limit=12&offset=0&sort_by=time_published&sort_dir=desc&listing_mode=true&listing_ids=${list}`;
  }

  formatPriceValue = (value) => {
    const price = new CurrencyPipe('en_us').transform(value, 'USD', 'symbol');

    return price;
  }
}
