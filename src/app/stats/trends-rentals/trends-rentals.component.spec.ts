import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendsRentalsComponent } from './trends-rentals.component';

describe('TrendsRentalsComponent', () => {
  let component: TrendsRentalsComponent;
  let fixture: ComponentFixture<TrendsRentalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendsRentalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendsRentalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
