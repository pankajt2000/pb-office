import { AgentApiService } from '../../services/api/agent-api.service';
import { EventEmitter, Injectable } from '@angular/core';
import { TrendsOptionsModel } from '../models/trends-options.model';
import { StatsOptionsModel } from '../models/stats-options.model';
import { StatsTrendsModel } from '../models/stats-trends.model';
import { TrendsQueryModel } from '../models/trends-query.model';
import { Observable } from 'rxjs/Observable';

declare var $: any;

@Injectable()
export class StatsService {

  public triggerSearch: EventEmitter<any> = new EventEmitter();

  private form_options: TrendsOptionsModel;
  private stats_options: any;


  constructor(private api: AgentApiService) {
  }

  getFormOptions(): Observable<TrendsOptionsModel> {
    return new Observable(observer => {
      if (this.form_options) {
        observer.next(this.form_options);
        observer.complete();
      } else {
        this.api.get(`search-form-options`).map(data => new TrendsOptionsModel().deserialize(data))
          .subscribe(data => {
            this.form_options = data;
            observer.next(this.form_options);
            observer.complete();
          });
      }
    });
  }

  getStatsOptions(): Observable<StatsOptionsModel> {
    return new Observable(observer => {
      if (this.stats_options) {
        observer.next(this.stats_options);
        observer.complete();
      } else {
        this.api.get(`stats-options`).map(data => new StatsOptionsModel().deserialize(data))
          .subscribe(data => {
            this.stats_options = data;
            observer.next(this.stats_options);
            observer.complete();
          });
      }
    });
  }

  getTrends(data: TrendsQueryModel) {
    return this.api.get('stats-trends?' + $.param(data), { observe: 'response' }).map(response => {
      return response.body;
    });
  }
  
  getCompanies(term: string) {
    return this.api.get(`suggest-company?name=${term}`).map(data => data);
  }
}
