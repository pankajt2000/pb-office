import {
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  Input,
  OnInit,
  Renderer2, ViewContainerRef,
} from '@angular/core';
import { ListingService } from '../../listings/services/listing.service';
import { PhotoModel } from '../../listings/models/photo.model';
import { GalleryActionsComponent } from '../components/gallery-actions/gallery-actions.component';

declare var $: any;

@Directive({
  selector: '[appListingGallery]',
  host: {
    '(click)': 'showGallery()',
  }
})
export class ListingGalleryDirective implements OnInit {

  @Input('appListingGallery') listing: any;
  @Input('gallery-index') index = 0;
  @Input('load-more-images') is_load_more = true;
  @Input('gallery-index-by-url') current_image_path;
  all_photos: PhotoModel[] = [];
  componentRef;

  constructor(private cfResolver: ComponentFactoryResolver,
              public vcRef: ViewContainerRef,
              private renderer: Renderer2,
              private elementRef: ElementRef,
              private listingService: ListingService) {
  }

  ngOnInit(): void {
    this.renderer.addClass(this.elementRef.nativeElement, 'cursor-pointer');
  }

  prepareImages(): any[] {
    if (!this.is_load_more) {
      this.listing.photos.sort((a, b) => a.properties.index < b.properties.index ? -1 : a.properties.index > b.properties.index ? 1 : 0);
      this.all_photos = this.listing.photos;
    }

    if (this.all_photos.length) {
      return this.all_photos.map((photo: PhotoModel) => {
        return { src: photo.getUrl(null), data: photo };
      });
    }

    return this.listing.getPhotoUrls().map((photo) => {
      return { src: photo };
    });
  }

  showGallery() {
    const images = this.prepareImages();

    $.magnificPopup.open({
      items: images,
      type: 'image',
      closeOnContentClick: false,
      closeOnBgClick: false,
      closeBtnInside: false,
      showCloseBtn: false,
      enableEscapeKey: true,
      preload: [0, 1],
      modal: false,
      gallery: {
        enabled: true,
        markup: '<div class="mfp-figure">' +
        '<div class="mfp-img"></div>' +
        '<div class="mfp-bottom-bar">' +
        '<div class="mfp-title"></div>' +
        '<div class="mfp-counter"></div>' +
        '</div>' +
        '</div>',
      },
      image: {
        verticalFit: true,
        titleSrc: function (item) {
          return '';
        },
      },
      callbacks: {
        open: () => {

          $('.mfp-container').click((e) => {
            if ($(e.target).hasClass('mfp-container')) {
              $.magnificPopup.instance.close();
            }
          });

          this.checkIndex();

          this.createActionComponent();
        },
        beforeClose: () => {
        },
        close: () => {
          // this.componentRef.destroy();
        }
      },
    });

    if (this.is_load_more && !this.all_photos.length) {
      this.loadAllImages();
    }
  }

  createActionComponent() {
    const factory = this.cfResolver.resolveComponentFactory(GalleryActionsComponent);
    this.componentRef = this.vcRef.createComponent(factory);
    this.componentRef.instance.item = this.listing;
    this.componentRef.instance.popupInstance = $.magnificPopup.instance;
    const rootElement = this.renderer.selectRootElement('.mfp-content');
    this.renderer.appendChild(
      rootElement,
      this.componentRef.injector.get(GalleryActionsComponent).elementRef.nativeElement
    );
    this.componentRef.instance.delete_photo.subscribe((index) => {
      this.deletePhoto(index);
    });
    this.componentRef.instance.disable_photo.subscribe((index) => {
      this.disablePhoto(index);
    });
  }

  checkIndex() {
    const images = this.prepareImages();
    if (this.current_image_path) {
      const index = this.prepareImages().findIndex(p => p.src === this.listing.getUrlByPath(this.current_image_path, null));
      this.index = index > -1 ? index : 0;
    }

    if (this.index <= images.length) {
      $.magnificPopup.instance.goTo(+this.index);
    }
  }

  loadAllImages() {
    this.listingService.getGallery(this.listing.advertisement_id).subscribe(data => {
      this.all_photos = data;
      this.all_photos.sort((a, b) => a.properties.index < b.properties.index ? -1 : a.properties.index > b.properties.index ? 1 : 0);

      this.updateGallery();
      this.checkIndex();
    });
  }

  updateGallery() {
    const mfp = $.magnificPopup.instance;
    mfp.items = this.prepareImages();
    mfp.updateItemHTML();
  }

  disablePhoto(index: number) {
    this.listingService.disablePhoto(this.listing.advertisement_id, this.all_photos[index].media_item_id).subscribe(() => {
      this.all_photos.splice(index, 1);
      this.updateGallery();
    });
  }

  deletePhoto(index: number) {

    this.listingService.removePhoto(this.listing.advertisement_id, +this.all_photos[index].media_item_id).subscribe(() => {
      this.all_photos.splice(index, 1);
      this.updateGallery();
    });
  }

}
