import { Directive, HostListener, Input } from '@angular/core';
import { SendModalComponent } from '../../listings/send-modal/send-modal.component';
import { BsModalService } from 'ngx-bootstrap';

@Directive({
  selector: '[appMailPopup]'
})
export class MailPopupDirective {

  @Input() email = '';

  constructor(private modalService: BsModalService) {
  }

  @HostListener('click', ['$event']) openMailPopup(e: Event) {
    e.stopPropagation();

    this.modalService.show(SendModalComponent, { class: 'modal-lg m-t-10', initialState: { email: this.email } });
  }

}
