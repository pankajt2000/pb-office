import { ChangeDetectorRef, Directive, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

declare var $: any;

export const ICHECK_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line
  useExisting: forwardRef(() => IcheckDirective),
  multi: true
};


@Directive({
  selector: '[appIcheck]',
  providers: [ICHECK_CONTROL_VALUE_ACCESSOR],
})
export class IcheckDirective implements OnInit, ControlValueAccessor {

  @Input() appIcheck: string;

  @Input() set is_partial(value: any) {
    this._is_partial = !!value;
    if (value) {
      $(this.el.nativeElement).iCheck('styler').parent().addClass('partial');
    } else {
      $(this.el.nativeElement).iCheck('styler').parent().removeClass('partial');
    }
  }

  @Input('checked') init_checkbox: boolean;

  @Output('checked') checked: EventEmitter<any> = new EventEmitter();
  @Output('unchecked') unchecked: EventEmitter<any> = new EventEmitter();

  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();

  @Input() set ngModel(value: any) {
    if (value) {
      $(this.el.nativeElement).iCheck('check');
    } else {
      $(this.el.nativeElement).iCheck('uncheck');
    }
  }

  _is_partial = false;
  is_checked = false;
  onChange: any = Function.prototype;
  onTouched: any = Function.prototype;

  constructor(private el: ElementRef, private cd: ChangeDetectorRef) {
    $(el.nativeElement).iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    }).on('ifChecked', (e) => {
      this.checked.emit(e);
      this.is_checked = true;

      if (this._is_partial) {
        $(this.el.nativeElement).iCheck('uncheck');
        setTimeout(() => $(this.el.nativeElement).iCheck('styler').parent().removeClass('checked'), 1);
      }
    }).on('ifUnchecked', (e) => {
      this.is_checked = false;
      this.unchecked.emit(e);
    });
  }

  ngOnInit() {
    if (this.init_checkbox !== null) {
      $(this.el.nativeElement).iCheck(this.init_checkbox ? 'check' : 'uncheck');
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    $(this.el.nativeElement).iCheck(isDisabled ? 'disable' : 'enable');
  }

  writeValue(obj: any): void {
    if (obj && !this.is_checked) {
      $(this.el.nativeElement).iCheck('check');
    } else if (!obj && this.is_checked) {
      $(this.el.nativeElement).iCheck('uncheck');
    }
  }
}
