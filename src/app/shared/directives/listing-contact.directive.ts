import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { SendModalComponent } from '../../listings/send-modal/send-modal.component';
import { BsModalService } from 'ngx-bootstrap';

declare var $: any;

@Directive({
  selector: '[appListingContact]'
})

export class ListingContactDirective {

  @Input('appListingContact') set item(item) {
    this._item = item;

    this.checkDisabling();
  }

  private _item: any;
  private is_disabled = false;

  constructor(private el: ElementRef, private modalService: BsModalService) {
  }

  @HostListener('click') openPopup() {
    if (!this.is_disabled) {
      this.modalService.show(SendModalComponent, { class: 'modal-lg m-t-10', initialState: { email: this._item.contact_address } });
    }
  }

  checkDisabling() {
    if (!this._item || !this._item.contact_address || !this._item.contact_address.trim()) {
      $(this.el.nativeElement).addClass('disabled');
      $(this.el.nativeElement).removeClass('cursor-pointer');
      this.is_disabled = true;
    } else {
      $(this.el.nativeElement).removeClass('disabled');
      $(this.el.nativeElement).addClass('cursor-pointer');
      this.is_disabled = false;
    }

    $(this.el.nativeElement).prop('disabled', this.is_disabled);
  }

}
