import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { MessageService } from '../../mailbox/services/message.service';
import { MessageModel } from '../../mailbox/models/message.model';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';

declare var $: any;

@Directive({
  selector: '[appPreviewAttachment]'
})
export class PreviewAttachmentDirective {

  @Input('appPreviewAttachment') file: { filename: string, mime_type: string, url?: string };
  @Input() message: MessageModel;
  @Output() downloading: EventEmitter<boolean> = new EventEmitter<boolean>();


  constructor(private messageService: MessageService, private alertService: BootstrapAlertService) {
  }

  @HostListener('click') openPopup() {

    if (this.file.mime_type.indexOf('image') === -1) {
      return this.downloadFile();
    }

    if (this.file.url) {
      return this.openImage();
    }
    this.downloading.emit(true);

    this.messageService.getPreviewAttachment(this.message, this.file).subscribe((data) => {
      this.file.url = window.URL.createObjectURL(data.body);
      this.downloading.emit(false);

      this.openImage();
    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError('Error: Attachment not found');
      } else {
        this.alertService.showError('Error: Cannot preview the attached file');
      }
      this.downloading.emit(false);
    });
  }

  openImage() {
    $.magnificPopup.open({
      items: [{ src: this.file.url }],
      type: 'image',
      callbacks: {
        open: () => {
          const button = $('<button class="btn btn-sm btn-default download-attachment-btn">Download</button>');
          button.click(() => this.downloadFile());
          $('.mfp-figure figure').append(button);
        },
        beforeClose: () => {
          $('.download-attachment-btn').remove();
        },
        close: () => {

        }
      },
    });
  }

  downloadFile() {
    this.downloading.emit(true);

    this.messageService.downloadAttachment(this.message, this.file.filename).subscribe((data: Response) => {
      const url = window.URL.createObjectURL(data.body);
      const link = document.createElement('a');
      link.download = this.file.filename;
      link.href = url;
      link.click();
      this.downloading.emit(false);
    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError('Error: Attachment not found');
      } else {
        this.alertService.showError('Error: Cannot download the attached file');
      }
      this.downloading.emit(false);
    });
  }
}
