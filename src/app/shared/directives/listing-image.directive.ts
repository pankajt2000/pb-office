import { Directive, ElementRef, HostBinding, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { config } from '../../config';

@Directive({
  selector: '[appListingImage]',
  host: {
    '(error)': 'onError()',
  }
})
export class ListingImageDirective  {

  @HostBinding('style.background-image')
  backgroundImage: string = 'url(' + this.getBackgroundImage() + ')';

  constructor(private elementRef: ElementRef) {
  }

  getBackgroundImage(size: string = config.media_sizes.wadmin) {
    return environment.transcode_media_base_url + `${size}/whitespace-blur-fill.png`;
  }

  getNoPhotoImage(size: string = config.media_sizes.wadmin) {
    return environment.transcode_media_base_url + `${size}/no-photo.jpg`;
  }

  getErrorImage(size: string = config.media_sizes.wadmin) {
    return environment.transcode_media_base_url + `${size}/load-error.png`;
  }

  onError() {
    if (this.elementRef.nativeElement.src.search(/\.[a-z0-9]+$/i) > -1) {
      this.elementRef.nativeElement.src = this.getErrorImage();
    } else {
      this.elementRef.nativeElement.src = this.getNoPhotoImage();
    }
  }

}
