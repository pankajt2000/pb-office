import { Pipe, PipeTransform } from '@angular/core';
import { FromUnixPipe, DateFormatPipe, ParsePipe } from 'ngx-moment';

@Pipe({
  name: 'appDateFormat'
})
export class AppDateFormatPipe implements PipeTransform {

  private formats = {
    'inbox': 'ddd, MMM D, Y',
    'full': 'MM/DD/YYYY',
    'short': 'MM/DD',
    'aform': 'MMM Do',
    'time': 'h:mm a'
  };

  transform(value: any, ...args: string[]): string {
    const type = isNaN(Number(value)) ? 'date' : 'timestamp';
    const fup = new FromUnixPipe();
    const pp = new ParsePipe();
    const dfp = new DateFormatPipe();

    for (const format of args) {
      if (format in this.formats === false) {
        throw new Error(`Unsupported format: ${format}`);
      }
    }

    switch (type) {
      case 'date':
        return dfp.transform(value, this.getFormat(args));
      case 'timestamp':
        if (value < 1000000000000) {
          value = fup.transform(value);
        }
        return dfp.transform(value, this.getFormat(args));
      default:
        throw new Error(`Unsupported value ${value}`);
    }
  }

  private getFormat(args: string[]): string {
    return args.map(a => this.formats[a]).join(', ');
  }

}
