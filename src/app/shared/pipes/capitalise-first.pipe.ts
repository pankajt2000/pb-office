import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitaliseFirst'
})
export class CapitaliseFirstPipe implements PipeTransform {

  transform(value: string, lowercaseOthers?: boolean, ucwords: boolean = false): string {
    if (!value) return '';

    if (ucwords) {
      return value
        .split(' ')
        .map(w => w.charAt(0).toUpperCase() + w.slice(1).toLowerCase())
        .join(' ');
    } else {
      const first = value.charAt(0).toUpperCase();
      const rest = lowercaseOthers ? value.slice(1).toLowerCase() : value.slice(1);

      return `${first}${rest}`;
    }
  }

}
