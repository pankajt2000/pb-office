import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'ellipsis'
})
export class EllipsisPipe implements PipeTransform {

	transform(value: string, type: string, limit: number): string {
		if (value === null) return '';

		if (value.length <= limit || limit < 0) return value;

		let ellipsis = '';

		if (type === 'words') {

			if (value.split(' ').length <= limit) {
				return value;
			}

			ellipsis = value.split(' ').slice(0, limit).join(' ');
		} else if (type === 'characters') {

			if (value.split('').length <= limit) {
				return value;
			}

			ellipsis = value.split('').slice(0, limit).join('');
		} else {
			throw new Error(`Unsupported ellipsis type: ${type}`);
		}

		return `${ellipsis}...`;
	}

}