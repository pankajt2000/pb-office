import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keysPipe'
})
export class KeysPipe implements PipeTransform {

  transform(value: object, ...args: any[]): {[key: string]: any}[] | null {
    if (!value) null;

    let data: {[key: string]: any}[] = [];

    for (let key of Object.keys(value)) {
      data.push( { key, value: value[key] });
    }

    return data;
  }

}
