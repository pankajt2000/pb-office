import { NgModule } from '@angular/core';
import { MomentModule } from 'ngx-moment';
import { BsDropdownModule, PaginationModule, ProgressbarModule, TooltipModule, TypeaheadModule } from 'ngx-bootstrap';
import { SignaturePadModule } from 'angular2-signaturepad';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../authentication/services/user.service';
import { LoaderComponent } from './loader/loader.component';
import { BrowserModule } from '@angular/platform-browser';
import { BootstrapAlertModule } from 'ngx-bootstrap-alert-service';
import { ClientService } from '../client/services/client.service';
import { SendModalComponent } from '../listings/send-modal/send-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ScrollbarModule } from 'ngx-scrollbar';
import { IcheckDirective } from './directives/icheck.directive';
import { SavedListingsItemComponent } from '../listings/saved-listings/saved-listings-item/saved-listings-item.component';
import { RouterModule } from '@angular/router';
import { ListingImageDirective } from './directives/listing-image.directive';
import { CapitaliseFirstPipe } from './pipes/capitalise-first.pipe';
import { EllipsisPipe } from './pipes/ellipsis.pipe';
import { KeysPipe } from './pipes/keys.pipe';
import { ModalImagesComponent } from './components/modal-images/modal-images.component';
import { SlickModule } from 'ngx-slick';
import { ListingGalleryDirective } from './directives/listing-gallery.directive';
import { GalleryActionsComponent } from './components/gallery-actions/gallery-actions.component';
import { GmailService } from '../mailbox/services/gmail.service';
import { SearchResultItemComponent } from '../search/search-result/search-result-item/search-result-item.component';
import { SearchResultItemListComponent } from '../search/search-result/search-result-item-list/search-result-item-list.component';
import { CreateClientComponent } from '../client/create-client/create-client.component';
import { NgUploaderModule } from 'ngx-uploader';
import { SendComponent } from '../mailbox/send/send.component';
import { AttachmentCardComponent } from '../mailbox/send/attachment-card/attachment-card.component';
import { SavedListinIconComponent } from './components/saved-listin-icon/saved-listin-icon.component';
import { SelectClientPopupComponent } from './components/saved-listin-icon/select-client-popup/select-client-popup.component';
import { EditClientComponent } from '../client/edit-client/edit-client.component';
import { ListingContactDirective } from './directives/listing-contact.directive';
import { SearchResultComponent } from '../search/search-result/search-result.component';
import { PreviewAttachmentDirective } from './directives/preview-attachment.directive';
import { ConfirmPopupComponent } from './components/confirm-popup/confirm-popup.component';
import { MailPopupDirective } from './directives/mail-popup.directive';
import { ListingPriceComponent } from './components/listing-price/listing-price.component';
import { CurrencyPipe } from '@angular/common';
import { AppDateFormatPipe } from './pipes/app-date-format.pipe';
import { ListingAnnotationDirective } from '../listings/annotation/listing-annotation.directive';
import { ListingAnnotationComponent } from '../listings/annotation/listing-annotation/listing-annotation.component';
import { ItemAnnotationComponent } from '../listings/annotation/item-annotation/item-annotation.component';
import { InfoPopupComponent } from './components/info-popup/info-popup.component';
import { CookieService } from 'ngx-cookie-service';
import { SignatureEditorComponent } from './components/signature-editor/signature-editor.component';


@NgModule({
  imports: [
    BrowserModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ReactiveFormsModule,
    NgSelectModule,
    ScrollbarModule,
    RouterModule,
    FormsModule,
    MomentModule,
    TypeaheadModule,
    SlickModule.forRoot(),
    PaginationModule.forRoot(),
    ProgressbarModule.forRoot(),
    SignaturePadModule
  ],
  declarations: [
    LoaderComponent,
    SendModalComponent,
    IcheckDirective,
    ListingImageDirective,
    SavedListingsItemComponent,
    CapitaliseFirstPipe,
    EllipsisPipe,
    KeysPipe,
    ModalImagesComponent,
    ListingGalleryDirective,
    GalleryActionsComponent,
    SearchResultComponent,
    SearchResultItemComponent,
    SearchResultItemListComponent,
    CreateClientComponent,
    SendComponent,
    AttachmentCardComponent,
    SavedListinIconComponent,
    SelectClientPopupComponent,
    ListingContactDirective,
    PreviewAttachmentDirective,
    ConfirmPopupComponent,
    MailPopupDirective,
    ListingAnnotationDirective,
    ListingAnnotationComponent,
    ItemAnnotationComponent,
    ListingPriceComponent,
    AppDateFormatPipe,
    InfoPopupComponent,
    SignatureEditorComponent,
  ],
  providers: [
    UserService,
    ClientService,
    GmailService,
    CurrencyPipe,
    CookieService,
  ],
  exports: [
    BrowserModule,
    MomentModule,
    BsDropdownModule,
    TooltipModule,
    FormsModule,
    ReactiveFormsModule,
    BootstrapAlertModule,
    NgSelectModule,
    ScrollbarModule,
    SlickModule,
    NgUploaderModule,
    PaginationModule,
    ProgressbarModule,

    TypeaheadModule,
    LoaderComponent,
    SendModalComponent,
    IcheckDirective,
    ListingImageDirective,
    ListingGalleryDirective,
    ListingContactDirective,
    PreviewAttachmentDirective,
    MailPopupDirective,
    ListingAnnotationDirective,

    SavedListingsItemComponent,
    CapitaliseFirstPipe,
    EllipsisPipe,
    KeysPipe,
    AppDateFormatPipe,
    ModalImagesComponent,
    SearchResultComponent,
    SearchResultItemComponent,
    SearchResultItemListComponent,
    CreateClientComponent,
    SendComponent,
    SavedListinIconComponent,
    SelectClientPopupComponent,
    ConfirmPopupComponent,
    InfoPopupComponent,
    ListingPriceComponent,
    ListingAnnotationComponent,
    ItemAnnotationComponent,
    SignatureEditorComponent,
  ],
  entryComponents: [
    ItemAnnotationComponent,
    ListingAnnotationComponent,
    SendModalComponent,
    ModalImagesComponent,
    GalleryActionsComponent,
    CreateClientComponent,
    SendComponent,
    SavedListinIconComponent,
    SelectClientPopupComponent,
    EditClientComponent,
    ConfirmPopupComponent,
    InfoPopupComponent,
    ListingPriceComponent
  ]
})
export class SharedModule {
}
