import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedListinIconComponent } from './saved-listin-icon.component';

describe('SavedListinIconComponent', () => {
  let component: SavedListinIconComponent;
  let fixture: ComponentFixture<SavedListinIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedListinIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedListinIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
