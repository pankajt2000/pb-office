import { Component, Input, OnInit } from '@angular/core';
import { SavedListingService } from '../../../listings/services/saved-listing.service';
import { ClientService } from '../../../client/services/client.service';
import { ClientModel } from '../../../client/models/client.model';
import { BsModalService } from 'ngx-bootstrap';
import { SendModalComponent } from '../../../listings/send-modal/send-modal.component';
import { SelectClientPopupComponent } from './select-client-popup/select-client-popup.component';

@Component({
  selector: 'app-saved-listin-icon',
  templateUrl: './saved-listin-icon.component.html',
  styleUrls: ['./saved-listin-icon.component.less']
})
export class SavedListinIconComponent implements OnInit {

  @Input() item;
  @Input() tooltipPlacement = 'bottom';
  @Input() withLabel = false;

  client: ClientModel;

  constructor(public savedListingService: SavedListingService,
              private clientService: ClientService,
              private modalService: BsModalService) {
    clientService.getCurrentClient().subscribe((client) => this.client = client);
  }

  ngOnInit() {
  }

  toogleListing() {
    if (this.client) {
      this.savedListingService.toogleListing(this.item.advertisement_id);
    } else {
      this.modalService.show(SelectClientPopupComponent, {
        class: 'modal-sm',
        initialState: { item: this.item }
      });
    }
  }

}
