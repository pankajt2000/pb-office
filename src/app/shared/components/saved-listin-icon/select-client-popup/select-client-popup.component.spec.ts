import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectClientPopupComponent } from './select-client-popup.component';

describe('SelectClientPopupComponent', () => {
  let component: SelectClientPopupComponent;
  let fixture: ComponentFixture<SelectClientPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectClientPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectClientPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
