import { Component, OnInit } from '@angular/core';
import { SavedListingModel } from '../../../../listings/models/saved-listing.model';
import { ClientModel } from '../../../../client/models/client.model';
import { ClientService } from '../../../../client/services/client.service';
import { BsModalRef } from 'ngx-bootstrap';
import { SavedListingService } from '../../../../listings/services/saved-listing.service';

@Component({
  selector: 'app-select-client-popup',
  templateUrl: './select-client-popup.component.html',
  styleUrls: ['./select-client-popup.component.less']
})
export class SelectClientPopupComponent implements OnInit {

  item: SavedListingModel;
  clients: ClientModel[];
  current_client: ClientModel;

  constructor(public bsModalRef: BsModalRef,
              private clientService: ClientService,
              private savedLsitngService: SavedListingService) {
    this.clientService.getAllSuggestClients().subscribe(clients => this.clients = clients);
  }

  ngOnInit() {
  }

  changeClient() {
    this.clientService.setCurrentClient(this.current_client);
    if (this.current_client) {
      this.savedLsitngService.saveListing(this.item.advertisement_id);

      this.bsModalRef.hide();
    }
  }
}
