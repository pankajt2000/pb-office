import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';

@Component({
  selector: 'app-signature-editor',
  templateUrl: './signature-editor.component.html',
  styleUrls: ['./signature-editor.component.less']
})
export class SignatureEditorComponent implements OnInit, AfterViewInit {

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  @Input('dataUrl') prefilledData: string;

  @Output() drawn: EventEmitter<string> = new EventEmitter<string>();

  options = {
    canvasWidth: 500, canvasHeight: 100
  };

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 2);
    this.signaturePad.clear();

    if (this.prefilledData) {
      this.signaturePad.fromDataURL(this.prefilledData);
    }
  }

  drawComplete() {
    const data_url = this.signaturePad.toDataURL();
    this.drawn.emit(data_url);
  }

  clear() {
    this.signaturePad.clear();
    this.drawn.emit('');
  }

  drawStart() {
  }

}
