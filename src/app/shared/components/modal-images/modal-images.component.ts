import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { SlickComponent } from 'ngx-slick';

declare var $: any;

@Component({
  selector: 'app-modal-images',
  templateUrl: './modal-images.component.html',
  styleUrls: ['./modal-images.component.less']
})
export class ModalImagesComponent implements OnInit {

  slickConfig = {
    dots: true,
  };

  photos: string[];
  selected_index: number;

  @ViewChild('slickModal') slickModal: SlickComponent;

  constructor(public bsModalRef: BsModalRef) {
  }

  ngOnInit() {

    if (this.selected_index > -1) {
      setTimeout(() => {
        $('.slick-dots button').eq(this.selected_index).click();
      }, 100);
    }
  }

}
