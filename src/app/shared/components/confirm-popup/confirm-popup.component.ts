import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-confirm-popup',
  templateUrl: './confirm-popup.component.html',
  styleUrls: ['./confirm-popup.component.less']
})
export class ConfirmPopupComponent implements OnInit {

  message: string;

  @Output() confirm = new EventEmitter();
  @Output() cancel = new EventEmitter();

  constructor(public bsModalRef: BsModalRef) {
  }

  ngOnInit() {
  }

  confirmAction() {
    this.confirm.emit();
    this.bsModalRef.hide();
  }

  cancelAction() {
    this.cancel.emit();
    this.bsModalRef.hide();
  }

}
