import { Component, Input, OnInit } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-listing-price',
  templateUrl: './listing-price.component.html',
  styleUrls: ['./listing-price.component.less']
})
export class ListingPriceComponent implements OnInit {

  @Input() item: any;
  @Input() is_short_version = true;

  constructor(private cp: CurrencyPipe) {
  }

  ngOnInit() {
  }

  getPrice() {
    return this.item.net_effective_price ? this.item.net_effective_price : this.item.price;
  }

  getGrossPriceLabel() {
    if (this.item.net_effective_price) {
      return 'Gross price: ' + (this.item.price ? '$' + this.cp.transform(this.item.price, 'USD', 'symbol-narrow', '1.0-2') : 'N/A');
    }
    return null;
  }
}
