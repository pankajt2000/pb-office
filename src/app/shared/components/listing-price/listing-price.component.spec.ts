import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingPriceComponent } from './listing-price.component';

describe('ListingPriceComponent', () => {
  let component: ListingPriceComponent;
  let fixture: ComponentFixture<ListingPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
