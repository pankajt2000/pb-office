import { Component, ElementRef, EventEmitter, HostListener, OnInit } from '@angular/core';
import { SavedListingService } from '../../../listings/services/saved-listing.service';
import { ListingModel } from '../../../listings/models/listing.model';
import { ClientModel } from '../../../client/models/client.model';
import { ClientService } from '../../../client/services/client.service';
import { ListingService } from '../../../listings/services/listing.service';
import { environment } from '../../../../environments/environment';
import { PhotoModel } from '../../../listings/models/photo.model';
import { AnnotateService } from '../../../listings/annotation/annotate.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { SearchService } from '../../../search/services/search.service';

declare var $: any;

@Component({
  selector: 'app-gallery-actions',
  templateUrl: './gallery-actions.component.html',
  styleUrls: ['./gallery-actions.component.less']
})
export class GalleryActionsComponent implements OnInit {

  item: ListingModel;
  client: ClientModel;
  popupInstance: any;
  coef: number;
  activeImageLoading: boolean;
  activeImageUrl: string;
  activeImageSize: {
    width: number;
    height: number;
  }
  showDetectedObjects = false;

  annotate_mode = false;

  delete_photo: EventEmitter<number> = new EventEmitter();
  disable_photo: EventEmitter<number> = new EventEmitter();

  photos: any[];

  colors_mappings: {type: string, color: string}[] = [];

  annotation_changed = false;

  palette = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231',
   '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe', '#008080',
   '#e6beff', '#9a6324', '#fffac8', '#800000', '#aaffc3', '#808000',
   '#ffd8b1', '#000075', '#808080', '#000000'];


  constructor(public elementRef: ElementRef,
              public savedListingService: SavedListingService,
              private listingService: ListingService,
              private annotateService: AnnotateService,
              private searchService: SearchService,
              private alertService: BootstrapAlertService,
              private clientService: ClientService) {
    this.clientService.getCurrentClient().subscribe(client => this.client = client);
    this.activeImageLoading = false;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    const activeImage = $('.mfp-img')[0];
    if (activeImage && this.activeImageSize) {
      this.coef = activeImage.width / this.activeImageSize.width;
    }
  }


  ngOnInit() {
    this.popupInstance.updateItemHTML();

    if (typeof this.item.photos[this.popupInstance.index] === 'string') {
      this.listingService.getGallery(this.item.advertisement_id).subscribe(photos => {
        this.photos = photos;
      });
    } else {
      this.photos = this.item.photos;
    }

    this.popupInstance.close = () => {
      if (this.annotation_changed) {
        this.searchService.triggerSearch.emit();
      }

      $.magnificPopup.proto.close.call(this.popupInstance);
    };
  }

  toggleDetectedObjects() {
    this.showDetectedObjects = !this.showDetectedObjects;
  }

  disablePhoto() {
    this.disable_photo.emit(this.popupInstance.index);
  }

  deletePhoto() {
    this.delete_photo.emit(this.popupInstance.index);
  }

  closeGallery() {
    this.popupInstance.close();
  }

  getDetectedObjects(): any[] {
    const annotate = this.getPhoto() ? (this.getPhoto().properties.pbcv_annotate as any) : null;
    const detectedObjects = annotate && annotate.detect_objects && annotate.detect_objects.value ? annotate.detect_objects.value.objects : [];
    return detectedObjects;
  }

  getColor(type: string) {
    const mapping = this.colors_mappings.find(x => x.type === type);
    if (mapping) {
      return mapping.color;
    }
    return 'red';
  }

  getPhoto(): PhotoModel {
    if (!this.photos) return null;
    const photo = this.photos[this.popupInstance.index];
    if (!photo) return null;
    const downloadLink = photo.source_url;
    if (photo.properties.pbcv_annotate
      && photo.properties.pbcv_annotate.detect_objects
      && photo.properties.pbcv_annotate.detect_objects.value
      && this.activeImageUrl !== downloadLink) {
      this.activeImageUrl = downloadLink;

      this.colors_mappings = [];
      const detectedObjects = photo.properties.pbcv_annotate.detect_objects.value.objects;

      detectedObjects.forEach(obj => {
        const mapping = this.colors_mappings.find(x => x.type === obj.type);
        if (!mapping) {
          const index = this.colors_mappings.length % this.palette.length;
          this.colors_mappings.push({
            type: obj.type,
            color: this.palette[index]
          })
        }
      });

      const img = new Image();
      img.onload = (e) => {
          this.coef = 1;
          const image = e.target as any;
          this.activeImageSize = {
            width: image.width,
            height: image.height
          };
          const activeImage = $('.mfp-img')[0];
          if(activeImage){
            this.coef = activeImage.width / this.activeImageSize.width;
          }
      };
      img.src = downloadLink;
    }
    return photo;
  }

  getAnnotations() {
    const photo = this.getPhoto();
    if (photo && photo.properties.pbcv_annotate) {
      return Object.values(photo.properties.pbcv_annotate).filter(o => o.type !== 'detect_objects');
    }

    return [];
  }

  getPhotoUrl() {
    if (typeof this.item.photos[this.popupInstance.index] === 'string') {
      return environment.media_base_url + this.item.photos[this.popupInstance.index];
    } else if (this.item.photos[this.popupInstance.index]) {
      return this.item.photos[this.popupInstance.index].getDownloadLink();
    }
  }

  ignoreAnnotation() {
    this.annotation_changed = true;
    const photo = this.getPhoto();
    if (photo) {
      if (photo.isAnnotateIgnore()) {
        this.annotateService.ignorePhoto(this.item.advertisement_id, photo.media_item_id).subscribe(() => {
        }, () => {
          this.alertService.showError('Could not ignore the photo');
        });
      } else {
        this.annotateService.stopIgnorePhoto(this.item.advertisement_id, photo.media_item_id).subscribe(() => {
        }, () => {
          this.alertService.showError('Could not remove the photo from ignore list');
        });
      }
    }
  }

  changeAnnotation() {
    this.annotation_changed = true;
    const photo = this.getPhoto();

    if (photo) {
      const postObject = {};
      for (const type in photo.properties.pbcv_annotate) {
        if (type === 'detect_objects' || !photo.properties.pbcv_annotate[type].new_value) {
          continue;
        }
        postObject[type] = photo.getAnnotateValue(type);
      }

      this.annotateService.savePhoto(this.item.advertisement_id, photo.media_item_id, postObject).subscribe((photo: PhotoModel) => {
        // this.photos[this.popupInstance.index] = photo;
      }, () => {
        this.alertService.showError('Could not save the annotations');
      });
    }
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(e) {
    if (!this.photos) {
      return;
    }
    if (this.getPhoto()) {
      if ($(e.target).hasClass('mfp-img')) {
        this.getPhoto().properties.show_options = ($(e.target).height() / 2) > (e.offsetY - 30);
      }
      if ($(e.target).hasClass('mfp-container')) {
        this.getPhoto().properties.show_options = false;
      }
    }
  }

}
