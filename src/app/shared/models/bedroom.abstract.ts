export abstract class BedroomAbstract {

  abstract bedrooms;

  getBedroomsText(is_short: boolean = false, omit_suffix: boolean = false): string {
    if(is_short && this.bedrooms==='Alcove Studio') return 'Alc. Std.';

    if (omit_suffix) return this.bedrooms;

    return this.bedrooms === parseFloat(this.bedrooms).toString() ? (this.bedrooms + ' br') : this.bedrooms;
  }

}
