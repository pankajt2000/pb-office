export interface DeserializableInterface<T> {
  deserialize(input: any): T;
}
