import { Component, Input, OnInit } from '@angular/core';
import { InquiryShortModel } from "../../dashboard/widgets/inquiries/models/inquiry-short.model";
import { InquiryDetailsModel } from "../../dashboard/widgets/inquiries/models/inquiry-details.model";
import { InquiriesService } from "../services/inquiries.service";
import { ConfirmPopupComponent } from "../../shared/components/confirm-popup/confirm-popup.component";
import { BsModalService } from "ngx-bootstrap";
import { BootstrapAlertService } from "ngx-bootstrap-alert-service";
import { AuthService } from '../../authentication/services/auth.service';

@Component({
  selector: 'app-inquiry-status',
  templateUrl: './inquiry-status.component.html',
  styleUrls: ['./inquiry-status.component.less']
})
export class InquiryStatusComponent implements OnInit {

  @Input('inquiry') inquiry: InquiryShortModel & InquiryDetailsModel;

  is_supervisor: boolean = false;

  private previous_status;
  
  constructor(
    private inquiriesService: InquiriesService,
    private modalService: BsModalService,
    private alertService: BootstrapAlertService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.previous_status = this.inquiry.status;

    this.authService.sessionInfo(false).subscribe(info => {
      this.is_supervisor = info.user.roles.map(r => r.toLowerCase()).includes('supervisor');
    });
  }

  canEdit() {
    return this.is_supervisor || ['NEW', 'PROGRESS'].includes(this.inquiry.status);
  }

  updateInquiryStatus(status: string) {
    const clean = status.split('_').map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()).join(' ');

    const modal = this.modalService.show(ConfirmPopupComponent, {
      class: 'modal-sm m-t-10', initialState: {
        message: `You are about to set this inquiry\'s status to "${clean}". Proceed?`
      }
    });

    modal.content.confirm.subscribe(() => {
      this.inquiriesService.updateInquiryStatus(this.inquiry.inquiry_id, status).subscribe((res: InquiryShortModel & InquiryDetailsModel) => {
        this.inquiry = res;

        this.previous_status = res.status;

        this.alertService.showSucccess('Inquiry status updated.');
      }, (_) => {
        this.alertService.showError('Something went wrong. Please try again later.');
        this.inquiry.status = this.previous_status;
      });
    });

    modal.content.cancel.subscribe(() => {
      this.inquiry.status = this.previous_status;
    });
  }

}
