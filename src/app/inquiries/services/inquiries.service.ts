import { Injectable } from "@angular/core";
import { AgentApiService } from "../../services/api/agent-api.service";
import { InquiryShortModel } from "../../dashboard/widgets/inquiries/models/inquiry-short.model";
import { InquiryDetailsModel } from "../../dashboard/widgets/inquiries/models/inquiry-details.model";

@Injectable()
export class InquiriesService {

  constructor(private api: AgentApiService) { }

  getInquiries(offset: number, limit: number = 10) {
    return this.api.get(`listing-inquiries?offset=${offset}&limit=${limit}`, { observe: 'response' }).map(response => {
      return [
        response.body.map(inquiry => new InquiryShortModel().deserialize(inquiry)),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

  getListingInquiryById(inquiry_id: string) {
    return this.api.get(`listing-inquiries/${inquiry_id}`).map(inquiry => new InquiryDetailsModel().deserialize(inquiry));
  }

  replyToInquiry(inquiry_id: string, body: string, cc?: string[]) {
    return this.api.post(`listing-inquiries-respond/${inquiry_id}`, { body, cc }).map(res => new InquiryDetailsModel().deserialize(res));
  }

  updateInquiryStatus(inquiry_id: string, status: string) {
    return this.api.put(`listing-inquiries-status/${inquiry_id}/${status}`, {});
  }

}
