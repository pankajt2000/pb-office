import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InquiriesService } from './services/inquiries.service';
import { InquiryStatusComponent } from './inquiry-status/inquiry-status.component';
import { FormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    InquiryStatusComponent
  ],
  providers: [
    InquiriesService
  ],
  exports: [
    InquiryStatusComponent
  ]
})
export class InquiriesModule { }
