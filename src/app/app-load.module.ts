import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppLoadService } from './app-load.service';

export function InitializeGapiObject(appLoadService: AppLoadService) {
  return () => appLoadService.initializeGoogleApi();
}

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: InitializeGapiObject, deps: [AppLoadService], multi: true }
  ]
})
export class AppLoadModule { }
