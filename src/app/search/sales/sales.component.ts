import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { RentalsComponent } from '../rentals/rentals.component';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService } from '../../client/services/client.service';
import { SearchService } from '../services/search.service';

@Component({
  selector: 'app-sales',
  templateUrl: './../rentals/rentals.component.html',
  styleUrls: ['./../rentals/rentals.component.less']
})
export class SalesComponent extends RentalsComponent {

  constructor(protected searchService: SearchService,
              protected route: ActivatedRoute,
              protected router: Router,
              protected clientService: ClientService,
              protected cd: ChangeDetectorRef) {
    super(searchService, route, router, clientService, cd);
  }

}
