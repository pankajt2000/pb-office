import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SearchService} from '../services/search.service';
import {ClientService} from '../../client/services/client.service';
import {SearchByCriteriaComponent} from '../search-by-criteria/search-by-criteria.component';
import {SearchOptionsModel} from '../models/search-options.model';

@Component({
  selector: 'app-client-new-listings',
  templateUrl: './client-new-listings.component.html',
  styleUrls: ['./client-new-listings.component.less']
})
export class ClientNewListingsComponent extends SearchByCriteriaComponent implements OnInit {

    client_id: number;

    search(is_load_more: boolean = false) {
        this.searching = true;

        this.searchService.getClientsNewListings(this.client_id, this.model.offset, this.model.limit, this.model.sort_by, this.model.sort_dir)
            .subscribe(results => {

                this.searching = false;
                this.search_result_count = results[1];
                this.search_result = is_load_more ? this.search_result.concat(results[0]) : results[0];

                this.updateListingsInfo(results[0]);

            }, (response) => {

                console.log(response);
            });
    }

    ngOnInit() {

        this.route.params.subscribe(params => {
            this.client_id = params['client_id'];
            this.clientService.getSuggestClients().subscribe(clients => {
                if (clients) {
                    clients.forEach(client => {
                        if (client.user_id == this.client_id) {
                            this.client = client;
                        }
                    });
                }
            });

            this.search();
        });
    }

}
