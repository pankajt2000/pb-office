import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientNewListingsComponent } from './client-new-listings.component';

describe('ClientNewListingsComponent', () => {
  let component: ClientNewListingsComponent;
  let fixture: ComponentFixture<ClientNewListingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientNewListingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientNewListingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
