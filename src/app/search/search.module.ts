import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RentalsComponent } from './rentals/rentals.component';
import { SalesComponent } from './sales/sales.component';
import { SearchService } from './services/search.service';
import { ModalModule, RatingModule, TabsModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { SearchByCriteriaComponent } from './search-by-criteria/search-by-criteria.component';
import { CreatedByMeComponent } from './created-by-me/created-by-me.component';
import { ClientNewListingsComponent } from './client-new-listings/client-new-listings.component';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    RatingModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    NgxCurrencyModule,
  ],
  providers: [
    SearchService
  ],
  declarations: [
    RentalsComponent,
    SalesComponent,
    SearchByCriteriaComponent,
    CreatedByMeComponent,
    ClientNewListingsComponent,
  ]
})
export class SearchModule {
}
