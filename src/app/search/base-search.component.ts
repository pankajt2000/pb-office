import { SearchQueryModel } from './models/search-query.model';
import { SearchOptionsModel } from './models/search-options.model';
import { Subject } from 'rxjs/Subject';

export abstract class BaseSearchComponent {

  ad_type: string;
  model: SearchQueryModel;
  formOptions: SearchOptionsModel;
  selected_borough = null;
  companies_to_loading = false;
  companiesToTypeahead = new Subject<string>();
  search_companies = [];

  fee_structure_sort_order = ['All', 'CYOF', 'Co-Broke'];
  manhattan_sort_order = ['16', '3', '14', '5', '1', '9', '2', '19', '7', '20', '4', '18', '25', '6', '24', '10', '26', '15', '22', '23', '12', '11','13', '21', '17', '27', '28', '29'];

  resortProperies(data) {
    data.fee_structure.sort((a, b) => {
      return this.fee_structure_sort_order.indexOf(a) - this.fee_structure_sort_order.indexOf(b);
    });

    if (data.boroughs && data.boroughs[0] && data.boroughs[0].neighborhoods) {
      data.boroughs[0].neighborhoods.sort((a, b) => {
        return this.manhattan_sort_order.indexOf(a.neighborhood_id) - this.manhattan_sort_order.indexOf(b.neighborhood_id);
      });
    }

    return data;
  }

  setAmenitities(amenity) {
    const index = this.model.amenities.indexOf(amenity);

    if (index === -1) {
      this.model.amenities.push(amenity);
    } else {
      this.model.amenities.splice(index, 1);
    }
  }

  setBuildingType(building_type) {
    if (building_type) {
      const index = this.model.building_type.indexOf(building_type);
      if (index === -1) {
        this.model.building_type.push(building_type);
        const all_index = this.model.building_type.indexOf('');
        if (all_index > -1) {
          this.model.building_type.splice(all_index, 1);
        }
      } else {
        this.model.building_type.splice(index, 1);
      }
    }

    if (!building_type || this.model.building_type.length === 0) {
      this.model.building_type = [''];
    }
  }

  setBorough(borough) {

    if (borough) {
      this.model.borough = borough.abbreviation;
      this.selected_borough = borough;
    } else {
      this.model.borough = '';
      this.model.neighborhood_id = [];
      this.selected_borough = null;
    }
  }

  setOther(other) {
    const index = this.model.other.indexOf(other);

    if (index === -1) {
      this.model.other.push(other);
    } else {
      this.model.other.splice(index, 1);
    }
  }

  setExposure(exposure) {
    const index = this.model.exposures.indexOf(exposure);

    if (index === -1) {
      this.model.exposures.push(exposure);
    } else {
      this.model.exposures.splice(index, 1);
    }
  }

  setOwnership(ownership) {
    const index = this.model.ownership.indexOf(ownership);

    if (index === -1) {
      this.model.ownership.push(ownership);
    } else {
      this.model.ownership.splice(index, 1);
    }
  }

  prepareNeighborhoods() {
    this.model.neighborhood = [];
    this.model.neighborhood_id = [];
    if (this.formOptions) {
      this.formOptions.boroughs.forEach(borough => {

        borough.neighborhoods.forEach((neighborhood) => {
          if (neighborhood.selected) {
            this.model.neighborhood_id.push(neighborhood.neighborhood_id);
          }
        });
      });
    }
  }
}
