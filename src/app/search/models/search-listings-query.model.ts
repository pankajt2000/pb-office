export class SearchListingsQueryModel {

  limit: number = 12;

  offset: number = 0;

  sort_by: string = 'time_published';

  sort_dir: 'asc' | 'desc' = 'desc';

  ad_type: string;

  listing_ids: string = '';

}
