export class SearchQueryModel {

  limit: number = 12;

  offset: number = 0;

  sort_by: string = 'time_published';

  sort_dir: 'asc' | 'desc' = 'desc';

  ad_type: string;

  ratings: number | '' | null;

  min_price: number;

  max_price: number;

  has_photos: number;

  borough: string;

  neighborhood: string[] = [];

  neighborhood_id: number[];

  building_type: string[];

  amenities: string[];

  ownership: string[];

  furnished: string;

  other: string[];

  pets: string;

  bathrooms: string;

  bedrooms: string;

  search: string = '';

  listing_ids: string = '';

  company_name: string[];

  has_concession: 0 | 1;

  advertising_mode: 0 | 1;

  fee_structure: string;

  exposures: string[];

  annotation_mode: 0 | 1 = 0;

  pbcv_status: string[];

  filter_tags: string[] = [];

}
