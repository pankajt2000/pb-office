import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { SearchResultItemComponent } from '../search-result-item/search-result-item.component';

@Component({
  selector: 'app-search-result-item-list',
  templateUrl: './search-result-item-list.component.html',
  styleUrls: ['./search-result-item-list.component.less']
})
export class SearchResultItemListComponent extends SearchResultItemComponent {


  ngOnInit() {
  }

}
