import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchResultItemListComponent } from './search-result-item-list.component';

describe('SearchResultItemListComponent', () => {
  let component: SearchResultItemListComponent;
  let fixture: ComponentFixture<SearchResultItemListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchResultItemListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
