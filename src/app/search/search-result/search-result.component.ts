import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SearchQueryModel } from '../models/search-query.model';
import { ListingModel } from '../../listings/models/listing.model';
import { ClientModel } from '../../client/models/client.model';
import { QuickviewService } from '../../layout/quickview/quickview.service';
import { ClientService } from '../../client/services/client.service';
import { SearchOptionsModel } from '../models/search-options.model';
import { SearchService } from '../services/search.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.less']
})
export class SearchResultComponent implements OnInit {


  @Input('form-data') set form_data(value: SearchQueryModel) {
    this._form_data = value;

    this.page = (+value.offset + +value.limit) / value.limit;
  }

  get form_data(): SearchQueryModel {
    return this._form_data;
  }

  private _form_data: SearchQueryModel;

  @Input() public result: ListingModel[];
  @Input() public client: ClientModel;
  @Input('search-label') public search_label = 'Search <strong>Results</strong>';
  @Input('sort-result') public sort_result = true;
  @Input('edit-search') public edit_search = true;
  @Input('form-options') public form_options: SearchOptionsModel;
  @Input('result-count') public total: number;
  @Input('advice-result') public advice_result: { count: number, title: string };
  @Input('search-id') public search_id: string = null;
  @Output('show-search-form') show_search_form: EventEmitter<boolean> = new EventEmitter();
  @Output('search') public search: EventEmitter<boolean> = new EventEmitter();
  @Output('search-tag') public search_tag: EventEmitter<boolean> = new EventEmitter();
  @Input() public gridlist: string;
  @Output('gridlistChange') viewChange: EventEmitter<string> = new EventEmitter<string>();

  @Input('tags-available') set tags_available(tags_available) {
    this.is_tags_available = parseInt(tags_available);
    this.tags_details = null;
    if (this.is_tags_available === 1) {
      this.form_options.tag_cloud_definition.forEach(tag => {
        tag.selected = false;
        this.form_data.filter_tags.forEach(tag_type => {
          if (tag.type === tag_type) {
            tag.selected = true;
          }
        });
      });

      if (this.search_id) {
        this.searchService.getTagsStatById(this.search_id).subscribe(data => {
          this.tags_details = data;
        });
      } else {
        const search_criteria = Object.assign({}, this.form_data, {
          filter_tags: this.form_data.filter_tags ? this.form_data.filter_tags.filter(t => t.length).join(',') : '',
          company_name: this.form_data.company_name.length ? ('"' + this.form_data.company_name.join('","') + '"') : '',
          pbcv_status: this.form_data.pbcv_status && this.form_data.pbcv_status.length ? this.form_data.pbcv_status.join(',') : '',
        });
        this.searchService.getTagsStat(search_criteria).subscribe(data => {
          this.tags_details = data;
        });
      }
    }
  }

  page: number;
  is_tags_available: number;
  tags_details: any = null;
  sort_list: { field: string, dir: 'asc' | 'desc', name: string }[];
  sort_data: { field: string, dir: 'asc' | 'desc', name: string };

  constructor(private quickviewService: QuickviewService, private clisentService: ClientService, private searchService: SearchService) {
  }

  ngOnInit() {

    if (this.isAdvertisementMode()) {
      this.search_label += '&nbsp;&mdash;&nbsp;Advertising Mode';
    }

    this.sort_list = [];
    this.form_options.sort_options.forEach(sort_option => {
      this.sort_list.push({ field: sort_option.value, dir: 'asc', name: sort_option.label + ' ASC' });
      this.sort_list.push({ field: sort_option.value, dir: 'desc', name: sort_option.label + ' DESC' });

      if (sort_option.value === this.form_data.sort_by) {
        const index = this.form_data.sort_dir === 'desc' ? this.sort_list.length - 1 : this.sort_list.length - 2;
        this.sort_data = this.sort_list[index];
      }
    });
  }

  showSearchForm() {
    if (this.client) {
      this.clisentService.setCurrentClient(this.client);
      this.quickviewService.setOpened(true);
      this.quickviewService.setState('saved_searches');
    } else {
      this.page = 1;
      this.form_data.offset = 0;
      this.show_search_form.emit(true);
    }
  }

  sortResult($event) {
    this.page = 1;
    this.form_data.offset = 0;
    this.form_data.sort_by = this.sort_data.field;
    this.form_data.sort_dir = this.sort_data.dir;
    this.search.emit();
  }

  searchText(e) {
    this.page = 1;
    this.form_data.offset = 0;
    this.form_data.search = e.target.value;
    this.search.emit();
  }

  pageChanged(event) {
    const offset = event.page * this.form_data.limit - this.form_data.limit;
    if (offset !== this.form_data.offset) {
      this.form_data.offset = offset;
      this.search.emit();
    }
  }

  isAdvertisementMode() {
    return this.form_data.advertising_mode === 1;
  }

  changeView(view: string) {
    this.viewChange.emit(view);
  }

  needPagination() {
    return +this.total > this.form_data.limit;
  }

  setTag(tag: { title: string, type: string, selected: boolean }) {
    tag.selected = !tag.selected;
    this.page = 1;
    this.search_tag.emit();
  }
}
