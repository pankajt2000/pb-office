import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { Component, Input, OnInit, ElementRef } from '@angular/core';
import { ListingModel } from '../../../listings/models/listing.model';
import { BsModalService } from 'ngx-bootstrap';
import { SavedListingService } from '../../../listings/services/saved-listing.service';
import { ClientModel } from '../../../client/models/client.model';
import { ClientService } from '../../../client/services/client.service';
import { ListingService } from '../../../listings/services/listing.service';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-search-result-item',
  templateUrl: './search-result-item.component.html',
  styleUrls: ['./search-result-item.component.less']
})
export class SearchResultItemComponent implements OnInit {

  @Input('item') item: ListingModel;
  @Input('adMode') advertisingMode?: 1 | 0;
  @Input('colspan') colspan?: number;
  @Input() annotationMode?: 1 | 0;

  client: ClientModel;

  constructor(private el: ElementRef,
              private modalService: BsModalService,
              private listingService: ListingService,
              private alertService: BootstrapAlertService,
              public savedListingService: SavedListingService,
              private clientService: ClientService,
              protected router: Router) {
    this.clientService.getCurrentClient().subscribe(client => this.client = client);
    this.colspan = this.colspan || 4;
    this.advertisingMode = this.advertisingMode || 0;
  }

  ngOnInit() {
    $(this.el.nativeElement).addClass('col-md-' + this.colspan);
  }

  advertise() {
    this.listingService.advertise(this.item.advertisement_id).subscribe(() => {
      this.item.advertised = 1;
    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError(`Couldn't find listing.`);
      } else {
        this.alertService.showError(errorData.error.errorMsg);
      }

    });
  }

  unadvertise() {
    this.listingService.removeAdvertise(this.item.advertisement_id).subscribe(() => {
      this.item.advertised = 0;
    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError(`Couldn't find listing`);
      } else {
        this.alertService.showError(errorData.error.errorMsg);
      }
    });
  }

}
