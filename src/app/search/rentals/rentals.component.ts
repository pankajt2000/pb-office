import { ChangeDetectorRef, Component, EventEmitter, HostListener, OnDestroy, OnInit, AfterViewInit } from '@angular/core';
import { SearchService } from '../services/search.service';
import { SearchQueryModel } from '../models/search-query.model';
import { ListingModel } from '../../listings/models/listing.model';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ClientService } from '../../client/services/client.service';
import { ClientModel } from '../../client/models/client.model';
import { BaseSearchComponent } from '../base-search.component';
import { SearchOptionsModel } from '../models/search-options.model';
import { isEqual } from 'lodash';
import { CurrencyMaskConfig } from 'ngx-currency/src/currency-mask.config';
import { debounceTime, distinctUntilChanged, switchMap, tap, filter } from 'rxjs/operators';
import { SearchListingsQueryModel } from '../models/search-listings-query.model';

declare var $: any;

@Component({
  selector: 'app-rentals',
  templateUrl: './rentals.component.html',
  styleUrls: ['./rentals.component.less']
})
export class RentalsComponent extends BaseSearchComponent implements OnInit, OnDestroy, AfterViewInit {

  scrollEvent: EventEmitter<any> = new EventEmitter<any>();
  changeFormEvent: EventEmitter<any> = new EventEmitter<any>();
  ad_type: string = this.route.snapshot.data.ad_type;
  model: SearchQueryModel = new SearchQueryModel();
  show_search_form = true;
  listing_mode = true;
  searching = false;
  search_result: ListingModel[] = [];
  search_result_count: number;
  tags_available: number;
  client: ClientModel;
  formOptions: SearchOptionsModel;
  scrolling = false;
  modelDiff = null;
  advice_result: { count: number, title: string };
  highlight: boolean;
  search_link: string;
  loaded = false;
  view = 'grid';
  clientSubscription: any;
  priceMaskConfig: CurrencyMaskConfig = {
    align: 'left',
    allowNegative: false,
    allowZero: true,
    decimal: ',',
    precision: 0,
    prefix: '$ ',
    suffix: '',
    thousands: '.',
    nullable: true
  };

  pbcv_statuses = [
    { label: 'No CV data', value: 'NA', selected: false },
    { label: 'Automatic', value: 'AUTO', selected: true },
    { label: 'Partial', value: 'PARTIAL', selected: false },
    { label: 'Manual', value: 'MANUAL', selected: false },
  ];

  constructor(protected searchService: SearchService,
              protected route: ActivatedRoute,
              protected router: Router,
              protected clientService: ClientService,
              protected cd: ChangeDetectorRef) {
    super();

    this.initModel();

    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
    ).subscribe(x => {
      if (!Object.keys(this.route.snapshot.queryParams).length) {
        this.resetFilter();
      }
    });


    this.changeFormEvent.debounceTime(100).subscribe(() => {

      this.prepareFilters();
      let is_equal = true;
      ['ad_type', 'borough', 'neighborhood_id', 'bedrooms', 'bathrooms', 'pets', 'min_price', 'max_price', 'building_type', 'ownership',
        'amenities', 'other', 'furnished', 'ratings', 'has_photos', 'company_name', 'has_concession', 'advertising_mode',
        'fee_structure', 'exposures', 'annotation_mode', 'pbcv_status', 'listing_ids', 'filter_tags']
        .forEach((filed) => {
          if (!isEqual(this.modelDiff[filed], this.model[filed])) {
            is_equal = false;
          }
        });

      if (!is_equal) {
        this.prepareFilters();
        this.updateAdvice();
        this.modelDiff = (JSON.parse(JSON.stringify(this.model)));
      } else if (!this.advice_result) {
        this.updateAdvice();
      }
      this.changeUrl(true);
    });
  }

  initModel(more_data = {}) {
    this.model = Object.assign(this.model, {
      ad_type: this.ad_type,
      ratings: '',
      min_price: null,
      max_price: null,
      has_photos: 0,
      borough: '',
      neighborhood_id: [],
      building_type: [],
      amenities: [],
      ownership: [],
      other: [],
      furnished: 'Unfurnished',
      pets: '',
      bathrooms: '',
      bedrooms: '',
      search: '',
      listing_ids: '',
      fee_structure: 'All',
      company_name: '',
      has_concession: '',
      advertising_mode: '',
      exposures: [],
      annotation_mode: 0,
      pbcv_status: [],
      filter_tags: [],
    }, more_data);
  }

  ngAfterViewInit() {
    this.searchService.getFormOptions().subscribe((data) => Promise.resolve(null).then(() => {
      data.boroughs = data.boroughs.filter(borough => borough.neighborhoods.length);
      data.tag_cloud_definition.map(tag => tag.selected = false);
      this.formOptions = this.resortProperies(data);
      this.initForm();

      this.clientSubscription = this.clientService.getCurrentClient().subscribe((client) => {
        this.client = client;

        if (!this.show_search_form) {
          this.updateListingsInfo(this.search_result);
        }
      });


      this.route.queryParams.distinctUntilChanged().subscribe((qParams) => {
        const params = Object.assign({}, qParams);
        this.modelDiff = (JSON.parse(JSON.stringify(this.model)));

        if (Object.keys(params).length) {
          params.has_photos = params.has_photos && !isNaN(params.has_photos) ? parseInt(params.has_photos) : null;
          params.annotation_mode = params.annotation_mode && !isNaN(params.annotation_mode) ? parseInt(params.annotation_mode) : null;
          params.advertising_mode = params.advertising_mode && !isNaN(params.advertising_mode) ? parseInt(params.advertising_mode) : null;
          params.pbcv_status = typeof params.pbcv_status === 'string' ? params.pbcv_status.split(',') : params.pbcv_status;
          params.filter_tags = typeof params.filter_tags === 'string' ? params.filter_tags.split(',') : (params.filter_tags || []);

          this.show_search_form = params.show_form === 'true';
          this.listing_mode = params.listing_mode === 'true';

          params.neighborhood_id = typeof params.neighborhood_id === 'string' ? [params.neighborhood_id] : params.neighborhood_id;
          this.formOptions.boroughs.forEach((borough) => {
            borough.neighborhoods.forEach((neighborhood) => {
              neighborhood.selected = params.neighborhood_id && params.neighborhood_id.indexOf(neighborhood.neighborhood_id) > -1;
            });
          });

          this.formOptions.tag_cloud_definition.forEach(tag => tag.selected = params.filter_tags.indexOf(tag.type) > -1);

          this.pbcv_statuses.forEach(status => {
            status.selected = params.pbcv_status && params.pbcv_status.indexOf(status.value) > -1;
          });

          if (params.company_name) {
            if (typeof params.company_name === 'string' && params.company_name.length) {
              /*params.company_name = params.company_name.slice(1, -1).split('","');*/
              params.company_name = params.company_name.split(",").map(item => item.trim().replace(/"/g, ""));
            }
            if (params.company_name.length > 0) {
              this.search_companies = params.company_name.map(c => {
                return { name: c, value: c };
              });
            }
          }

          this.model = Object.assign(this.model, params);

          if (!this.show_search_form && !this.search_result.length) {
            this.search();
          }
        } else {
          this.listing_mode = false;
          this.show_search_form = true;
          this.initModel();
          this.initForm();

          this.scrolling = true;
          $('html, body').animate({ scrollTop: 0 }, 200, () => {
            this.scrolling = false;
          });

        }

        this.changeForm();

        this.loaded = true;
      });
    }));
  }

  ngOnInit() {
    this.scrollEvent.debounceTime(50).distinctUntilChanged().subscribe(($event) => {

      if (this.scrolling || !this.loaded) {
        return;
      }

      this.prepareFilters();

      this.router.navigate(['/search/' + this.model.ad_type], {
        queryParams: Object.assign(this.model, {
          show_form: this.show_search_form,
          listing_mode: this.listing_mode
        }),
        replaceUrl: true
      });
    });

    this.initSearchCompanies();

    this.searchService.triggerSearch.subscribe(() => {
      this.search();
    });

  }

  initSearchCompanies() {
    this.companiesToTypeahead.pipe(
      tap(() => this.companies_to_loading = true),
      distinctUntilChanged(),
      debounceTime(200),
      switchMap(term => this.searchService.getCompanies(term)),
    ).subscribe(x => {
      this.search_companies = x.map(c => {
        return { name: c, value: c };
      });
      this.companies_to_loading = false;
      this.cd.markForCheck();
    }, () => {
      this.search_companies = [];
    });
  }

  initForm() {
    if (this.formOptions) {
      this.formOptions.boroughs.forEach((borough) => {
        borough.neighborhoods.forEach((neighborhood) => {
          neighborhood.selected = false;
        });
      });
      this.formOptions.tag_cloud_definition.map(tag => tag.selected = false);
    }
  }

  @HostListener('window:scroll', ['$event']) onScrollEvent($event) {

    this.scrollEvent.emit($event);
  }

  showSearchForm(e) {
    this.show_search_form = true;

    this.changeUrl();
  }

  private _beforeSearch() {
    this.show_search_form = false;

    this.tags_available = 0;

    this.changeUrl();
  }

  changeUrl(replace_url: boolean = false) {
    if (!this.loaded) {
      return;
    }

    this.prepareFilters();
    const filter_query = Object.assign(this.model, { show_form: this.show_search_form, listing_mode: this.listing_mode });

    this.search_link = this.router.createUrlTree(['/search/' + this.model.ad_type], { queryParams: filter_query }).toString();
    this.router.navigate(['/search/' + this.model.ad_type], {
      queryParams: filter_query,
      replaceUrl: replace_url
    });
  }

  enterEvent(event) {
    if ($(event.srcElement).attr('role') !== 'combobox') {
      this.searchTop();
    }
  }

  searchTop(event?) {
    this.scrolling = true;
    $('html, body').animate({ scrollTop: 0 }, 0, () => {
      this.scrolling = false;
    });

    return this.search(event);
  }

  search(event?) {

    if (event && event.ctrlKey) {
      window.open(event.toElement.href);
      return;
    }

    if (this.searching) {
      return;
    }

    this.searching = true;

    this._beforeSearch();

    const model = this.listing_mode
      ? Object.assign(new SearchListingsQueryModel(), {
        sort_by: this.model.sort_by,
        sort_dir: this.model.sort_dir,
        ad_type: this.model.ad_type,
        listing_ids: this.model.listing_ids,
        offset: this.model.offset,
        limit: this.model.limit
      }) : this.model;

    let search_criteria = Object.assign({}, model);

    if (!this.listing_mode) {
      search_criteria = Object.assign(search_criteria, {
        company_name: this.model.company_name.length ? ('"' + this.model.company_name.join('","') + '"') : '',
        pbcv_status: this.model.pbcv_status && this.model.pbcv_status.length ? this.model.pbcv_status.join(',') : '',
      });
    }
    search_criteria = Object.assign(search_criteria, {
      filter_tags: this.model.filter_tags ? this.model.filter_tags.filter(t => t.length).join(',') : '',
    });

    this.searchService.search(search_criteria).subscribe((data) => {
      this.tags_available = data[2];
      this.search_result = data[0];

      this.updateListingsInfo(data[0]);

      this.search_result_count = data[1];
      this.searching = false;
    }, () => {

      this.searching = false;
    });

    return false;
  }

  updateListingsInfo(data) {
    const listing_ids = data.map(item => item.advertisement_id);
    if (this.client && listing_ids.length) {
      this.clientService.getListingsInfo(this.client, listing_ids).subscribe((info: any[]) => {
        info.forEach((value) => {
          this.search_result.forEach(listing => {
            if (listing.advertisement_id === value.advertisement_id) {
              Object.assign(listing, value);
            }
          });
        });
      });
    }
  }

  changeForm() {
    this.changeFormEvent.emit();
  }

  searchTag() {
    this.model.offset = 0;
    this.changeUrl(true);
    this.search();
  }

  updateAdvice() {
    const model = this.listing_mode
      ? Object.assign({
        sort_by: this.model.sort_by,
        sort_dir: this.model.sort_dir,
        ad_type: this.model.ad_type,
        listing_ids: this.model.listing_ids
      }) : this.model;

    const search_criteria = Object.assign({}, model, this.listing_mode ? {} : {
      company_name: this.model.company_name.length ? ('"' + this.model.company_name.join('","') + '"') : '',
      pbcv_status: this.model.pbcv_status.join(','),
      filter_tags: this.model.filter_tags ? this.model.filter_tags.filter(t => t.length).join(',') : '',
    });
    this.searchService.getSearchAdvice(search_criteria).subscribe(data => {
      this.advice_result = data;
      // this.search_result_count = this.advice_result.count;
      this.highlightResoult();
    });
  }

  highlightResoult() {
    this.highlight = true;
    setTimeout(() => this.highlight = false, 500);
  }

  resetFilter() {

    /** @todo find a possibility to uncheck without using query */
    $('[name="neighborhoods"]').iCheck('uncheck');

    this.initModel();
    this.initForm();
    this.changeForm();

    this.changeUrl(true);

    this.updateAdvice();
  }

  ngOnDestroy() {
    if (this.clientSubscription) {
      this.clientSubscription.unsubscribe();
    }
  }

  prepareFilters() {
    this.preparePBCVStatus();
    this.prepareNeighborhoods();
    this.prepareTagsCloud();
  }

  preparePBCVStatus() {
    this.model.pbcv_status = [];
    this.pbcv_statuses.forEach(status => {
      if (status.selected) {
        this.model.pbcv_status = [status.value];
      }
    });
  }

  prepareTagsCloud() {
    this.model.filter_tags = [];
    this.formOptions.tag_cloud_definition.forEach((tag) => {
      if (tag.selected) {
        this.model.filter_tags.push(tag.type);
      }
    });
  }

  setAnotationMode() {
    this.model.annotation_mode = 1;
    this.model.advertising_mode = 0;
    if (this.pbcv_statuses.length === 0) {
      this.pbcv_statuses.find(status => status.value === 'AUTO').selected = true;
    }
  }

  setAdvertisingMode() {
    this.model.advertising_mode = 1;
    this.model.annotation_mode = 0;
  }
}
