import { Component } from '@angular/core';
import { AdvertisedListComponent } from '../../listings/advertised-list/advertised-list.component';

@Component({
  selector: 'app-created-by-me',
  templateUrl: './created-by-me.component.html',
  styleUrls: ['./created-by-me.component.less']
})
export class CreatedByMeComponent extends AdvertisedListComponent {

  search(is_load_more: boolean = false) {
    this.searching = true;

    this.searchService.getCreatedByMe(this.offset, this.limit, this.sort_by, this.sort_dir)
      .subscribe(results => {

        this.searching = false;
        this.search_result_count = results[1];
        this.result = is_load_more ? this.result.concat(results[0]) : results[0];

        this.updateListingsInfo(results[0]);

      }, (response) => {

      });
  }


}
