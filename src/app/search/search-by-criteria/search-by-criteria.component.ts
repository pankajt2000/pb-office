import { Component, OnInit } from '@angular/core';
import { SearchQueryModel } from '../models/search-query.model';
import { SearchService } from '../services/search.service';
import { ActivatedRoute } from '@angular/router';
import { ClientService } from '../../client/services/client.service';
import { ClientModel } from '../../client/models/client.model';
import { SearchOptionsModel } from '../models/search-options.model';

@Component({
  selector: 'app-search-by-criteria',
  templateUrl: './search-by-criteria.component.html',
  styleUrls: ['./search-by-criteria.component.less']
})
export class SearchByCriteriaComponent implements OnInit {

  searching = false;
  search_result_count: 0;
  search_result = [];
  model: SearchQueryModel = Object.assign(new SearchQueryModel(), {
    ad_type: 'rental',
    min_price: 0,
    max_price: 0,
    has_photos: 0,
    borough: '',
    neighborhood: [],
    building_type: [],
    amenities: [],
    ownership: [],
    other: [],
  });
  search_id: string;
  client: ClientModel;
  form_options: SearchOptionsModel;
  advice_result: { count: number, title: string };
  view = 'grid';
  tags_available = 0;

  constructor(protected searchService: SearchService, protected route: ActivatedRoute, protected clientService: ClientService) {
    this.searchService.getFormOptions().subscribe(data => {
      this.form_options = data;
      this.form_options.tag_cloud_definition.map(tag => tag.selected = false);
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {

      if (this.form_options) {
        this.form_options.tag_cloud_definition.map(tag => tag.selected = false);
      }

      this.search_id = params['search_id'];

      this.clientService.getSuggest().subscribe(clients => {

        clients.forEach(client => {
          if (client.searches) {
            client.searches.forEach(search_criteria => {
              if (this.search_id === search_criteria.search_id) {
                this.client = client;
                this.model = Object.assign(this.model, search_criteria.search);

                this.advice_result = { title: search_criteria.title, count: search_criteria.results_count };
              }
            });
          }
        });


      });

      if (this.search_id) {
        this.search();
      }

      window.scroll(0, 0);
    });
  }

  search(is_load_more: boolean = false) {
    this.searching = true;
    this.tags_available = 0;

    let tags = '';
    if (this.form_options) {
      tags = this.form_options.tag_cloud_definition.filter(tag => tag.selected).map(tag => tag.type).join(',');
    }

    if (is_load_more === false) {
      this.model.offset = 0;
    }

    this.searchService.searchById(this.search_id, this.model.offset, this.model.limit, this.model.sort_by, this.model.sort_dir, tags)
      .subscribe(results => {

        this.searching = false;
        this.search_result_count = results[1];
        this.search_result = is_load_more ? this.search_result.concat(results[0]) : results[0];
        this.tags_available = results[2];

        this.updateListingsInfo(results[0]);

      }, (response) => {

        console.log(response);
      });
  }

  updateListingsInfo(data) {
    const listing_ids = data.map(item => item.advertisement_id);
    if (this.client && listing_ids.length) {
      this.clientService.getListingsInfo(this.client, listing_ids).subscribe((info: any[]) => {
        info.forEach((value) => {
          this.search_result.forEach(listing => {
            if (listing.advertisement_id === value.advertisement_id) {
              Object.assign(listing, value);
            }
          });
        });
      });
    }
  }

}
