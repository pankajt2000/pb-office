import { AgentApiService } from '../../services/api/agent-api.service';
import { EventEmitter, Injectable } from '@angular/core';
import { SearchOptionsModel } from '../models/search-options.model';
import { SearchQueryModel } from '../models/search-query.model';
import { SearchListingsQueryModel } from '../models/search-listings-query.model';
import { ListingModel } from '../../listings/models/listing.model';
import { BuildingDetailsModel } from '../../building/models/building-details.model';
import { Observable } from 'rxjs/Observable';

declare var $: any;

@Injectable()
export class SearchService {

  public triggerSearch: EventEmitter<any> = new EventEmitter();

  private form_options: SearchOptionsModel;


  constructor(private api: AgentApiService) {
  }

  getFormOptions(): Observable<SearchOptionsModel> {
    return new Observable(observer => {
      if (this.form_options) {
        observer.next(this.form_options);
        observer.complete();
      } else {
        this.api.get(`search-form-options`).map(data => new SearchOptionsModel().deserialize(data))
          .subscribe(data => {
            this.form_options = data;
            observer.next(this.form_options);
            observer.complete();
          });
      }
    });
  }

  search(data: SearchQueryModel | SearchListingsQueryModel) {
    return this.api.get('search-listings?' + $.param(data), { observe: 'response' }).map(response => {
      return [
        response.body.map((item: ListingModel) => new ListingModel().deserialize(item)),
        response.headers.get('Adm-Result-Total-Count'),
        response.headers.get('Adm-Tag-Cloud-Available')
      ];
    });
  }

  getTagsStat(data: SearchQueryModel | SearchListingsQueryModel) {
    return this.api.get('search-listings-tag-cloud?' + $.param(data));
  }

  getTagsStatById(searchId: string) {
    return this.api.get(`clients-saved-search-tag-cloud/${searchId}`);
  }

  getBuilding(buildingId: number | string) {
    return this.api.get(`search-buildings/${buildingId}`).map(data => new BuildingDetailsModel().deserialize(data));
  }

  getSavedSearches(client_id: number) {
    return this.api.get(`clients-saved-searches/${client_id}`).map(data => {
      return data.map(search => {
        search.search = Object.assign(new SearchQueryModel(), search.search);
        return search;
      });
    });
  }

  getClientsNewListings(client_id: number, offset: number = 0, limit: number = 10, sort_by: string = 'price', sort_dir: string = 'desc') {
    return this.api.get(`clients-new-listings/${client_id}?days=&limit=${limit}&offset=${offset}&sort_by=${sort_by}&sort_dir=${sort_dir}`, { observe: 'response' }).map(response => {
      return [
        response.body.map((item: ListingModel) => new ListingModel().deserialize(item)),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

  getSearchAdvice(data: SearchQueryModel | SearchListingsQueryModel) {
    // const params: any = Object.assign({}, data);
    // params.neighborhood = params.neighborhood.join(',');

    return this.api.get(`search-listings-advice?${$.param(data)}`).map(data => data);
  }

  createSavedSearche(client_id: number, data: object) {
    return this.api.post(`clients-saved-searches/${client_id}`, data).map(search => search);
  }

  updateSavedSearche(client_id: number, search_id: string, data: object) {
    return this.api.put(`clients-saved-searches/${client_id}/${search_id}`, data).map(search => search);
  }

  searchById(search_id: string, offset: number = 0, limit: number = 10, sort_by: string = 'price', sort_dir: string = 'desc', tags: string = '') {
    return this.api.get(`clients-saved-search/${search_id}?limit=${limit}&offset=${offset}&sort_by=${sort_by}&sort_dir=${sort_dir}&filter_tags=${tags}`
      , { observe: 'response' }).map(response => {
      return [
        response.body.map((item: ListingModel) => new ListingModel().deserialize(item)),
        response.headers.get('Adm-Result-Total-Count'),
        response.headers.get('Adm-Tag-Cloud-Available')
      ];
    });
  }

  searchAdvertised(offset: number = 0, limit: number = 10, sort_by: string = 'price', sort_dir: string = 'desc') {
    return this.api.get(`publish-ad?limit=${limit}&offset=${offset}&sort_by=${sort_by}&sort_dir=${sort_dir}`
      , { observe: 'response' }).map(response => {
      return [
        response.body.map((item: ListingModel) => new ListingModel().deserialize(item)),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

  getCreatedByMe(offset: number = 0, limit: number = 10, sort_by: string = 'price', sort_dir: string = 'desc') {
    return this.api.get(`search-created-by-me?limit=${limit}&offset=${offset}&sort_by=${sort_by}&sort_dir=${sort_dir}`
      , { observe: 'response' }).map(response => {
      return [
        response.body.map((item: ListingModel) => new ListingModel().deserialize(item)),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

  getCompanies(term: string) {
    return this.api.get(`suggest-company?name=${term}`).map(data => data);
  }

  removeSuspend(client_id, search_id) {
    return this.api.delete(`clients-saved-searches-suspend/${client_id}/${search_id}`);
  }

  suspendSearch(client_id, search_id, params: { reason: string, days: number } = { reason: null, days: 30 }) {
    return this.api.post(`clients-saved-searches-suspend/${client_id}/${search_id}`, params);
  }

}
