import { DeserializableInterface } from '../../shared/models/deserializable.interface';

export class BuildingAdviceModel implements DeserializableInterface<BuildingAdviceModel> {

  normalized_address: string;
  latitude: number;
  longitude: number;
  zip: string;
  borough: string;
  formatted_address: string;
  streets: string;
  neighborhood_id: string;


  deserialize(input: any): BuildingAdviceModel {
    Object.assign(this, input);
    return this;
  }

}
