import { ListingInterface } from './listing.interface';
import { environment } from '../../../environments/environment';
import { config } from '../../config';
import { BedroomAbstract } from '../../shared/models/bedroom.abstract';

export class ListingModel extends BedroomAbstract implements ListingInterface {

  active: 0 | 1;
  advertised: 0 | 1;
  nofee: 0 | 1;
  has_360: boolean;
  ad_type: string;
  advertisement_id: string;
  apartment_id: string;
  photos: string[] | any[] = [];
  address: string;
  price: number;
  net_effective_price: number;
  price_change: number;
  price_change_ts: number;
  streets: string;
  neighborhood: string;
  rooms: string;
  bedrooms: string;
  bathrooms: number;
  rating: number;
  size: number;
  furnished: number;
  access_type: string;
  access_type_info: string;
  building_type: string;
  ownership: string;
  pet_policy: string;
  unit: string;
  monthly: number;
  cc: number;
  has_360s: number;
  has_floorplan: number;
  is_on_market: number;
  application_pending: number;
  dom: string;
  fee_structure: string;
  available_from: string;
  ribbon: { text: string, color: string };
  featured_photo: string;
  photo_featured_2: string;
  photo_floorplan?: {
    path: string;
  };
  open_house_info: string;
  outdoor_space_info: string;
  concession: string;
  web_url: string;
  pbcv_status: string;
  pbcv_floorplan_sizes?: string;

  photos_count?: number;
  matching_clients_count: number;

  mouseenter: boolean;
  checked: boolean;

  sent: number = 0;
  viewed: number = 0;
  deleting?: boolean = false;

  pbcv_statuses = [
    { label: 'No CV data', value: 'NA', color: '#afafaf' },
    { label: 'Automatic', value: 'AUTO', color: '#52bfea' },
    { label: 'Partial', value: 'PARTIAL', color: '#f1a408' },
    { label: 'Manual', value: 'MANUAL', color: '#55ba5c' },
  ];

  getFirstPhotoUrl(size: string = config.media_sizes.wcomb): string {
    return (this.photos && this.photos.length > 0) ? this.getUrlByPath(this.photos[0], size) : this.getUrlByPath(this.featured_photo, size);
  }

  getUrlByPath(photo: string, size: string = config.media_sizes.wcomb): string {

    if (size === null) return environment.media_base_url + photo;

    if (!photo) return null;

    return environment.transcode_media_base_url + size + '/' + photo;
  }

  getPhotoUrls(size: string = null): string[] {
    const url = [];

    for (const photo of this.photos) {
      if (typeof photo === 'string') {
        url.push(this.getUrlByPath(photo, size));
      } else {
        url.push(this.getUrlByPath(photo.path, size));
      }
    }

    return url;
  }

  getSmallPhotoUrls(): string[] {
    return this.getPhotoUrls(config.media_sizes.wadmin);
  }

  getConcession() {
    return this.concession === 'N/A' ? '' : this.concession;
  }

  getAnotationLabel() {
    return this.pbcv_statuses.find(status => this.pbcv_status === status.value).label;
  }

  getAnotationColor() {
    return this.pbcv_statuses.find(status => this.pbcv_status === status.value).color;
  }

  deserialize(input: any): ListingModel {
    Object.assign(this, input);
    return this;
  }

}
