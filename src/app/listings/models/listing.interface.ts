import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { ListingModel } from './listing.model';

export interface ListingInterface extends DeserializableInterface<ListingModel> {


}
