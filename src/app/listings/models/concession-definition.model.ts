import { DeserializableInterface } from '../../shared/models/deserializable.interface';

export class ConcessionDefinitionModel implements DeserializableInterface<ConcessionDefinitionModel> {

  price: string | number;
  concession_definition: {
    op_months: number | string,
    op_amount: number | string,
    and_or: "and" | "or",
    free_months: number | string,
    free_on_lease: number | string
  };

  deserialize(input: any): ConcessionDefinitionModel {
    Object.assign(this, input);
    return this;
  }

}
