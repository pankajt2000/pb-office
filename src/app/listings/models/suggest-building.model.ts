import { DeserializableInterface } from '../../shared/models/deserializable.interface';

export class SuggestBuildingModel implements DeserializableInterface<SuggestBuildingModel> {

  building_id: string;
  address: string;
  borough: string;
  zip: string;
  city: string;
  state: string;
  neighborhood_id: string;
  time_created: string;
  source_id: string;
  ownership: string;
  company: string;
  block: string;
  lot: string;
  streets: string;
  latitude: string;
  longitude: string;
  year_built: string;
  description: string;
  company_phone: string;
  company_address: string;
  contact: string;
  contact_phone: string;
  contact_email: string;
  sale_requirements: string;
  qualifications: string;
  path: string;
  management_company_id: string;
  street_view_available: string;
  has_errors: string;
  unstable: string;
  unstable_time_updated: string;
  leasing_contact_id: string;
  floors: string;
  units_total: string;
  units_residental: string;
  building_age: string;
  neighborhood: string;
  address2: string;
  sales: string;
  rentals: string;
  building_type: string;
  pet_policy: string;

  deserialize(input: any): SuggestBuildingModel {
    Object.assign(this, input);
    return this;
  }

}
