import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { ListingModel } from './listing.model';
import { MessageModel } from '../../mailbox/models/message.model';

export class BuildingModel implements DeserializableInterface<BuildingModel> {

  building_id; string;
  address: string;
  address2: string;
  neighborhood: string;
  streets: string;
  latitude: number;
  longitude: number;
  ownership: string;
  street_view_available: number;
  building_info: string;
  amenities: string[];
  qualifications2: string[];
  qualifications: string[];
  sale_requirements: any;
  featured_photo: any;
  rentals: ListingModel[];
  sales: ListingModel[];


  deserialize(input: any): BuildingModel {
    input['rentals'] = input['rentals'].map((listing) => Object.assign(new ListingModel(), listing));
    input['sales'] = input['sales'].map((listing) => Object.assign(new ListingModel(), listing));
    Object.assign(this, input);
    return this;
  }

}
