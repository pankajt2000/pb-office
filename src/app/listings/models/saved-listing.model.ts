import { environment } from '../../../environments/environment';
import { config } from '../../config';
import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { BedroomAbstract } from '../../shared/models/bedroom.abstract';

export class SavedListingModel extends BedroomAbstract implements DeserializableInterface<SavedListingModel> {

  active: number;
  address: string;
  advertisement_id: string;
  application_pending: number;
  access_type_info: string;
  bathrooms: number;
  bedrooms: string;
  featured_photo: string;
  furnished: number;
  is_on_market: number;
  price: number;
  net_effective_price: number;
  ribbon: { color: string, text: string };
  rooms: string;
  sent: number;
  size: number;
  unit: string;
  viewed: number;
  web_url: string;
  ownership: string;
  checked = false;
  building_type: string;
  has_360: boolean;
  advertised: 0 | 1;

  hasPhoto() {
    return !!this.featured_photo;
  }

  getFirstPhotoUrl(size: string = config.media_sizes.wlist): string {
    return this.featured_photo ? this.getUrlByPath(this.featured_photo, size) : '';
  }

  getUrlByPath(photo: string, size: string = null): string {
    if (size === null) {
      return environment.media_base_url + photo;
    }
    return environment.transcode_media_base_url + size + '/' + photo;
  }

  deserialize(input: any): SavedListingModel {
    Object.assign(this, input);
    return this;
  }

}
