import { DeserializableInterface } from '../../shared/models/deserializable.interface';

export class OpenHouseModel implements DeserializableInterface<OpenHouseModel> {

  open_house_id: string;
  start_time: string;
  end_time: string;
  description: string;
  by_appointment_only: 0 | 1;
  type: string;

  date = '';
  time_from = '';
  time_to = '';
  appointment_only: 0 | 1 = 0;
  edit_state = false;
  editing = false;
  deleting = false;
  submitted = false;


  deserialize(input: any): OpenHouseModel {
    Object.assign(this, input);
    this.formatDate();
    return this;
  }

  convertTime12to24(time12h) {
    const [time, modifier] = time12h.split(' ');

    let [hours, minutes] = time.split(':');

    if (hours === '12') {
      hours = '00';
    }

    if (modifier === 'PM') {
      hours = parseInt(hours, 10) + 12;
    }

    return hours + ':' + minutes;
  }

  formatDate() {
    if (this.type === 'external') {
      return;
    }
    const start_date = new Date(this.start_time);
    this.date = start_date.toLocaleString('en-us', { month: 'long' }) + ' ' + start_date.getDay() + ' ' + start_date.getFullYear();
    this.time_from = start_date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }).toLowerCase();
    this.time_to = new Date(this.end_time).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }).toLowerCase();
    this.appointment_only = this.by_appointment_only;
  }

  prepareForRequest(advertisement_id: string) {
    let date = new Date(this.date);
    const userTimezoneOffset = date.getTimezoneOffset() * 60000;
    date = new Date(date.getTime() - userTimezoneOffset);
    const formatted_date = this.date ? date.toISOString().slice(0, 10) : new Date().toISOString().slice(0, 10);
    return {
      start_time: formatted_date + ' ' + this.convertTime12to24(this.time_from) + ':00',
      end_time: formatted_date + ' ' + this.convertTime12to24(this.time_to) + ':00',
      description: '',
      by_appointment_only: this.appointment_only,
      advertisement_id: advertisement_id
    };
  }

}
