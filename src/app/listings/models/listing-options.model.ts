import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { forEach } from '@angular/router/src/utils/collection';

export class ListingOptionsModel implements DeserializableInterface<ListingOptionsModel> {

  room_types: {
    room_type: string,
    editors: { title?: string, field: string, editor_type: string, options?: string[] }[]
  }[] = [];
  outdoor_space_options: string[] = [];
  bedrooms_options: string[] = [];
  bathrooms_options: string[] = [];
  view_options: string[] = [];
  condition_options: string[] = [];
  theta_options: {
    preset_id: string,
    name: string,
    camera_flows: { _filter: string, exposureCompensation: number, whiteBalance: string }[]
  }[] = [];
  ad_type_options: string[] = [];
  access_type_options: string[] = [];
  furnished_options: string[] = [];
  fee_structure_options: string[] = [];
  rls_condition_options: string[] = [];
  exposure_options: string[] = [];

  getRoomFields(room_type, only_types: string[] = [], except_types: string[] = [], except_fields: string[] = []) {
    let fields = this.room_types.find(room => room.room_type === room_type).editors;

    fields = this.sortFields(fields);

    if (only_types.length) fields = fields.filter(field => only_types.indexOf(field.editor_type) > -1);
    if (except_types.length) fields = fields.filter(field => except_types.indexOf(field.editor_type) === -1);
    if (except_fields.length) fields = fields.filter(field => except_fields.indexOf(field.field) === -1);

    return fields;
  }

  sortFields(fields) {
    const simple_fields = ['dropdown', 'text'];
    fields.sort((a: any, b: any) => {
      if (simple_fields.indexOf(a.editor_type) > -1 && simple_fields.indexOf(b.editor_type) === -1) {
        return 1;
      } else if (simple_fields.indexOf(a.editor_type) === -1 && simple_fields.indexOf(b.editor_type) > -1) {
        return -1;
      } else {
        return 0;
      }
    });
    return fields;
  }

  deserialize(input: any): ListingOptionsModel {
    Object.assign(this, input);
    return this;
  }

}
