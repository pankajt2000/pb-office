import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { BedroomAbstract } from '../../shared/models/bedroom.abstract';

export class ApartmentModel extends BedroomAbstract implements DeserializableInterface<ApartmentModel> {

  address: string;
  zip: string;
  ownership: string;
  unit: string;
  advertisement_id: string;
  price: number;
  ad_type: string;
  apartment_id: string;
  size: number;
  bathrooms: number;
  access_type: string;
  layout: string;
  bedrooms: string;
  building_id: string;
  furnished: string;

  getformatedSize() {
    return this.size > 0 ? this.size.toString().replace(/(\d+)(\d{3})/, '$1,$2') + ' sqft' : 'N/A';
  }

  deserialize(input: any): ApartmentModel {
    Object.assign(this, input);
    return this;
  }

}
