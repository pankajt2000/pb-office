import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { environment } from '../../../environments/environment';
import { config } from '../../config';
import { PhotoModel } from './photo.model';
import { BedroomAbstract } from '../../shared/models/bedroom.abstract';

export class ListingPreviewModel extends BedroomAbstract implements DeserializableInterface<ListingPreviewModel> {

  360: PhotoModel[];
  advertisement_id: string;
  advertised: 0 | 1;
  ad_type: string;
  description: string;
  access_type: string;
  furnished_type: string;
  price: number;
  published: number;
  apartment_id: string;
  unit: string;
  bedrooms: string;
  bathrooms: string;
  building_id: string;
  address: string;
  zip: string;
  city: string;
  state: string;
  address2: string;
  nofee: 0 | 1;
  concession_definition: {
    op_months: number | string,
    op_amount: number | string,
    and_or: 'and' | 'or',
    free_months: number,
    free_on_lease: number
  };
  fee_structure: string;
  commission_structure: number;
  common_charges: number;
  monthly_taxes: number;
  min_lease_period: number;
  max_lease_period: number;
  available_from: string;
  first_showing_date: string;
  co_exclusive: 0 | 1;
  include_in_rls: 0 | 1;
  exposures: string | string[];
  rooms_details:
    {
      preview_building_details_id: string,
      room_title: string,
      room_type: string,
      room_id: string,
      building_type: string,
      amenities: string[],
      pet_policy: string,
      notice: string | null,
      ratings: {
        Quality: number,
        Light: number,
        Layout: number,
        Size: number,
        Renovation: number,
        Value: number,
        Overall: number
      },
      features: string[],
      outdoor_spaces: string[]
      condition: any | null,
      direction: any | null,
      floor: any | null,
      exposures: any | null,
      exposure: any | null,
      kitchen_type: any | null,
      flooring: any | null,
      bed_size: any | null,
    }[];
  rooms_advice: {
    'room_id': string,
    'room_type': string,
    'title': string
  }[];
  photos: PhotoModel[];
  vt: any | null;

  getFieldValue(room_type: string, field: any, option?: string): string {
    const room = this.rooms_details.find(r => r.room_type === room_type);
    if (room && field.field in room && field.editor_type !== 'stars') {
      return room[field.field];
    } else if (room && field.editor_type === 'stars' && (field.field in room) && (option in room[field.field])) {
      return room[field.field][option];
    }
    return '';
  }

  getEnabledPhotos() {
    return this.photos.filter(p => p.properties.enabled);
  }

  getDisabledPhotos() {
    return this.photos.filter(p => !p.properties.enabled);
  }

  getPhotosUrl(): any[] {
    return this.photos.map(photo => {
      return { src: this.getPhotoUrl(photo, null) };
    });
  }

  get360PhotosUrl(): any[] {
    return this[360].map(photo => {
      return { src: this.getPhotoUrl(photo, null) };
    });
  }

  getPhotoUrl(photo, size: string = config.media_sizes.wadmin) {

    if (photo.upload_file && photo.upload_file.url) return photo.upload_file.url;

    if (size === null) {
      return environment.media_base_url + photo.path;
    }
    return environment.transcode_media_base_url + size + '/' + photo.path;
  }

  getRoomOptions() {
    return this.rooms_advice.filter((room) => room.room_id !== 'building_details');
  }

  deserialize(input: any): ListingPreviewModel {
    input.photos = input.photos || [];
    input.photos.map((photo) => new PhotoModel().deserialize(photo));
    Object.assign(this, input);
    return this;
  }

}
