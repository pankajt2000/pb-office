import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { environment } from '../../../environments/environment';
import { BedroomAbstract } from '../../shared/models/bedroom.abstract';
import { PhotoModel } from './photo.model';
import { ListingModel } from './listing.model';

export class ListingDetailsModel extends BedroomAbstract implements DeserializableInterface<ListingDetailsModel> {

  ad_type: string;
  advertisement_id: string;
  advertised: 0 | 1;
  nofee: 0 | 1;
  active: 1 | 0;
  available_from: string;
  price: number;
  net_effective_price: number;
  price_change: number;
  price_change_time: string;
  price_change_ts: number;
  price_history: { time: number, price: number }[];
  updated_on: string;
  dom: number;
  ribbon: { color: string, text: string };
  apartment_id: string;
  unit: string;
  size: number;
  bedrooms: string;
  bathrooms: string;
  apartment_amenities: string[];
  pet_policy: string;
  access_type: string;
  building_id: string;
  access_type_info: string;
  building_type: string;
  streets: string;
  address: string;
  address2: string;
  ownership: string;
  year_built: number;
  building_description: string;
  description: string;
  building_amenities: string[];
  latitude: number;
  longitude: number;
  street_view_available: number;
  sale_requirements: string;
  qualifications: string[];
  neighborhood: string;
  commission_structure: string;
  building_info: string;
  price_details_formatted: any[];
  photos: PhotoModel[];
  rating: number;
  preview_ratings: { Light: number, Layout: number, Size: number, Renovation: number, Value: number, Overall: number };
  360: any[];
  has_360: boolean;
  leasing_contact: { name: string, address: string, phone: string, email: string, fax: string, website: string };
  viewing_contacts: Array<{ name: string, address: string, phone_number: string, email: string, fax: string, website: string }>;
  management_company: { name: string, address: string, phone: string, email: string, fax: string, website: string };
  source: { name: string };
  listing_details: { label: string, value: string }[];
  open_house: any[];
  access_info: string;
  web_url: string;
  lease_term_info: string;
  concession: string;
  source_info: string;
  furnished_type: string;
  open_house_info: string;
  agents: {
    user_id: string,
    enabled: string,
    type: string,
    first_name: string,
    last_name: string,
    phone: string,
    email: string,
    client_type: string,
    time_created: string,
    last_activity: 0,
    name: string,
    photo: string
  }[];
  view_source_info: {
    source_type: string,
    parameters: any,
    data ?: any,
  };
  related_listings: ListingModel[];

  getPhoto(index, size?: string) {
    if (index in this.photos) {
      return this.getUrlByPath(this.photos[index].path, size);
    }
    return '';
  }

  getAllPhotos() {
    const data = [];

    this.photos.forEach((photo, index) => data.push({ src: this.getPhoto(index) }));

    return data;
  }

  getPhotoUrls(size: string = null): string[] {
    const url = [];

    for (const photo of this.photos) {
      if (typeof photo === 'string') {
        url.push(this.getUrlByPath(photo, size));
      } else {
        url.push(this.getUrlByPath(photo.path, size));
      }
    }

    return url;
  }

  getUrlByPath(photo: string, size: string = null): string {
    if (size === null) {
      return environment.media_base_url + photo;
    }
    return environment.transcode_media_base_url + size + '/' + photo;
  }

  getDomText() {
    return this.dom ? `${this.dom} day` + (this.dom > 1 ? 's' : '') + ' ago' : 'today';
  }

  getConcession() {
    return this.concession === 'N/A' ? '' : this.concession;
  }

  deserialize(input: any): ListingDetailsModel {
    input.photos = input.photos.map(photo => new PhotoModel().deserialize(photo));
    input.related_listings = input.related_listings.map(item => new ListingModel().deserialize(item));
    Object.assign(this, input);
    return this;
  }

}
