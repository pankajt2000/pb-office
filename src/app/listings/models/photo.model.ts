import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { UploadFile } from 'ngx-uploader/index';
import { config } from '../../config';
import { environment } from '../../../environments/environment';

export class PhotoModel implements DeserializableInterface<PhotoModel> {

  media_item_id: string;
  media_folder_id: string;
  external: any;
  type: string;
  content_type: string;
  size: string;
  properties: {
    pbcv_ignore: 0 | 1,
    new_pbcv_ignore: boolean,
    show_options: boolean,
    role: string,
    featured: boolean;
    room_id: string;
    enabled: boolean;
    index: number;
    pbcv_annotate: {
      time_updated?: number,
      type?: string,
      updated_by?: string,
      value?: string,
      new_value?: string,
      invalid?: boolean
    }[] | object;
    detect_objects: object;
  };
  name: string;
  path: string;
  source_url: string;
  created_by: string;
  time_created: string;
  updated_by: string;
  time_updated: string;
  title: string;
  loading: boolean;
  has_error: boolean;

  upload_file: UploadFile;

  not_available = '---';

  getUrl(size: string = config.media_sizes.wcomb): string {
    if (!this.path) {
      return null;
    }

    if (size === null) {
      return environment.media_base_url + this.path;
    }

    return environment.transcode_media_base_url + size + '/' + this.path;
  }

  initAnnotate(type) {
    if (!this.properties.pbcv_annotate) {
      this.properties.pbcv_annotate = {};
    }

    if (!this.properties.pbcv_annotate[type]) {
      this.properties.pbcv_annotate[type] = {};
    }
  }

  getAnnotateOriginValue(type) {
    if (this.properties.pbcv_annotate && this.properties.pbcv_annotate[type]) {
      return this.properties.pbcv_annotate[type].value;
    }

    return this.not_available;
  }

  getAnnotateValue(type: string): string {
    if (this.properties.pbcv_annotate && this.properties.pbcv_annotate[type]) {
      return this.properties.pbcv_annotate[type].new_value || this.properties.pbcv_annotate[type].value || this.not_available;
    }

    return this.not_available;
  }

  setAnnotateValue(type: string, value: string) {
    this.initAnnotate(type);

    this.properties.pbcv_annotate[type].new_value = value;
    this.properties.pbcv_annotate[type].invalid = false;
  }

  isAutoAnnotate(type) {
    if (!this.properties.pbcv_annotate || !this.properties.pbcv_annotate[type]
      || Object.keys(this.properties.pbcv_annotate[type]).length === 0) {
      return false;
    }

    return !this.properties.pbcv_annotate[type].new_value && this.properties.pbcv_annotate[type].updated_by === '0'
      && this.getAnnotateValue(type) !== this.not_available;
  }

  isInvalidAnnotation(type) {
    this.initAnnotate(type);
    return this.properties.pbcv_annotate[type] && this.properties.pbcv_annotate[type].invalid;
  }

  setInvalidAnnotation(type) {
    this.initAnnotate(type);

    this.properties.pbcv_annotate[type].invalid = true;
  }

  tooglePbcvIgnore() {
    if ('new_pbcv_ignore' in this.properties) {
      this.properties.new_pbcv_ignore = !this.properties.new_pbcv_ignore;
    } else {
      this.properties.new_pbcv_ignore = !this.properties.pbcv_ignore;
    }
  }

  isAnnotateIgnore() {
    if ('new_pbcv_ignore' in this.properties) {
      return this.properties.new_pbcv_ignore;
    }

    return this.properties.pbcv_ignore;
  }

  getDownloadLink() {
    return environment.media_base_url + this.path;
  }

  deserialize(input: any): PhotoModel {
    Object.assign(this, input);
    return this;
  }

}
