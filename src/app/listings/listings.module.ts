import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from '../services/api/api.service';
import { BsDatepickerModule, ModalModule, RatingModule, TabsModule } from 'ngx-bootstrap';
import { ListingService } from './services/listing.service';
import { ViewListingComponent } from './view-listing/view-listing.component';
import { environment } from '../../environments/environment';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { NewListingComponent } from './new-listing/new-listing.component';
import { ConcessionComponent } from './new-listing/concession/concession.component';
import { BuildingService } from './services/building.service';
import { ApartmentService } from './services/apartment.service';
import { NewBuildingComponent } from './new-building/new-building.component';
import { NewApartmentComponent } from './new-apartment/new-apartment.component';
import { EditListingComponent } from './edit-listing/edit-listing.component';
import { BrowserModule } from '@angular/platform-browser';
import { MediaManagementComponent } from './edit-listing/media-management/media-management.component';
import { ContextMenuModule } from 'ngx-contextmenu';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { NguiMapModule} from '@ngui/map';
import { AdvertisedListComponent } from './advertised-list/advertised-list.component';
import { RecentListingComponent } from './recent-listing/recent-listing.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { CalendarService } from './services/calendar.service';
import { AnnotateService } from './annotation/annotate.service';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ModalModule.forRoot(),
    SharedModule,
    RatingModule.forRoot(),
    TabsModule.forRoot(),
    BrowserModule,
    ContextMenuModule.forRoot(),
    NgxDnDModule,
    NgxCurrencyModule,
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=' + environment.gmap_api_key}),
    BsDatepickerModule.forRoot()
  ],
  providers:[
    ApiService,
    ListingService,
    AnnotateService,
    BuildingService,
    ApartmentService,
    CalendarService,
  ],
  declarations: [
    ViewListingComponent,
    NewListingComponent,
    ConcessionComponent,
    NewBuildingComponent,
    NewApartmentComponent,
    EditListingComponent,
    MediaManagementComponent,
    AdvertisedListComponent,
    RecentListingComponent,
  ],
  entryComponents: [
    NewBuildingComponent,
    NewApartmentComponent,
    MediaManagementComponent,
  ]
})
export class ListingsModule { }
