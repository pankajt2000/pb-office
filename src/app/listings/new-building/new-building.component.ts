import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BuildingService } from '../services/building.service';
import { BuildingAdviceModel } from '../models/building-advice.model';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { SuggestBuildingModel } from '../models/suggest-building.model';
import { ListingOptionsModel } from '../models/listing-options.model';
import { SearchService } from '../../search/services/search.service';

@Component({
  selector: 'app-new-building',
  templateUrl: './new-building.component.html',
  styleUrls: ['./new-building.component.less']
})
export class NewBuildingComponent implements OnInit {

  @Output() created = new EventEmitter();

  searchTerm = new EventEmitter<string>();
  options: ListingOptionsModel = new ListingOptionsModel();
  boroughs: {
    abbreviation: string;
    name: string;
    neighborhoods: {
      neighborhood_id: number;
      abbreviation: string;
      name: string;
      selected?: boolean;
    }[];
  }[] = [];
  buildingForm: FormGroup;
  submitted = false;
  advice_text: string;
  advising = false;
  creating = false;

  constructor(public bsModalRef: BsModalRef,
              private fb: FormBuilder,
              private alertService: BootstrapAlertService,
              private buildingService: BuildingService,
              private searchService: SearchService) {

    this.initNeighborhoods();
  }

  ngOnInit() {


    this.searchTerm
      .debounceTime(1000)
      .distinctUntilChanged()
      .subscribe((text: string) => {
        this.getAdvice(text).subscribe((address: BuildingAdviceModel) => {

          this.buildingForm.controls.latitude.patchValue(address.latitude);
          this.buildingForm.controls.longitude.patchValue(address.longitude);
          this.buildingForm.controls.zip.patchValue(address.zip);
          this.buildingForm.controls.streets.patchValue(address.streets);
          this.buildingForm.controls.neighborhood_id.patchValue(address.neighborhood_id);
          this.buildingForm.controls.address.patchValue(address.normalized_address);

          this.advising = false;
        }, response => {
          this.advising = false;
        });

      });


    if (this.advice_text) {
      this.searchTerm.next(this.advice_text);
    }

    this.buildingForm = this.fb.group({
      address: [null, Validators.required],
      ownership: [null, Validators.required],
      building_type: [null, Validators.required],
      pet_policy: [null],
      description: [null],
      latitude: [null, Validators.required],
      longitude: [null, Validators.required],
      neighborhood_id: [null, Validators.required],
      streets: [null, Validators.required],
      zip: [null, Validators.required],
    });

  }

  submitBuilding() {
    this.submitted = true;
    if (this.buildingForm.invalid) {
      return false;
    }

    this.creating = true;

    this.buildingService.createBuilding(this.buildingForm.value).subscribe((building: SuggestBuildingModel) => {
      this.creating = false;

      this.created.emit(building);

      this.alertService.showSucccess('Building successfully created');

      this.bsModalRef.hide();

    }, response => {

      this.alertService.showError('Error occurs while creating the building');

      this.creating = false;
    });

  }

  getAdvice(text) {
    this.advising = true;
    return this.buildingService.getAdvice(text);
  }

  initNeighborhoods() {
    this.searchService.getFormOptions().subscribe((data) => {
      this.boroughs = data.boroughs;
    });
  }
}
