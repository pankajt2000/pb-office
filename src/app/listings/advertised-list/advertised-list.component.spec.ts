import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertisedListComponent } from './advertised-list.component';

describe('AdvertisedListComponent', () => {
  let component: AdvertisedListComponent;
  let fixture: ComponentFixture<AdvertisedListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertisedListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertisedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
