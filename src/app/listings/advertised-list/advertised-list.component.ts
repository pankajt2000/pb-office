import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../client/services/client.service';
import { ClientModel } from '../../client/models/client.model';
import { ActivatedRoute } from '@angular/router';
import { SearchService } from '../../search/services/search.service';
import { SearchOptionsModel } from '../../search/models/search-options.model';

@Component({
  selector: 'app-advertised-list',
  templateUrl: './advertised-list.component.html',
  styleUrls: ['./advertised-list.component.less']
})
export class AdvertisedListComponent implements OnInit {

  searching = false;
  search_result_count: 0;
  result = [];
  client: ClientModel;
  form_options: SearchOptionsModel;
  sort_list: { field: string, dir: 'asc' | 'desc', name: string }[] = [];
  sort_data: { field: string, dir: 'asc' | 'desc', name: string };
  sort_by: string = 'time_published';
  sort_dir: string = 'desc';
  gridlist = 'grid';
  offset: number = 0;
  limit: number = 12;

  constructor(protected searchService: SearchService, private route: ActivatedRoute, private clientService: ClientService) {

    this.clientService.getCurrentClient().subscribe(client => this.client = client);

    this.searchService.getFormOptions().subscribe(data => {
      this.form_options = data;

      this.form_options.sort_options.forEach(sort_option => {
        this.sort_list.push({ field: sort_option.value, dir: 'asc', name: sort_option.label + ' ASC' });
        this.sort_list.push({ field: sort_option.value, dir: 'desc', name: sort_option.label + ' DESC' });

        if (sort_option.value === this.sort_by) {
          const index = this.sort_dir === 'desc' ? this.sort_list.length - 1 : this.sort_list.length - 2;
          this.sort_data = this.sort_list[index];
        }
      });
    });

    this.search();
  }

  sortResult($event) {
    this.sort_by = this.sort_data.field;
    this.sort_dir = this.sort_data.dir;
    this.search();
  }

  ngOnInit() {
  }

  search(is_load_more: boolean = false) {
    this.searching = true;

    this.searchService.searchAdvertised(this.offset, this.limit, this.sort_by, this.sort_dir)
      .subscribe(results => {

        this.searching = false;
        this.search_result_count = results[1];
        this.result = is_load_more ? this.result.concat(results[0]) : results[0];

        this.updateListingsInfo(results[0]);

      }, (response) => {

      });
  }


  loadMore(e) {
    this.offset = this.offset + this.limit;
    this.search(true);
  }

  updateListingsInfo(data) {
    const listing_ids = data.map(item => item.advertisement_id);
    if (this.client && listing_ids.length) {
      this.clientService.getListingsInfo(this.client, listing_ids).subscribe((info: any[]) => {
        info.forEach((value) => {
          this.result.forEach(listing => {
            if (listing.advertisement_id === value.advertisement_id) {
              Object.assign(listing, value);
            }
          });
        });
      });
    }
  }


}
