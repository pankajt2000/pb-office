import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { SavedListingService } from '../services/saved-listing.service';
import { SavedListingModel } from '../models/saved-listing.model';
import { ApplicationDetailsModel } from '../../application/models/application-details.model';

@Component({
  selector: 'app-send-modal',
  templateUrl: './send-modal.component.html',
  styleUrls: ['./send-modal.component.less']
})
export class SendModalComponent implements OnInit {

  listings: SavedListingModel[] = [];
  items: SavedListingModel[] = [];
  sentEvent = new EventEmitter();
  aformDetails: ApplicationDetailsModel;
  dosFormId: string;
  invoiceId: string;

  subject: string;
  email: string;
  message: string;

  constructor(public bsModalRef: BsModalRef, private savedListingService: SavedListingService) {

  }

  ngOnInit() {
    this.items = this.listings.concat([]);
  }

  afterSend() {
    this.bsModalRef.hide();
    this.sentEvent.emit();
  }

}
