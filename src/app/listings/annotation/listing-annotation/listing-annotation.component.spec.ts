import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingAnnotationComponent } from './listing-annotation.component';

describe('ListingAnnotationComponent', () => {
  let component: ListingAnnotationComponent;
  let fixture: ComponentFixture<ListingAnnotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingAnnotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingAnnotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
