import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { ListingModel } from '../../models/listing.model';
import { ListingDetailsModel } from '../../models/listing-details.model';
import { PhotoModel } from '../../models/photo.model';
import { chunk } from 'lodash';
import { AnnotateService } from '../annotate.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { AnnotateOptionModel } from '../annotate-option.model';
import { config } from '../../../config';
import { SearchService } from '../../../search/services/search.service';

declare var $: any;

@Component({
  selector: 'app-listing-annotation',
  templateUrl: './listing-annotation.component.html',
  styleUrls: ['./listing-annotation.component.less']
})
export class ListingAnnotationComponent implements OnInit {

  page = 1;
  total = 0;
  item: ListingModel | ListingDetailsModel;

  photos: PhotoModel[];
  list: PhotoModel[][] = [];
  annotate_options: AnnotateOptionModel[];

  image_sizes = config.media_sizes;

  saving = false;

  constructor(public bsModalRef: BsModalRef,
              private annotationService: AnnotateService,
              private alertService: BootstrapAlertService,
              private searchService: SearchService) {
  }

  ngOnInit() {
    this.annotationService.getGallery(this.item.advertisement_id).subscribe((photos) => {
      this.photos = photos;
      this.list = chunk(this.photos, 4);
      this.total = this.list.length;
    });
    this.annotationService.getAnotateOptions().subscribe(options => {
      const editors = ['select', 'select_sm', 'select_lg']
      this.annotate_options = options.filter(o => editors.includes(o.editor));
    });
  }

  nextStep() {
    if (this.validateCurrentPage()) {
      this.page = this.page + 1;
    }
  }

  showOptions(photo: PhotoModel, event) {
    if ($(event.target).hasClass('btn-ignore')) {
      photo.properties.show_options = false;
      return;
    }

    if ($(event.target).hasClass('listing-photo')) {
      photo.properties.show_options = ($(event.target).height() / 2) > (event.offsetY - 30);
    }

    if ($(event.target).hasClass('list-group') 
      || $(event.target).hasClass('btn') 
      || $(event.target).hasClass('select')
      || $(event.target).hasClass('btn-group')) {
      photo.properties.show_options = true;
    }
  }

  hideOptions(photo: PhotoModel, event) {
    if (!$(event.target).hasClass('listing-photo') 
      && !$(event.target).hasClass('list-group')) {
      photo.properties.show_options = false;
    }
  }

  validateCurrentPage() {
    let isValid = true;
    this.list[this.page - 1].forEach((photo: PhotoModel) => {
      console.log(this.annotate_options);
      this.annotate_options.forEach(option => {
        if (!option.isValid(photo)) {
          photo.setInvalidAnnotation(option.type);
          isValid = false;
        }
      });
    });

    return isValid;
  }

  save() {

    if (!this.validateCurrentPage()) {
      return;
    }

    this.saving = true;

    const postObject = {};

    this.photos.forEach((photo: PhotoModel) => {
      if ('new_pbcv_ignore' in photo.properties && photo.properties.new_pbcv_ignore !== !!photo.properties.pbcv_ignore) {
        if (photo.properties.new_pbcv_ignore) {
          this.annotationService.ignorePhoto(this.item.advertisement_id, photo.media_item_id).subscribe(() => {
          }, (error) => {
            this.alertService.showError('Cannot ignore photo #' + photo.media_item_id);
          });
        } else {
          this.annotationService.stopIgnorePhoto(this.item.advertisement_id, photo.media_item_id).subscribe(() => {
          }, (error) => {
            this.alertService.showError('Cannot unmark ignore photo #' + photo.media_item_id);
          });
        }
      }

      if (!photo.isAnnotateIgnore()) {
        postObject[photo.media_item_id] = {};
        for (const type in photo.properties.pbcv_annotate) {
          if (type === 'detect_objects') {
            continue;
          }
          postObject[photo.media_item_id][type] = photo.getAnnotateValue(type);
        }
      }
    });

    this.annotationService.save(this.item.advertisement_id, postObject).subscribe(() => {

      this.bsModalRef.hide();

      this.searchService.triggerSearch.emit();

      this.alertService.showSucccess('Annotations successfully saved');
    }, () => {

      this.searchService.triggerSearch.emit();

      this.alertService.showError('Cannot save annotation');

      this.bsModalRef.hide();
    });
  }
}
