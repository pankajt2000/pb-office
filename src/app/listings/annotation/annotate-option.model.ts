import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { PhotoModel } from '../models/photo.model';

export class AnnotateOptionModel implements DeserializableInterface<AnnotateOptionModel> {

  editor: string;
  only_if: {}[];
  options: string[];
  list_options: string[][];
  title: string;
  type: string;

  canShow(photo: PhotoModel) {
    if (this.only_if) {
      for (const only_if of this.only_if) {
        for (const type in only_if) {
          if (photo.getAnnotateValue(type) === only_if[type]) {
            return true;
          }
        }
      }
      return false;
    }
    return true;
  }

  isValid(photo: PhotoModel) {
    return photo.isAnnotateIgnore() || !this.canShow(photo) || this.options.indexOf(photo.getAnnotateValue(this.type)) > -1;
  }

  deserialize(input: any): AnnotateOptionModel {
    Object.assign(this, input);
    return this;
  }
}
