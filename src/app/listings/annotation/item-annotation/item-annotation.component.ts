import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { AnnotateOptionModel } from '../annotate-option.model';
import { PhotoModel } from '../../models/photo.model';
import { AnnotateService } from '../annotate.service';

declare var $: any;

@Component({
  selector: 'app-item-annotation',
  templateUrl: './item-annotation.component.html',
  styleUrls: ['./item-annotation.component.less']
})
export class ItemAnnotationComponent implements OnInit {

  annotate_options: AnnotateOptionModel[];
  @Input() photo: PhotoModel;
  @Input() saving: boolean;
  @Output() change = new EventEmitter();
  @Output() ignore = new EventEmitter();

  constructor(private annotationService: AnnotateService) {
  }

  ngOnInit() {
    this.annotationService.getAnotateOptions().subscribe(options => {
      options = options.filter(o => o.type !== 'detect_objects');
      options.map(o => {
        if (o.editor === 'select_lg' || o.type === 'classify_photos') {
          const half_items = Math.round(o.options.length / 2);
          o.list_options = [o.options.slice(0, half_items), o.options.slice(half_items)];
        } else {
          o.list_options = [o.options];
        }
      });
      this.annotate_options = options;
    });
  }

  showOptions(event) {
    if ($(event.target).hasClass('btn-ignore')) {
      this.photo.properties.show_options = false;
      return;
    }

    if ($(event.target).hasClass('listing-photo')) {
      this.photo.properties.show_options = ($(event.target).height() / 2) > event.offsetY;
    }

    if ($(event.target).hasClass('list-group') 
      || $(event.target).hasClass('btn') 
      || $(event.target).hasClass('select')
      || $(event.target).hasClass('btn-group')) {
      this.photo.properties.show_options = true;
    }
  }

  changeAnnotation(type, value) {
    this.photo.setAnnotateValue(type, value);

    this.change.emit();
  }

  toogleIgnore() {
    this.photo.tooglePbcvIgnore();

    this.ignore.emit();
  }

  @HostListener('mousemove', ['$event'])
  onMouseMove(event) {
    this.showOptions(event);
  }
}
