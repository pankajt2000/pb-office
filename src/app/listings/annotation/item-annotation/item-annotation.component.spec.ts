import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemAnnotationComponent } from './item-annotation.component';

describe('ItemAnnotationComponent', () => {
  let component: ItemAnnotationComponent;
  let fixture: ComponentFixture<ItemAnnotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemAnnotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemAnnotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
