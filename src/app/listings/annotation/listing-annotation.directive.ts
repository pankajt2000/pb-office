import { Directive, HostListener, Input } from '@angular/core';
import { ListingModel } from '../models/listing.model';
import { ListingDetailsModel } from '../models/listing-details.model';
import { BsModalService } from 'ngx-bootstrap';
import { ListingAnnotationComponent } from './listing-annotation/listing-annotation.component';

@Directive({
  selector: '[appListingAnnotation]'
})
export class ListingAnnotationDirective {

  @Input('appListingAnnotation') item: ListingModel | ListingDetailsModel;

  @HostListener('click') openPopup() {
    this.modalService.show(ListingAnnotationComponent, {
      backdrop: 'static',
      class: 'modal-xl m-t-10',
      initialState: { item: this.item }
    });
  }

  constructor(private modalService: BsModalService) {
  }

}
