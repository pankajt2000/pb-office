import { AgentApiService } from '../../services/api/agent-api.service';
import { PhotoModel } from '../models/photo.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AnnotateOptionModel } from './annotate-option.model';

@Injectable()
export class AnnotateService {

  annotate_options;

  constructor(private api: AgentApiService) {
  }


  getAnotateOptions(): Observable<AnnotateOptionModel[]> {
    return new Observable(observer => {
      if (this.annotate_options) {
        observer.next(this.annotate_options);
        observer.complete();
      } else {
        this.api.get(`pbcv-annotate-options?is_new=1`).map(data => data.map(item => new AnnotateOptionModel().deserialize(item)))
          .subscribe(data => {
            this.annotate_options = data;
            observer.next(this.annotate_options);
            observer.complete();
          });
      }
    });
  }

  ignorePhoto(listing_id, photo_id) {
    return this.api.post(`pbcv-ignore-listing-media/${listing_id}/${photo_id}`, {});
  }

  stopIgnorePhoto(listing_id, photo_id) {
    return this.api.delete(`pbcv-ignore-listing-media/${listing_id}/${photo_id}`);
  }

  getGallery(listing_id: string) {
    return this.api.get(`search-listings-gallery/${listing_id}`)
      .map((response) => response.map(photo => new PhotoModel().deserialize(photo)));
  }

  save(listing_id, data) {
    return this.api.post(`pbcv-annotate-listing/${listing_id}`, data);
  }

  savePhoto(listing_id, photo_id, data) {
    return this.api.post(`pbcv-annotate-listing-media/${listing_id}/${photo_id}`, data).map(photo => new PhotoModel().deserialize(photo));
  }

}
