import { Component, OnInit } from '@angular/core';
import {ClientModel} from '../../client/models/client.model';
import {ClientService} from '../../client/services/client.service';
import {BsModalService} from 'ngx-bootstrap';
import {SavedListingService} from '../services/saved-listing.service';
import {ListingService} from '../services/listing.service';
import { SearchService } from '../../search/services/search.service';
import { SearchOptionsModel } from '../../search/models/search-options.model';

@Component({
  selector: 'app-recent-listing',
  templateUrl: './recent-listing.component.html',
  styleUrls: ['./recent-listing.component.less']
})
export class RecentListingComponent implements OnInit {

  searching = false;
  search_result_count: 0;
  search_result = [];
  model: { offset: number, limit: number } = {
    offset: 0,
    limit: 12
  };
  client: ClientModel;
  form_options: SearchOptionsModel;
  view = 'grid';
  advice_result: { count: number, title: string };

  constructor(private listingService: ListingService,
              private modalService: BsModalService,
              private searchService: SearchService,
              public savedListingService: SavedListingService,
              private clientService: ClientService) {
    this.searchService.getFormOptions().subscribe(data => this.form_options = data);
    this.clientService.getCurrentClient().subscribe(client => this.client = client);
  }

  ngOnInit() {
    this.search();
    window.scroll(0, 0);
  }

  search(is_load_more: boolean = false) {
    this.searching = true;

    this.listingService.getRecentListings('', this.model.offset, this.model.limit)
      .subscribe(results => {
        this.searching = false;
        this.search_result_count = results[1];
        this.search_result = is_load_more ? this.search_result.concat(results[0]) : results[0];
      }, (response) => {
        this.searching = false;
      });
  }

  loadMore(e) {
    this.model.offset = this.model.offset + this.model.limit;
    this.search(true);
  }

}
