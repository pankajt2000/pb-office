import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedListingsItemComponent } from './saved-listings-item.component';

describe('SavedListingsItemComponent', () => {
  let component: SavedListingsItemComponent;
  let fixture: ComponentFixture<SavedListingsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedListingsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedListingsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
