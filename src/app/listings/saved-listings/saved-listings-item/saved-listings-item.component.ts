import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SavedListingModel } from '../../models/saved-listing.model';
import { SavedListingService } from '../../services/saved-listing.service';

@Component({
  selector: 'app-saved-listings-item',
  templateUrl: './saved-listings-item.component.html',
  styleUrls: ['./saved-listings-item.component.less']
})
export class SavedListingsItemComponent implements OnInit {

  @Input() public item: SavedListingModel;
  @Input() public readonly: boolean= false;
  @Input('checked') public is_checked: boolean = false;
  @Output() public checked = new EventEmitter<SavedListingModel>();
  @Output() public unchecked = new EventEmitter<SavedListingModel>();

  constructor(private savedListingService: SavedListingService) {
  }

  ngOnInit() {
  }

  selectItem() {
    this.checked.emit(this.item);
  }

  unselectItem() {
    this.unchecked.emit(this.item);
  }

  removeItem() {
    this.savedListingService.toogleListing(this.item.advertisement_id);
  }

  toogleItem() {
    this.item.checked = !this.item.checked;
  }

}

