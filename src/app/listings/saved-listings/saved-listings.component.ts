import { AfterViewChecked, ChangeDetectorRef, Component, DoCheck, OnInit } from '@angular/core';
import { ListingModel } from '../models/listing.model';
import { SendModalComponent } from '../send-modal/send-modal.component';
import { BsModalService } from 'ngx-bootstrap';
import { SavedListingService } from '../services/saved-listing.service';
import { SavedListingModel } from '../models/saved-listing.model';

@Component({
  selector: 'app-saved-listings',
  templateUrl: './saved-listings.component.html',
  styleUrls: ['./saved-listings.component.less']
})
export class SavedListingsComponent implements OnInit {

  is_opened = false;
  delete_after_sending = false;
  saved_listings: SavedListingModel[] = [];
  sent_listings: SavedListingModel[] = [];

  selected_listings: SavedListingModel[] = [];

  constructor(private savedListingService: SavedListingService, private modalService: BsModalService, private cdRef: ChangeDetectorRef) {
    this.savedListingService.getSavedListings().subscribe((items: SavedListingModel[]) => {
      this.saved_listings = items.filter(item => !item.sent);
      this.sent_listings = items.filter(item => item.sent);
      this.selected_listings = [];
    });
  }

  sendListings() {
    this.is_opened = false;
    const modalWindow = this.modalService.show(SendModalComponent, {
      class: 'modal-lg m-t-10',
      initialState: { listings: this.selected_listings, delete_after_sending: this.delete_after_sending }
    });
    modalWindow.content.sentEvent.subscribe(() => {
      this.savedListingService.initSavedListings();
    });

  }

  selectAllSaved() {
    this.saved_listings.forEach(item => item.checked = true);
  }

  unselectAllSaved() {
    this.saved_listings.forEach(item => item.checked = false);
  }

  removeAllSelectedSaved() {
    if (!this.selected_listings.length) {
      return;
    }
    const ids = this.selected_listings.map((item) => item.advertisement_id);
    this.savedListingService.deleteByIDs(ids);
    this.selected_listings = [];
  }

  ngOnInit() {
  }

}
