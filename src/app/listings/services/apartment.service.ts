import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { ApartmentModel } from '../models/apartment.model';

@Injectable()
export class ApartmentService {

  constructor(private api: AgentApiService) {

  }

  getApartments(term: string = '', limit: number = 10, offset: number = 0) {
    return this.api.get(`suggest-apartments?search=${term}&limit=${limit}&offset=${offset}`)
      .map(data => {
        return data.map(building => new ApartmentModel().deserialize(building));
      });
  }

  getApartmentsByBuilding(building_id: string, unit: string) {
    return this.api.get(`buildings-apartment/${building_id}/${unit}`)
      .map(data => new ApartmentModel().deserialize(data));
  }

  createApartment(building_id: string, data: object) {
    return this.api.post(`buildings-apartment/${building_id}`, data)
      .map(apartment => new ApartmentModel().deserialize(apartment));
  }

}
