import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { ListingModel } from '../models/listing.model';
import { SearchService } from '../../search/services/search.service';
import { ListingDetailsModel } from '../models/listing-details.model';
import { ClientModel } from '../../client/models/client.model';
import { BuildingModel } from '../models/building.model';
import { SavedListingModel } from '../models/saved-listing.model';
import { ListingOptionsModel } from '../models/listing-options.model';
import { ListingPreviewModel } from '../models/listing-preview.model';
import { PhotoModel } from '../models/photo.model';
import { ConcessionDefinitionModel } from '../models/concession-definition.model';

@Injectable()
export class ListingService {

  constructor(private api: AgentApiService, private searchService: SearchService) {
  }

  getNewListings(offset: number = 0, limit: number = 10) {
    return this.api.get(`dashboard-new-listings-web?limit=${limit}&offset=${offset}`)
      .map(data => {
        return data.map((client) => {
          client.listings = client.listings.map((item: ListingModel) => new ListingModel().deserialize(item));
          return client;
        });
      });
  }

  getListing(listing_id: number) {
    return this.api.get(`search-listings/${listing_id}`).map(data => new ListingDetailsModel().deserialize(data));
  }

  getMatchedClients(listing_id: number) {
    return this.api.get(`search-listings/${listing_id}/matching-clients`)
      .map(data => data.map(client => new ClientModel().deserialize(client)));
  }

  getSiblingListings(building_id: number) {
    return this.api.get(`search-buildings/${building_id}?inactive_too=1`)
      .map(data => new BuildingModel().deserialize(data));
  }

  createListing(data) {
    return this.api.post(`preview-listings`, data).map((response) => response);
  }

  validatePreviewListing(ad_type: string, apartment_id: string) {
    return this.api.post(`preview-listings-validate`, { ad_type, apartment_id }).map((response) => new ListingPreviewModel().deserialize(response));
  }

  getPreviewListing(listing_id: number) {
    return this.api.get(`preview-listings/${listing_id}`).map((response) => new ListingPreviewModel().deserialize(response));
  }

  getSavedListings(client_id: number) {
    return this.api.get(`clients-saved-listings/${client_id}`)
      .map(data => data.map(listing => new SavedListingModel().deserialize(listing)));
  }

  addSavedListings(client_id: number, listing_idss: string[]) {
    return this.api.post(`clients-saved-listings/${client_id}`, { listing_ids: listing_idss })
      .map(data => data.map(listing => new SavedListingModel().deserialize(listing)));
  }

  deleteOneSavedListing(client_id: number, listing_id: string) {
    return this.api.delete(`clients-saved-listings/${client_id}/${listing_id}`)
      .map(data => data.map(listing => new SavedListingModel().deserialize(listing)));
  }

  deleteSavedListings(client_id: number, listing_ids: string[]) {
    return this.api.delete(`clients-saved-listings/${client_id}`, { body: { listing_ids: listing_ids } })
      .map(data => data.map(listing => {
        return new SavedListingModel().deserialize(listing);
      }));
  }

  getListingOptions() {
    return this.api.get('preview-listings-options').map(options => new ListingOptionsModel().deserialize(options));
  }

  getListingDecription(listing: ListingModel | ListingPreviewModel) {
    return this.api.get( `preview-listings-generate-description/${listing.advertisement_id}`);
  }

  deleteListing(listing: ListingModel | ListingPreviewModel, reason: string) {
    return this.api.delete(`search-listings/${listing.advertisement_id}/reason/${reason}`);
  }

  updateRoom(listing_id: string, data: any) {
    return this.api.put(`preview-listings-room/${listing_id}`, data);
  }

  updateListing(listing_id: string, data: { price: string, description: string }) {
    return this.api.put(`preview-listings/${listing_id}`, data).map((response) => new ListingPreviewModel().deserialize(response));
  }

  publish(listing_id: string) {
    return this.api.post(`preview-listings-publish/${listing_id}`, {});
  }

  advertise(listing_id: string) {
    return this.api.post(`publish-ad/${listing_id}`, {});
  }

  removeAdvertise(listing_id: string) {
    return this.api.delete(`publish-ad/${listing_id}`);
  }

  removePhoto(listing_id: string, photo_id: number) {
    return this.api.delete(`preview-listings-photo/${listing_id}/${photo_id}`);
  }

  movePhoto(listing_id: string, photo_id: string, room_id: string) {
    return this.api.put(`preview-listings-photo-assign/${listing_id}/${photo_id}/${room_id}`, {});
  }

  markPhotoAsFeatured(listing_id: string, photo_id: string) {
    return this.api.post(`preview-listings-photo-featured/${listing_id}/${photo_id}`, {});
  }

  markPhotoAsFloorplan(listing_id: string, photo_id: string) {
    return this.api.post(`preview-listings-photo-floorplan/${listing_id}/${photo_id}`, {});
  }

  unmarkPhotoAsFloorplan(listing_id: string, photo_id: string) {
    return this.api.delete(`preview-listings-photo-floorplan/${listing_id}/${photo_id}`);
  }

  disablePhoto(listing_id: string, photo_id: string) {
    return this.api.delete(`preview-listings-photo-enable/${listing_id}/${photo_id}`);
  }

  updateActivePhotos(listing_id: string, photo_ids: string[]) {
    return this.api.put(`preview-listings-photo-gallery/${listing_id}`, {
      media_item_ids: photo_ids,
    });
  }

  getGallery(listing_id: string) {
    return this.api.get(`search-listings-gallery/${listing_id}`)
      .map((response) => response.map(photo => new PhotoModel().deserialize(photo)));
  }

  getConcessionText(concessionModel: ConcessionDefinitionModel) {
    return this.api.post(`preview-listings-concession-advice`, {
      price: concessionModel.price,
      concession_definition: concessionModel.concession_definition
    });
  }

  getRecentListings(search: string = '', offset: number = 0, limit: number = 10) {
    return this.api.get(
      `search-listings-recent?search=${search}&limit=${limit}&offset=${offset}`,
      { observe: 'response' }
    ).map(response => {
      return [
        response.body.map((item: ListingModel) => new ListingModel().deserialize(item)),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

  getSourceJson(listing_id: string){
    return this.api.get(`search-listings/${listing_id}/json-source`);
  }

}
