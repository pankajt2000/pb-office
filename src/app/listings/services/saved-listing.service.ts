import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SearchService } from '../../search/services/search.service';
import { ListingService } from './listing.service';
import { ClientService } from '../../client/services/client.service';
import { SavedListingModel } from '../models/saved-listing.model';
import { ClientModel } from '../../client/models/client.model';

@Injectable()
export class SavedListingService {

  private saved_listings: BehaviorSubject<SavedListingModel[]> = new BehaviorSubject<SavedListingModel[]>([]);
  private current_client: ClientModel;

  constructor(private api: AgentApiService,
              private searchService: SearchService,
              private listingService: ListingService,
              private clientService: ClientService) {
    this.initSavedListings();
  }

  initSavedListings() {
    this.clientService.getCurrentClient().subscribe((client) => {
      this.current_client = client;
      if (client) {
        this.listingService.getSavedListings(client.user_id).subscribe((listings) => {
          this.setSavedListings(listings);
        });
      } else {
        this.setSavedListings([]);
      }
    });
  }

  setSavedListings(value: SavedListingModel[]) {
    this.saved_listings.next(value);
  }

  isSaved(advertisement_id: string): boolean {
    return !!this.saved_listings.getValue().find(saved_item => {
      return saved_item.advertisement_id === advertisement_id && !saved_item.sent;
    });
  }

  isSent(advertisement_id: string): boolean {
    return !!this.saved_listings.getValue().find((saved_item) => {
      return !!(saved_item.advertisement_id === advertisement_id && saved_item.sent);
    });
  }

  saveListing(advertisement_id) {
    this.listingService.addSavedListings(this.current_client.user_id, [advertisement_id])
      .subscribe(listings => this.setSavedListings(listings));
  }

  toogleListing(advertisement_id: string) {

    if (this.current_client) {
      if (this.isSaved(advertisement_id)) {
        this.listingService.deleteOneSavedListing(this.current_client.user_id, advertisement_id)
          .subscribe(listings => this.setSavedListings(listings));
      } else {
        this.saveListing(advertisement_id);
      }
    }
  }

  deleteByIDs(ids: string[]) {
    if (this.current_client) {
      this.listingService.deleteSavedListings(this.current_client.user_id, ids)
        .subscribe(listings => this.setSavedListings(listings));
    }
  }

  getSavedListings() {
    return this.saved_listings;
  }

}
