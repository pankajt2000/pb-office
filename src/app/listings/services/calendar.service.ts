import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { OpenHouseModel } from '../models/open-house.model';

@Injectable()
export class CalendarService {

  constructor(private api: AgentApiService) {
  }

  getOpenHouse(advertisement_id) {
    return this.api.get(`calendar-listing-open-house/${advertisement_id}`)
      .map(items => items.map(item => new OpenHouseModel().deserialize(item)));
  }

  addDate(params: { start_time: string, end_time: string, description: string, by_appointment_only: 0 | 1, advertisement_id: string }) {
    return this.api.post(`calendar-open-house`, params).map(item => new OpenHouseModel().deserialize(item));
  }

  editDate(open_house_id: string, params: { start_time: string, end_time: string, description: string, by_appointment_only: 0 | 1, advertisement_id: string }) {
    return this.api.put(`calendar-open-house/${open_house_id}`, params).map(item => new OpenHouseModel().deserialize(item));
  }

  deleteDate(open_house_id: string){
    return this.api.delete(`calendar-open-house/${open_house_id}`);
  }
}
