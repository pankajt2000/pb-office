import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { SuggestBuildingModel } from '../models/suggest-building.model';
import { BuildingAdviceModel } from '../models/building-advice.model';
import { BuildingModel } from '../models/building.model';

@Injectable()
export class BuildingService {

  constructor(private api: AgentApiService) {

  }

  getBuildings(term: string = '', limit: number = 10, offset: number = 0) {
    return this.api.get(`suggest-buildings?search=${term}&limit=${limit}&offset=${offset}`)
      .map(data => {
        return data.map(building => new SuggestBuildingModel().deserialize(building));
      });
  }

  getAdvice(term: string) {
    return this.api.post(`buildings-advice`, { address: term })
      .map(data => new BuildingAdviceModel().deserialize(data));
  }

  createBuilding(data) {
    return this.api.post(`buildings`, data)
      .map(response => new SuggestBuildingModel().deserialize(response));
  }


}
