import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApartmentService } from '../services/apartment.service';
import { SuggestBuildingModel } from '../models/suggest-building.model';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { ListingOptionsModel } from '../models/listing-options.model';

@Component({
  selector: 'app-new-apartment',
  templateUrl: './new-apartment.component.html',
  styleUrls: ['./new-apartment.component.less']
})
export class NewApartmentComponent implements OnInit {

  @Output() created = new EventEmitter();

  options: ListingOptionsModel = new ListingOptionsModel;
  unit: string = '';
  apartmentForm: FormGroup;
  building: SuggestBuildingModel;
  submitted = false;
  creating = false;

  constructor(public bsModalRef: BsModalRef,
              private fb: FormBuilder,
              private alertService: BootstrapAlertService,
              private apartmentService: ApartmentService) {

    this.apartmentForm = this.fb.group({
      unit: [null, Validators.required],
      size: [null],
      bedrooms: [null],
      bathrooms: [null],
    });

  }

  ngOnInit() {
    this.apartmentForm.controls.unit.patchValue(this.unit);
  }

  submitApartment() {
    this.submitted = true;
    if (this.apartmentForm.invalid) {
      return false;
    }

    this.creating = true;

    this.apartmentService.createApartment(this.building.building_id, this.apartmentForm.value).subscribe((apartment) => {
      this.created.emit(apartment);

      this.alertService.showSucccess('Apartment successfully created');

      this.bsModalRef.hide();

    }, response => {

      this.alertService.showError('Error occurs while creating the apartment');

      this.creating = false;
    });


  }
}


