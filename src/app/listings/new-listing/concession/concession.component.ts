import { Component, EventEmitter, Input, Output, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ListingService } from '../../services/listing.service';
import { ConcessionDefinitionModel } from '../../models/concession-definition.model';
import { CurrencyMaskConfig } from 'ngx-currency/src/currency-mask.config';

@Component({
  selector: 'app-concession-editor',
  templateUrl: './concession.component.html',
  styleUrls: [ './concession.component.less' ]
})
export class ConcessionComponent implements OnInit, OnChanges {

  @Input() price: number;
  @Output() concessionUpdated = new EventEmitter<ConcessionDefinitionModel>();

  concessionText: string = '';
  netEffectivePrice: number = 0;
  concessionLoading: boolean = false;
  concessionForm: FormGroup = null;

  commissionType: string = 'amount';

  opMonthsValid: boolean = true;

  previousAmount: string | number = 0;


  currencyMaskOpts: CurrencyMaskConfig = {
    align: "left",
    allowNegative: false,
    allowZero: false,
    decimal: ",",
    precision: 0,
    prefix: "$ ",
    suffix: "",
    thousands: ".",
    nullable: true
  };

  public constructor(
    private fb: FormBuilder,
    private listingService: ListingService
  ) {

  }

  toggleCommissionType(evt) {
    const newCommissionType = evt.target.value;

    switch (newCommissionType) {
      case 'amount':
        this.previousAmount = this.concessionForm.controls.op_months.value;
        this.concessionForm.controls.op_months.patchValue("0");
        this.concessionForm.controls.op_amount.patchValue(this.previousAmount);

        break;
      case 'months':
        this.previousAmount = this.concessionForm.controls.op_amount.value;
        this.concessionForm.controls.op_amount.patchValue("0");
        this.concessionForm.controls.op_months.patchValue(this.previousAmount);

        break;
    }

    this.validateOpMonths({ target: { value: this.concessionForm.controls.op_months.value }});
  }

  ngOnInit(): void {
    this.concessionForm = this.fb.group({
      op_months: ["0"],
      op_amount: ["0"],
      and_or: ['', Validators.required],
      free_months: ["0"],
      free_on_lease: ["12"]
    });

    this.toggleCommissionType({ target: { value: this.commissionType }});
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateConcessionModel();
  }

  updateConcessionModel() {
    if (!this.concessionForm) return;

    this.price = this.price || 0;

    const base = {
      price: this.price,
      concession_definition: this.concessionForm.value
    };

    const cm: ConcessionDefinitionModel = new ConcessionDefinitionModel().deserialize(base);

    this.concessionLoading = true;
    this.listingService.getConcessionText(cm).subscribe((data) => {
      this.concessionText = data.concession_text;
      this.netEffectivePrice = data.net_effective_price;
      this.concessionLoading = false;

      this.concessionUpdated.emit(cm);
    });
  }

  setAndOr(andor) {
    this.concessionForm.get('and_or').setValue(andor);
    this.updateConcessionModel();
  }

  showOpMonthsText() {
    return this.commissionType === 'months' && this.concessionForm.get('op_months').value !== '0' && this.price && this.price !== 0;
  }

  validateOpMonths(event: any) {
    const val: string = event.target.value;
    const allowed: string[] = ["0", "0.5", "1", "1.5", "2"];

    this.opMonthsValid = allowed.indexOf(val) !== -1;
  }

  getFreeMonths(): number[] {
    let months: number[] = [];

    for (let i = 0; i <= 5; i = i + 0.5) {
      months = [...months, i];
    }

    return months;
  }

  getMonthsLease(): number[] {
    let months: number[] = [];

    for (let i = 12; i <= 36; i++) {
      months.push(i);
    }

    return months;
  }
}
