import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { BuildingService } from '../services/building.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SuggestBuildingModel } from '../models/suggest-building.model';
import { ApartmentModel } from '../models/apartment.model';
import { ApartmentService } from '../services/apartment.service';
import { BsModalService } from 'ngx-bootstrap';
import { NewBuildingComponent } from '../new-building/new-building.component';
import { NewApartmentComponent } from '../new-apartment/new-apartment.component';
import { ListingService } from '../services/listing.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { Router } from '@angular/router';
import { EmptyObservable } from 'rxjs/observable/EmptyObservable';
import { ListingOptionsModel } from '../models/listing-options.model';
import { CurrencyMaskConfig } from 'ngx-currency/src/currency-mask.config';
import {ConcessionDefinitionModel} from '../models/concession-definition.model';
import { ListingPreviewModel } from "../models/listing-preview.model";

declare var $: any;

@Component({
  selector: 'app-new-listing',
  templateUrl: './new-listing.component.html',
  styleUrls: ['./new-listing.component.less']
})
export class NewListingComponent implements OnInit, OnDestroy {

  private _building_text: string = '';
  options: ListingOptionsModel = new ListingOptionsModel();
  buildings: SuggestBuildingModel[];
  buildings_to_loading: boolean;
  building: SuggestBuildingModel | null;
  buildingsToTypeahead = new Subject<string>();

  unitTerm = new Subject<string>();
  apartment: ApartmentModel;
  search_apartment = false;

  listingForm: FormGroup;
  submitted = false;
  creating = false;

  validate_listing: boolean = false;
  existing_ad: ListingPreviewModel = null;

  priceMaskConfig: CurrencyMaskConfig = {
    align: "left",
    allowNegative: false,
    allowZero: true,
    decimal: ".",
    precision: 0,
    prefix: "",
    suffix: "%",
    thousands: ",",
    nullable: true
  };

  private exposures: {
    'N': boolean, 'S': boolean, 'E': boolean, 'W': boolean
  } = {
    'N': false,
    'S': false,
    'E': false,
    'W': false
  };

  constructor(
    private modalService: BsModalService,
    private apartmentService: ApartmentService,
    private buildingService: BuildingService,
    protected listingService: ListingService,
    private alertService: BootstrapAlertService,
    private cd: ChangeDetectorRef,
    protected fb: FormBuilder,
    private router: Router,
  ) {
    this.buildingService.getBuildings().subscribe(data => this.buildings = data);

    this.listingService.getListingOptions().subscribe(options => this.options = options);

  }

  ngOnInit(): void {

    this.listingForm = this.fb.group({
      building: [null, Validators.required],
      unit: [{ value: '', disabled: true }, Validators.required],
      apartment_id: [null, Validators.required],
      price: [null, Validators.required],
      ad_type: ['rental', Validators.required],
      fee_structure: ['Co-Broke', Validators.required],
      commission_structure: [15],
      access_type: ['EXCLUSIVE', Validators.required],
      description: [null],
      nofee: [0],
      advertise: [0],
      furnished_type: ['Unfurnished', Validators.required],
      rls_condition: ['new', Validators.required],
      exposures: [''],
      include_in_rls: [0],
      co_exclusive: [0],
      available_from: [this.today(), Validators.required],
      first_showing_date: [this.today()],
      common_charges: [0],
      monthly_taxes: [0],
      concession_definition: [null],
      min_lease_period: [12],
      max_lease_period: [12]
    });
    this.listingForm.controls.unit.disable();

    this.buildingsToTypeahead.pipe(
      tap(() => this.buildings_to_loading = true),
      distinctUntilChanged(),
      debounceTime(300),
      switchMap((term, index) => {
        if (term.trim().length) {
          this._building_text = term;
          return this.buildingService.getBuildings(term);
        }

        this.buildings_to_loading = false;
        return new EmptyObservable();
      }),
    ).subscribe(x => {
      this.buildings = x;
      this.buildings_to_loading = false;
      this.cd.markForCheck();
    }, () => {
      this.buildings = [];
    });

    this.unitTerm.debounceTime(300).subscribe(text => this.updateApartment(text));

    $('.page-content').addClass('page-wizard').addClass('wizardbg');
  }

  onConcessionUpdate(cm: ConcessionDefinitionModel) {
    this.listingForm.controls.concession_definition.patchValue(cm.concession_definition);
  }

  ngOnDestroy(): void {
    $('.page-content').removeClass('page-wizard').removeClass('wizardbg');
  }

  addBuilding() {
    const bsModalRef = this.modalService.show(NewBuildingComponent, {
      initialState: {
        advice_text: this._building_text,
        options: this.options
      }
    });

    bsModalRef.content.created.subscribe((building: SuggestBuildingModel) => {
      this.buildings = [building];
      this.building = building;
      this.listingForm.controls.building.patchValue(building.building_id);
    });

  }

  updateBuilding(e) {
    this.building = e;
    if (e) {
      this.listingForm.controls.unit.enable();
    } else {
      this.listingForm.controls.unit.disable();
    }

    this.apartment = null;
    this.listingForm.controls.unit.patchValue(null);
    this.listingForm.controls.apartment_id.patchValue(null);

  }

  addApartment() {
    const bsModalRef = this.modalService.show(NewApartmentComponent, {
      initialState: {
        building: this.building,
        unit: this.listingForm.controls.unit.value,
        options: this.options,
      }
    });

    bsModalRef.content.created.subscribe((apartment: ApartmentModel) => {
      this.apartment = apartment;
      this.listingForm.controls.unit.patchValue(this.apartment.unit);
      this.listingForm.controls.apartment_id.patchValue(this.apartment.apartment_id);
    });

  }

  updateApartment(unit) {
    this.search_apartment = true;
    this.apartmentService.getApartmentsByBuilding(this.building.building_id, unit).subscribe(res => {
      this.apartment = res;
      this.listingForm.controls.unit.patchValue(this.apartment.unit);
      this.listingForm.controls.apartment_id.patchValue(this.apartment.apartment_id);

      this.validateNoActiveListing();

      this.search_apartment = false;
    }, (response) => {
      this.apartment = null;
      this.existing_ad = null;
      this.listingForm.controls.apartment_id.patchValue(null);
      this.search_apartment = false;
    });
  }

  setAdType(type) {
    this.listingForm.get('ad_type').setValue(type);
    this.validateNoActiveListing();
  }

  validateNoActiveListing() {
    if (!this.apartment || !this.listingForm.get('ad_type').value) return;

    const ad_type = this.listingForm.get('ad_type').value;

    this.validate_listing = true;
    this.listingService.validatePreviewListing(ad_type, this.apartment.apartment_id).subscribe(res => {
      this.existing_ad = res.advertisement_id ? res : null;

      this.validate_listing = false;
    }, (err) => {
      this.validate_listing = false;
    });
  }

  submitListings() {
    this.submitted = true;

    if (this.listingForm.controls.concession_definition.value) {
      if (["0", "0.5", "1", "1.5", "2"].indexOf(this.listingForm.controls.concession_definition.value.op_months) === -1)
        return;
    }

    if (this.listingForm.invalid || this.existing_ad !== null) return;

    this.creating = true;
    const data = this.listingForm.value;
    data.apartment_id = this.apartment.apartment_id;
    this.listingService.createListing(data).subscribe((data) => {
      this.alertService.showSucccess('Building successfully created');
      this.creating = false;

      this.router.navigate([`listing/${data['advertisement_id']}/details`]);

    }, () => {
      this.alertService.showError('Error occurs while creating the listing');
      this.creating = false;
    });

  }

  toggleExposure(exposure: string) {
    this.exposures[exposure] = !this.exposures[exposure];

    const selectedExposures = Object.keys(this.exposures).filter(e => this.exposures[e]);

    this.listingForm.controls.exposures.patchValue(selectedExposures.join(','));
  }

  exposureSelected(exposure: string) {
    return this.exposures[exposure];
  }

  showMonthlyTaxes() {
    return this.building &&
           this.building.ownership &&
           this.building.ownership.toLowerCase() === 'condo';
  }

  today() {
    const d = new Date();
    const yyyy = d.getFullYear();
    const mm = d.getMonth() + 1 < 10 ? `0${d.getMonth() + 1}` : d.getMonth() + 1;
    const dd = d.getDate() < 10 ? `0${d.getDate()}` : d.getDate();

    return `${yyyy}-${mm}-${dd}`;
  }

}
