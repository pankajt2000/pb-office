import { Component, HostListener, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ListingService } from '../services/listing.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { ListingDetailsModel } from '../models/listing-details.model';
import { ClientModel } from '../../client/models/client.model';
import { ListingModel } from '../models/listing.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { SavedListingService } from '../services/saved-listing.service';
import { ClientService } from '../../client/services/client.service';
import { QuickviewService } from '../../layout/quickview/quickview.service';
import { SearchService } from '../../search/services/search.service';
import { MessageService } from '../../mailbox/services/message.service';
import { MessageModel } from '../../mailbox/models/message.model';
import { BuildingModel } from '../models/building.model';

declare var $: any;

@Component({
  selector: 'app-view-listing',
  templateUrl: './view-listing.component.html',
  styleUrls: ['./view-listing.component.less']
})
export class ViewListingComponent implements OnInit, OnDestroy {

  loading = true;
  popup_mode = false;
  bsModalRef: BsModalRef;
  listing_id;
  listing: ListingDetailsModel;
  building: BuildingModel;
  matched_clients: ClientModel[] = [];
  listings_for_rent: ListingModel[] = [];
  listings_for_sales: ListingModel[] = [];
  ratings;
  delete_reasons: { id: string; label: string; type: string }[] = [];
  client: ClientModel;
  clients: ClientModel[];
  modalRef: BsModalRef;
  client_for_delete: ClientModel;
  streeViewMode: boolean;
  active_listings: 1 | 0 = 1;

  is_sticky_header = false;
  sticky_width = 'inherit';

  show_mobile_map = false;

  @HostListener('window:scroll', ['$event']) onScrollEvent($event) {

    const prev_evelemt = $('.previewlistingimage');
    if (prev_evelemt.length) {
      this.is_sticky_header = $(window).scrollTop() > (prev_evelemt.position().top + prev_evelemt.height());
    }

    if (this.is_sticky_header) {
      this.sticky_width = $('.listing-details').width() - 10 + 'px';
      $('.listing-details-section').css('margin-top', $('.header-name').height() + 10);
    } else {
      this.sticky_width = 'inherit';
      $('.listing-details-section').css('margin-top', 0);
    }
  }

  @ViewChild('streetView') streetViewElement: any;
  @ViewChild('mapView') mapView: any;
  map: google.maps.StreetViewPanorama;

  constructor(private route: ActivatedRoute,
              public listingService: ListingService,
              private clientService: ClientService,
              public savedListingService: SavedListingService,
              private searchService: SearchService,
              private mailService: MessageService,
              private quickviewService: QuickviewService,
              private router: Router,
              private modalService: BsModalService,
              private alertService: BootstrapAlertService) {

    this.clientService.getCurrentClient().subscribe(client => this.client = client);
    this.clientService.getAllSuggestClients().subscribe(clients => {
      this.clients = clients;
      if(this.listing_id){
        this.initMatchingClients(this.listing_id);
      }
    });
  }

  initListing(listing_id) {

    this.listingService.getListing(+listing_id).subscribe((item) => {

      this.listing = item;

      this.initSiblingListings();

      this.onGoogleLoaded();

      this.loading = false;

    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError('Listing not found');
      } else {
        this.alertService.showError('Error: Cannot show the listing');
      }

      this.loading = false;
    });

    if (this.clients) {
      this.initMatchingClients(listing_id);
    }
  }

  onGoogleLoaded() {
    setTimeout(() => {
      if ('google' in window) {
        this.initStreetView();
      } else {
        this.onGoogleLoaded();
      }
    }, 500);
  }

  initSiblingListings() {
    this.listingService.getSiblingListings(+this.listing.building_id).subscribe((building) => {
      this.building = building;
      this.listings_for_rent = building.rentals;
      this.listings_for_sales = building.sales;
    });
  }

  initStreetView() {
    const streetViewService = new google.maps.StreetViewService();
    const latLng = new google.maps.LatLng(this.listing.latitude, this.listing.longitude);
    const map = this.map = new google.maps.StreetViewPanorama(this.streetViewElement.nativeElement);
    const processSVData = function (data, status) {
      if (status === google.maps.StreetViewStatus.OK) {
        map.setPano(data.location.pano);
        map.setVisible(false);
        map.setVisible(true);
      }
    };
    streetViewService.getPanorama({
      location: latLng,
      radius: 100,
      source: google.maps.StreetViewSource.OUTDOOR
    }, processSVData);
  }


  initMatchingClients(listing_id) {
    this.listingService.getMatchedClients(+listing_id).subscribe((clients: ClientModel[]) => {
      this.matched_clients = clients;
      this.matched_clients.forEach(client => {
        const c = this.clients.find(sclient => sclient.user_id === client.user_id);
        client.suspended = c && c.hasSuspendedSearch();
        client.searches = c.searches;
      });
    });
  }

  openPhotos(index) {
    $.magnificPopup.open({
      items: this.listing.getAllPhotos(),
      type: 'image',
      gallery: {
        enabled: true
      },
      callbacks: {
        open: () => {
          $.magnificPopup.instance.goTo(index);
        }
      },
    });
  }

  getRatings() {
    if (!this.ratings) {
      this.ratings = Object.entries(this.listing.preview_ratings);
    }

    return this.ratings;
  }

  getDescription() {
    return this.listing.description ? this.listing.description.replace(new RegExp('\n', 'g'), '<br />') : '';
  }

  toogleSavedListing() {
    this.savedListingService.toogleListing(this.listing.advertisement_id);
  }

  toogleStreetView() {
    this.streeViewMode = !this.streeViewMode;
    setTimeout(() => {
      google.maps.event.trigger(this.map, 'resize');
    }, 100);
  }


  ngOnInit(): void {
    if (this.listing_id) {
      this.initListing(this.listing_id);
    } else {
      this.route.params.subscribe(params => {

        this.loading = true;

        this.listing_id = params['listing_id'] || 0;
        window.scroll(0, 0);
        this.initListing(this.listing_id);
      });
    }

    if (!this.popup_mode) {
      $('.page-content').addClass('listing-details');
    }

    $('body').on('click', '.gm-fullscreen-control', () => {
      this.show_mobile_map = !this.show_mobile_map;
    });
  }

  ngOnDestroy(): void {
    if (!this.popup_mode) {
      $('.page-content').removeClass('listing-details');
    }
  }

  deleteMatchedClient(client: ClientModel) {
    this.modalRef.hide();

    client.deleting = true;
    this.clientService.delete(client).subscribe(() => {
      this.initMatchingClients(this.listing_id);
      client.deleting = false;
    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError('Client not found');
      } else {
        this.alertService.showError('Error: Cannot delete the client');
      }

      client.deleting = false;
    });
  }

  search(client: ClientModel) {
    this.router.navigate([`search/criteria/${client.search_id}`]);
  }

  editSearch(client: ClientModel) {
    this.clientService.getAllSuggestClients().subscribe(clients => clients.forEach(c => {
      if (c.user_id === client.user_id) {
        this.clientService.setCurrentClient(c);
      }
    })).unsubscribe();
    this.quickviewService.setOpened(true);
    this.quickviewService.setState('saved_searches');
  }

  loadingDeleteReason() {
    if (!this.delete_reasons.length) {
      this.searchService.getFormOptions().subscribe((data) => {
        this.delete_reasons = data.status_change_reasons[this.listing.ad_type];
      });
    }
  }

  deleteListing(listing, reason) {
    listing.deleting = true;

    this.listingService.deleteListing(listing, reason.id).subscribe(() => {

      this.initSiblingListings();

    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError('Listing not found');
      } else {
        this.alertService.showError('Error: Cannot delete the listing');
      }

      listing.deleting = false;
    });
  }

  openModal(template: TemplateRef<any>, client: ClientModel) {
    this.client_for_delete = client;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  decline() {
    this.client_for_delete = null;
    this.modalRef.hide();
  }

  saleListingsCount() {
    return this.listings_for_sales.filter(l => l.active === this.active_listings).length;
  }

  rentListingsCount() {
    return this.listings_for_rent.filter(l => l.active === this.active_listings).length;
  }

  percentChangePrice(item1, item2) {
    return (100 - (item1.price / item2.price * 100)).toFixed(2);
  }

  removeSuspend(client) {
    client.suspending = true;
    this.searchService.removeSuspend(client.user_id, client.search_id).subscribe(() => {
      this.initMatchingClients(this.listing_id);
      client.suspending = false;
    });
  }

  suspendSearch(client) {
    client.suspending = true;
    this.searchService.suspendSearch(client.user_id, client.search_id).subscribe(() => {
      this.initMatchingClients(this.listing_id);
      client.suspending = false;
    });
  }

  openSourceJson(template) {
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
    if (!this.listing.view_source_info.data) {
      this.listingService.getSourceJson(this.listing.advertisement_id).subscribe(data => {
        this.listing.view_source_info.data = data;
      });
    }
  }

  openSourceEmail(template) {
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
    if (!this.listing.view_source_info.data) {
      this.mailService.getSourceMail(this.listing.view_source_info.parameters.email_id).subscribe(data => {
        this.listing.view_source_info.data = data;
      });
    }
  }

  putInIframe(message: MessageModel, $event: Event) {
    const iframePointer: any = $event.currentTarget;

    iframePointer.contentWindow.document.open('text/html', 'replace');
    iframePointer.contentWindow.document.write(message.email_html);
    iframePointer.contentWindow.document.close();
    setTimeout(() => {
      iframePointer.style.height = iframePointer.contentWindow.document.body.scrollHeight + 'px';
      $('a', iframePointer.contentWindow.document).attr('target', '_blank');
    }, 200);

  }

  showMobileMap() {
    $('.map .gm-fullscreen-control').click();
  }

}
