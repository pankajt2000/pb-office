import { Component, ElementRef, EventEmitter, Input, OnInit, ViewChild } from '@angular/core';
import { humanizeBytes, UploaderOptions, UploadInput, UploadOutput, UploadFile } from 'ngx-uploader';
import { environment } from '../../../../environments/environment';
import { config } from '../../../config';
import { ListingPreviewModel } from '../../models/listing-preview.model';
import { ListingService } from '../../services/listing.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { PhotoModel } from '../../models/photo.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

declare var $: any;

@Component({
  selector: 'app-media-management',
  templateUrl: './media-management.component.html',
  styleUrls: ['./media-management.component.less']
})
export class MediaManagementComponent implements OnInit {

  @Input() listing: ListingPreviewModel;
  @Input() options;

  uploadInput: EventEmitter<UploadInput>;
  dragOver: boolean;
  uploaderOptions: UploaderOptions;
  loading = false;
  photo_for_delete: PhotoModel;
  modalRef: BsModalRef;
  modal360Ref: BsModalRef;
  default_room_id = 'apartment_general';

  loading360 = false;
  drag360Over: boolean;
  upload360Input: EventEmitter<UploadInput>;
  @ViewChild('upload360modal') upload360modal: ElementRef;
  @ViewChild('upload360input') upload360input: ElementRef;

  constructor(private listingService: ListingService, private alertService: BootstrapAlertService, private modalService: BsModalService) {
    this.uploaderOptions = { concurrency: 1 };
    this.uploadInput = new EventEmitter<UploadInput>();
    this.upload360Input = new EventEmitter<UploadInput>();
  }

  ngOnInit() {
    this.listing.photos.sort((a, b) => a.properties.index < b.properties.index ? -1 : a.properties.index > b.properties.index ? 1 : 0);
    this.listing[360].sort((a, b) => a.properties.index < b.properties.index ? -1 : a.properties.index > b.properties.index ? 1 : 0);
  }

  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'allAddedToQueue') {

      const event: UploadInput = {
        type: 'uploadAll',
        url: environment.agent_base_url + `preview-listings-photo/${this.listing.advertisement_id}/${this.default_room_id}`,
        method: 'POST',
        fieldName: 'photo',
        data: {},
        headers: {
          'Adm-App-Auth': environment.app_auth_header,
          'Adm-Session-ID': localStorage.getItem(config.access_token_local_storage)
        }
      };
      this.uploadInput.emit(event);

    } else if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') {

      this.readImage(output.file);

      this.listing.photos.push(new PhotoModel().deserialize({
        properties: { room_id: this.default_room_id, enabled: 1 },
        upload_file: output.file,
        loading: true,
      }));

    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {

    } else if (output.type === 'removed') {

    } else if (output.type === 'dragOver') {
      this.dragOver = true;
    } else if (output.type === 'dragOut' || output.type === 'drop') {
      this.dragOver = false;
    } else if (output.type === 'rejected' && typeof output.file !== 'undefined') {
      console.log(output.file.name + ' rejected');
    } else if (output.type === 'done') {

      if (output.file.responseStatus === 200) {
        this.listing.photos = this.listing.photos.map((photo) => {
          if (photo.upload_file && photo.upload_file.id === output.file.id) {
            output.file.response.upload_file = output.file;
            return new PhotoModel().deserialize(output.file.response);
          }
          return photo;
        });
      } else if ('errorMsg' in output.file.response) {
        const index = this.listing.photos.findIndex((photo) => photo.upload_file && photo.upload_file.id === output.file.id);
        this.listing.photos.splice(index);
        this.alertService.showError(output.file.response.errorMsg);
      }
    }
  }

  onUpload360Output(output: UploadOutput): void {
    if (output.type === 'allAddedToQueue') {

      this.upload360Modal();

    } else if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') {

      this.readImage(output.file);
      this.listing[360].push(new PhotoModel().deserialize({
        properties: { room_id: '' },
        upload_file: output.file,
        loading: false,
      }));

    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {

    } else if (output.type === 'removed') {

    } else if (output.type === 'dragOver') {
      this.drag360Over = true;
    } else if (output.type === 'dragOut' || output.type === 'drop') {
      this.drag360Over = false;
    } else if (output.type === 'rejected' && typeof output.file !== 'undefined') {
      console.log(output.file.name + ' rejected');
    } else if (output.type === 'done') {
      if (output.file.responseStatus === 200) {
        this.listing[360] = this.listing[360].map((photo) => {
          if (photo.upload_file && photo.upload_file.id === output.file.id) {
            output.file.response.upload_file = output.file;
            return new PhotoModel().deserialize(output.file.response);
          }
          return photo;
        });
      } else if ('errorMsg' in output.file.response) {
        const index = this.listing[360].findIndex((photo) => photo.upload_file && photo.upload_file.id === output.file.id);
        this.listing[360].splice(index);
        this.alertService.showError(output.file.response.errorMsg);
      }
      if (!this.getPending360Photos().length) {
        this.close360Modal();
      }
    }
  }

  uploadPending360Photo(photo: PhotoModel, replace = false) {
    photo.loading = true;
    if (replace) {
      const old_photo = this.listing[360].find(p => {
        return p.media_item_id && p.properties.room_id === photo.properties.room_id;
      });
      this.deletePhoto(old_photo, () => {
        this._uploading360Photo(photo);
      });
    } else {
      this._uploading360Photo(photo);
    }
  }

  private _uploading360Photo(photo) {
    const event: UploadInput = {
      type: 'uploadAll',
      url: environment.agent_base_url + `preview-listings-360/${this.listing.advertisement_id}/${photo.properties.room_id}`,
      method: 'POST',
      fieldName: 'photo',
      id: photo.upload_file.id,
      data: {},
      headers: {
        'Adm-App-Auth': environment.app_auth_header,
        'Adm-Session-ID': localStorage.getItem(config.access_token_local_storage)
      }
    };
    this.upload360Input.emit(event);
  }

  checkUniqRoom(photo: PhotoModel) {
    if (!photo.properties.room_id) {
      return true;
    }

    return this.listing[360].filter(p => photo.properties.room_id === p.properties.room_id).length === 1;
  }

  removePanding360(photo: PhotoModel) {
    const index = this.listing[360].indexOf(photo);

    if (index > -1) {
      this.listing[360].splice(index, 1);
    }

    if (!this.getPending360Photos().length) {
      this.close360Modal();
    }
  }

  getUploaded360() {
    return this.listing[360].filter(p => this.isUploaded(p));
  }

  isUploaded(photo: PhotoModel) {
    return !!photo.media_item_id;
  }

  getPending360Photos() {
    return this.listing[360].filter(p => !this.isUploaded(p));
  }

  upload360Modal() {
    if (!this.getPending360Photos().length) return;

    this.modal360Ref = this.modalService.show(this.upload360modal, { ignoreBackdropClick: true, keyboard: false });
  }

  close360Modal() {
    this.upload360input.nativeElement.value = '';
    this.modal360Ref.hide();

    this.listing[360].forEach(photo => {
      if (!this.isUploaded(photo)) {
        this.removePanding360(photo);
      }
    });
  }

  confirmDeletion(template, photo: PhotoModel) {
    this.photo_for_delete = photo;
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  decline() {
    this.photo_for_delete = null;
    this.modalRef.hide();
  }

  deletePhoto(photo, callback = null) {
    if (this.modalRef) this.modalRef.hide();

    photo.loading = true;
    this.listingService.removePhoto(this.listing.advertisement_id, photo.media_item_id).subscribe((data) => {
      if (photo.type === 'image') {
        const index = this.listing.photos.indexOf(photo);
        if (index > -1) {
          this.listing.photos.splice(index, 1);
        }
      } else if (photo.type === '360') {
        const index = this.listing[360].indexOf(photo);
        if (index > -1) {
          this.listing[360].splice(index, 1);
        }
      }

      if (callback) {
        callback();
      }

    }, (errorData) => {
      photo.loading = false;

      if (errorData.status === 404) {
        this.alertService.showError(`Error: Photo not found`);
      } else {
        this.alertService.showError(`Error: Cannot remove the photo`);
      }
    });
  }

  readImage(file: any) {
    file.loading = true;
    const reader = new FileReader();
    reader.onload = (event: any) => file.url = event.target.result;
    reader.readAsDataURL(file.nativeFile);
    file.loading = false;
  }

  openPhotos(photo: PhotoModel) {
    $.magnificPopup.open({
      items: this.listing.getPhotosUrl(),
      type: 'image',
      gallery: {
        enabled: true
      },
      callbacks: {
        open: () => {
          $.magnificPopup.instance.goTo(this.listing.photos.indexOf(photo));
        }
      },
    });
  }

  open360Photos(photo: PhotoModel) {
    $.magnificPopup.open({
      items: this.listing.get360PhotosUrl(),
      type: 'image',
      gallery: {
        enabled: true
      },
      callbacks: {
        open: () => {
          $.magnificPopup.instance.goTo(this.listing[360].indexOf(photo));
        }
      },
    });
  }

  changeRoom(photo: PhotoModel) {
    photo.loading = true;

    this.listingService.movePhoto(this.listing.advertisement_id, photo.media_item_id, photo.properties.room_id).subscribe(() => {
      photo.loading = false;
    });
  }

  markAsFeatured(photo: PhotoModel) {

    photo.loading = true;

    this.listingService.markPhotoAsFeatured(this.listing.advertisement_id, photo.media_item_id).subscribe(() => {

      photo.loading = false;

      this.listing.photos.forEach(p => p.properties.featured = false);
      photo.properties.featured = true;

    }, (errorData) => {

      if (errorData.status === 404) {
        this.alertService.showError(`Error: Photo not found`);
      } else {
        this.alertService.showError(`Error: Cannot mark the photo`);
      }
    });
  }

  markAsFloorplan(photo: PhotoModel) {

    photo.loading = true;

    this.listingService.markPhotoAsFloorplan(this.listing.advertisement_id, photo.media_item_id).subscribe(() => {

      photo.loading = false;

      photo.properties.role = 'floorplan';

    }, (errorData) => {

      if (errorData.status === 404) {
        this.alertService.showError(`Error: Photo not found`);
      } else {
        this.alertService.showError(`Error: Cannot mark the photo`);
      }
    });
  }

  unMarkAsFloorplan(photo: PhotoModel) {

    photo.loading = true;

    this.listingService.unmarkPhotoAsFloorplan(this.listing.advertisement_id, photo.media_item_id).subscribe(() => {

      photo.loading = false;
      photo.properties.role = '';

    }, (errorData) => {

      if (errorData.status === 404) {
        this.alertService.showError(`Error: Photo not found`);
      } else {
        this.alertService.showError(`Error: Cannot unmark the photo`);
      }
    });
  }

  disablePhoto(photo: PhotoModel) {
    photo.loading = true;

    photo.properties.enabled = false;

    this.syncPhotosOrder(() => {
      photo.loading = false;
    });
  }

  enablePhoto(photo: PhotoModel) {
    photo.loading = true;

    photo.properties.enabled = true;

    this.syncPhotosOrder(() => {
      photo.loading = false;
    });
  }

  isFloorplan(photo: PhotoModel) {
    return photo.properties.role === 'floorplan';
  }

  isNotFloorplan(photo: PhotoModel) {
    return photo.properties.role !== 'floorplan';
  }

  is360(photo: PhotoModel) {
    return this.listing[360].indexOf(photo) > -1;
  }

  syncPhotosOrder(callback?) {
    const ids = this.listing.photos.filter(photo => photo.properties.enabled).map(photo => photo.media_item_id);

    this.listingService.updateActivePhotos(this.listing.advertisement_id, ids).subscribe((data) => {
      if (typeof callback === 'function') {
        callback(data);
      }
    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError(`Error: Photo not found`);
      } else {
        this.alertService.showError(`Error: Cannot move the photo`);
      }
      this.listing.photos.concat([]);
    });
  }

  moveEnabledPhoto($event) {
    const index = this.listing.photos.indexOf($event.value);

    this.listing.photos[index].properties.enabled = true;
    $event.value.loading = true;

    /** move photo*/
    this.listing.photos.splice($event.dropIndex, 0, this.listing.photos.splice(index, 1)[0]);

    this.syncPhotosOrder(() => $event.value.loading = false);
  }

  moveDisabledPhoto($event) {
    const index = this.listing.photos.indexOf($event.value);

    if (!this.listing.photos[index].properties.enabled) return;

    this.listing.photos[index].properties.enabled = false;
    $event.value.loading = true;

    /** move photo*/
    this.listing.photos.splice($event.dropIndex, 0, this.listing.photos.splice(index, 1)[0]);

    this.syncPhotosOrder(() => $event.value.loading = false);
  }

}
