import { Component, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ListingService } from '../services/listing.service';
import { ListingOptionsModel } from '../models/listing-options.model';
import { ActivatedRoute } from '@angular/router';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { ListingPreviewModel } from '../models/listing-preview.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { isEqual, differenceWith } from 'lodash';
import { SearchService } from '../../search/services/search.service';
import { CalendarService } from '../services/calendar.service';
import { OpenHouseModel } from '../models/open-house.model';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { BuildingDetailsModel } from '../../building/models/building-details.model';
import { ConcessionDefinitionModel } from '../models/concession-definition.model';

declare var $: any;

@Component({
  selector: 'app-edit-listing',
  templateUrl: './edit-listing.component.html',
  styleUrls: ['./edit-listing.component.less']
})
export class EditListingComponent implements OnInit, OnDestroy, OnChanges {

  listing_options: ListingOptionsModel;
  listing: ListingPreviewModel;
  building: BuildingDetailsModel;
  loading = true;
  publishing = false;
  advertising = false;
  saving = false;
  listingForm: FormGroup;
  selected_tab = 'data';
  submitted = false;
  origin_form_data: any;
  has_changes = false;
  delete_reasons: { id: string; label: string; type: string }[] = [];
  internalCalendarItems: OpenHouseModel[];
  externalCalendarItems: OpenHouseModel[];
  show_open_house_form = false;
  calendarTimes = [
    '12:00 am', '1:00 am', '2:00 am', '3:00 am', '4:00 am', '5:00 am', '6:00 am', '7:00 am', '8:00 am', '9:00 am', '10:00 am', '11:00 am',
    '12:00 pm', '1:00 pm', '2:00 pm', '3:00 pm', '4:00 pm', '5:00 pm', '6:00 pm', '7:00 pm', '8:00 pm', '9:00 pm', '10:00 pm', '11:00 pm',
  ];
  newOpenHouse = new OpenHouseModel();
  dateBsConfig: Partial<BsDatepickerConfig> = { dateInputFormat: 'MMMM D YYYY', containerClass: 'theme-blue' };
  adding = false;

  commissionType: string;
  opMonthsValid: boolean;
  previousAmount: number | string;
  concessionLoading: boolean;

  concessionText: string = '';
  netEffectivePrice: number;

  private exposures: {
    'N': boolean, 'S': boolean, 'W': boolean, 'E': boolean } = {
    'N': false,   'S': false,   'W': false,   'E': false };

  constructor(private listingService: ListingService,
              private calendarService: CalendarService,
              private searchService: SearchService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private alertService: BootstrapAlertService) {
    this.listingService.getListingOptions().subscribe(option => {

      this.listing_options = option;

      this.route.params.subscribe(params => {

        this.loading = true;

        const listing_id = params['listing_id'] || 0;
        window.scroll(0, 0);

        this.initListing(listing_id);
      });
    });
    this.route.fragment.subscribe(fragment => {
      if (['media', 'data'].indexOf(fragment) > -1) this.selected_tab = fragment;
    });
  }

  initListing(listing_id: number) {
    this.listingService.getPreviewListing(+listing_id).subscribe((item) => {

      this.listing = item;

      this.searchService.getBuilding(this.listing.building_id).subscribe((item) => {
        this.building = item;

        this.loading = false;

        this.initExposures();

        this.initForm();

        this.initCalendar();

        if (this.listing.ad_type === 'rental') {
          this.initConcessionData();
        }
      })

    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError('Listing not found');
      } else {
        this.alertService.showError('Error: Cannot show the listing');
      }

      this.loading = false;
    });
  }

  initCalendar() {
    this.calendarService.getOpenHouse(this.listing.advertisement_id).subscribe(items => {
      this.internalCalendarItems = items.filter(i => i.type === 'internal');
      this.externalCalendarItems = items.filter(i => i.type === 'external');
    });
  }

  initExposures() {
    const exposures = this.listing.rooms_details.find(r => r.hasOwnProperty('exposures')).exposures;

    if (!exposures) return;

    const mapfn = (e) => this.exposures[e] = true;

    if (typeof exposures === 'string') {
      exposures.split(',').map(mapfn);
    } else {
      exposures.map(mapfn);
    }
  }

  initForm() {
    this.listingForm = this.fb.group({
      basic: this.fb.group({
        price: [this.listing.price, Validators.required],
        description: [this.listing.description, Validators.required],
        access_type: [this.listing.access_type, Validators.required],
        furnished_type: [this.listing.furnished_type, Validators.required],
        fee_structure: [this.listing.fee_structure],
        commission_structure: [this.listing.commission_structure],
        monthly_taxes: [this.listing.monthly_taxes],
        common_charges: [this.listing.common_charges],
        available_from: [this.listing.available_from],
        first_showing_date: [this.listing.first_showing_date],
        min_lease_period: [this.listing.min_lease_period],
        max_lease_period: [this.listing.max_lease_period],
        nofee: [this.listing.nofee],
        co_exclusive: [this.listing.co_exclusive],
        include_in_rls: [this.listing.include_in_rls]
      }),
      concession_definition: this.fb.group({
        op_months: [this.listing.concession_definition.op_months],
        op_amount: [this.listing.concession_definition.op_amount],
        and_or: [this.listing.concession_definition.and_or],
        free_months: [this.listing.concession_definition.free_months],
        free_on_lease: [this.listing.concession_definition.free_on_lease]
      }),
      rooms: this.fb.array(this._addRooms())
    });

    this.origin_form_data = JSON.parse(JSON.stringify(this.listingForm.value));
    this.has_changes = false;

    this.listingForm.valueChanges.subscribe((value) => {

      this.has_changes = !isEqual(this.origin_form_data, value);
    });
  }

  private _addRooms() {

    const rooms: FormGroup[] = [];

    this.listing.rooms_advice.forEach((room, index) => {

      const fields = this.listing_options.getRoomFields(room.room_type);
      const room_fields = {};
      fields.forEach(field => {
        if (field.editor_type === 'stars') {
          if (!(field.field in room_fields)) {
            room_fields[field.field] = this.fb.group({});
          }
          field.options.forEach(option => {
            room_fields[field.field]
              .addControl(option, this.fb.control(this.listing.getFieldValue(room.room_type, field, option), Validators.required));
          });
        } else {
          room_fields[field.field] = [this.listing.getFieldValue(room.room_type, field)];
        }
      });

      rooms.push(this.fb.group(room_fields));
    });

    return rooms;
  }

  checkedCheckbox(type, index, field, value) {
    const field_control = this.listingForm.get(`${type}.${index}.${field}`);
    const array = field_control.value;

    if (array.indexOf(value) === -1) {
      array.push(value);
      field_control.patchValue(array);
      field_control.markAsDirty();
    }
  }

  uncheckedCheckbox(type, index, field, value) {
    const field_control = this.listingForm.get(`${type}.${index}.${field}`);
    const array: string[] = field_control.value;
    const i = array.indexOf(value);

    if (i > -1) {
      array.splice(i, 1);
    }
    field_control.patchValue(array);
    field_control.markAsDirty();
  }

  generateDescription() {
    this.loading = true;

    this.listingService.getListingDecription(this.listing).subscribe(res => {
      this.listingForm.get('basic.description').patchValue(res.description);

      this.loading = false;
    }, () => {
      this.alertService.showError('Unable to update description');
      this.loading = false;
    })
  }

  saveListing() {
    this.submitted = true;
    this.saving = true;
    if (this.listingForm.get('basic').dirty) {
      this.loading = true;
      const data = {
        ...this.listingForm.get('basic').value,
        concession_definition: this.listingForm.get('concession_definition').value
      };
      this.listingService.updateListing(this.listing.advertisement_id, data).subscribe((listing) => {
        this.listing = listing;
        this.listingForm.get('basic').markAsPristine();
        this.loading = false;

        this.alertService.showSucccess('Listing successfully saved');
        this.submitted = false;

      }, (errorData) => {
        if (errorData.status === 404) {
          this.alertService.showError('Listing not found');
        } else {
          this.alertService.showError('Error: Cannot save the listing');
        }

        this.loading = false;
      });
    }

    this.listing.rooms_advice.forEach((room, index) => {
      const room_control = this.listingForm.get('rooms.' + index);
      if (room_control.dirty) {
        this.loading = true;
        this.listingService.updateRoom(this.listing.advertisement_id, Object.assign(room, room_control.value)).subscribe(data => {
          room_control.patchValue(data);
          room_control.markAsPristine();
          this.loading = false;

          this.alertService.showSucccess(`${room.title} successfully saved`);

        }, (errorData) => {
          if (errorData.status === 404) {
            this.alertService.showError(`Error: ${room.title} not found`);
          } else {
            this.alertService.showError(`Error: Cannot save the ${room.title} info`);
          }

          this.loading = false;
        });
      }
    });

  }

  unpublishListing(reasone) {
    this.publishing = true;
    this.listingService.deleteListing(this.listing, reasone.id).subscribe(() => {
      this.publishing = false;
      this.listing.published = 0;
    }, () => this.publishing = false);
  }

  loadingDeleteReason() {
    if (!this.delete_reasons.length) {
      this.searchService.getFormOptions().subscribe((data) => {
        this.delete_reasons = data.status_change_reasons[this.listing.ad_type];
      });
    }
  }

  publishListing() {
    this.publishing = true;
    this.listingService.publish(this.listing.advertisement_id).subscribe(() => {
      this.publishing = false;
      this.listing.published = 1;
    }, () => this.publishing = false);
  }

  advertise() {
    this.advertising = true;
    this.listingService.advertise(this.listing.advertisement_id).subscribe(() => {
      this.listing.advertised = 1;
      this.advertising = false;
    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError(`Error: the listing not found`);
      } else {
        this.alertService.showError(errorData.error.errorMsg);
      }

      this.advertising = false;
    });
  }

  removeAdvertise() {
    this.advertising = true;
    this.listingService.removeAdvertise(this.listing.advertisement_id).subscribe(() => {
      this.listing.advertised = 0;
      this.advertising = false;
    }, (errorData) => {
      if (errorData.status === 404) {
        this.alertService.showError(`Error: the listing not found`);
      } else {
        this.alertService.showError(errorData.error.errorMsg);
      }

      this.advertising = false;
    });
  }

  ngOnInit() {
    $('.page-content').addClass('page-wizard');
    $('.page-content').addClass('wizardbg');
  }

  ngOnDestroy() {
    $('.page-content').removeClass('page-wizard');
    $('.page-content').removeClass('wizardbg');
  }

  resetForm() {
    this.initListing(+this.listing.advertisement_id);
    this.submitted = false;
  }

  setNoFee(value) {
    this.listingForm.controls.basic.get('nofee').patchValue(value);
    this.listingForm.controls.basic.get('nofee').markAsDirty();
  }

  setCoExclusive(value) {
    this.listingForm.controls.basic.get('co_exclusive').patchValue(value);
    this.listingForm.controls.basic.get('co_exclusive').markAsDirty();
  }

  setIncludeInRLS(value) {
    this.listingForm.controls.basic.get('include_in_rls').patchValue(value);
    this.listingForm.controls.basic.get('include_in_rls').markAsDirty();
  }

  toggleExposure(exposure: string) {
    this.exposures[exposure] = !this.exposures[exposure];

    const selectedExposures = Object.keys(this.exposures).filter(e => this.exposures[e]);

    this
      .listingForm
      .controls
      .rooms['controls']
      .find((c: FormGroup) => c.controls.hasOwnProperty('exposures'))
      .patchValue(selectedExposures.join(','));
  }

  exposureSelected(exposure: string): boolean {
    return this.exposures[exposure];
  }

  addNewOpenHouse() {

    this.newOpenHouse.submitted = true;

    if (!this.newOpenHouse.date || !this.newOpenHouse.time_from || !this.newOpenHouse.time_to) {
      return;
    }

    this.adding = true;

    this.calendarService.addDate(this.newOpenHouse.prepareForRequest(this.listing.advertisement_id)).subscribe((record) => {
      this.adding = false;

      this.initCalendar();

      this.submitted = false;
      this.show_open_house_form = false;
      this.newOpenHouse = new OpenHouseModel();
    }, (errorData) => {

    });
  }

  editOpenHouse(item: OpenHouseModel) {

    item.submitted = true;

    if (!item.date || !item.time_from || !item.time_to) {
      return;
    }

    item.editing = true;

    this.calendarService.editDate(item.open_house_id, item.prepareForRequest(this.listing.advertisement_id)).subscribe((record) => {
      item.editing = false;

      this.initCalendar();

      item.submitted = false;
      item.edit_state = false;
    }, (errorData) => {

    });
  }

  removeOpenHouse(item: OpenHouseModel) {
    item.deleting = true;
    this.calendarService.deleteDate(item.open_house_id).subscribe(() => {
      item.deleting = false;

      this.initCalendar();
    });
  }

  // Concession specific

  initConcessionData() {
    const cd = this.listingForm.get('concession_definition').value;

    if (cd.op_months == '0' || !cd.op_months) {
      this.commissionType = 'amount';
      this.previousAmount = cd.op_months;
    } else {
      this.commissionType = 'months';
      this.previousAmount = cd.op_amount;

      this.validateOpMonths({ target: { value: cd.op_months }});
    }

    this.updateConcessionModel();

    // this.toggleCommissionType({ target: { value: this.commissionType } });
  }


  toggleCommissionType(evt) {
    const newCommissionType = evt.target.value;
    const cd = this.listingForm.get('concession_definition');

    switch (newCommissionType) {
      case 'amount':
        this.previousAmount = cd['controls'].op_months.value || "0";
        cd['controls'].op_months.patchValue("0");
        cd['controls'].op_amount.patchValue(this.previousAmount);

        break;
      case 'months':
        this.previousAmount = cd['controls'].op_amount.value || "0";
        cd['controls'].op_amount.patchValue("0");
        cd['controls'].op_months.patchValue(this.previousAmount);

        break;
    }

    this.validateOpMonths({ target: { value: cd['controls'].op_months.value }});
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateConcessionModel();
  }

  updateConcessionModel() {
    if (!this.listingForm || this.listingForm.get('concession_definition').invalid) return;

    const price = this.listingForm.controls.basic.get('price').value || 0;

    const base = {
      price: price,
      concession_definition: this.listingForm.get('concession_definition').value
    };

    const cm: ConcessionDefinitionModel = new ConcessionDefinitionModel().deserialize(base);

    // Quickfix
    if (cm.concession_definition.op_amount === null) cm.concession_definition.op_amount = '0';
    if (cm.concession_definition.op_months === null) {
      cm.concession_definition.op_months = '0';
      this.validateOpMonths({ target: { value: cm.concession_definition.op_months }});
    }

    this.concessionLoading = true;
    this.listingService.getConcessionText(cm).subscribe((data) => {
      this.concessionText = data.concession_text;
      this.netEffectivePrice = data.net_effective_price;
      this.concessionLoading = false;
    });
  }

  validateOpMonths(event: any) {
    const val: string = `${event.target.value}`;
    const allowed: string[] = ["0", "0.5", "1", "1.5", "2"];

    this.opMonthsValid = allowed.includes(val);
  }

  getFreeMonths(): string[] {
    let months: string[] = [];

    for (let i = 0; i <= 5; i = i + 0.5) {
      months = [...months, `${i}`];
    }

    return months;
  }

  getMonthsLease(): string[] {
    let months: string[] = [];

    for (let i = 12; i <= 36; i++) {
      months.push(`${i}`);
    }

    return months;
  }

}
