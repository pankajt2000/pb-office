import { Injectable } from "@angular/core";
import { AgentApiService } from "../../services/api/agent-api.service";

@Injectable()
export class PerformanceService {

  constructor(private api: AgentApiService) {}

  getPerformance(type: string, agent_id: string, provider: string, since: string) {
    return this.api.get(`performance?type=${type}&agent_id=${agent_id}&provider=${provider}&since=${since}`);
  }

}
