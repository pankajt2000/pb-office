import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerformanceComponent } from './overview/performance.component';
import { SharedModule } from "../shared/shared.module";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { PerformanceService } from "./services/performance.service";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NgxChartsModule
  ],
  providers: [PerformanceService],
  declarations: [PerformanceComponent],
  exports: [
    PerformanceComponent
  ]
})
export class PerformanceModule { }
