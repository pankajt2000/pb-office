import { Component, OnInit } from '@angular/core';
import { PerformanceService } from "../services/performance.service";
import { ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";
import { AgentService } from "../../agent/services/agent.service";

@Component({
  selector: 'app-performance',
  templateUrl: './performance.component.html',
  styleUrls: ['./performance.component.less']
})
export class PerformanceComponent implements OnInit {

  responseTimeYAxisValues = [60*5, 60*30, 60*60, 60*60*2, 60*60*3, 60*60*4];

  colorScheme = {
    domain: ['#5899DA', '#E8743B', '#19A979', '#ED4A7B', '#945ECF', '#13A4B4', '#525DF4', '#BF399E', '#6C8893', '#EE6868', '#2F6497']
  };

  loading: boolean = false;
  type: string = '';

  charts = {
    inquiry_count: null,
    response_times: null
  };

  filterForm: FormGroup;
  agent_options: Array<{ name: string, user_id: string }> = [];

  constructor(
    private performanceService: PerformanceService,
    private agentService: AgentService,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.loading = true;

    this.route.params.subscribe(params => {
      this.type = params['type'] || 'rental';

      this.charts.response_times = null;
      this.charts.inquiry_count = null;

      if (!this.loading) {
        this.loading = true;
      }

      this.agentService.getAgents().subscribe(data => {
        this.initForm();
        this.agent_options = data[0].map(agent => ({ name: agent.name, user_id: agent.user_id }));

        this.initCharts();
      });
    })
  }

  initCharts() {
    if (!this.loading) this.loading = true;

    const agent_id = this.filterForm.get('agent_id').value;
    const provider = this.filterForm.get('provider').value;
    const since = this.filterForm.get('since').value;

    this.performanceService.getPerformance(this.type, agent_id, provider, since).subscribe(data => {
      for (const segment in data) {
        const columns = data[segment].columns;
        const rows = data[segment].rows;

        let chart_data = [];

        for (let d in data[segment].data) {
          const item = data[segment].data[d];
          const col = columns.find(x => x.id === d);

          if (col) {
            const line = {
              name: col.label,
              series: rows.map(x => ({
                name: x.label,
                value: item[x.id] ? item[x.id].value : 0
              }))
            };

            chart_data.push(line);
          }
        }

        this.charts[segment] = {
          legend: true,
          data: chart_data
        };
      }

      this.loading = false;
    });
  }

  initForm() {
    this.filterForm = this.fb.group({
      agent_id: ['-1'],
      provider: ['no_test'],
      since: ['last_month']
    })
  }

  formatAsTime(val: any, precise: boolean = false) {
    switch (val) {
      case 0:
        if (precise) {
          return '0';
        }
        /* falls through */
      case 300:
        if (precise) {
          return '5m';
        }
        return '0 - 5m';
      case 1800:
        return '30m';
      case 3600:
        return '1h';
      case 7200:
        return '2h';
      case 10800:
        return '3h';
      default:
        return '3h+';
    }
  }

}
