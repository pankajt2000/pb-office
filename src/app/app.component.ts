import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './authentication/services/auth.service';
import { UserService } from './authentication/services/user.service';
import { BootstrapAlertService, ToastMessageModel } from 'ngx-bootstrap-alert-service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { environment } from '../environments/environment';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, OnDestroy {

  loaded: boolean = false;

  autoClearInterval = null;
  messageList: ToastMessageModel[] = [];

  constructor(private authService: AuthService,
              private userService: UserService,
              private bootstrapAlertService: BootstrapAlertService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private titleService: Title
  ) {
  }

  ngOnDestroy() {
    if (this.autoClearInterval) {
      clearInterval(this.autoClearInterval);
    }
  }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.authService.sessionInfo().subscribe(
        (data) => {
          this.userService.setUser(data.user);
          this.loaded = true;
        }, error => {
          this.loaded = true;
          this.authService.logout();
        });
    } else {
      this.loaded = true;
    }

    this.bootstrapAlertService.getAlertEvent().subscribe(r => {
      this.messageList.push(r);

      if (this.autoClearInterval) {
        clearInterval(this.autoClearInterval);
      }

      this.autoClearInterval = setInterval(() => {
        if (this.messageList.length > 0) {
          this.messageList.shift();
        }
      }, 3500);
    });

    this.router.events
      .filter((event) => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map((route) => {
        while (route.firstChild) route = route.firstChild;
        return route;
      })
      .filter((route) => route.outlet === 'primary')
      .mergeMap((route) => route.data)
      .subscribe((event) => {
        if ('title' in event) {
          this.titleService.setTitle(`${event['title']} - ${environment.app_title}`);
        } else {
          this.titleService.setTitle(environment.app_title);
        }
      });
  }
}
