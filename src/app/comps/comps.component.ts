import { Component, OnInit } from '@angular/core';
import { CompsOverviewModel } from "./models/comps-overview.model";
import { CompsService } from "./services/comps.service";
import { CurrencyMaskConfig } from "ngx-currency/src/currency-mask.config";
import { Router } from "@angular/router";
import { AuthService } from "../authentication/services/auth.service";
import { CompsListingModel } from "./models/comps-listing.model";
import { Subject } from "rxjs";
import { debounceTime, distinctUntilChanged, switchMap, tap } from "rxjs/operators";
import { BuildingService } from "../building/services/building.service";
import { BootstrapAlertService } from "ngx-bootstrap-alert-service";
import { CompsBuildingModel } from "./models/comps-building.model";

@Component({
  selector: 'app-comps',
  templateUrl: './comps.component.html',
  styleUrls: ['./comps.component.less']
})
export class CompsComponent implements OnInit {

  buildings_autocomplete = {
    values: [],
    typeahead: new Subject<string>(),
    loading: false,
  };

  buildings_to_add = {};

  loading: boolean = false;
  comps_loading: boolean = false;
  comps: CompsOverviewModel;

  is_supervisor: boolean = false;

  open_building_accordion_group: string;

  priceMaskConfig: CurrencyMaskConfig = {
    align: 'left',
    allowNegative: false,
    allowZero: true,
    decimal: '.',
    precision: 0,
    prefix: '$ ',
    suffix: '',
    thousands: ',',
    nullable: true
  };

  constructor(
    private compsService: CompsService,
    private authService: AuthService,
    private buildingService: BuildingService,
    private alertService: BootstrapAlertService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loading = true;

    this.authService.sessionInfo().subscribe(info => {
      this.is_supervisor = info.user.roles.map(r => r.toLowerCase()).includes('supervisor');

      this.initTypeahead();
      this.initComps();
    })
  }

  initTypeahead() {
    this.buildings_autocomplete.values = [];
    this.buildings_autocomplete.typeahead = new Subject<string>();

    this.buildings_autocomplete.typeahead.pipe(
      tap(() => this.buildings_autocomplete.loading = true),
      distinctUntilChanged(),
      debounceTime(200),
      switchMap(term => this.buildingService.getBuildings(term, null, null))
    ).subscribe(x => {
      const values = x[0];

      this.buildings_autocomplete.values = values.map(building => {
        return {
          name: `${building.address}`,
          value: `${building.building_id}`
        }
      });
      this.buildings_autocomplete.loading = false;
    });
  }

  initComps() {
    this.compsService.getComps().subscribe(comps => {
      this.comps = comps;

      if (this.loading) {
        this.loading = false;
      }
    });
  }

  search(listing: CompsListingModel) {
    const listing_ids = this.comps.comps.find(c => c.building_id === listing.building_id).comps_listings;
    const queryParams = {
      furnished: 'All',
      building_type: listing.search.building_type,
      min_price: listing.search.min_price,
      max_price: listing.search.max_price,
      pets: listing.search.pets,
      listing_ids: listing_ids.join(','),
      show_form: false
    };

    if ( listing.search.ad_type === 'rental') {
      this.router.navigate(['/search/rental'], { queryParams });
    } else {
      this.router.navigate(['/search/sale'], { queryParams });
    }
    
  }

  getFilteredCompsListings(): CompsListingModel[] {
    if (this.is_supervisor) {
      return this.comps.base_listings;
    }

    return this.comps.base_listings.filter(l => this.getCompsListingsCount(l) > 0);
  }

  getFilteredCompsBuildings(): CompsBuildingModel[] {
    return this.comps.comps.filter(b => this._showCompBuilding(b));
  }

  addCompsToBuilding(building_id) {
    if (!this.buildings_to_add[building_id]) {
      return;
    }

    this.comps_loading = true;

    this.compsService.addComps(building_id, this.buildings_to_add[building_id]).subscribe(_ => {
      this.initTypeahead();
      this.initComps();

      this.comps_loading = false;
    }, err => {
      this._handleError(err);
      this.comps_loading = false;
    })
  }

  deleteComp(building_id, comp_id) {
    this.comps_loading = true;

    this.compsService.deleteComps(building_id, comp_id).subscribe(_ => {
      this.initTypeahead();
      this.initComps();

      this.comps_loading = false;
    }, err => {
      this._handleError(err);
      this.comps_loading = false;
    });
  }

  getCompsListingsCount(listing: CompsListingModel): number {
    return this.comps.comps.find(comp => comp.building_id === listing.building_id).comps_listings.length;
  }

  private _showCompBuilding(building: CompsBuildingModel): boolean {
    return this.is_supervisor || building.comps_listings.length > 0;
  }

  private _handleError(error) {
    if (error.status < 500) {
      this.alertService.showError(error.error.errorMsg);
    } else {
      this.alertService.showError('An unexpected error occurred.');
    }
  }

}
