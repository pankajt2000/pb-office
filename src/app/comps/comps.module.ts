import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompsComponent } from './comps.component';
import { CompsService } from "./services/comps.service";
import { SharedModule } from "../shared/shared.module";
import { AccordionModule } from "ngx-bootstrap";
import { NgxCurrencyModule } from 'ngx-currency';
import { RouterModule } from "@angular/router";

@NgModule({
  imports: [
    NgxCurrencyModule,
    AccordionModule.forRoot(),
    RouterModule,

    CommonModule,
    SharedModule
  ],
  declarations: [CompsComponent],
  providers: [
    CompsService
  ]
})
export class CompsModule { }
