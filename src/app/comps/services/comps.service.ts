import { Injectable } from "@angular/core";
import { AgentApiService } from "../../services/api/agent-api.service";
import { CompsOverviewModel } from "../models/comps-overview.model";
import { CompsListingModel } from "../models/comps-listing.model";

@Injectable()
export class CompsService {

  constructor(private api: AgentApiService) {}

  getComps() {
    return this.api.get(`comps`).map(res => new CompsOverviewModel().deserialize(res));
  }

  getListingComps(listing_id: string) {
    return this.api.get(`comps/${listing_id}`).map(res => new CompsListingModel().deserialize(res));
  }

  addComps(building_id: string, building_ids: string[]) {
    if (!Array.isArray(building_ids)) {
      building_ids = [building_ids];
    }

    return this.api.post(`comps/${building_id}`, { building_ids });
  }

  deleteComps(building_id: string, building_ids: string[]) {
    if (!Array.isArray(building_ids)) {
      building_ids = [building_ids];
    }

    return this.api.delete(`comps/${building_id}`, { body: { building_ids } });
  }

}
