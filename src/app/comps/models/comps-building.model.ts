import { DeserializableInterface } from "../../shared/models/deserializable.interface";

export class CompsBuildingModel implements DeserializableInterface<CompsBuildingModel> {

  building_id: string;
  address: string;
  comps_listings: string[];
  comps_buildings: Array<{ building_id: string, address: string, management?: string }>;

  constructor() {}

  deserialize(input: any): CompsBuildingModel {
    Object.assign(this, input);
    return this;
  }

}
