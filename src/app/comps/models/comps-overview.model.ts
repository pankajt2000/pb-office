import { DeserializableInterface } from "../../shared/models/deserializable.interface";
import { CompsListingModel } from "./comps-listing.model";
import { CompsBuildingModel } from "./comps-building.model";

export class CompsOverviewModel implements DeserializableInterface<CompsOverviewModel> {

  base_listings: CompsListingModel[];
  comps: CompsBuildingModel[];

  constructor() {}

  deserialize(input: any): CompsOverviewModel {
    input.base_listings = input.base_listings.map(listing => new CompsListingModel().deserialize(listing));
    input.comps = input.comps.map(comp => new CompsBuildingModel().deserialize(comp));
    Object.assign(this, input);
    return this;
  }

}
