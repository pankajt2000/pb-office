import { DeserializableInterface } from "../../shared/models/deserializable.interface";
import { BedroomAbstract } from "../../shared/models/bedroom.abstract";

export class CompsListingModel extends BedroomAbstract implements DeserializableInterface<CompsListingModel> {
  advertisement_id: string;
  building_id: string;
  address: string;
  unit: string;
  price: string & number;
  bedrooms: string & number;
  bathrooms: string & number;
  search: {
    ad_type: string,
    min_price: number,
    max_price: number,
    building_type: string,
    pets: string
  };

  deserialize(input: any): CompsListingModel {
    Object.assign(this, input);
    return this;
  }

}
