import { Injectable } from '@angular/core';
import { AgentApiService } from '../../../../services/api/agent-api.service';

@Injectable()
export class NotificationService {


  constructor(public api: AgentApiService) {
  }

  getNotifications() {
    return this.api.get('dashboard-notifications');
  }

}
