import { Component, OnInit } from '@angular/core';
import { NotificationService } from './services/notification.service';
import { GmailService } from '../../../mailbox/services/gmail.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.less']
})
export class NotificationComponent implements OnInit {

  notifications: { type: string, title: string }[] = [];

  configuration = {
    'GMAIL_NOT_AUTHORIZED': {
      icon: 'fa-envelope',
      color: 'c-red',
      action: () => {
        this.gmailService.getAuthUrl().subscribe((data) => {
          window.location.href = data.auth_url;
        });
      }
    },
  };

  constructor(private notificationService: NotificationService, private gmailService: GmailService) {
  }

  ngOnInit() {
    this.notificationService.getNotifications().subscribe(data => {
      this.notifications = data;
    });
  }

}
