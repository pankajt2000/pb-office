import { DeserializableInterface } from '../../../shared/models/deserializable.interface';
import { environment } from '../../../../environments/environment';

export class AlertModel implements DeserializableInterface<any> {

  subject: string;
  last_active: number;
  participants: object[];
  unread_count: number;
  last_message: string;
  last_user: {
    user_id: number,
    type: string,
    phone: string,
    email: string,
    last_activity: string,
    name: string,
    photo: string,
  };

  getAvatarUrl() {
    return environment.media_base_url + this.last_user.photo;
  }

  deserialize(input: any): AlertModel {
    Object.assign(this, input);
    return this;
  }
}
