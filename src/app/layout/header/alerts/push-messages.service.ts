import { Injectable } from '@angular/core';
import { AgentApiService } from '../../../services/api/agent-api.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { HttpEvent } from '@angular/common/http';
import { UserService } from '../../../authentication/services/user.service';
import { UserModel } from '../../../authentication/models/user.model';

declare var $: any;

@Injectable()
export class PushMessagesService {

  private last_updated_data: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  private last_connection: BehaviorSubject<Observable<HttpEvent<any>>> = new BehaviorSubject<Observable<HttpEvent<any>>>(null);
  private is_connected: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  private is_active: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  private subscription;
  private user: UserModel;

  constructor(public api: AgentApiService, private userService: UserService) {
    this.rotatePushMessages();

    window.addEventListener('online', () => {
      this.is_connected.next(true);

      this.last_updated_data.next(null);
      if (this.is_active.getValue()) this.rotatePushMessages();
    });

    window.addEventListener('offline', () => {

      setTimeout(() => {
        this.is_connected.next(false);
        if (this.last_connection.getValue()) {
          this.subscription.unsubscribe();
          this.last_connection.next(null);
        }
      }, 300);
    });

    $(window).blur(() => {
      this.is_active.next(false);
    });
    $(window).focus(() => {
      this.is_active.next(true);
    });

    this.is_active.subscribe((status) => {
      if (status && !this.last_connection.getValue() && this.subscription) {
        this.rotatePushMessages();
      }
    });

    this.userService.getUser().subscribe(user => {
      if (!user && this.subscription) {
        this.last_updated_data.next(null);
        this.subscription.unsubscribe();
      }

      if (user && !this.user) {
        this.user = user;
        this.rotatePushMessages();
      } else {
        this.user = user;
      }
    });
  }

  rotatePushMessages() {
    if (!this.user) {
      return;
    }

    const params = this.prepareParams();
    return this.subscription = this.getAlertsCount(params).subscribe((data: any) => {
      this.is_connected.next(true);

      if (this.last_updated_data.value && data) {
        data = this.mergeMailboxes(data);
      }

      this.last_updated_data.next(data);

      this.last_connection.next(null);

      if (this.is_active.getValue()) this.rotatePushMessages();

    }, (errorResponse) => {
      setTimeout(() => {
        if (this.is_active.getValue()) this.rotatePushMessages();
      }, 3000);
      this.is_connected.next(false);
    });
  }

  prepareParams() {
    if (this.last_updated_data.value && 'mailboxes' in this.last_updated_data.value) {
      const params = {};
      this.last_updated_data.value.mailboxes.forEach(label => params[label.id] = label.history_id);

      return '?' + $.param(params);
    } else {
      return '';
    }
  }

  getPushMessages() {
    return this.last_updated_data;
  }

  getAlertsCount(params: string) {
    this.last_connection.next(this.api.get(`dashboard-counts${params}`));

    return this.last_connection.getValue();
  }

  getConnectedStatus() {
    return this.is_connected;
  }

  mergeMailboxes(data) {
    const mailboxes = this.last_updated_data.value.mailboxes;
    for (let j in data.mailboxes) {
      for (let k in mailboxes) {
        if (mailboxes[k].id === data.mailboxes[j].id) {
          mailboxes[k] = data.mailboxes[j];
        }
      }
    }
    data.mailboxes = mailboxes;
    return data;
  }

}
