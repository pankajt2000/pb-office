import { Component, Input, OnInit } from '@angular/core';
import { MessageService } from '../../../mailbox/services/message.service';
import { MessageModel } from '../../../mailbox/models/message.model';
import { ThreadModel } from '../../../mailbox/models/thread.model';
import { PushMessagesService } from './push-messages.service';

@Component({
  selector: 'app-header-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.less']
})
export class AlertsComponent implements OnInit {

  messages: ThreadModel[] = [];
  count: number = 0;
  is_connected = true;

  constructor(public messageService: MessageService, private pushMessagesService: PushMessagesService) {
  }

  ngOnInit() {
    this.getPushMessages();
  }

  getPushMessages() {
    this.messageService.alert_messages.getValue();

    this.pushMessagesService.getPushMessages().subscribe((data) => {
      if (data && 'mailboxes' in data && data.mailboxes.length) {
        const inbox = data.mailboxes.find(l => l.id === 'INBOX');
        if (!this.count) {
          this.count = inbox.messagesUnread;
        } else if (this.count !== inbox.messagesUnread) {
          this.getLastThreads();
        }

        if (this.messageService.alert_messages.getValue().length) {
          this.messages = this.messageService.alert_messages.getValue();
        } else {
          this.getLastThreads();
        }
      }
    });

    this.pushMessagesService.getConnectedStatus().subscribe(status => {
      this.is_connected = status;
    });
  }

  getLastThreads() {
    this.messageService.getThreads('INBOX', 3).subscribe(response => {
      this.messages = response.threads;
      this.messageService.alert_messages.next(response.threads);
    });
  }

}
