import { Component, NgZone, OnInit } from '@angular/core';
import { ClientService } from '../../client/services/client.service';
import { ClientModel } from '../../client/models/client.model';
import { NavigationEnd, Router } from '@angular/router';
import { UserModel } from '../../authentication/models/user.model';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../authentication/services/auth.service';
import { config } from '../../config';
import { UserService } from '../../authentication/services/user.service';
import { PushMessagesService } from './alerts/push-messages.service';

declare var $: any;


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  jump_to_list: { url: any, name: string }[];
  navigate_to_url: any = '';
  user: UserModel;
  impersonating: boolean;
  clients: ClientModel[];
  current_client: ClientModel;
  logo_background: string = environment.logo_background;
  collapsed = false;
  current_url: string;

  constructor(private clientService: ClientService,
              private authService: AuthService,
              private userService: UserService,
              private pushMessagesService: PushMessagesService,
              private ngZone: NgZone,
              private router: Router) {

    this.authService.sessionInfo().subscribe(data => {
      this.user = data.user;

      this.impersonating = data.session.parent_session_id !== null;
    });

    this.current_url = this.router.routerState.snapshot.url;

    this.router.events.subscribe(data => {
      if (data instanceof NavigationEnd) {
        this.current_url = data.url;
      }
    });

    this.clientService.getSidebarColapsed().subscribe((value) => {
      this.collapsed = value;

      if (this.collapsed) {
        $('body').addClass('sidebar-collapsed');
        $('body').addClass('sidebar-show');
      } else {
        $('body').removeClass('sidebar-collapsed');
        $('body').removeClass('sidebar-show');
      }
    });
  }

  ngOnInit() {
    this.clientService.getAllSuggestClients().subscribe((clients: ClientModel[]) => {
      this.clients = clients;

      this.clientService.getCurrentClient().subscribe((client) => {
        this.current_client = client;
        this.regenerateLinks();
      });

    });

  }

  regenerateLinks() {
    this.jump_to_list = [
      { url: ['/mailbox/inbox', this.current_client ? { q: this.current_client.email } : {}], name: 'Mailbox' },
      {
        url: (this.current_client && this.current_client.searches && this.current_client.searches.length) ?
          [`search/criteria/${this.current_client.searches[0].search_id}`] :
          ['search/rental'],
        name: 'Search'
      },
    ];
  }

  isCurrentUserSupervisor() {
    return this.user && this.user.roles.includes('SUPERVISOR');
  }

  updateClient() {
    this.clientService.setCurrentClient(this.current_client);
  }

  navigateTo() {
    this.router.navigate(this.navigate_to_url);
  }

  menutoggle() {
    this.clientService.setSidebarColapsed(!$('body').hasClass('sidebar-show'));
  }

  restoreSession(e) {
    e.preventDefault();
    e.stopPropagation();

    this.authService.restoreSession().subscribe(data => {
      localStorage.setItem(config.access_token_local_storage, data.session.user_session_id);

      this.clientService.getSuggest().subscribe((response) => response);
      this.pushMessagesService.rotatePushMessages();

      this.userService.setUser(data.user);

      this.ngZone.run(_ => { location.href = 'agent/list'; });
    })
  }

}
