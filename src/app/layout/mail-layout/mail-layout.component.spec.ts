import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailLayoutComponent } from './mail-layout.component';

describe('MailLayoutComponent', () => {
  let component: MailLayoutComponent;
  let fixture: ComponentFixture<MailLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
