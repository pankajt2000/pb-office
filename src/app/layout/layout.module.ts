import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { NavigationComponent } from './sidenav/navigation/navigation.component';
import { AlertsComponent } from './header/alerts/alerts.component';
import { PushMessagesService } from './header/alerts/push-messages.service';
import { MailLayoutComponent } from './mail-layout/mail-layout.component';
import { SharedModule } from '../shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { SavedListingsComponent } from '../listings/saved-listings/saved-listings.component';
import { QuickviewComponent } from './quickview/quickview.component';
import { QuickviewService } from './quickview/quickview.service';
import { SavedSearchComponent } from './quickview/saved-search/saved-search.component';
import { RatingModule, TabsModule } from 'ngx-bootstrap';
import { UserDetailsComponent } from './sidenav/user-details/user-details.component';
import { NotificationComponent } from './header/notification/notification.component';
import { NotificationService } from './header/notification/services/notification.service';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    LayoutRoutingModule,
    NgSelectModule,
    TabsModule.forRoot(),
    RatingModule.forRoot(),
    NgxCurrencyModule
  ],
  providers: [
    PushMessagesService,
    QuickviewService,
    NotificationService,
  ],
  declarations: [
    LayoutComponent,
    FooterComponent,
    HeaderComponent,
    SidenavComponent,
    NavigationComponent,
    AlertsComponent,
    MailLayoutComponent,
    SavedListingsComponent,
    QuickviewComponent,
    SavedSearchComponent,
    UserDetailsComponent,
    NotificationComponent,
  ]
})
export class LayoutModule {
}
