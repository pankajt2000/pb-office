import { Component, OnInit, ViewChild } from '@angular/core';
import { environment } from '../../../environments/environment';
import { config } from '../../config';
import { Observable } from 'rxjs/Observable';
import { ScrollbarComponent } from 'ngx-scrollbar';
import 'rxjs/add/observable/fromEvent';
import { AuthService } from '../../authentication/services/auth.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.less']
})
export class SidenavComponent implements OnInit {

  title: string;
  background: string;
  app_version = config.app_version;
  @ViewChild(ScrollbarComponent) scrollRef: ScrollbarComponent;

  constructor(private authService: AuthService) {
    Observable.fromEvent(window, 'resize')
        .debounceTime(200)
        .subscribe(() => {
          if (this.scrollRef) {
            this.scrollRef.update();
          }
        });
  }

  ngOnInit() {
    this.authService.sessionInfo().subscribe(info => {
      this.title = environment.logo_title;
      this.background = info.session.parent_session_id !== null? 'green' : environment.logo_background;
    })
  }

  toggleFullScreen() {

    const doc:any = document;
    const docEl:any = document.documentElement;

    if (!doc.fullscreenElement && !doc.msFullscreenElement && !doc.webkitIsFullScreen && !doc.mozFullScreenElement) {
      if (docEl.requestFullscreen) {
        docEl.requestFullscreen();
      } else if (docEl.webkitRequestFullScreen) {
        docEl.webkitRequestFullscreen();
      } else if (docEl.webkitRequestFullScreen) {
        docEl.webkitRequestFullScreen();
      } else if (docEl.msRequestFullscreen) {
        docEl.msRequestFullscreen();
      } else if (docEl.mozRequestFullScreen) {
        docEl.mozRequestFullScreen();
      }
    } else {
      if (doc.exitFullscreen) {
        doc.exitFullscreen();
      } else if (doc.webkitExitFullscreen) {
        doc.webkitExitFullscreen();
      } else if (doc.webkitCancelFullScreen) {
        doc.webkitCancelFullScreen();
      } else if (doc.msExitFullscreen) {
        doc.msExitFullscreen();
      } else if (doc.mozCancelFullScreen) {
        doc.mozCancelFullScreen();
      }
    }

  }

}
