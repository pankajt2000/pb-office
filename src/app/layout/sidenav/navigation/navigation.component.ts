import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { FolderService } from '../../../mailbox/services/folder.service';
import { BsModalService } from 'ngx-bootstrap';
import { CreateClientComponent } from '../../../client/create-client/create-client.component';
import { ClientService } from '../../../client/services/client.service';
import { UserService } from '../../../authentication/services/user.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.less']
})
export class NavigationComponent implements OnInit {

  selected_item;
  selected_path: string;
  selected_url = '';

  menuList: {
    label: string,
    url?: string,
    action?: () => any,
    icon: string,
    role?: string,
    children?: { label: string, url?: string, role?: string, action?: () => any, selected?: boolean, count?: any }[]
  }[] = [
    {
      label: 'Dashboard',
      url: '/app/dashboard',
      icon: 'icon-home',
    },
    {
      label: 'Mailbox',
      url: '/mailbox',
      icon: 'icon-envelope',
      children: [
        {
          label: 'Send Mail',
          url: '/mailbox/send',
        }
      ],
    },
    {
      label: 'Search Rentals',
      url: '/search/rental',
      icon: 'fa fa-search searchrightbar'
    },
    {
      label: 'Search Sales',
      url: '/search/sale',
      icon: 'fa fa-search searchrightbar'
    },
    {
      label: 'Listings',
      url: '/listing',
      icon: 'fa fa-building searchrightbar',
      children: [
        {
          label: 'Create New',
          url: '/listing/new'
        },
        {
          label: 'My Listings',
          url: '/search/created-by-me',
        },
        {
          label: 'Recently Viewed',
          url: '/listing/recent'
        },
        {
          label: 'Advertised',
          url: '/listing/advertised'
        },
        {
          label: 'Comps',
          url: '/listing/comps'
        }
      ]
    },
    {
      label: 'Clients',
      url: '/client',
      icon: 'fa fa-user',
      children: [
        {
          label: 'Create New',
          action: () => {
            this.modalService.show(CreateClientComponent);
          },
        },
        {
          label: 'My Clients',
          url: '/client/list',
        },
        {
          label: 'Applications',
          url: '/application/list'
        },
        {
          label: 'Deals',
          url: '/client/deals/list'
        },
        {
          label: 'DOS',
          url: '/client/dos/list'
        }
      ]
    },
    {
      label: 'Invoices',
      url: '/invoices/list',
      icon: 'fas fa-signature',
      children: [
        {
          label: 'Rentals',
          url: '/invoices/list/rental'
        },
        {
          label: 'Sales',
          url: '/invoices/list/sale'
        }
      ]
    },
    {
      label: 'Buildings',
      url: '/building/list',
      icon: 'fa fa-home black',
    },
    {
      label: 'Payments',
      url: '/payments/list',
      icon: 'fa fa-dollar-sign',
    },
    {
      label: 'Agents',
      url: '/agent/list',
      icon: 'fa fa-user searchrightbar',
      role: 'SUPERVISOR'
    },
    {
      label: 'Performance',
      icon: 'fas fa-chart-bar',
      role: 'SUPERVISOR',
      children: [
        {
          label: 'Performance Rentals',
          url: '/performance/rental'
        },
        {
          label: 'Performance Sales',
          url: '/performance/sale'
        }
      ]
    },
    {
      label: 'Stats',
      url: '/stats',
      icon: 'icon-envelope',
      children: [
        {
          label: 'Trends Rentals',
          url: '/stats/rental',
        },
        {
          label: 'Trends Sales',
          url: '/stats/sale',
        }
      ],
    },
  ];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private userService: UserService,
              private folderService: FolderService,
              private clientService: ClientService,
              private modalService: BsModalService) {

    this.selected_path = '/' + this.route.snapshot.routeConfig.path;
    this.selected_url = this.router.routerState.snapshot.url;

    this.folderService.featchFolders().subscribe(folders => folders);

    this.router.events.subscribe(data => {
      if (data instanceof NavigationEnd) {
        this.selected_url = data.url;
        this.checkSelectedItem();
      }
    });

    this.folderService.getFolders().subscribe((folders) => {
      /** mailbox has index 1 */
      this.menuList[1].children = [
        {
          label: 'Send Mail',
          url: '/mailbox/send'
        }
      ];
      folders.filter(folder => folder.labelListVisibility !== 'labelHide')
        .forEach(folder => {
          this.menuList[1].children.push({ label: this.ucWord(folder.name), url: '/mailbox/' + folder.id, count: folder.messagesUnread });
        });
      this.checkSelectedItem();
    });
  }

  ngOnInit() {
  }

  toogleItem(item) {
    this.selected_item = this.selected_item !== item ? item : null;
  }

  checkSelectedItem() {
    this.menuList.forEach((item) => {
      if ('children' in item) {
        if (item.url === this.selected_path) {
          this.selected_item = item;
        }

        if (item.children) {
          item.children.forEach(child => {
            child.selected = this.selected_url === child.url;
          });
        }
      }
    });
  }

  shouldShow(item: any): boolean {
    if (!item.role) return true;

    return item.role && this.userService.hasRole(item.role);
  }

  ucWord(word: string) {
    return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
  }

  navigate(item) {

    if (item.action) {
      return item.action();
    }

    this.clientService.setCurrentClient(null);
    item.selected = true;

    if (item.url) {
      this.router.navigate([item.url]);
    }

    this.clientService.setSidebarColapsed(false);
  }

}
