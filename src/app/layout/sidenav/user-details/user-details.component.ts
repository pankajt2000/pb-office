import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../authentication/services/user.service';
import { UserModel } from '../../../authentication/models/user.model';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.less']
})
export class UserDetailsComponent implements OnInit {

  user: UserModel;

  constructor(private userService: UserService) {
    this.userService.getUser().subscribe((user) =>  this.user = user);
  }

  ngOnInit() {
  }

}
