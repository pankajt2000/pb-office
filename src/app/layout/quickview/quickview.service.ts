import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class QuickviewService {

  public opened: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public state: BehaviorSubject<string> = new BehaviorSubject<string>('saved_searches');

  setState(value: string) {
    this.state.next(value);
  }

  getState() {
    return this.state;
  }

  setOpened(value: boolean) {
    this.opened.next(value);
  }

  getOpened() {
    return this.opened;
  }

}
