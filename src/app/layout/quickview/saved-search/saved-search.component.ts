import { CurrencyMaskConfig } from 'ngx-currency/src/currency-mask.config';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit } from '@angular/core';
import { SearchQueryModel } from '../../../search/models/search-query.model';
import { SearchService } from '../../../search/services/search.service';
import { ClientModel } from '../../../client/models/client.model';
import { BaseSearchComponent } from '../../../search/base-search.component';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { Router } from '@angular/router';
import { QuickviewService } from '../quickview.service';
import { isEqual, differenceWith } from 'lodash';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';

declare var $;

@Component({
  selector: 'app-saved-search',
  templateUrl: './saved-search.component.html',
  styleUrls: ['./saved-search.component.less']
})
export class SavedSearchComponent extends BaseSearchComponent implements OnInit {

  priceMaskConfig: CurrencyMaskConfig = {
    align: 'left',
    allowNegative: false,
    allowZero: true,
    decimal: '.',
    precision: 0,
    prefix: '$ ',
    suffix: '',
    thousands: ',',
    nullable: true
  };

  @Input() set client(client: ClientModel) {
    this._client = client;
    this.create = false;

    this.cleanSearchCriteria();

    if (this.formOptions) {
      this.cleanBorough();
    } else {
      return;
    }

    if (this._client && this._client.searches && this._client.searches.length) {
      this.client_search = this._client.searches[0];
      if (this.client_search.search.company_name.length > 0) {
        this.search_companies = this.client_search.search.company_name.map(c => {
          return { name: c, value: c };
        });
      }
      this.model = Object.assign(this.model, this._client.searches[0].search);
      this.formOptions.boroughs.forEach(borough => {
        borough.neighborhoods.forEach((neighborhood => {
          neighborhood.selected = this.model.neighborhood_id.indexOf(neighborhood.neighborhood_id) > -1;
        }));
      });
      // this.formOptions.boroughs = this.formOptions.boroughs;

      this.modelDiff = (JSON.parse(JSON.stringify(this.model)));
    }
  }

  get client() {
    return this._client;
  }

  client_search: any;
  model = null;
  modelDiff = null;
  _client: ClientModel;
  create = false;
  saving = false;
  highlight = false;
  suspending = false;
  saveEventEmiter: EventEmitter<any> = new EventEmitter<any>();

  constructor(private searchService: SearchService,
              private alertService: BootstrapAlertService,
              private router: Router,
              private quickviewService: QuickviewService,
              protected cd: ChangeDetectorRef) {
    super();
    this.cleanSearchCriteria();
    this.searchService.getFormOptions().subscribe((data) => {
      data.boroughs = data.boroughs.filter(borough => borough.neighborhoods.length);
      this.formOptions = this.resortProperies(data);
      this.initForm();
    });
  }

  initForm() {
    this.cleanBorough();

    this.initSearchCompanies();
  }

  ngOnInit() {
    this.saveEventEmiter.debounceTime(200).subscribe(() => {
      this._autosave();
    });
  }

  initSearchCompanies() {
    this.companiesToTypeahead.pipe(
      tap(() => this.companies_to_loading = true),
      distinctUntilChanged(),
      debounceTime(200),
      switchMap(term => this.searchService.getCompanies(term)),
    ).subscribe(x => {
      this.search_companies = x.map(c => {
        return { name: c, value: c };
      });
      this.companies_to_loading = false;
      this.cd.markForCheck();
    }, () => {
      this.search_companies = [];
    });
  }

  cleanSearchCriteria() {
    this.search_companies = [];
    this.model = Object.assign(new SearchQueryModel(), {
      ad_type: 'rental',
      min_price: 0,
      max_price: 0,
      has_photos: 0,
      borough: '',
      neighborhood_id: [],
      building_type: [],
      amenities: [],
      ownership: [],
      other: [],
      exposures: [],
    });
    this.modelDiff = (JSON.parse(JSON.stringify(this.model)));
  }

  cleanBorough() {

    this.formOptions.boroughs.forEach((borough) => {
      borough.neighborhoods.forEach((neighborhood) => {
        neighborhood.selected = false;
      });
    });
  }

  saveSearch() {
    this.saving = true;

    let saved_searches = null;
    if (this.client.searches && this.client.searches.length && this.client.searches[0].created_by !== '0') {
      saved_searches = this.searchService.updateSavedSearche(this.client.user_id, this.client.searches[0].search_id, this.model);
    } else {
      saved_searches = this.searchService.createSavedSearche(this.client.user_id, this.model);
    }

    saved_searches.subscribe(data => {
      if (!this._client.searches) {
        this._client.searches = [];
      }
      
      this._client.searches[0] = data;
      this.saving = false;
      this.highlightResoult();
    }, response => {
      this.saving = false;
      this.alertService.showError('An error occurs while saving search criteria');
    });
  }

  autosave() {
    if (!this.formOptions || !this.quickviewService.opened.getValue()) {
      return;
    }

    this.prepareNeighborhoods();
    let is_equal = true;
    ['ad_type', 'borough', 'neighborhood_id', 'bedrooms', 'bathrooms', 'pets', 'min_price', 'max_price', 'building_type', 'ownership',
      'amenities', 'other', 'furnished', 'ratings', 'has_photos', 'company_name', 'has_concession', 'fee_structure', 'exposures']
      .forEach((filed) => {
        if (!isEqual(this.modelDiff[filed], this.model[filed])) {
          is_equal = false;
        }
      });

    if (!is_equal) {
      this.saveEventEmiter.emit();
    }
  }

  _autosave() {
    this.saveSearch();
    this.modelDiff = (JSON.parse(JSON.stringify(this.model)));
  }

  search() {
    this.quickviewService.setOpened(false);
    this.router.navigate([`search/criteria/${this.client.searches[0].search_id}`, { v: Math.floor(Math.random() * 100) }]);
  }

  createSearchCriteria() {
    this.saveSearch();
  }

  highlightResoult() {
    this.highlight = true;
    setTimeout(() => this.highlight = false, 500);
  }

  removeSuspend() {
    this.suspending = true;
    this.searchService.removeSuspend(this.client.user_id, this.client_search.search_id).subscribe(() => {
      this.client_search.suspended = 0;
      this.suspending = false;
    });
  }

  suspendSearch() {
    this.suspending = true;
    this.searchService.suspendSearch(this.client.user_id, this.client_search.search_id).subscribe(() => {
      this.client_search.suspended = 1;
      this.suspending = false;
    });
  }
}
