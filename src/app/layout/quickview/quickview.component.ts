import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../client/services/client.service';
import { ClientModel } from '../../client/models/client.model';
import { QuickviewService } from './quickview.service';

@Component({
  selector: 'app-quickview',
  templateUrl: './quickview.component.html',
  styleUrls: ['./quickview.component.less']
})
export class QuickviewComponent implements OnInit {

  opened = false;
  state: string;
  clients: ClientModel[];
  current_client: ClientModel;

  tabs_menu = [
    { label: 'Saved searches', index: 'saved_searches' },
    { label: 'Listings', index: 'listings' },
    // { label: 'Messages', index: 'messages' },
    // { label: 'Appointments', index: 'appointments' },
    // { label: 'Documents', index: 'documents' },
  ];

  constructor(private clientService: ClientService, public quickviewService: QuickviewService) {
    this.quickviewService.getOpened().subscribe(opened => this.opened = opened);
    this.quickviewService.getState().subscribe(state => this.state = state);
  }

  ngOnInit() {
    this.clientService.getSuggestClients().subscribe((clients) => this.clients = clients);
    this.clientService.getCurrentClient().subscribe((client) => this.current_client = client);
  }

  updateClient() {
    this.clientService.setCurrentClient(this.current_client);
  }

  changeState(state: string) {
    this.quickviewService.setState(state);
  }

  toogleSection() {
    this.quickviewService.setOpened(!this.opened);
  }
}
