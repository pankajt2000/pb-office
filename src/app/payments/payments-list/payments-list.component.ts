import { Component, OnInit } from '@angular/core';
import { PaymentsService } from '../services/payments.service';
import { PaymentListModel } from '../models/payment-list.model';
import { AuthService } from '../../authentication/services/auth.service';
import { AgentService } from '../../agent/services/agent.service';
import {BsModalService} from 'ngx-bootstrap';
import {InfoPopupComponent} from '../../shared/components/info-popup/info-popup.component';
import { AgentModel } from '../../agent/models/agent.model';

@Component({
  selector: 'app-payments-list',
  templateUrl: './payments-list.component.html',
  styleUrls: ['./payments-list.component.less']
})
export class PaymentsListComponent implements OnInit {

  limit = 15;
  offset = 0;
  loaded = false;
  loading = false;
  total: number;
  payments: PaymentListModel[] = [];

  is_supervisor: boolean;

  agent_list: AgentModel[];
  selected_agent_id: any = null;

  constructor(
    private authService: AuthService,
    private agentService: AgentService,
    private modalService: BsModalService,
    private paymentsService: PaymentsService
  ) { }

  ngOnInit() {
    this.initPayments();
  }

  initPayments() {
    this.loading = true;
    this.loaded = false;

    this.authService.sessionInfo().subscribe((info) => {
      this.is_supervisor = info.user.roles.map(r => r.toLowerCase()).includes('supervisor');

      if (this.is_supervisor) {
        this.selected_agent_id = "-1";

        this.agentService.getAgents().subscribe((agents) => {
          this.agent_list = agents[0].filter(a => !a.is_test_user && a.active).map(a => { return { user_id: a.user_id, name: a.name }});
        });
      }

      this.loadPayments();
    })
  }

  loadPayments(force_refresh: boolean = false) {
    this.loaded = false;
    this.loading = true;

    this.paymentsService.getPayments(this.offset, this.limit, this.selected_agent_id).subscribe(data => {
      if (!force_refresh) {
        this.payments = this.payments.concat(data[0]);
      } else {
        this.payments = data[0];
      }
      this.total = data[1];

      this.loaded = true;
      this.loading = false;
    })
  }

  showErrorDetails(payment: PaymentListModel) {
    const modal = this.modalService.show(InfoPopupComponent, {
      class: 'modal-sm m-t-10', initialState: {
        message: `Payment declined with the following message: "${payment.error}"`
      }
    });
  }

  loadMore() {
    this.offset = this.payments.length;

    this.loadPayments();
  }

  isSupervisor() {
    return this.is_supervisor;
  }
}
