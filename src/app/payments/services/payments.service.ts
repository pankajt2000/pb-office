import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { PaymentListModel } from '../models/payment-list.model';

@Injectable()
export class PaymentsService
{
  constructor(private api: AgentApiService) {

  }

  getPayments(offset: number = 0, limit: number = 10, agent_id?: number | string) {
    return this.api.get(`payments?offset=${offset}&limit=${limit}&agent_id=${agent_id}`, { observe: 'response' }).map(res => {
      return [
        res.body.map(payment => new PaymentListModel().deserialize(payment)),
        res.headers.get('Adm-Result-Total-Count')
      ];
    });
  }
}
