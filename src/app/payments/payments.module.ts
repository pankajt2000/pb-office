import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentsListComponent } from './payments-list/payments-list.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { PaymentsService } from './services/payments.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule
  ],
  providers: [
    PaymentsService
  ],
  declarations: [
    PaymentsListComponent
  ]
})
export class PaymentsModule { }
