import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { CurrencyPipe } from '@angular/common';

export class PaymentListModel implements DeserializableInterface<PaymentListModel>
{
  client_name: string;
  client_email: string;

  agent_name: string;

  last4: string;
  card_type: string;
  amount: number & string;

  status: string;

  error?: string;

  time_updated: string;

  constructor() { }

  getAmountFormatted() {
    if (this.amount == 'N/A') {
      return this.amount;
    }

    const fractions = parseInt(this.amount) / 100;
    const currPipe = new CurrencyPipe('en-us');

    return currPipe.transform(fractions, 'USD', 'symbol');
  }

  deserialize(input: any): PaymentListModel {
    Object.assign(this, input);
    return this;
  }
}
