import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { SavedListingModel } from "../../listings/models/saved-listing.model";

export class SigninDetailsModel implements DeserializableInterface<SigninDetailsModel> {

  agent_name: string;
  agent_email: string;
  agent_phone: string;

  form_data: {
    first_name: string,
    last_name: string,
    email: string,
    phone: string,
    with_broker: boolean,
    broker_name?: string,
    broker_firm?: string,
    broker_email?: string,
    broker_phone?: string,
    pets: 'none' | 'dogs' | 'cats'
  };

  listings: SavedListingModel[];

  constructor() {}

  deserialize(input: any): SigninDetailsModel {
    input.listings = input.listings.map(listing => new SavedListingModel().deserialize(listing));

    Object.assign(this, input);
    return this;
  }

}
