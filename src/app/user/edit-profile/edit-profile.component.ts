import { Component, ElementRef, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { UserService } from '../../authentication/services/user.service';
import { UserModel } from '../../authentication/models/user.model';
import { UploaderOptions, UploadInput, UploadOutput } from 'ngx-uploader/index';
import { config } from '../../config';
import { environment } from '../../../environments/environment';
import { of } from 'rxjs/observable/of';
import { concatMap } from 'rxjs/operators';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.less']
})
export class EditProfileComponent implements OnInit {

  userForm: FormGroup;
  submitting = false;
  errorMsg = '';
  userModel: UserModel;
  origin_user_data;
  submit_disabled = true;

  uploading = false;
  dragOver: boolean;
  uploadInput: EventEmitter<UploadInput>;
  uploaderOptions: UploaderOptions;
  @ViewChild('uploadinput') uploadinput: ElementRef;

  submit_preference_disabled = true;
  preferenceForm: FormGroup;
  preferenceErrorMsg = '';
  origin_preference_data;
  show_rh_pwd: boolean = false;

  signatureForm: FormGroup;
  existing_signature: string = null;

  constructor(private fb: FormBuilder, private userService: UserService, private alertService: BootstrapAlertService) {

    this.userForm = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern(/^[0-9()+-]+$/)]],
      position: [''],
      description: [''],
    });
    this.userForm.valueChanges.debounceTime(100).subscribe(() => {
      this.submit_disabled = this.userForm.invalid || JSON.stringify(this.origin_user_data) === JSON.stringify(this.userForm.value);
    });

    this.uploaderOptions = { concurrency: 1 };
    this.uploadInput = new EventEmitter<UploadInput>();

    this.preferenceForm = this.fb.group({
      agent_notification_email: ['', Validators.required],
      agent_notification_push: ['', Validators.required],
      agent_publish_ad_se: ['', Validators.required],
      agent_publish_ad_na: ['', Validators.required],
      agent_publish_ad_rh: ['', Validators.required],
      agent_show_on_signin_form: [''],
      agent_rh_bot_enabled: [''],
      agent_rh_bot_username: [''],
      agent_rh_bot_password: [''],
      agent_license_number: [''],
      agent_aliases: [''],
    });
    this.preferenceForm.valueChanges.debounceTime(100).subscribe(() => {
      this.submit_preference_disabled = this.preferenceForm.invalid || JSON.stringify(this.origin_preference_data) === JSON.stringify(this.preferenceForm.value);
    });

    this.signatureForm = this.fb.group({
      agent_signature: ['', Validators.required]
    })
  }

  ngOnInit() {
    // todo: messy, but works. needs rework @marko
    this.userService.getAgent().subscribe(user => {
      if (user.preferences.agent_signature_id) {
        this.userService.getAgentSignature('dataurl').subscribe(data => {
          this.existing_signature = data;

          this.userForm.patchValue(user);
          this.preferenceForm.patchValue(user.preferences);
          this.userModel = user;

          this.origin_user_data = this.userForm.value;
          this.origin_preference_data = this.preferenceForm.value;
        });
      } else {
        this.userForm.patchValue(user);
        this.preferenceForm.patchValue(user.preferences);
        this.userModel = user;

        this.origin_user_data = this.userForm.value;
        this.origin_preference_data = this.preferenceForm.value;
      }
    })
  }

  updateUser() {

    if (this.userForm.invalid) return;

    this.errorMsg = '';
    this.submitting = true;

    this.userService.updateAgent(this.userForm.value).subscribe((user) => {
      this.userModel = Object.assign(this.userModel, user);
      this.userService.setUser(this.userModel);
      this.userForm.patchValue(user);
      this.userForm.markAsPristine();

      this.origin_user_data = this.userForm.value;

      this.submitting = false;

    }, (errorData) => {

      this.submitting = false;

      if (errorData.error) {
        this.errorMsg = errorData.error.errorMsg;
      }
    });
  }

  changePreferences() {

    if (this.preferenceForm.invalid) return;

    this.preferenceErrorMsg = '';
    this.submitting = true;

    this.userService.changePreferences(this.preferenceForm.value).subscribe(() => {
      this.alertService.showSucccess('Preferences saved successfully');

      this.submit_preference_disabled = true;
      this.origin_preference_data = this.preferenceForm.value;
      this.preferenceForm.markAsPristine();

      this.submitting = false;
    }, (errorData) => {

      this.submitting = false;

      if (errorData.error) {
        this.preferenceErrorMsg = errorData.error.errorMsg;
      }
    });
  }

  toggleRhPasswordShow() {
    this.show_rh_pwd = !this.show_rh_pwd;
  }

  onSignatureDrawn(data_url: string) {
    this.signatureForm.get('agent_signature').patchValue(data_url);
  }

  updateAgentSignature() {
    const blob = this._dataURItoBlob(this.signatureForm.get('agent_signature').value);
    const fd = new FormData();
    fd.append('agent-signature', blob, 'agent-signature.png');

    this.submitting = true;

    this.userService.updateAgentSignature(fd).subscribe(() => {
      this.submitting = false;
    });
  }

  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'allAddedToQueue') {

      if (!this.uploadinput.nativeElement.value) return;

      const event: UploadInput = {
        type: 'uploadAll',
        url: environment.agent_base_url + `agent-profile-photo`,
        method: 'POST',
        fieldName: 'photo',
        data: {},
        headers: {
          'Adm-App-Auth': environment.app_auth_header,
          'Adm-Session-ID': localStorage.getItem(config.access_token_local_storage)
        }
      };
      this.uploadInput.emit(event);
      this.uploading = true;

    } else if (output.type === 'dragOver') {
      this.dragOver = true;
    } else if (output.type === 'dragOut' || output.type === 'drop') {
      this.dragOver = false;
    } else if (output.type === 'done') {

      this.uploading = false;

      if (output.file.responseStatus === 200) {

        this.userService.getAgent().subscribe(user => {
          this.userModel = user;
          this.userService.setUser(this.userModel);
        });
        // this.userModel = Object.assign(this.userModel, output.file.response);
      } else if ('errorMsg' in output.file.response) {
        this.alertService.showError(output.file.response.errorMsg);
      }

      this.uploadinput.nativeElement.value = '';
    }
  }

  private _dataURItoBlob(data_url: string): Blob {

    if (data_url === '') {
      return null;
    }

    const byteString = atob(data_url.split(',')[1]);

    const mimeString = data_url.split(',')[0].split(':')[1].split(';')[0];

    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ab], { type: mimeString });
  }
}
