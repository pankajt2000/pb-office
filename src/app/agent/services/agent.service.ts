import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { AgentModel } from '../models/agent.model';
import { EventLogModel } from '../models/event-log.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AgentService {

  constructor(private api: AgentApiService) {

  }

  getAgents(offset: number = 0, limit: number = 10, fragment?: string, sort?: (a, b) => number) {
    let url = `agents?offset=${offset}&limit=${limit}`;

    if (fragment) {
      url += `&search=${fragment}`;
    }

    sort = sort || ((a, b) => 0);

    return this.api.get(url, { observe: 'response' }).map(response => {
      return [
        response.body.map(agent => new AgentModel().deserialize(agent)).sort(sort),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

  getAgent(user_id) {
    return this.api.get(`agents/${user_id}`).map(agent => new AgentModel().deserialize(agent));
  }

  getPerformances(user_id: string, since: string) {
    return this.api.get(`agents/${user_id}/performance?since=${since}`);
  }

  getEventLogs(user_id, offset: number = 0, limit: number = 10) {
    return this.api.get(`agents/${user_id}/event-log?offset=${offset}&limit=${limit}`, { observe: 'response' }).map(response => {
      return [
        response.body.map(application => new EventLogModel().deserialize(application)),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

}
