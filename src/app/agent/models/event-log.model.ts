import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { ClientModel } from '../../client/models/client.model';

export class EventLogModel implements DeserializableInterface<EventLogModel> {

  _id: number;
  event_time: number;
  event_name: string;
  event_title: string;
  client?: ClientModel;
  description?: string;

  deserialize(input: any): EventLogModel {
    Object.assign(this, input);
    return this;
  }

}
