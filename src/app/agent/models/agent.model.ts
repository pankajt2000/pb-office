import { DeserializableInterface } from '../../shared/models/deserializable.interface';

export class AgentModel implements DeserializableInterface<AgentModel> {

  user_id: string;
  name: string;
  phone: string;
  email: string;
  active: boolean;
  last_active: string;
  time_created: number;
  active_listings: number;
  active_clients: number;

  has_oauth_data: boolean;
  is_test_user: boolean;

  counts?: {
    applications: {
      month: number,
      week: number
    },
    inquiries: {
      month: number,
      week: number
    },
    messages: {
      rh: {
        month: number,
        week: number
      },
      se: {
        month: number,
        week: number
      },
    }
  };

  deserialize(input: any): AgentModel {
    Object.assign(this, input);
    return this;
  }

}
