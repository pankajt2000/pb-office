import { Component, OnInit } from '@angular/core';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { AgentModel } from '../models/agent.model';
import { AgentService } from '../services/agent.service';

@Component({
  selector: 'app-agent-list',
  templateUrl: './agent-list.component.html',
  styleUrls: ['./agent-list.component.less']
})
export class AgentListComponent implements OnInit {

  limit = 0;
  offset = 0;
  loaded = false;
  total: number;
  result: AgentModel[] = [];

  constructor(private agentService: AgentService, private alertService: BootstrapAlertService) {
  }

  ngOnInit() {
    this.initAgents();
  }

  initAgents() {
    this.agentService.getAgents(this.offset, this.limit).subscribe(data => {
      [this.result, this.total] = data;
      this.loaded = true;
    }, () => {
      this.alertService.showError('Error: Cannot load application');
    });
  }

}
