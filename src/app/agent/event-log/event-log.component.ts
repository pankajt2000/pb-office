import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AgentService } from '../services/agent.service';
import { EventLogModel } from '../models/event-log.model';

@Component({
  selector: 'app-event-log',
  templateUrl: './event-log.component.html',
  styleUrls: ['./event-log.component.less']
})
export class EventLogComponent implements OnInit {

  limit = 10;
  offset = 0;
  result: EventLogModel[] = [];
  total: number;
  loaded = false;
  user_id;

  constructor(private route: ActivatedRoute, private agentService: AgentService) {
    this.route.params.subscribe(params => {
      if (params['user_id']) {
        this.user_id = params['user_id'];
        this.initEventLogs();
      }
    });
  }

  ngOnInit() {
  }

  initEventLogs() {
    this.agentService.getEventLogs(this.user_id, this.offset, this.limit).subscribe((events) => {
      this.loaded = true;
      [this.result, this.total] = events;
    });

  }

  pageChanged(event) {
    this.loaded = false;
    this.offset = (event.page - 1) * this.limit;
    this.initEventLogs();
  }
}
