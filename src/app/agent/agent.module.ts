import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgentListComponent } from './agent-list/agent-list.component';
import { SharedModule } from '../shared/shared.module';
import { AgentService } from './services/agent.service';
import { RouterModule } from '@angular/router';
import { EventLogComponent } from './event-log/event-log.component';
import { AgentDetailsComponent } from './agent-details/agent-details.component';
import { NgxChartsModule } from "@swimlane/ngx-charts";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    NgxChartsModule
  ],
  providers: [
    AgentService
  ],
  declarations: [
    AgentListComponent,
    EventLogComponent,
    AgentDetailsComponent
  ]
})
export class AgentModule {
}
