import { Component, OnInit } from '@angular/core';
import { AgentModel } from "../models/agent.model";
import { ActivatedRoute } from "@angular/router";
import { AgentService } from "../services/agent.service";
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-agent-details',
  templateUrl: './agent-details.component.html',
  styleUrls: ['./agent-details.component.less']
})
export class AgentDetailsComponent implements OnInit {

  loading: boolean = false;

  agent: AgentModel;

  colorScheme = {
    domain: ['#5899DA', '#E8743B', '#19A979', '#ED4A7B', '#945ECF', '#13A4B4', '#525DF4', '#BF399E', '#6C8893', '#EE6868', '#2F6497']
  };
  performances = {
    'new_inquiries': { data: null, legend: null },
    'closed_inquiries': { data: null, legend: null },
    'messages_sent': { data: null, legend: null }
  };

  filterForm: FormGroup;

  constructor(
    private agentService: AgentService,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.loading = true;

    this.filterForm = this.fb.group({
      since: ['last_month']
    });

    this.route.params.flatMap(params => {
      const user_id = params['user_id'] || 0;

      return this.agentService.getAgent(user_id);
    })
    .subscribe(agent => {
      this.agent = agent;

      this.initCharts();
    });

  }

  initCharts() {
    this.loading = true;

    const since = this.filterForm.get('since').value;

    this.agentService.getPerformances(this.agent.user_id, since).subscribe(data => {
      for (const segment in data) {
        const columns = data[segment].columns;
        const rows = data[segment].rows;

        let chart_data = [];

        for (let d in data[segment].data) {
          const item = data[segment].data[d];
          const col = columns.find(x => x.id === d);

          if (col) {
            const line = {
              name: col.label,
              series: rows.map(x => ({
                name: x.label,
                value: item[x.id] ? item[x.id].value : 0
              }))
            };

            chart_data.push(line);
          }
        }

        this.performances[segment] = {
          legend: true,
          data: chart_data
        };
      }

      this.loading = false;
    });
  }

}
