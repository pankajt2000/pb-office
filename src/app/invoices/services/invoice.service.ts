import { Injectable } from "@angular/core";
import { AgentApiService } from "../../services/api/agent-api.service";
import { InvoiceListModel } from "../models/invoice-list.model";
import { InvoiceDetailsModel } from "../models/invoice-details.model";

@Injectable()
export class InvoiceService {

  constructor(private api: AgentApiService) {}

  getInvoices(type: string, offset: number = 0, limit: number = 10, agent_id?: string, status?: string) {
    return this.api.get(`op-invoices?type=${type}&offset=${offset}&limit=${limit}&agent_id=${agent_id}&status=${status}`, { observe: 'response' }).map(response => {
      return [
        response.body.map(invoice => new InvoiceListModel().deserialize(invoice)),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

  getInvoice(invoice_id: string) {
    return this.api.get(`op-invoices/${invoice_id}`).map(invoice => new InvoiceDetailsModel().deserialize(invoice));
  }

  createInvoice(type: string, data) {
    return this.api.post(`op-invoices?type=${type}`, data).map(response => response.op_invoice_id);
  }

  updateInvoice(invoice_id: string, data?, status?) {
    data = data || null;
    status = status || null;

    return this.api.put(`op-invoices/${invoice_id}`, { data, status }).map(res => new InvoiceDetailsModel().deserialize(res));
  }

  deleteInvoice(invoice_id: string) {
    return this.api.delete(`op-invoices/${invoice_id}`);
  }

}
