import { Component, OnInit } from '@angular/core';
import { AgentModel } from "../../agent/models/agent.model";
import { BsModalService } from "ngx-bootstrap";
import { AuthService } from "../../authentication/services/auth.service";
import { AgentService } from "../../agent/services/agent.service";
import { ActivatedRoute, Router } from '@angular/router';
import { InvoiceService } from "../services/invoice.service";
import { InvoiceListModel } from "../models/invoice-list.model";
import { ConfirmPopupComponent } from "../../shared/components/confirm-popup/confirm-popup.component";
import { SendModalComponent } from '../../listings/send-modal/send-modal.component';
import { DatePipe } from '@angular/common';
import { DealListModel } from '../../deals/models/deal-list.model';

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.less']
})
export class InvoiceListComponent implements OnInit {

  limit = 15;
  offset = 0;
  loaded = false;
  loading = false;
  total: number;
  invoices: InvoiceListModel[] = [];

  is_supervisor: boolean = false;

  agent_list: AgentModel[];
  selected_agent_id: any = null;

  invoice_status_filter: string = 'all';

  type: string = 'rental';

  constructor(
    private invoiceService: InvoiceService,
    private modalService: BsModalService,
    private authService: AuthService,
    private agentService: AgentService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.type = params['type'] || 'rental';

      this.initInvoices();
    })
  }

  initInvoices() {
    this.loading = true;
    this.loaded = false;

    this.authService.sessionInfo().subscribe((info) => {
      this.is_supervisor = info.user.roles.map(r => r.toLowerCase()).includes('supervisor');

      if (this.is_supervisor) {
        this.selected_agent_id = "-1";

        this.agentService.getAgents().subscribe((agents) => {
          this.agent_list = agents[0].filter(a => !a.is_test_user && a.active).map(a => { return { user_id: a.user_id, name: a.name }});
        });
      }

      this.loadInvoices();
    })
  }

  loadInvoices(force_refresh: boolean = true) {
    this.loaded = false;
    this.loading = true;

    this.invoiceService.getInvoices(this.type, this.offset, this.limit, this.selected_agent_id, this.invoice_status_filter).subscribe(data => {
      if (!force_refresh) {
        this.invoices = this.invoices.concat(data[0]);
      } else {
        this.invoices = data[0];
      }

      this.total = data[1];

      this.loaded = true;
      this.loading = false;
    })
  }

  createNewInvoice() {
    this.router.navigate(['/invoices', 'new'], { queryParams: { type: this.type }});
  }

  onStatusChange(event, invoice: InvoiceListModel) {
    const status = event.target.value;

    this.invoiceService.updateInvoice(invoice.invoice_id, null, status).subscribe(_ => {
      this.loadInvoices(true);
    });
  }

  sendAsPdf(invoice: InvoiceListModel) {
    const subject = `Invoice ${invoice.property_address}, ${invoice.property_unit}`;

    const message = `Hi

Please see invoice attached.`;

    const modal = this.modalService.show(SendModalComponent, {
      initialState: {
        email: invoice.contact_email, subject, message, invoiceId: invoice.invoice_id
      }
    });
  }

  deleteInvoice(invoice: InvoiceListModel) {
    const modal = this.modalService.show(ConfirmPopupComponent, {
      class: 'modal-sm m-t-10', initialState: {
        message: 'Are you sure you wish to delete this invoice?'
      }
    });

    modal.content.confirm.subscribe(() => {
      this.loaded = false;
      this.loading = true;

      this.invoiceService.deleteInvoice(invoice.invoice_id).subscribe(res => {
        this.loadInvoices(true);
      });
    });
  }

  loadMore() {
    this.offset = this.invoices.length;

    this.loadInvoices();
  }

  isSupervisor() {
    return this.is_supervisor;
  }

}
