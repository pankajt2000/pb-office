import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInvoiceRentalComponent } from './edit-invoice-rental.component';

describe('EditInvoiceRentalComponent', () => {
  let component: EditInvoiceRentalComponent;
  let fixture: ComponentFixture<EditInvoiceRentalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInvoiceRentalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInvoiceRentalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
