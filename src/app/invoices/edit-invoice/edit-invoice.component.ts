import { Component, OnInit } from '@angular/core';
import { InvoiceDetailsModel } from "../models/invoice-details.model";
import { FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { InvoiceService } from "../services/invoice.service";
import { BootstrapAlertService } from "ngx-bootstrap-alert-service";
import { CurrencyMaskConfig } from "ngx-currency/src/currency-mask.config";
import { AuthService } from "../../authentication/services/auth.service";

@Component({
  selector: 'app-edit-invoice',
  templateUrl: './edit-invoice.component.html',
  styleUrls: ['./edit-invoice.component.less']
})
export class EditInvoiceComponent implements OnInit {

  loading = false;

  invoice: InvoiceDetailsModel;

  is_supervisor: boolean;

  priceMaskConfig: CurrencyMaskConfig = {
    align: 'left',
    allowNegative: false,
    allowZero: true,
    decimal: '.',
    precision: 2,
    prefix: '$ ',
    suffix: '',
    thousands: ',',
    nullable: true
  };

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected alertService: BootstrapAlertService,
    protected invoiceService: InvoiceService,
    protected authService: AuthService
  ) { }

  ngOnInit() {
    this.loading = true;

    this.route.params.subscribe(params => {
      const invoice_id = params['invoice_id'];

      this.authService.sessionInfo().subscribe(info => {
        this.is_supervisor = info.user.roles.map(r => r.toLowerCase()).includes('supervisor');

        this.invoiceService.getInvoice(invoice_id).subscribe(invoice => {
          this.invoice = invoice;

          this.loading = false;
        })
      })
    })
  }

  protected submit(invoice_form: FormGroup) {
    const data = invoice_form.get('data').value;

    if (data.contact_email === '' && data.contact_phone === '') {
      this.alertService.showError('Please enter either a contact email or phone');
      return;
    }

    if (data.tenants) {
      data.tenants = data.tenants.split(',').map((tenant: string) => tenant.trim());
    }

    this.loading = true;

    return this.invoiceService.updateInvoice(this.invoice.invoice_id, data).subscribe(invoice => {
      this.invoice = invoice;

      this.alertService.showSucccess('Invoice updated successfully.');

      this.loading = false;
    }, (error) => {
      this.alertService.showError('Something went wrong. Please try again later.');
      this.loading = false;
    });
  }

  cancel() {
    this.router.navigate(['/invoices', 'list', this.invoice.type]);
  }

}
