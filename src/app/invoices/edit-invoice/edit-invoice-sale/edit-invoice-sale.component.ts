import { Component, Input, OnInit } from '@angular/core';
import { InvoiceDetailsModel } from '../../models/invoice-details.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { InvoiceService } from '../../services/invoice.service';
import { AuthService } from '../../../authentication/services/auth.service';
import { EditInvoiceComponent } from '../edit-invoice.component';

@Component({
  selector: 'app-edit-invoice-sale',
  templateUrl: './edit-invoice-sale.component.html',
  styleUrls: ['./edit-invoice-sale.component.less']
})
export class EditInvoiceSaleComponent extends EditInvoiceComponent implements OnInit {

  @Input('invoice') invoice: InvoiceDetailsModel;

  invoice_form: FormGroup;

  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected alertService: BootstrapAlertService,
              protected invoiceService: InvoiceService,
              private fb: FormBuilder,
              protected authService: AuthService) {
    super(route, router, alertService, invoiceService, authService);
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.invoice_form = this.fb.group({
      data: this.fb.group({
        management_company: [this.invoice.data.management_company, Validators.required],
        management_address_1: [this.invoice.data.management_address_1, Validators.required],
        management_address_2: [this.invoice.data.management_address_2, Validators.required],
        contact_name: [this.invoice.data.contact_name, Validators.required],
        contact_email: [this.invoice.data.contact_email],
        contact_phone: [this.invoice.data.contact_phone],

        status: [this.invoice.data.status],

        buyer: [this.invoice.data.buyer],
        address: [this.invoice.data.address],
        unit: [this.invoice.data.unit],
        sales_price: [this.invoice.data.sales_price],
        commission_amount: [this.invoice.data.commission_amount]
      })
    });
  }

  sendForm() {
    this.submit(this.invoice_form).add(_ => {
      this.initForm();
    });
  }
}
