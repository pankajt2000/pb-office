import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInvoiceSaleComponent } from './edit-invoice-sale.component';

describe('EditInvoiceSaleComponent', () => {
  let component: EditInvoiceSaleComponent;
  let fixture: ComponentFixture<EditInvoiceSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInvoiceSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInvoiceSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
