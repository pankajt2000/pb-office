import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { environment } from '../../../environments/environment';

export class InvoiceListModel implements DeserializableInterface<InvoiceListModel> {

  invoice_id: string;

  agent_name: string;

  invoice_no: string;
  date: string;

  status: string;

  management_company: string;

  property_address: string;
  property_unit: string;

  contact_name: string;
  contact_email: string;
  contact_phone: string;

  constructor() {}

  deserialize(input: any): InvoiceListModel {
    Object.assign(this, input);
    return this;
  }

  getDownloadUrl() {
    return `${environment.agent_base_url}op-invoices/${this.invoice_id}/download`;
  }

}
