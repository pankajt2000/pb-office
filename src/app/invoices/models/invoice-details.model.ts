import { DeserializableInterface } from "../../shared/models/deserializable.interface";

type GeneralData = {
  invoice_no: string,
  date: string,
  status: string,
  management_company: string,
  management_address_1: string,
  management_address_2: string,
  contact_name: string,
  contact_email: string,
  contact_phone: string,
  address: string,
  unit: string,
  commission_amount: string
}

type RentalData = {
  tenants: string[],
  price: string,
  lease_start_date: string,
}

type SalesData = {
  buyer: string,
  sales_price: string,
}

export class InvoiceDetailsModel implements DeserializableInterface<InvoiceDetailsModel> {

  invoice_id: string;

  type: 'rental' | 'sale';

  data: GeneralData & (RentalData & SalesData);

  constructor() {}

  deserialize(input: any): InvoiceDetailsModel {
    Object.assign(this, input);
    return this;
  }

}
