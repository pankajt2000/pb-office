import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceDetailsSaleComponent } from './invoice-details-sale.component';

describe('InvoiceDetailsSaleComponent', () => {
  let component: InvoiceDetailsSaleComponent;
  let fixture: ComponentFixture<InvoiceDetailsSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceDetailsSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceDetailsSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
