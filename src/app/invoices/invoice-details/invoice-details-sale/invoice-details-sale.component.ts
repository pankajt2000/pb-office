import { Component, Input, OnInit } from '@angular/core';
import { InvoiceDetailsModel } from '../../models/invoice-details.model';
import { InvoiceDetailsComponent } from '../invoice-details.component';
import { ActivatedRoute, Router } from '@angular/router';
import { InvoiceService } from '../../services/invoice.service';

@Component({
  selector: 'app-invoice-details-sale',
  templateUrl: './invoice-details-sale.component.html',
  styleUrls: ['./invoice-details-sale.component.less']
})
export class InvoiceDetailsSaleComponent extends InvoiceDetailsComponent implements OnInit {

  @Input('invoice') invoice: InvoiceDetailsModel;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected invoiceService: InvoiceService
  ) {
    super(route, router, invoiceService);
  }

  ngOnInit() {
  }

}
