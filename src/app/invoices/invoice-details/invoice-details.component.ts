import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InvoiceService } from '../services/invoice.service';
import { InvoiceDetailsModel } from '../models/invoice-details.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.less']
})
export class InvoiceDetailsComponent implements OnInit {

  loading: boolean;
  invoice: InvoiceDetailsModel;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected invoiceService: InvoiceService
  ) {
    this.route.params.subscribe(params => {
      const invoice_id = params['invoice_id'];

      this.invoiceService.getInvoice(invoice_id).subscribe(invoice => {
        this.invoice = invoice;
      });
    });
  }

  ngOnInit() {
  }

  getLeaseStartDateFormatted(): string {
    if (this.invoice.data.lease_start_date === '') {
      return 'N/A';
    }

    const d = new Date(this.invoice.data.lease_start_date);

    return new DatePipe('en-US').transform(d, 'MM/dd/y');
  }

}
