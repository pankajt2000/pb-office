import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceDetailsRentalComponent } from './invoice-details-rental.component';

describe('InvoiceDetailsRentalComponent', () => {
  let component: InvoiceDetailsRentalComponent;
  let fixture: ComponentFixture<InvoiceDetailsRentalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceDetailsRentalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceDetailsRentalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
