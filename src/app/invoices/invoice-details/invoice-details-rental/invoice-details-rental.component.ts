import { Component, Input, OnInit } from '@angular/core';
import { InvoiceDetailsComponent } from '../invoice-details.component';
import { InvoiceDetailsModel } from '../../models/invoice-details.model';
import { ActivatedRoute, Router } from '@angular/router';
import { InvoiceService } from '../../services/invoice.service';

@Component({
  selector: 'app-invoice-details-rental',
  templateUrl: './invoice-details-rental.component.html',
  styleUrls: ['./invoice-details-rental.component.less']
})
export class InvoiceDetailsRentalComponent extends InvoiceDetailsComponent implements OnInit {

  @Input('invoice') invoice: InvoiceDetailsModel;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected invoiceService: InvoiceService
  ) {
    super(route, router, invoiceService);
  }

  ngOnInit() {
  }

}
