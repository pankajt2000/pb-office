import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { InvoiceDetailsComponent } from './invoice-details/invoice-details.component';
import { EditInvoiceComponent } from './edit-invoice/edit-invoice.component';
import { CreateInvoiceComponent } from './create-invoice/create-invoice.component';
import { InvoiceService } from "./services/invoice.service";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { NgxCurrencyModule } from "ngx-currency";
import { EditInvoiceRentalComponent } from './edit-invoice/edit-invoice-rental/edit-invoice-rental.component';
import { EditInvoiceSaleComponent } from './edit-invoice/edit-invoice-sale/edit-invoice-sale.component';
import { InvoiceDetailsRentalComponent } from './invoice-details/invoice-details-rental/invoice-details-rental.component';
import { InvoiceDetailsSaleComponent } from './invoice-details/invoice-details-sale/invoice-details-sale.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NgxCurrencyModule,

    SharedModule
  ],
  providers: [
    InvoiceService
  ],
  declarations: [
    InvoiceListComponent,
    InvoiceDetailsComponent,
    EditInvoiceComponent,
    CreateInvoiceComponent,
    EditInvoiceRentalComponent,
    EditInvoiceSaleComponent,
    InvoiceDetailsRentalComponent,
    InvoiceDetailsSaleComponent
  ],
  exports: [
    InvoiceListComponent,
    InvoiceDetailsComponent,
    EditInvoiceComponent,
    CreateInvoiceComponent
  ]
})
export class InvoicesModule { }
