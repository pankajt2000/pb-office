import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { InvoiceService } from "../services/invoice.service";
import { ActivatedRoute, Router } from '@angular/router';
import { BootstrapAlertService } from "ngx-bootstrap-alert-service";
import { AuthService } from '../../authentication/services/auth.service';
import { AgentModel } from '../../agent/models/agent.model';
import { AgentService } from '../../agent/services/agent.service';

@Component({
  selector: 'app-create-invoice',
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.less']
})
export class CreateInvoiceComponent implements OnInit {

  loading = false;
  invoice_form: FormGroup;

  is_supervisor: boolean;

  agent_list: AgentModel[];
  selected_agent: string;

  type: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private invoiceService: InvoiceService,
    private alertService: BootstrapAlertService,
    private authService: AuthService,
    private agentService: AgentService
  ) { }

  ngOnInit() {
    this.loading = true;

    this.route.queryParams.subscribe(params => {
      this.type = params['type'] || 'rental';

      this.authService.sessionInfo().subscribe((data) => {
        this.is_supervisor = data.user.roles.map(r => r.toLowerCase()).includes('supervisor');

        this.agentService.getAgents().subscribe((agents) => {
          this.agent_list = agents[0].filter(a => !a.is_test_user && a.active).map(a => { return { user_id: a.user_id, name: a.name }});

          this.selected_agent = data.user.user_id;

          this.invoice_form = this.fb.group({
            management_company: ['', Validators.required],
            management_address_1: ['', Validators.required],
            management_address_2: ['', Validators.required],
            contact_name: ['', Validators.required],
            contact_email: [''],
            contact_phone: ['']
          });

          this.loading = false;
        });
      });
    });
  }

  submit() {
    const data = this.invoice_form.getRawValue();

    if (data.contact_email === '' && data.contact_phone === '') {
      this.alertService.showError('Please enter either the contact\'s email or phone number.');
      return;
    }

    data.agent_id = this.selected_agent;

    this.invoiceService.createInvoice(this.type, data).subscribe(invoice_id => {
      this.router.navigate(['/invoices', invoice_id, 'edit']);
    })
  }

  cancel() {
    this.router.navigate(['/invoices', 'list', this.type]);
  }

}
