import { ApplicationDetailsModel } from './../../application/models/application-details.model';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { distinctUntilChanged, debounceTime, switchMap, tap } from 'rxjs/operators';
import { ContactModel } from '../models/contact.model';
import { ContactService } from '../services/contact.service';
import { MessageService } from '../services/message.service';
import { FolderService } from '../services/folder.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { ClientService } from '../../client/services/client.service';
import { ClientModel } from '../../client/models/client.model';
import { ListingService } from '../../listings/services/listing.service';
import { SavedListingModel } from '../../listings/models/saved-listing.model';

declare var $: any;

@Component({
  selector: 'app-send',
  templateUrl: './send.component.html',
  styleUrls: ['./send.component.less']
})
export class SendComponent implements OnInit {

  isCC: boolean;
  isBcc: boolean;

  @Input() modal = false;
  @Input() dosFormId: string;
  @Input() invoiceId: string;
  @Input() aformDetails: ApplicationDetailsModel;
  @Input() attachedListings = [];
  @Input() prefilledEmail: string;
  @Input() prefilledSubject: string;
  @Input() prefilledMessage: string;
  @Output() afterSend = new EventEmitter();

  searchTerm = new Subject<string>();
  search = '';
  page = 0;
  per_oage = 10;

  sending: boolean;

  sortField = 'time_created';
  sortDirection: 'desc' | 'asc' = 'desc';
  sortData: { label: string, field: string }[] = [
    { label: 'Recent', field: 'time_created' },
    { label: 'Alphabetically', field: 'first_name' },
  ];

  contacts: ContactModel[] = [];
  search_contacts: ContactModel[] = [];

  contacts_to_loading: boolean = false;
  contacts_cc_loading: boolean = false;
  contacts_bcc_loading: boolean = false;
  submitted: boolean = false;
  contactsToTypeahead = new Subject<string>();
  contactsCcTypeahead = new Subject<string>();
  contactsBccTypeahead = new Subject<string>();

  messageForm: FormGroup;
  last_term: string = '';

  clients: ClientModel[];
  attach_listings = false;
  saved_listings: SavedListingModel[] = [];
  selected_listings: string[] = [];

  applicable_fields = [];
  attach_aform_pdf = true;

  constructor(public contactService: ContactService,
              public messageServices: MessageService,
              private clientService: ClientService,
              private listingService: ListingService,
              private router: Router,
              private cd: ChangeDetectorRef,
              private fb: FormBuilder,
              private alertService: BootstrapAlertService,
              folderService: FolderService) {
    folderService.setCurrentFolder(null);
    clientService.getSuggestClients().subscribe(clients => this.clients = clients);
  }

  ngOnInit() {

    this.searchTerm
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(text => this.update_contact_list(text))
      .subscribe(res => this.contacts = res);

    this.update_contact_list().subscribe(data => {
      this.contacts = data;
      this.search_contacts = data.slice(0);
    });

    this.loadToSearchContacts();
    this.loadCcSearchContacts();
    this.loadBccSearchContacts();

    this.messageForm = this.fb.group({
      to: [this.fb.array([[], [Validators.required, Validators.email]]), Validators.required],
      cc: [],
      bcc: [],
      subject: ['', Validators.required],
      body: [''],
      attachments: []
    });
    this.messageForm.get('to').patchValue([]);

    if (this.modal) {
      if (this.attachedListings.length) {
        this.clientService.getCurrentClient().subscribe((client: any) => {
          if (client) {
            this.toogleContact(client);
          }
        });
        this.attach_listings = true;
        this.attachedListings.forEach(item => {
          item.checked = true;
        });
        this.saved_listings = this.attachedListings;
      }

      if (this.aformDetails) {
        this.applicable_fields = this.getAformFileFields();
      }
    }
    if (this.prefilledEmail) {
      this.toogleContact(new ContactModel().deserialize(this.addContact(this.prefilledEmail)));
    }
    if (this.prefilledSubject) {
      this.messageForm.get('subject').patchValue(this.prefilledSubject);
    }
    if (this.prefilledMessage) {
      this.messageForm.get('body').patchValue(this.prefilledMessage);
    }
  }

  sortContacts(field_name: string) {

    this.page = 0;

    if (this.sortField === field_name) {
      this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    }

    this.sortField = field_name;

    this.update_contact_list().subscribe((data: ContactModel[]) => this.contacts = data);
  }

  loadMore() {
    this.page++;
    this.update_contact_list().subscribe((data: ContactModel[]) => this.contacts = this.contacts.concat(data));
  }

  update_contact_list(search_text: string = null) {
    if (search_text !== null) {
      this.search = search_text;
    }
    return this.contactService.getContacts(this.search, this.per_oage, this.per_oage * this.page, this.sortField, this.sortDirection);
  }

  selectAll() {
    // this.search_contacts = this.search_contacts.concat(this.contacts);
    // this.messageForm.controls.to.patchValue(this.contacts.map(contact => contact.email));
  }

  toogleContact(contact: ContactModel) {
    let exist = false;
    this.search_contacts.forEach((c) => {
      if (c.email === contact.email) {
        exist = true;
      }
    });
    if (!exist) this.search_contacts = this.search_contacts.concat([contact]);
    const index = this.messageForm.value.to.indexOf(contact.email);
    if (index > -1) {
      this.messageForm.value.to.splice(index, 1);
    } else {
      this.messageForm.value.to.push(contact.email);
    }
    this.messageForm.get('to').patchValue(this.messageForm.value.to);
  }

  sendMessage() {

    this.submitted = true;
    if (!this.messageForm.valid) {
      return;
    }

    this.sending = true;
    const params = this.messageForm.value;
    if (this.messageForm.value.to.length === 1) {
      const contact = this.contacts.find((c) => this.messageForm.value.to.indexOf(c.email) > -1);
      params.to_name = contact ? contact.name : null;
    }

    if (this.selected_listings.length) {
      params.payload = { type: 'listings', listing_ids: this.selected_listings };
    }

    if (this.aformDetails) {
      params.payload = {
        type: 'aform',
        aform_id: this.aformDetails.application_form_id,
        file_list: this.applicable_fields.filter(field => field.send).map(field => field.name),
        attach_aform_pdf: this.attach_aform_pdf
      };
    }

    if (this.dosFormId) {
      params.payload = { type: 'dos', dos_form_id: this.dosFormId };
    }

    if (this.invoiceId) {
      params.payload = { type: 'invoice', invoice_id: this.invoiceId };
    }

    if (!(this.selected_listings || this.selected_listings.length) && !this.aformDetails && !this.dosFormId && !this.invoiceId) {
      params.payload = null;
    }

    this.messageServices.sendMessage(params).subscribe(
      (data) => {
        this.sending = false;

        if (!this.modal) {
          this.router.navigate(['/mailbox/' + data.labelIds[0]], { fragment: data.threadId });
        }

        this.alertService.showSucccess('Your message has been sent!');

        this.afterSend.emit(data);
      },
      (errorData) => {
        this.sending = false;

        this.alertService.showError(errorData.error.errorMsg);
      });
  }

  loadToSearchContacts() {
    this.contactsToTypeahead.pipe(
      tap(() => this.contacts_to_loading = true),
      distinctUntilChanged(),
      debounceTime(100),
      switchMap(term => {
        this.last_term = term;
        return this.contactService.getContacts(term);
      }),
    ).subscribe(x => {
      const contact = this.search_contacts.find(c => this.messageForm.value.to.indexOf(c.email) > -1);
      this.search_contacts = contact ? [contact].concat(x) : x;
      this.contacts_to_loading = false;
      this.cd.markForCheck();
    }, () => {
      this.search_contacts = [];
    });
  }

  loadCcSearchContacts() {
    this.contactsCcTypeahead.pipe(
      tap(() => this.contacts_cc_loading = true),
      distinctUntilChanged(),
      debounceTime(100),
      switchMap(term => this.contactService.getContacts(term)),
    ).subscribe(x => {
      this.search_contacts = x;
      this.contacts_cc_loading = false;
      this.cd.markForCheck();
    }, () => {
      this.search_contacts = [];
    });
  }

  loadBccSearchContacts() {
    this.contactsBccTypeahead.pipe(
      tap(() => this.contacts_bcc_loading = true),
      distinctUntilChanged(),
      debounceTime(100),
      switchMap(term => this.contactService.getContacts(term)),
    ).subscribe(x => {
      this.search_contacts = x;
      this.contacts_bcc_loading = false;
      this.cd.markForCheck();
    }, () => {
      this.search_contacts = [];
    });
  }

  addContact(email) {
    return { email: email, name: email, tag: true, user_id: null };
  }

  makeContactIfValid() {
    setTimeout(() => {
      if (this.last_term && this.messageForm.value.to.indexOf(this.last_term) === -1) {
        this.toogleContact(new ContactModel().deserialize(this.addContact(this.last_term)));
      }
    }, 110);
  }

  clearToMail() {
    this.last_term = '';
    $('[name="to"] .ng-input input').val('');
  }

  onFocus() {
    const contact = this.search_contacts.find(c => this.messageForm.value.to.indexOf(c.email) > -1);
    if (contact && contact.user_id === null) {
      $('[name="to"] .ng-input input').val(contact.name);
    }
  }

  changeToMail() {
    this.clearToMail();

    if (!this.modal) {

      this.saved_listings = [];

      const client = this.clients.find(c => this.messageForm.value.to.indexOf(c.email) > -1);

      if (client) {
        this.listingService.getSavedListings(client.user_id).subscribe((listings) => {
          this.saved_listings = listings.filter(l => !l.sent);
        });
      }
    }

  }

  getAformFileFields() {
    return this.aformDetails.report
      .filter((field) => field.field.type === 'file' && field.field.field !== 'signature')
      .map(field => {
        return {
          label: field.field.title,
          name: field.field.field,
          send: false,
          disabled: !field.value,
          url: field.value && field.value.url ? field.value.url : null
        };
      });
  }

  onFilesAttached(files: FileList) {
    if (files.length === 0) {
      return;
    }

    const fileArray: File[] = Array.from(files);

    fileArray.forEach((f: File) => {
      let reader = new FileReader();
      reader.readAsDataURL(f);
      reader.onload = () => {
        let attachments = this.messageForm.get('attachments').value === null ? [] : this.messageForm.get('attachments').value;

        if (attachments.find(el => el.name === f.name)) {
          return;
        }

        attachments.push({
          name: f.name,
          type: f.type,
          size_estimate: this.formatBytes(f.size),
          data: reader.result.split(',')[1]
        });

        this.messageForm.get('attachments').patchValue(attachments);
      };
    });
  }

  removeAttachment(f) {
    let attachments: any[] = this.messageForm.get('attachments').value;

    const idx = attachments.findIndex((att) => f.name === att.name);

    if (idx > -1) {
      attachments.splice(idx, 1);
      this.messageForm.get('attachments').patchValue(attachments);
    }
  }

  formatBytes(x) {
    const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    let l = 0, n = parseInt(x, 10) || 0;
    while (n >= 1024 && ++l) {
      n = n / 1024;
    }

    return (n.toFixed(n >= 10 || l < 1 ? 0 : 1) + ' ' + units[l]);
  }

}
