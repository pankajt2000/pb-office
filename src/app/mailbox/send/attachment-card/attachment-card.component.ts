import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-attachment-card',
  templateUrl: './attachment-card.component.html',
  styleUrls: ['./attachment-card.component.less']
})
export class AttachmentCardComponent implements OnInit {

  @Input() file: {
    name: string,
    type: string,
    size_estimate: string
  };

  constructor() { }

  ngOnInit() {
  }

  getCardStyle() {
    const archiveMimeTypes = [
      "application/x-7z-compressed",
      "application/x-rar-compressed",
      "application/x-zip-compressed",
      "application/zip"
    ];

    const msWordMimeTypes = [
      "application/msword",
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    ]

    const msExcelMimeTypes = [
      "application/vnd.ms-excel",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    ]

    if (this.file.type.indexOf('image') > -1) {
      return { icon: 'fas fa-file-image', backgroundColor: '#f2cf98'};
    }

    if (this.file.type.indexOf('pdf') > -1) {
      return { icon: 'fas fa-file-pdf', backgroundColor: '#e69f84'};
    }

    if (msWordMimeTypes.includes(this.file.type)) {
      return { icon: 'fas fa-file-word', backgroundColor: 'blue'};
    }

    if (msExcelMimeTypes.includes(this.file.type)) {
      return { icon: 'fas fa-file-excel', backgroundColor: 'green'};
    }

    if (archiveMimeTypes.includes(this.file.type)) {
      return { icon: 'fas fa-file-archive', backgroundColor: '#D49bad'};
    }

    return { icon: 'fas fa-file-alt', backgroundColor: '#dad0d0'};
  }
}
