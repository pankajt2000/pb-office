import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MessageModel } from '../../models/message.model';
import { MessageService } from '../../services/message.service';
import { FolderService } from '../../services/folder.service';
import { Router } from '@angular/router';
import { SavedListingService } from '../../../listings/services/saved-listing.service';
import { ClientModel } from '../../../client/models/client.model';
import { ClientService } from '../../../client/services/client.service';
import { QuickviewService } from '../../../layout/quickview/quickview.service';
import { ThreadModel } from '../../models/thread.model';
import { DomSanitizer } from "@angular/platform-browser";

declare var $: any;
declare var document: any;
declare var window: any;

@Component({
  selector: 'app-message-details',
  templateUrl: './message-details.component.html',
  styleUrls: ['./message-details.component.less']
})
export class MessageDetailsComponent implements OnInit {

  downloading: boolean;
  selected_thread: ThreadModel;
  selected_listings: string[] = [];
  saved_listings = [];
  client: ClientModel;

  attachedFiles: {
    name: string,
    size_estimate: string,
    type: string,
    data: any
  }[] = [];

  @Output() syncAllMessages = new EventEmitter();

  @Input()
  set thread(thread: ThreadModel) {
    if (this.selected_thread && this.selected_thread.id === thread.id) return;

    this.selected_thread = thread;

    for (const messaghe of this.selected_thread.messages) {
      this.markAsSeen(messaghe);
    }

    setTimeout(() => {
      this.syncAllMessages.emit();
    }, 1000);
  }

  get thread(): ThreadModel {
    return this.selected_thread;
  }

  constructor(private messageService: MessageService,
              private folderService: FolderService,
              public savedListingService: SavedListingService,
              private clientSercive: ClientService,
              public quickviewService: QuickviewService,
              private router: Router,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.savedListingService.getSavedListings().subscribe(data => this.saved_listings = data.filter(item => !item.sent));
    this.clientSercive.getCurrentClient().subscribe(data => this.client = data);

    this.quickviewService.getOpened().subscribe((is_opened) => {
      if (is_opened && this.selected_thread.from_user && this.client.user_id !== this.selected_thread.from_user.user_id) {
        this.clientSercive.setCurrentClientById(this.selected_thread.from_user.user_id.toString());
      }
    });
  }

  getExcerptAsSafeHTML(excerpt: string) {
    return this.sanitizer.bypassSecurityTrustHtml(excerpt);
  }

  isOpened(thread_message, is_last) {
    thread_message.opened = (!thread_message.seen || is_last || thread_message.toogle /*|| !thread_message.isMultiline()*/);
    return thread_message.opened;
  }

  markAsSeen(message?: MessageModel) {
    message = message || this.selected_thread.messages[0];
    if (!message.seen) {
      this.messageService.martAsSeen(message).subscribe(() => {
        message.seen = 1;
      });
    }
  }

  putInIframe(message: MessageModel, $event: Event) {
    const iframePointer: any = $event.currentTarget;

    iframePointer.contentWindow.document.open('text/html', 'replace');
    iframePointer.contentWindow.document.write(message.email_html);
    iframePointer.contentWindow.document.close();
    setTimeout(() => {
      iframePointer.style.height = iframePointer.contentWindow.document.body.scrollHeight + 'px';
      $('a', iframePointer.contentWindow.document).attr('target', '_blank');
    }, 200);

  }

  getDetails(callback?) {
    this.messageService.getThread(this.selected_thread).subscribe((thread) => {
      this.selected_thread = thread;

      if (callback) callback();
    });
  }

  replyMessage(message: MessageModel) {

    let payload = null;
    if (this.selected_listings.length) {
      payload = { type: 'listings', listing_ids: this.selected_listings };
    }

    message.replying = true;

    this.messageService.replyMessage(message, message.reply_message, payload, this.attachedFiles).subscribe((data) => {
      this.getDetails(() => {
        message.reply_message = '';
        message.replying = false;
      });

      if (this.selected_listings.length) {
        this.savedListingService.initSavedListings();
      }
    });
  }

  forwardMessage(message: MessageModel) {
    // message.forwarding = true;
    // this.messageService.forwardMessage(message, message.forward_message).subscribe((data) => {
    //   this.getDetails(() => {
    message.forward_message = '';
    // message.forwarding = false;
    //   });
    // });
  }

  show_forward_form(message: MessageModel) {
    // message.reply_form = false;
    // message.forward_form = true;
  }

  show_reply_form(message: MessageModel) {
    message.forward_form = false;
    message.reply_form = true;
  }

  delete_message(message: MessageModel) {
    this.messageService.deleteMessage(message).subscribe(() => {
      this.getDetails();
    });
  }

  delete_thread(message: ThreadModel) {
    this.messageService.deleteThread(message).subscribe(() => {
      this.router.navigate(['/mailbox']);
    });
  }

  unread_message(message: MessageModel) {
    this.messageService.martAsUnread(message).subscribe(() => {
      this.router.navigate(['/mailbox']);
    });
  }

  onFilesAttached(files: FileList) {
    if (files.length === 0) {
      return;
    }
    
    const fileArray: File[] = Array.from(files);

    fileArray.forEach((f: File) => {
      let reader = new FileReader();
      reader.readAsDataURL(f);
      reader.onload = () => {     
        if (this.attachedFiles.find(el => el.name === f.name)) {
          return;
        }

        this.attachedFiles.push({
          name: f.name,
          type: f.type,
          size_estimate: this.formatBytes(f.size),
          data: reader.result.split(',')[1]
        });

        console.log('Attached files', this.attachedFiles);
      }
    });
  }

  removeAttachment(f) {
    const idx = this.attachedFiles.findIndex((att) => f.name === att.name);

    if (idx > -1) {
      this.attachedFiles.splice(idx, 1);
    }
  }

  formatBytes(x) {
    const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    let l = 0, n = parseInt(x, 10) || 0;
    while (n >= 1024 && ++l) {
        n = n/1024;
    }

    return (n.toFixed(n >= 10 || l < 1 ? 0 : 1) + ' ' + units[l]);
  }

  backToList() {
    window.location.hash = '';
  }
}
