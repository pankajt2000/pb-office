import { AfterViewChecked, ChangeDetectorRef, Component, HostListener, OnInit } from '@angular/core';
import { MessageModel } from '../models/message.model';
import { MessageService } from '../services/message.service';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { ActivatedRoute } from '@angular/router';
import { FolderService } from '../services/folder.service';
import { FolderModel } from '../models/folder.model';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { PushMessagesService } from '../../layout/header/alerts/push-messages.service';
import { ClientService } from '../../client/services/client.service';
import { ThreadModel } from '../models/thread.model';
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.less']
})
export class InboxComponent implements OnInit, AfterViewChecked {

  searchTerm = new Subject<string>();
  search = '';
  per_oage = 20;
  sortField = 'time_created';
  sortDirection: 'desc' | 'asc' = 'desc';
  scroll_height;
  next_page_token: string;
  loading = true;
  loading_more = false;

  sortData: { label: string, field: string }[] = [
    { label: 'Recent', field: 'time_created' },
    { label: 'Alphabetically', field: 'subject' },
  ];

  threads: ThreadModel[] = [];
  displayed_thread: ThreadModel;
  selected_threads: string[] = [];
  folders: FolderModel[] = [];
  folder_id: string;
  folder_model: FolderModel;
  is_partial_select = false;
  select_all = false;
  q_filter = '';

  constructor(private messageService: MessageService,
              private pushMessagesService: PushMessagesService,
              private folderService: FolderService,
              private clientService: ClientService,
              private alertService: BootstrapAlertService,
              private cdRef: ChangeDetectorRef,
              private router: Router,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer) {

    this.route.params.subscribe(params => {

      this.threads = [];
      this.next_page_token = null;
      this.folder_id = params['folder'] || null;
      this.loading = true;

      const old_q = this.q_filter;
      this.q_filter = ('q' in params) ? `from:${params['q']}` : '';

      this.folderService.getFolders().subscribe((folders) => {

        this.folders = folders;

        const old_folder = this.folder_model;

        if (this.folder_id && this.folders.length) {
          this.folder_model = this.folders.find((folder) => folder.id === this.folder_id);
        }

        if (this.folders.length && !this.folder_model) {
          this.folder_model = this.folders.find((folder) => folder.id === 'INBOX');
        }

        if (this.folder_model && (old_folder !== this.folder_model || old_q !== this.q_filter)) {
          this.folderService.setCurrentFolder(this.folder_model);
          this.initMessages();
        }
      });
    });

  }

  openMessage(thread: ThreadModel) {
    window.location.hash = thread.id;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.scroll_height = window.innerHeight;
  }

  ngOnInit() {

    this.scroll_height = window.innerHeight;

    this.pushMessagesService.getPushMessages().subscribe((resp) => {
      if (resp && 'mailboxes' in resp && this.folder_model) {
        if (resp.mailboxes.find(l => l.id === this.folder_model.id && l.threadsTotal !== this.folder_model.threadsTotal)) {
          this.syncAllMessages();
        }

        /** update folder models */
        this.folders.forEach((folder_model, index) => {
          resp.mailboxes.forEach((folder) => {
            if (folder.id === folder_model.id) {
              this.folders[index] = Object.assign(this.folders[index], folder);
            }
          });
        });
        this.folderService.setFolders(this.folders);
      }
    });

    this.searchTerm
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(text => {
        const folder_name = this.folder_model ? this.folder_model.id : '';
        return this.messageService.getThreads(folder_name, this.per_oage, '', text);
      })
      .subscribe(data => {
        this.threads = data.threads;
        this.next_page_token = data.next_page_token;
      });

  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  initMessages() {
    this.update_message_list()
      .subscribe((data) => {
        this.threads = data.threads;
        this.next_page_token = data.next_page_token;

        this.loading = false;

        this.route.fragment.subscribe(fragment => {

          if (fragment) {


            this.displayed_thread = null;
            for (const thread of this.threads) {
              if (thread.id === fragment) {
                this.displayed_thread = thread;
                return;
              }
            }

            this.messageService.getThreadById(fragment).subscribe((message) => {
              this.displayed_thread = message;
            }, (errorData) => {
              if (errorData.status === 404) {
                this.alertService.showError('Message not found');
              } else {
                this.alertService.showError('Oops something went wrong');
              }
            });
          } else {
            this.displayed_thread = null;
          }
        });

      });
  }

  update_message_list(token: string = null) {

    const folder_name = this.folder_model ? this.folder_model.id : '';

    return this.messageService.getThreads(folder_name, this.per_oage, token, this.q_filter);
  }

  getSnippetAsSafeHTML(snippet: string) {
    return this.sanitizer.bypassSecurityTrustHtml(snippet);
  }

  sortMessages(field_name: string) {
    // this.next_page_token = null;
    //
    // if (this.sortField === field_name) {
    //   this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    // }
    //
    // this.sortField = field_name;
    //
    // this.update_message_list().subscribe((data) => {
    //   this.threads = data.threads;
    //   this.next_page_token = data.next_page_token;
    // });
  }

  composeMessage() {
    this.router.navigate(['mailbox', 'send']);
  }

  loadMore() {
    this.loading_more = true;
    this.update_message_list(this.next_page_token).subscribe((data) => {
      this.threads = this.threads.concat(data.threads);
      this.next_page_token = data.next_page_token;
      this.loading_more = false;
    });
  }

  selectAllMessages() {
    if (this.is_partial_select) {
      this.unselectAllMessages();
      return;
    }

    this.is_partial_select = false;

    this.selected_threads = [];
    this.threads.forEach(thread => {
      if (this.selected_threads.indexOf(thread.id) === -1) {
        this.selected_threads.push(thread.id);
      }
      thread.selected = true;
    });
  }

  unselectAllMessages() {
    this.selected_threads = [];
    this.threads.forEach(message => {
      message.selected = false;
    });
    this.is_partial_select = false;
    this.select_all = false;
  }

  selectThread(message: MessageModel) {
    message.selected = true;
    if (this.selected_threads.indexOf(message.id) === -1) {
      this.selected_threads.push(message.id);
    }

    this.is_partial_select = (this.selected_threads.length > 0 && this.selected_threads.length !== this.threads.length);
  }

  unselectThread(message: MessageModel) {
    message.selected = false;
    const index = this.selected_threads.indexOf(message.id);

    if (index > -1) {
      this.selected_threads.splice(index, 1);
    }

    this.is_partial_select = (this.selected_threads.length > 0 && this.selected_threads.length !== this.threads.length);
    this.select_all = false;
  }

  markAs(flag) {
    this.threads.forEach(thread => {
      if (thread.selected) {
        if (flag === 'seen') {
          thread.seen = 1;
        }

        if (this.displayed_thread && this.displayed_thread.id === thread.id) {
          window.location.hash = '';
        }
      }
    });

    this.messageService.massMarkAs(flag, this.selected_threads).subscribe(() => {
      this.syncAllMessages();
      this.unselectAllMessages();
    });
  }

  unmarkAs(flag) {
    this.threads.forEach(thread => {
      if (thread.selected) {
        if (flag === 'seen') {
          thread.seen = 0;
        }
      }
    });
    this.messageService.massUnMark(flag, this.selected_threads).subscribe(() => {
      this.syncAllMessages();
      this.unselectAllMessages();
    });
  }

  deleteMessages() {
    this.selected_threads.forEach((id) => {
      const index = this.threads.findIndex((thread) => thread.id === id);
      if (index > -1) {
        this.threads.splice(index, 1);
      }
    });
    this.removeMessages(this.selected_threads);
    this.unselectAllMessages();
  }

  addMessageTag(message: ThreadModel, tag: string) {
    this.messageService.addTag(message, tag).subscribe(() => {
      this.syncAllMessages();
      this.clientService.getSuggest().subscribe(data => data);
    }, (resp) => {
      this.alertService.showError(resp.error.errorMsg);
      this.syncAllMessages();
      this.clientService.getSuggest().subscribe(data => data);
    });
  }

  syncMessage(thread) {
    this.messageService.getThread(thread).subscribe((data) => {
      const index = this.threads.indexOf(thread);
      this.threads[index] = data;
    });
  }

  syncAllMessages() {

    if (!this.threads.length) return;

    const folder_name = this.folder_model ? this.folder_model.id : '';
    const count = this.threads.length < 10 ? 10 : this.threads.length;

    this.messageService.getThreads(folder_name, count).subscribe(data => {
      this.threads = data.threads;
      this.next_page_token = data.next_page_token;
    });
  }

  removeMessage(message: ThreadModel) {
    this.messageService.deleteThread(message).subscribe(data => data, (data) => {
      if (data.status === 404) {
        this.alertService.showError('Message not found');
      } else {
        this.alertService.showError('Oops something went wrong');
      }
      this.syncAllMessages();
    });

    const index = this.threads.indexOf(message);
    if (index > -1) this.threads.splice(index, 1);

    if (this.displayed_thread && this.displayed_thread.id === message.id) {
      window.location.hash = '';
    }
  }

  removeMessages(ids: string[]) {
    if (!ids.length) return;

    this.messageService.deleteThreads(ids).subscribe(data => data, (data) => {
      if (data.status === 404) {
        this.alertService.showError('Messages not found');
      } else {
        this.alertService.showError('Oops something went wrong');
      }
      this.selected_threads = [];
      this.is_partial_select = false;
      this.syncAllMessages();
    });
  }
}
