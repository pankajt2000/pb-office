import { environment } from '../../../environments/environment';
import { DeserializableInterface } from '../../shared/models/deserializable.interface';

export class ContactModel implements DeserializableInterface<ContactModel> {

  user_id: number;
  email: string;
  photo: string;
  phone: string;
  name: string;
  time_created: number;

  getAvatarUrl() {
    return environment.media_base_url + this.photo;
  }

  hasAvatar() {
    return !!this.photo;
  }

  getSenderName() {
    return this.name ? this.name : this.email;
  }

  deserialize(input: any): ContactModel {
    Object.assign(this, input);
    return this;
  }

}
