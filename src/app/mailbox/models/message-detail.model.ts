import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { environment } from '../../../environments/environment';
import { MessageModel } from './message.model';

export class MessageDetailModel implements DeserializableInterface<MessageDetailModel> {

  _id: string;
  message_id: string;
  thread: MessageModel[];


  deserialize(input: any): MessageDetailModel {
    input['thread'] = input['thread'].map((thread) => Object.assign(new MessageModel(), thread));
    Object.assign(this, input);
    return this;
  }
}
