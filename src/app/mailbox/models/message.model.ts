import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { environment } from '../../../environments/environment';
import { SavedListingModel } from '../../listings/models/saved-listing.model';

export class MessageModel implements DeserializableInterface<any> {

  id: string;
  message_id: string;
  thread_id: string;
  label_ids: string[];
  time_created: number;
  excerpt: string;
  subject: string;
  from_name: string;
  from_email: string;
  to_name: string;
  to_email: string;
  seen: 0 | 1;
  email_plain: string;
  email_html: string;
  has_attachments: 0 | 1;
  attachments: { filename: string, mime_type: string }[];
  from_user?: {
    user_id: number;
    enabled: boolean;
    first_name: string;
    last_name: string;
    phone: number;
    email: string;
    name: string;
    photo: string;
  };
  from_info: {
    type: string,
    title: string,
    bgColor: string,
    color: string
  };
  payload: { type: string, listing_ids: string[], listings: SavedListingModel[] };

  opened: boolean = false;
  forward_message: string = '';
  forward_form: boolean = false;
  forwarding: boolean = false;

  reply_message: string = '';
  reply_form: boolean = false;
  replying: boolean = false;

  attach_listings = false;

  selected = false;
  show_payload = false;

  getAvatarUrl() {
    return environment.media_base_url + (this.from_user ? this.from_user.photo : '');
  }

  hasAvatar() {
    return this.from_user && this.from_user.photo;
  }

  hasPayload() {
    return 'listings' in this.payload;
  }

  getSubject() {
    return this.subject || '(no subject)';
  }

  getSenderName() {
    return this.from_user ? this.from_user.name : this.from_email;
  }

  hasAttachment() {
    return this.has_attachments;
  }

  getCreateTime() {
    return new Date(this.time_created * 1000);
  }

  deserialize(input: any): MessageModel {
    if (input.payload && input.payload.type === 'listings') {
      input.payload.listings = input.payload.listings.map(listing => new SavedListingModel().deserialize(listing));
    }
    Object.assign(this, input);
    return this;
  }

  isIframeContent() {
    return this.email_html && this.email_html.indexOf('<html') >= 0;
  }

  getBody() {
    return this.email_html ? this.email_html : this.email_plain;
  }

  isMultiline() {
    return this.email_html && this.email_html.indexOf('<div') >= 0;
  }

  getPrefixForwardMessage() {
    return `
    ---------- Forwarded message ----------
From: ${this.from_email}
Date: ${this.getCreateTime()}
Subject: ${this.subject}
    `;
  }

  getReplyQuote() {
    return `
    <div class="gmail_extra"><br>
      <div class="gmail_quote">On ${this.getCreateTime().toUTCString()}, ${this.getSenderName()} <span dir="ltr">&lt;<a
        href="mailto:${this.from_email}" target="_blank">${this.from_email}</a>&gt;</span> wrote:<br>
    <blockquote class="gmail_quote" style="margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex">${this.getBody()}</blockquote>
  </div>
  <br></div>
`;
  }
}
