import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { environment } from '../../../environments/environment';
import { MessageModel } from './message.model';

export class ThreadModel implements DeserializableInterface<any> {

  from_email: string;
  from_user?: {
    user_id: number;
    enabled: boolean;
    first_name: string;
    last_name: string;
    phone: number;
    email: string;
    name: string;
    photo: string;
  };
  from_info: {
    type: string,
    title: string,
    bgColor: string,
    color: string
  };
  from_name: string;
  has_attachments: 0 | 1;
  id: string;
  messages: MessageModel[];
  seen: 0 | 1;
  snippet: string;
  subject: string;
  thread_count: number;
  time_created: number;
  unseen_count: number;

  opened: boolean = false;
  forward_message: string = '';
  forward_form: boolean = false;
  forwarding: boolean = false;

  reply_message: string = '';
  reply_form: boolean = false;
  replying: boolean = false;

  attach_listings = false;

  selected = false;

  hasAttachment() {
    return this.has_attachments;
  }

  hasPayload() {
    return this.messages.filter(message => 'listing_ids' in message.payload).length > 0;
  }

  getAvatarUrl() {
    return environment.media_base_url + (this.from_user ? this.from_user.photo : '');
  }

  hasAvatar() {
    return this.from_user && this.from_user.photo;
  }

  getSenderName() {
    return this.from_name ? this.from_name : this.from_email;
  }

  getSubject() {
    return this.subject || '(no subject)';
  }

  deserialize(input): ThreadModel {
    input.messages = input.messages.map(message => new MessageModel().deserialize(message));
    Object.assign(this, input);
    return this;
  }
}
