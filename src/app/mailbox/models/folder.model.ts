import { DeserializableInterface } from '../../shared/models/deserializable.interface';

export class FolderModel implements DeserializableInterface<FolderModel> {

  id: string;
  labelListVisibility: string;
  messageListVisibility: string;
  messagesTotal: number;
  messagesUnread: number;
  name: string;
  threadsTotal: number;
  threadsUnread: number;
  type: string;

  deserialize(input: any): FolderModel {
    Object.assign(this, input);
    return this;
  }

}
