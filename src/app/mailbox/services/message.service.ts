import { MessageModel } from '../models/message.model';
import { AgentApiService } from '../../services/api/agent-api.service';
import { Injectable } from '@angular/core';
import { MessageDetailModel } from '../models/message-detail.model';
import { ThreadModel } from '../models/thread.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../../environments/environment';

@Injectable()
export class MessageService {

  alert_messages: BehaviorSubject<ThreadModel[]> = new BehaviorSubject<ThreadModel[]>([]);

  constructor(private api: AgentApiService) {
  }

  getMessages(
    search: string = '',
    folder: string = 'INBOX',
    limit: number = 10,
    offset: number = 0,
    sort_by: string = 'time_created',
    sort_dir: string = 'DESC') {
    return this.api.get(
      `mailbox-message?limit=${limit}&offset=${offset}&sort_by=${sort_by}&sort_dir=${sort_dir}&search=${search}&mailbox=${folder}`)
      .map(data => {
        return data.map((item: MessageModel) => new MessageModel().deserialize(item));
      });
  }

  getThreads(label: string, limit: number = 10, next_page_token?: string, q_filter: string = '') {
    return this.api.get(
      `gmail-threads?label=${label}&count=${limit}&q=${q_filter}` + (next_page_token ? `&page_token=${next_page_token}` : ''))
      .map(data => {
        if (data) {
          data.threads = data.threads.map((item: ThreadModel) => new ThreadModel().deserialize(item));
          return data;
        }
        return [];
      });
  }

  martAsSeen(thread: ThreadModel | MessageModel) {
    return this.api.post(`gmail-flags/${thread.id}/seen`, {});
  }

  martAsUnread(thread: ThreadModel | MessageModel) {
    return this.api.delete(`gmail-flags/${thread.id}/seen`);
  }

  massUnMark(flag: string, ids: string[]) {
    return this.api.delete(`gmail-flags/${flag}?message_ids=${ids.join(',')}`);
  }

  massMarkAs(flag: string, ids: string[]) {
    return this.api.post(`gmail-flags/${flag}?message_ids=${ids.join(',')}`, {});
  }

  addTag(message: ThreadModel, tag) {
    return this.api.post(`gmail-message-tag/${message.id}/${tag}`, {});
  }

  updateTag(message: ThreadModel, tag) {
    return this.api.put(`mailbox-tag-email/${message.id}/${tag}`, {});
  }

  deleteMessage(message: MessageModel) {
    return this.api.delete(`gmail-messages/${message.id}`);
  }

  deleteThread(message: ThreadModel) {
    return this.api.delete(`gmail-messages?message_ids=${message.id}`);
  }

  deleteThreads(ids: string[]) {
    return this.api.delete(`gmail-messages?message_ids=${ids.join(',')}`);
  }

  downloadAttachment(message: MessageModel, file) {
    return this.api.get(`gmail-messages/${message.id}/attachment/${file}`, { responseType: 'blob', observe: 'response' });
  }

  getDetails(message: ThreadModel) {
    return this.getDetailsById(message.id);
  }

  getThread(message: ThreadModel) {
    return this.getThreadById(message.id);
  }

  getDetailsById(id: string) {
    return this.api.get('mailbox-message/' + id).map(data => {
      return new MessageDetailModel().deserialize(data);
    });
  }

  getThreadById(id: string) {
    return this.api.get('gmail-threads/' + id).map(data => {
      return new ThreadModel().deserialize(data);
    });
  }

  replyMessage(message: MessageModel, text: string, payload?: { type: string }, attachments?: any[]) {
    return this.api.post(`gmail-messages/${message.thread_id}`, {
      in_reply_to: message.message_id,
      cc: [],
      bcc: [],
      body: text, /*+ '\n' + message.getReplyQuote()*/
      payload: payload,
      attachments: attachments
    });
  }

  sendMessage(params) {
    params.cc = params.cc || [];
    params.bcc = params.bcc || [];
    return this.api.post(`gmail-messages`, params);
  }

  getPreviewAttachment(message: MessageModel, file) {
    return this.api.get(`gmail-messages/${message.id}/preview/${file.filename}`, { responseType: 'blob', observe: 'response' });
  }

  getSourceMail(mailId){
    return this.api.get(`gmail-listings-source/${mailId}`).map(message => new MessageModel().deserialize(message));
  }

  // forwardMessage(message: MessageModel, text: string) {
  //   return this.api.post(`mailbox-message/${message.id}/forward`, {
  //     tp: [],
  //     cc: [],
  //     bcc: [],
  //     subject: 'Fwd: ' + message.subject,
  //     body: text + '\n' + message.getBody()
  //   });
  // }

}
