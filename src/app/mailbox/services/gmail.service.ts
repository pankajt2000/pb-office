import { AgentApiService } from '../../services/api/agent-api.service';
import { Injectable } from '@angular/core';

@Injectable()
export class GmailService {

  constructor(private api: AgentApiService) {
  }

  getAuthUrl(){
    return this.api.get('gmail-auth');
  }

  getTemplates(context: string, params?: { [key: string]: any }) {
    let param_str = '';

    if (params) {
      param_str = '?' + Object.keys(params).map(key => `${key}=${params[key]}`).join('&');
    }

    return this.api.get(`gmail-context-templates/${context}${param_str}`);
  }
}
