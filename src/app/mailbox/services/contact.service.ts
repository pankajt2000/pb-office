import { AgentApiService } from '../../services/api/agent-api.service';
import { Injectable } from '@angular/core';
import { ContactModel } from '../models/contact.model';
import 'rxjs/add/observable/of';

@Injectable()
export class ContactService {

  constructor(private api: AgentApiService) {
  }

  getContacts(search: string = '', limit: number = 10, offset: number = 0, sort_by: string = 'last_active', sort_dir: 'asc' | 'desc' = 'desc') {
    return this.api.get(`suggest-contacts?search=${search}&limit=${limit}&offset=${offset}&sort_by=${sort_by}&sort_dir=${sort_dir}`)
      .map(data => {
        return data.map((item: ContactModel) => new ContactModel().deserialize(item));
      });
  }

}
