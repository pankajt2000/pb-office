import { AgentApiService } from '../../services/api/agent-api.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FolderModel } from '../models/folder.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FolderService {

  private folders: BehaviorSubject<FolderModel[]> = new BehaviorSubject<FolderModel[]>([]);
  private current_folder: BehaviorSubject<FolderModel> = new BehaviorSubject<FolderModel>(null);

  constructor(private api: AgentApiService) {
  }

  public setFolders(value: FolderModel[]): void {
    this.folders.next(value);
  }

  public getFolders(): Observable<FolderModel[]> {
    return this.folders;
  }

  public setCurrentFolder(value: FolderModel): void {
    if (this.current_folder.getValue() !== value) {
      this.current_folder.next(value);
    }
  }

  public getCurrentFolder(): Observable<FolderModel> {
    return this.current_folder;
  }

  featchFolders(force = false): Observable<FolderModel[]> {
    if (this.folders.getValue().length && !force) {
      return this.getFolders();
    }

    this.api.get(`gmail-labels`)
      .map(data => data.map(item => new FolderModel().deserialize(item)))
      .subscribe(folders => this.setFolders(folders));
    return this.getFolders();
  }

}
