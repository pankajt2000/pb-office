import { Component, Input, OnInit } from '@angular/core';
import { GmailService } from "../services/gmail.service";
import { FormControl, FormGroup } from "@angular/forms";

type Template = { subject?: string, message: string };

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.less']
})
export class TemplatesComponent implements OnInit {

  @Input('context') context: string;
  @Input('form-group') formGroup: FormGroup;
  @Input('subject-control-name') subjectControl: string;
  @Input('message-control-name') messageControl: string;
  @Input('other-params') params: {[key: string]: any};

  templates: Template[] = [];

  constructor(
    private gmailService: GmailService
  ) { }

  ngOnInit() {
    if (!this.subjectControl) {
      this.subjectControl = 'subject';
    }

    if (!this.messageControl) {
      this.messageControl = 'message';
    }

    this.gmailService.getTemplates(this.context, this.params).subscribe(templates => {
      this.templates = templates;
    })
  }

  templateSelected(template: Template) {
    if (template.subject) {
      this.formGroup.get(this.subjectControl).patchValue(template.subject);
    }

    this.formGroup.get(this.messageControl).patchValue(template.message);
  }

}
