import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InboxComponent } from './inbox/inbox.component';
import { LayoutModule } from '../layout/layout.module';
import { SharedModule } from '../shared/shared.module';
import { ContactService } from './services/contact.service';
import { FolderService } from './services/folder.service';
import { MessageDetailsComponent } from './inbox/message-details/message-details.component';
import { ContextMenuModule } from 'ngx-contextmenu';
import { TemplatesComponent } from './templates/templates.component';

@NgModule({
  declarations: [
    InboxComponent,
    MessageDetailsComponent,
    TemplatesComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    LayoutModule,
    ContextMenuModule.forRoot()
  ],
  providers: [
    ContactService,
    FolderService,
  ],
  exports: [
    TemplatesComponent
  ]
})
export class MailboxModule { }
