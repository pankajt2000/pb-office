// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const config = {

  app_version: 'v1.18.3',

  media_sizes: {
    jango_contact: 'jango-contact',
    wlist: 'wlist',   // 80x80
    wdgrid: 'wdgrid', // 80x40
    wcomb: 'wcomb',   // 200x120
    wadmin: 'wadmin', // 200x250
    wlisting: 'wlisting-l' // 555x500
  },

  login_page_url: '/auth/login',
  access_token_local_storage: 'access_token',
  is_google_session: 'is_google_session',
  user_local_storage: 'user_data',
  default_content_type: 'application/json',
};
