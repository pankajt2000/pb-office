import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/throw';

import { environment } from '../../../environments/environment';
import { Error } from './interfaces/error.interface';
import { config } from '../../config';
import { Router } from '@angular/router';

@Injectable()
export class BaseService {

  api_url: string = environment.app_base_url;

  constructor(protected http: HttpClient, protected router: Router) {
  }

  get(url, options?) {

    options = this.setAuthHeader(options);

    // Helper service to start ng2-slim-loading-bar progress bar
    // this.helperService.startLoader();
    return this.http.get(this.perepareUrl(url), options)
      .map((res) => {
        return this.handleResponse(res);
      })
      .catch((error: HttpErrorResponse) => this.handleError(error))
      .finally(() => {
        // stop ng2-slim-loading-bar progress bar
        // this.helperService.stopLoader();
      });
  }

  post(url, postBody: any, options?) {

    options = this.setAuthHeader(options);
    // this.helperService.startLoader();
    if (options) {
      return this.http.post(this.perepareUrl(url), postBody, options)
        .map((res) => {
          return this.handleResponse(res);
        })
        .catch((error: HttpErrorResponse) => this.handleError(error))
        .finally(() => {
          // this.helperService.stopLoader();
        });
    } else {
      return this.http.post(this.perepareUrl(url), postBody)
        .map((res) => {
          return this.handleResponse(res);
        })
        .catch((error: HttpErrorResponse) => Observable.throw(error))
        .finally(() => {
          // this.helperService.stopLoader();
        });
    }
  }

  handleError(error: HttpErrorResponse) {

    if (error.status === 401) {
      localStorage.setItem(config.access_token_local_storage, '');

      this.router.navigate([config.login_page_url]);
    }

    return Observable.throw(error);
  }

  delete(url, options?: any) {

    options = this.setAuthHeader(options);

    // this.helperService.startLoader();
    return this.http.delete(this.perepareUrl(url), options)
    // .map((res: HttpResponse<any>) => {
    //   return this.handleResponse(res);
    // })
      .catch((error: Response) => Observable.throw(error))
      .finally(() => {
        // this.helperService.stopLoader();
      });
  }

  put(url, putData, options?) {

    options = this.setAuthHeader(options);

    // this.helperService.startLoader();
    return this.http.put(this.perepareUrl(url), putData, options)
    // .map((res: HttpResponse<any>) => {
    //   return this.handleResponse(res);
    // })
      .catch((error: Response) => Observable.throw(error))
      .finally(() => {
        // this.helperService.stopLoader();
      });
  }

  upload(url: string, file: File) {
    const formData: FormData = new FormData();
    if (file) {
      formData.append('files', file, file.name);
    }
    return this.post(this.perepareUrl(url), formData);
  }

  handleResponse(res): any {
    // My API sends a new jwt access token with each request,
    // so store it in the local storage, replacing the old one.
    // this.refreshToken(res);
    if (res instanceof Object && 'errorMsg' in res) {
      const error: Error = { errorMsg: res.errorMsg };
      // throw new Error(this.errorHandler.parseCustomServerErrorToString(error));
    } else {
      return res;
    }
  }

  refreshToken(res: HttpResponse<any>) {
    const token = res.headers.get(config.access_token_local_storage);
    if (token) {
      localStorage.setItem(config.access_token_local_storage, `${token}`);
    }
  }

  perepareUrl(method) {
    return this.api_url + method;
  }

  setAuthHeader(options) {

    if (options && options.headers) {
      // options.set('Adm-App-Auth', 'custom header value');
      // options.headers.set('X-CustomHeader', 'custom header value');
    } else {
      options = Object.assign(options || {}, {
        headers: new HttpHeaders()
          .set('Adm-App-Auth', environment.app_auth_header)
          .set('Adm-Session-ID', localStorage.getItem(config.access_token_local_storage) || '')
          .set('X-Requested-With', 'XMLHttpRequest')
      });
    }

    return options;
  }

}
