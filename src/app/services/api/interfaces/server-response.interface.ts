
export interface ServerResponse {
  data?: any;
  errorMsg?: string;
}
