import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class AgentApiService extends BaseService {

  api_url: string = environment.agent_base_url;

}
