import { ConfirmPopupComponent } from './../../shared/components/confirm-popup/confirm-popup.component';
import { BsModalService } from 'ngx-bootstrap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from '../services/application.service';
import { ApplicationDetailsModel } from '../models/application-details.model';

@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.less']
})
export class ApplicationDetailsComponent implements OnInit {

  objectKeys = Object.keys;
  application: ApplicationDetailsModel;
  loading = true;
  sections: {
    error: any[],
    field: { title: string, field: string, section: string, type: string, description: string, required: 0 | 1 },
    value: any,
  }[][];

  constructor(private route: ActivatedRoute,
              private modalService: BsModalService,
              private applicationService: ApplicationService,
              private router: Router) {
    this.route.params.subscribe(params => {
      if (params['application_id']) {
        this.initApplication(params['application_id']);
      }
    });
  }

  ngOnInit() {
  }

  initApplication(application_id) {
    this.loading = true;

    this.applicationService.getDetails(application_id).subscribe((application) => {
      this.application = application;

      this.sections = application.getSections();
      this.loading = false;
    });
  }

  onUploadOutput(files, field_name): void {
    this.loading = true;
    const file: File = files[0];

    this.applicationService.updateFormField(this.application.application_form_id, field_name, file).subscribe((application: ApplicationDetailsModel) => {
      this.application = application;

      this.sections = application.getSections();
      this.loading = false;
    })
  }

  resetFormField(field) {
    const modal = this.modalService.show(ConfirmPopupComponent, {
      class: 'modal-sm m-t-10', initialState: {
        message: 'Are you sure you wish to reset this field?'
      }
    });

    modal.content.confirm.subscribe(() => {
      this.loading = true;

      this.applicationService.resetFormField(this.application.application_form_id, field.field).subscribe((application) => {
        this.application = application;

        this.sections = application.getSections();
        this.loading = false;
      });
    });
  }

  checkSignature() {
    let found: boolean = false;
    for (const name in this.sections) {
      const section_data = this.sections[name];

      for (const i in section_data) {
        const item = section_data[i];

        if (item.field.type === 'file' && item.field.title === 'Signature' && item.value && item.value.url) {
          found = true;
        }
      }
    }

    return found;
  }

  removeApplication(application) {
    this.router.navigate([`application/list`]);
  }
}
