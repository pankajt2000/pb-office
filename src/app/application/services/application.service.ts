import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { ApplicationModel } from '../models/application.model';
import { ApplicationDetailsModel } from '../models/application-details.model';

@Injectable()
export class ApplicationService {

  constructor(private api: AgentApiService) {

  }

  getApplications(offset: number = 0, limit: number = 10, agent_id?: number) {
    return this.api.get(`aform?offset=${offset}&limit=${limit}&agent_id=${agent_id}`, { observe: 'response' }).map(response => {
      return [
        response.body.map(application => new ApplicationModel().deserialize(application)),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

  updateFormField(application_form_id, field_name, data) {
    const form_data = new FormData();
    form_data.append(field_name, data);
    
    return this.api.post(`aform-field/${application_form_id}?field_name=${field_name}`, form_data).map(application => new ApplicationDetailsModel().deserialize(application));
  }

  resetFormField(application_form_id, field_name) {
    return this.api.delete(`aform-field/${application_form_id}?field_name=${field_name}`).map(application => new ApplicationDetailsModel().deserialize(application));
  }

  getDetails(application_id) {
    return this.api.get(`aform/${application_id}`).map(application => new ApplicationDetailsModel().deserialize(application));
  }

  deleteApplication(application_form_id) {
    return this.api.delete(`aform/${application_form_id}`);
  }
}
