import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationListComponent } from './application-list/application-list.component';
import { ApplicationDetailsComponent } from './application-details/application-details.component';
import { SharedModule } from '../shared/shared.module';
import { ApplicationService } from './services/application.service';
import { RouterModule } from '@angular/router';
import { DeleteApplicationDirective } from './directives/delete-application.directive';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
  ],
  providers:[
    ApplicationService,
  ],
  declarations: [
    ApplicationListComponent,
    ApplicationDetailsComponent,
    DeleteApplicationDirective
  ]
})
export class ApplicationModule { }
