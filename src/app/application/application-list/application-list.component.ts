import { AgentService } from './../../agent/services/agent.service';
import { AuthService } from './../../authentication/services/auth.service';
import { ApplicationDetailsModel } from './../models/application-details.model';
import { Component, OnInit } from '@angular/core';
import { ApplicationService } from '../services/application.service';
import { ApplicationModel } from '../models/application.model';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { BsModalService } from 'ngx-bootstrap';
import { SendModalComponent } from '../../listings/send-modal/send-modal.component';

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.less']
})
export class ApplicationListComponent implements OnInit {

  limit = 15;
  offset = 0;
  loaded = false;
  loading = false;
  total: number;
  result: ApplicationModel[] = [];

  is_supervisor: boolean;

  agent_list: any[];
  selected_agent_id: any = null;

  constructor(
      private applicationService: ApplicationService,
      private modalService: BsModalService,
      private alertService: BootstrapAlertService,
      private authService: AuthService,
      private agentService: AgentService
    ) {}

  ngOnInit() {
    this.initApplications();
  }

  // initApplications() {
  //   this.loading = true;
  //   this.applicationService.getApplications(this.offset, this.limit).subscribe(data => {
  //     this.result = this.result.concat(data[0]);
  //     this.total = data[1];

  //     this.authService.sessionInfo().subscribe((info) => {
  //       this.is_supervisor = info.user.roles.map(r => r.toLowerCase()).includes('supervisor');

  //       if (this.is_supervisor) {
  //         this.agentService.getAgents().subscribe((agents) => {
  //           this.agent_list = agents[0];
  //           console.log('Got agent list', this.agent_list);

  //           this.loaded = true;
  //           this.loading = false;
  //         });
  //       } else {
  //         this.loaded = true;
  //         this.loading = false;
  //       }
  //     });
  //   }, (errorData) => {
  //     this.alertService.showError('Error: Cannot load application');
  //     this.loading = false;
  //   });
  // }

  initApplications() {
    this.loading = true;
    this.loaded = false;

    this.authService.sessionInfo().subscribe((info) => {
      this.is_supervisor = info.user.roles.map(r => r.toLowerCase()).includes('supervisor');

      if (this.is_supervisor) {
        this.selected_agent_id = "-1";

        this.agentService.getAgents().subscribe((agents) => {
          this.agent_list = agents[0].map(a => { return { id: a.user_id, name: a.name }});
        });
      }

      this.loadApplications();
    })
  }

  loadApplications(force_refresh: boolean = false) {
    this.loaded = false;
    this.loading = true;

    this.applicationService.getApplications(this.offset, this.limit, this.selected_agent_id).subscribe(data => {
      if (!force_refresh) {
        this.result = this.result.concat(data[0]);
      } else {
        this.result = data[0];
      }
      this.total = data[1];

      this.loaded = true;
      this.loading = false;
    })
  }

  sendApplication(aformId: string) {
    this.applicationService.getDetails(aformId).subscribe((aform: ApplicationDetailsModel) => {
      const modal = this.modalService.show(SendModalComponent, {
        class: 'modal-lg m-t-10',
        initialState: { aformDetails: aform }
      });
    })
  }

  removeApplication(application: ApplicationModel) {
    this.total--;
    this.result.splice(this.result.indexOf(application), 1);
  }

  loadMore() {
    this.offset = this.result.length;

    this.loadApplications();
  }

  isSupervisor() {
    return this.is_supervisor;
  }

}
