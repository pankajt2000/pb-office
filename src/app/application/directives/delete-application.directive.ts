import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { ApplicationModel } from '../models/application.model';
import { BsModalService } from 'ngx-bootstrap';
import { ConfirmPopupComponent } from '../../shared/components/confirm-popup/confirm-popup.component';
import { ApplicationService } from '../services/application.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';

@Directive({
  selector: '[appDeleteApplication]'
})
export class DeleteApplicationDirective {

  @Input('appDeleteApplication') application: ApplicationModel;
  @Output('deleted') deleteEventEmiter: EventEmitter<ApplicationModel> = new EventEmitter<ApplicationModel>();

  constructor(private modalService: BsModalService, private applicationService: ApplicationService, private alertService: BootstrapAlertService) {
  }

  @HostListener('click') openConfirm() {
    const modal = this.modalService.show(ConfirmPopupComponent, {
      class: 'modal-sm m-t-10', initialState: {
        message: 'Do you really want to delete this application?'
      }
    });

    modal.content.confirm.subscribe(() => {
      this.applicationService.deleteApplication(this.application.application_form_id).subscribe(() => {

      }, (errorData) => {
        if (errorData.status === 404) {
          this.alertService.showError('Application not found');
        } else {
          this.alertService.showError('Error: Cannot delete the application');
        }
      });
      this.deleteEventEmiter.emit(this.application);
    });
  }

}
