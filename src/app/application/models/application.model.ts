import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { environment } from '../../../environments/environment';

export class ApplicationModel implements DeserializableInterface<ApplicationModel> {

  application_form_id: string;
  building_address: string;
  unit: string;
  client: {
    user_id: string,
    enabled: '0' | '1',
    type: string,
    first_name: string,
    last_name: string,
    phone: string,
    email: string,
    gender: string,
    year_born: string,
    client_type: string,
    source_type: string,
    source_value: string,
    time_created: number,
    last_activity: number,
    name: string,
    photo: string,
    status: string,
    status_color: string
  };
  agent: {
    user_id: string,
    enabled: '1' | '0',
    type: string,
    first_name: string,
    last_name: string,
    phone: string,
    email: string,
    gender: string,
    year_born: string,
    client_type: string,
    source_type: string,
    source_value: string,
    time_created: number,
    last_activity: number,
    name: string,
    photo: string
  };
  status: string;
  status_color: string;
  time_created: number;
  time_updated: number;

  getDownloadUrl() {
    return `${environment.agent_base_url}aform/${this.application_form_id}/download`;
  }

  deserialize(input: any): ApplicationModel {
    Object.assign(this, input);
    return this;
  }

}
