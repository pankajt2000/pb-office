import { DeserializableInterface } from '../../shared/models/deserializable.interface';
import { ApplicationModel } from './application.model';

export class ApplicationDetailsModel extends ApplicationModel implements DeserializableInterface<ApplicationDetailsModel> {

  report: {
    error: any[],
    field: { title: string, field: string, section: string, type: string, description: string, required: 0 | 1 },
    value: string & {
      url: string,
      filename: string,
      media_item_id: string | number
    },
  }[];

  getSections() {
    const sections = [];

    this.report.forEach(item => {

      if(typeof item.field.section === 'undefined') return;

      if (!(item.field.section in sections)) {
        sections[item.field.section] = [];
      }
      sections[item.field.section].push(item);
    });

    return sections;
  }

  deserialize(input: any): ApplicationDetailsModel {
    Object.assign(this, input);
    return this;
  }

}
