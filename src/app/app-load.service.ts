import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable()
export class AppLoadService {

  constructor() { }

  initializeGoogleApi() {
    return new Promise((resolve, reject) => {
      gapi.load('auth2', _ => {
        setTimeout(() => {
          (window as any).$$auth2 = gapi.auth2.init({
            client_id: environment.google_client_id,
            scope: 'https://mail.google.com/'
          });

          resolve();
        }, 1);
      });
    });
  }
}
