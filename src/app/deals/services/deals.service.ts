import { Injectable } from '@angular/core';
import { AgentApiService } from '../../services/api/agent-api.service';
import { DealDetailsModel } from '../models/deal-details.model';
import { DealListModel } from '../models/deal-list.model';
import { ApartmentModel } from '../../listings/models/apartment.model';

@Injectable()
export class DealsService {

  constructor(private api: AgentApiService) { }

  getDeals(offset: number = 0, limit: number = 10, agent_id?: number, status?: string) {
    return this.api.get(`deal-sheets?offset=${offset}&limit=${limit}&agent_id=${agent_id}&status=${status}`, { observe: 'response' }).map(response => {
      return [
        response.body.map(sheet => new DealListModel().deserialize(sheet)),
        response.headers.get('Adm-Result-Total-Count')
      ];
    });
  }

  getDeal(deal_id: string) {
    return this.api.get(`deal-sheets/${deal_id}`).map(deal_sheet => new DealDetailsModel().deserialize(deal_sheet));
  }

  createNewDeal(client_id) {
    return this.api.post(`deal-sheets`, { client_id });
  }

  updateDeal(deal_id: string, deal_sheet?, status?) {
    deal_sheet = deal_sheet || null;
    status = status || null;

    return this.api.put(`deal-sheets/${deal_id}`, { deal_sheet, status }).map(deal_sheet => new DealDetailsModel().deserialize(deal_sheet));
  }

  finalizeDeal(deal_id: string, deal_sheet?) {
    return this.api.post(`deal-sheets/${deal_id}`, { deal_sheet });
  }

  updateDealFile(deal_id: string, file_name: string, file) {
    const fd = new FormData();
    fd.append(file_name, file);

    return this.api.post(`deal-sheets-file/${deal_id}?file_name=${file_name}`, fd).map(deal => new DealDetailsModel().deserialize(deal));
  }

  deleteDeal(deal_id: string) {
    return this.api.delete(`deal-sheets/${deal_id}`);
  }

  getAddressTypeahead(q) {
    return this.api.get(`deal-sheets-typeahead?type=address&q=${q}`);
  }

  getUnitTypeahead(address, q) {
    return this.api.get(`deal-sheets-typeahead?type=unit&address=${address}&q=${q}`, { observe: 'response' }).map(res => {
      return res.body.map(apt => new ApartmentModel().deserialize(apt));
    });
  }

}
