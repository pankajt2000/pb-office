import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DealsListComponent } from './deals-list/deals-list.component';
import { DealDetailsComponent } from './deal-details/deal-details.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { DealsService } from './services/deals.service';
import { EditDealComponent } from './edit-deal/edit-deal.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { NewDealComponent } from './new-deal/new-deal.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    NgxCurrencyModule
  ],
  providers: [
    DealsService,
  ],
  declarations: [
    DealsListComponent,
    DealDetailsComponent,
    EditDealComponent,
    NewDealComponent
  ]
})
export class DealsModule { }
