import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DealsService } from '../services/deals.service';
import { ClientService } from '../../client/services/client.service';
import { Subject } from 'rxjs/Subject';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { ClientModel } from '../../client/models/client.model';
import { ClientDetailsModel } from '../../client/models/client-details.model';
import { of } from 'rxjs/observable/of';
import { BsModalService } from 'ngx-bootstrap';
import { CreateClientComponent } from '../../client/create-client/create-client.component';
import { AuthService } from '../../authentication/services/auth.service';

@Component({
  selector: 'app-new-deal',
  templateUrl: './new-deal.component.html',
  styleUrls: ['./new-deal.component.less']
})
export class NewDealComponent implements OnInit {

  loading = false;
  checking_source: boolean = false;

  client_source: string;

  is_supervisor: boolean;

  client_data = {
    values: [],
    typeahead: new Subject<string>(),
    loading: false,
    selected: null,
    switch_map: (term) => this.clientService.getClients( 10, 0, 'CURRENT', term),
    on_change: (data) => {
      this.checking_source = true;

      if (!data || !data.value) {
        this.checking_source = false;
        return;
      }

      this.clientService.getClientDetails(data.value).subscribe(client => {
        this.client_source = client.source_value;

        this.checking_source = false;
      })
    },
    create: (term) => {
      const first_name = term.split(' ').shift();
      const last_name = term.split(' ').slice(1).join(' ');

      const modal = this.modalService.show(CreateClientComponent, { initialState: { first_name, last_name } });
      modal.content.client_data = { first_name, last_name };

      modal.content.clientCreated.subscribe(id => {
        this.client_data.values = [{ name: term, value: id }];
        this.client_data.selected = id;
        this.client_data.on_change({ name: term, value: id });
      });
    }
  };

  constructor(
    private dealsService: DealsService,
    private authService: AuthService,
    private clientService: ClientService,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.loading = true;

    this.authService.sessionInfo().subscribe(data => {
      this.is_supervisor = data.user.roles.map(r => r.toLowerCase()).includes('supervisor');

      this.route.queryParams
        .map(params => params.client_id)
        .switchMap(id => !!id ? this.clientService.getClientDetails(id) : of({}))
        .subscribe((client: ClientDetailsModel) => {
          this.client_data.selected = client.user_id;
          this.client_data.values.push({ name: client.name, value: client.user_id });

          this.initTypeahead();

          this.loading = false;
        });
    })
  }

  ngOnInit() {
  }

  initTypeahead() {
    this.client_data.typeahead.pipe(
      tap(() => this.client_data.loading = true),
      distinctUntilChanged(),
      debounceTime(200),
      switchMap(term => this.client_data.switch_map(term))
    ).subscribe(x => {
      this.client_data.values = x[0].map((client: ClientModel) => {
        return { name: client.name, value: client.user_id }
      });

      this.client_data.loading = false;
    }, () => {
      this.client_data.values = [];
      this.client_data.loading = false;
    });
  }

  submit() {
    this.dealsService.createNewDeal(this.client_data.selected).subscribe(res => this.router.navigate(['/client', 'deals', res.deal_id, 'edit']));
  }

  cancel() {
    this.router.navigate(['/client', 'deals', 'list']);
  }

}
