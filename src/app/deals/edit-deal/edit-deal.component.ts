import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DealDetailsModel } from '../models/deal-details.model';
import { DealsService } from '../services/deals.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrencyMaskConfig } from 'ngx-currency/src/currency-mask.config';
import { CurrencyPipe, Location } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import { distinctUntilChanged, tap, debounceTime, switchMap } from 'rxjs/operators';
import { ClientService } from '../../client/services/client.service';
import { SearchService } from '../../search/services/search.service';
import { BootstrapAlertService } from 'ngx-bootstrap-alert-service';
import { AuthService } from "../../authentication/services/auth.service";
import { BsModalService } from "ngx-bootstrap";
import { ConfirmPopupComponent } from "../../shared/components/confirm-popup/confirm-popup.component";

declare var $: any;

@Component({
  selector: 'app-edit-deal',
  templateUrl: './edit-deal.component.html',
  styleUrls: ['./edit-deal.component.less']
})
export class EditDealComponent implements OnInit, OnDestroy {

  dealForm: FormGroup;

  deal: DealDetailsModel;

  is_supervisor: boolean;

  loading = false;
  saving = false;
  submitted = false;

  autofills = {
    basic_info: {
      client_name: {
        values: [],
        typeahead: new Subject<string>(),
        loading: false,
        display_prop: 'name',
        is_complex: true,
        switch_map: (term) => this.clientService.getClients(10, 0, 'CURRENT', term, (a, b) => {
          return a.first_name.charCodeAt(0) - b.first_name.charCodeAt(0);
        }),
        on_select: (data) => {
          if (!data || !data.model) {
            return;
          }

          this.dealForm.get('basic_info.client_phone').patchValue(data.model.phone);
          this.dealForm.get('basic_info.client_email').patchValue(data.model.email);
        },
        create: (term) => ({ name: term, value: term, model: { phone: '', email: '' } })
      },
      management: {
        values: [],
        typeahead: new Subject<string>(),
        loading: false,
        display_prop: null,
        is_complex: false,
        switch_map: (term) => this.searchService.getCompanies(term),
        on_select: (data) => {},
        create: (term) => ({ name: term, value: term })
      },
      apt_address: {
        values: [],
        typeahead: new Subject<string>(),
        loading: false,
        display_prop: null,
        is_complex: false,
        switch_map: (term) => this.dealsService.getAddressTypeahead(term),
        on_select: (data) => {},
        create: (term) => ({ name: term, value: term })
      },
      apt_unit: {
        values: [],
        typeahead: new Subject<string>(),
        loading: false,
        display_prop: 'unit',
        is_complex: true,
        switch_map: (term) => this.dealsService.getUnitTypeahead(this.dealForm.get('basic_info.apt_address').value, term),
        on_select: (data) => {
          this.dealForm.get('basic_info.apt_price').patchValue(data.model.price);
        },
        create: (term) => ({ name: term, value: term, model: { price: 0 } })
      }
    }
  };

  priceMaskConfig: CurrencyMaskConfig = {
    align: 'left',
    allowNegative: false,
    allowZero: true,
    decimal: '.',
    precision: 2,
    prefix: '$ ',
    suffix: '',
    thousands: ',',
    nullable: true
  };

  constructor(private dealsService: DealsService,
              private clientService: ClientService,
              private searchService: SearchService,
              private alertService: BootstrapAlertService,
              private modalService: BsModalService,
              private authService: AuthService,
              private fb: FormBuilder,
              private location: Location,
              private route: ActivatedRoute,
              private router: Router) {

    this.route.params.subscribe(params => {
      this.loading = true;

      const deal_sheet_id = params['deal_sheet_id'] || 0;
      window.scroll(0, 0);

      this.authService.sessionInfo().subscribe(info => {
        this.is_supervisor = info.user.roles.map(r => r.toLowerCase()).includes('supervisor');

        this.initDeal(deal_sheet_id);
      });

      this.loading = false;
    });
  }

  initDeal(deal_sheet_id) {
    this.dealsService.getDeal(deal_sheet_id).subscribe(item => {
      if (item.status !== 'draft' && !this.is_supervisor) {
        this.router.navigate(['/client', 'deals', 'list']);
      }

      this.deal = item;

      this.initForm();
    });
  }

  initAutocompletes() {
    for (const category in this.autofills) {
      for (const item in this.autofills[category]) {
        const curr = this.autofills[category][item];
        const prefilled_value = this.deal.deal_sheet[category][item];

        if (prefilled_value) {
          curr.values = [...curr.values, { name: prefilled_value, value: prefilled_value }];
        }

        curr.typeahead.pipe(
          tap(() => curr.loading = true),
          distinctUntilChanged(),
          debounceTime(200),
          switchMap(term => curr.switch_map(term))
        ).subscribe(x => {
          const iterable = x.length === 2 && Array.isArray(x[0]) ? x[0] : x;

          curr.values = iterable.map(obj => {
            const val = curr.display_prop ? obj[curr.display_prop] : obj;

            return { name: val, value: val, model: curr.is_complex ? obj : null }
          });

          curr.loading = false;
        }, () => {
          curr.values = [];
          curr.loading = false;
        });
      }
    }
  }

  initForm() {
    this.dealForm = this.fb.group({
      basic_info: this.fb.group({
        agent_name: [this.deal.deal_sheet.basic_info.agent_name, Validators.required],
        date: [this.deal.deal_sheet.basic_info.date, Validators.required],
        deal_no: [this.deal.deal_sheet.basic_info.deal_no, Validators.required],
        apt_address: [this.deal.deal_sheet.basic_info.apt_address, Validators.required],
        apt_unit: [this.deal.deal_sheet.basic_info.apt_unit, Validators.required],
        apt_price: [this.deal.deal_sheet.basic_info.apt_price, Validators.required],
        management: [this.deal.deal_sheet.basic_info.management],
        lease_start: [this.deal.deal_sheet.basic_info.lease_start],
        client_name: [this.deal.deal_sheet.basic_info.client_name, Validators.required],
        client_phone: [this.deal.deal_sheet.basic_info.client_phone],
        client_email: [this.deal.deal_sheet.basic_info.client_email],
        roommates: [this.deal.deal_sheet.basic_info.roommates]
      }),
      paperwork: this.fb.group({
        lease_pages: [this.deal.deal_sheet.paperwork.lease_pages],
        lease_pages_2: [this.deal.deal_sheet.paperwork.lease_pages_2],
        dos: [this.deal.deal_sheet.paperwork.dos],
        commission_check: [this.deal.deal_sheet.paperwork.commission_check]
      }),
      commissions: this.fb.array(this._addCommissions()),
      funds: this.fb.group({
        paid: [this.deal.deal_sheet.funds.paid],
        methods: this.fb.array(this._addFundMethods()),
        funds_previously_deposited: [this.deal.deal_sheet.funds.funds_previously_deposited]
      })
    });

    this.initAutocompletes();
  }

  get commissions(): FormArray { return this.dealForm.get('commissions') as FormArray; }
  get fund_methods(): FormArray { return this.dealForm.get('funds.methods') as FormArray; }

  private _addCommissions() {
    const commissions: FormGroup[] = [];

    this.deal.deal_sheet.commissions.forEach((commission, index) => {

      commissions.push(this.fb.group({
        description: [commission.description],
        fee: [commission.fee],
        op: [commission.op],
        deducts: [commission.deducts]
      }));

    });

    return commissions;
  }

  private _addFundMethods() {
    const fund_methods: FormGroup[] = [];

    this.deal.deal_sheet.funds.methods.forEach((method, index) => {

      fund_methods.push(this.fb.group({
        label: [method.label], checked: [method.checked]
      }));

    });

    return fund_methods;
  }

  save(draft = true) {
    this.loading = true;
    this.saving = true;

    const deal_sheet_id = this.deal.deal_sheet_id;
    const new_deal = new DealDetailsModel().deserialize({
      deal_sheet_id: this.deal.deal_sheet_id,
      status: this.deal.status,
      deal_sheet: this.dealForm.getRawValue()
    });

    if (!draft) {

      const modal = this.modalService.show(ConfirmPopupComponent, {
        initialState: {
          message: 'You are about to finalize this deal and submit it for review. You will no longer be able to edit it after submission. Are you sure you wish to submit the deal?'
        }
      });

      modal.content.confirm.subscribe(_ => {
        this.dealsService.finalizeDeal(deal_sheet_id, new_deal.deal_sheet).subscribe(_ => {
          this.router.navigate(['/client', 'deals', 'list']);
        });
      });

      modal.content.cancel.subscribe(_ => {
        this.loading = false;
        this.saving = false;
      });

    } else {
      this.dealsService.updateDeal(deal_sheet_id, new_deal.deal_sheet, new_deal.status).subscribe(deal => {

          this.deal = deal;
          this.initForm();

          this.loading = false;
          this.saving = false;

          this.alertService.showSucccess('Deal successfuly saved.');

      });
    }
  }

  onUploadOutput(files, file_name): void {
    this.loading = true;
    const file: File = files[0];

    this.dealsService.updateDealFile(this.deal.deal_sheet_id, file_name, file).subscribe((deal: DealDetailsModel) => {
      this.route.queryParams.subscribe(params => {

        const new_deal_sheet = this.dealForm.getRawValue();
        new_deal_sheet.paperwork = deal.deal_sheet.paperwork;

        const updated_deal = {
          deal_sheet_id: this.deal.deal_sheet_id,
          deal_sheet: new_deal_sheet,
          status: this.deal.status
        };

        this.deal = new DealDetailsModel().deserialize(updated_deal);
        this.initForm();

        this.loading = false;
      });
    });
  }

  onStatusChange(event: any) {
    this.loading = true;
    const status = event.target.value;

    this.dealsService.updateDeal(this.deal.deal_sheet_id, null, status).subscribe((deal: DealDetailsModel) => {
      const updated_deal = Object.assign({}, deal, {
        deal_sheet_id: this.deal.deal_sheet_id,
        deal_sheet: this.dealForm.getRawValue(),
        status: status
      });

      this.deal = new DealDetailsModel().deserialize(updated_deal);
      this.initForm();

      this.loading = false;
    });
  }

  getCommissionSum(type) {
    const total = this.dealForm.get('commissions').value.reduce((acc, curr) => {
      return curr.deducts ? acc - curr[type] : acc + curr[type];
    }, 0);

    return new CurrencyPipe('en-us').transform(total, 'USD', 'symbol');
  }

  getCommissionPriceMaskConfig(commission) {
    return {
      align: 'left',
      allowNegative: false,
      allowZero: true,
      decimal: '.',
      precision: 2,
      prefix: `${commission.deducts ? '- ' : ''}$ `,
      suffix: '',
      thousands: ',',
      nullable: true
    };
  }

  ngOnInit() {
    const page_content = $('.page-content');
    page_content.addClass('page-wizard');
    page_content.addClass('wizardbg');
  }

  ngOnDestroy() {
    const page_content = $('.page-content');
    page_content.removeClass('page-wizard');
    page_content.removeClass('wizardbg');
  }

  cancel() {
    this.router.navigate(['/client', 'deals', 'list']);
  }

}
