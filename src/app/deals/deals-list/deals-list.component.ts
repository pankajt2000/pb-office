import { BsModalService } from 'ngx-bootstrap';
import { DealsService } from '../services/deals.service';
import { Component, OnInit } from '@angular/core';
import { DealListModel } from '../models/deal-list.model';
import { AuthService } from '../../authentication/services/auth.service';
import { AgentService } from '../../agent/services/agent.service';
import {Router} from '@angular/router';
import { AgentModel } from '../../agent/models/agent.model';

@Component({
  selector: 'app-deals-list',
  templateUrl: './deals-list.component.html',
  styleUrls: ['./deals-list.component.less']
})
export class DealsListComponent implements OnInit {

  limit = 15;
  offset = 0;
  loaded = false;
  loading = false;
  total: number;
  deals: DealListModel[] = [];

  is_supervisor: boolean = false;

  agent_list: AgentModel[];
  selected_agent_id: any = null;

  deal_status_filter: string = 'all';

  constructor(
    private dealsService: DealsService,
    private authService: AuthService,
    private agentService: AgentService,
    private router: Router
  ) {}

  ngOnInit() {
    this.initDeals();
  }

  initDeals() {
    this.loading = true;
    this.loaded = false;

    this.authService.sessionInfo().subscribe((info) => {
      this.is_supervisor = info.user.roles.map(r => r.toLowerCase()).includes('supervisor');

      if (this.is_supervisor) {
        this.selected_agent_id = "-1";

        this.agentService.getAgents().subscribe((agents) => {
          this.agent_list = agents[0].filter(a => !a.is_test_user && a.active).map(a => { return { user_id: a.user_id, name: a.name }});
        });
      }

      this.loadDeals();
    })
  }

  loadDeals(force_refresh: boolean = false) {
    this.loaded = false;
    this.loading = true;

    this.dealsService.getDeals(this.offset, this.limit, this.selected_agent_id, this.deal_status_filter).subscribe(data => {
      if (!force_refresh) {
        this.deals = this.deals.concat(data[0]);
      } else {
        this.deals = data[0];
      }
      this.total = data[1];

      this.loaded = true;
      this.loading = false;
    })
  }

  createNewDeal() {
    this.router.navigate(['/client', 'deals', 'new']);
  }

  deleteDeal(deal: DealListModel) {
    this.loaded = false;
    this.loading = true;

    this.dealsService.deleteDeal(deal.deal_sheet_id).subscribe(res => {
      this.loadDeals(true);
    });
  }

  onStatusChange(event, deal: DealListModel) {
    const status = event.target.value;

    this.dealsService.updateDeal(deal.deal_sheet_id, null, status).subscribe(deal => {
      this.loadDeals(true);
    });
  }

  loadMore() {
    this.offset = this.deals.length;

    this.loadDeals();
  }

  isSupervisor() {
    return this.is_supervisor;
  }

}
