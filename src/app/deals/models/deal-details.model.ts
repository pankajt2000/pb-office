import { DeserializableInterface } from "../../shared/models/deserializable.interface";

export class DealDetailsModel implements DeserializableInterface<DealDetailsModel>
{
  deal_sheet_id: string;

  status: string;
  draft: boolean;

	deal_sheet: {
    basic_info: {
      agent_name: string,
      date: string,
      deal_no: number | string,

      apt_address: string,
      apt_unit: string,
      apt_price: number,

      management: string,
      lease_start: string,

      client_name: string,
      client_phone: string,
      client_email: string,

      roommates: string,
    };

    paperwork: {
      lease_pages: {
        filename: string,
        url: string,
        value: string
      },
      lease_pages_2: {
        filename: string,
        url: string,
        value: string
      }
      dos: {
        filename: string,
        url: string,
        value: string
      },
      commission_check: {
        filename: string,
        url: string,
        value: string
      }
    };

    commissions: {
      description: string,
      fee: number,
      op: number,
      deducts: boolean
    }[];

    funds: {
      paid: boolean,
      methods: {
        label: string,
        checked: boolean
      }[],
      funds_previously_deposited: boolean
    };
  };

	constructor() {}

	deserialize(input: any): DealDetailsModel {
		Object.assign(this, input);
		return this;
	}
}
