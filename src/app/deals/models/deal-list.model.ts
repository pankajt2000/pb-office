import {DeserializableInterface} from '../../shared/models/deserializable.interface';

export class DealListModel implements DeserializableInterface<DealListModel>
{
  deal_sheet_id: string;
  deal_no: number;

  client_name: string;
  client_email: string;

  building_address: string;
  unit: string;

  agent_name: string;

  status: string;
  draft: boolean;

  date_submitted: string;

  constructor() {}

  deserialize(input: any): DealListModel {
    Object.assign(this, input);
    return this;
  }
}
