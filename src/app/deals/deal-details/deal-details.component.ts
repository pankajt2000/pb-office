import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DealsService } from '../services/deals.service';
import { DealDetailsModel } from '../models/deal-details.model';
import { CurrencyPipe } from '@angular/common';
import { AuthService } from '../../authentication/services/auth.service';
import { UserModel } from '../../authentication/models/user.model';

@Component({
  selector: 'app-deal-details',
  templateUrl: './deal-details.component.html',
  styleUrls: ['./deal-details.component.less']
})
export class DealDetailsComponent implements OnInit {

  deal_details: DealDetailsModel;
  loading: boolean;

  is_supervisor: boolean;

  feeCommissionSum: string;
  opCommissionSum: string;

  constructor(private route: ActivatedRoute,
              private dealsService: DealsService,
              private authService: AuthService,
              private router: Router) {
    this.route.params
      .map(params => params.deal_sheet_id)
      .filter(id => !!id)
      .subscribe(id => this.initDealSheet(id));
  }

  ngOnInit() {
  }

  initDealSheet(deal_sheet_id) {
    this.loading = true;

    this.authService.sessionInfo().subscribe(info => {
      const user: UserModel = info.user;
      this.is_supervisor = user.roles.map(r => r.toLowerCase()).includes('supervisor');

      this.dealsService.getDeal(deal_sheet_id).subscribe((deal) => {
        this.deal_details = deal;
  
        this.feeCommissionSum = this.getCommissionSum('fee');
        this.opCommissionSum = this.getCommissionSum('op');
  
        this.loading = false;
      });
    })
  }

  getCommissionSum(type) {
    const total = this.deal_details.deal_sheet.commissions.reduce((acc, curr) => {
      return curr.deducts ? acc - curr[type] : acc + curr[type];
    }, 0);

    return new CurrencyPipe('en-us').transform(total, 'USD', 'symbol');
  }

  formatCommissionValue(commission, type) {
    const value = commission[type] || 0;
    const price = new CurrencyPipe('en_us').transform(value, 'USD', 'symbol');
    const prefix = commission.deducts && value !== 0 ? '- ' : '';

    return `${prefix}${price}`;
  }

}
