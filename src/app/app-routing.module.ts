import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { MailLayoutComponent } from './layout/mail-layout/mail-layout.component';
import { InboxComponent } from './mailbox/inbox/inbox.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './authentication/login/login.component';
import { AuthLayoutComponent } from './authentication/auth-layout/auth-layout.component';
import { AuthGuardService } from './authentication/services/auth-guard.service';
import { ResetPasswordComponent } from './authentication/reset-password/reset-password.component';
import { LogoutComponent } from './authentication/logout/logout.component';
import { SendComponent } from './mailbox/send/send.component';
import { RentalsComponent } from './search/rentals/rentals.component';
import { SalesComponent } from './search/sales/sales.component';
import { TrendsRentalsComponent } from './stats/trends-rentals/trends-rentals.component';
import { TrendsSalesComponent } from './stats/trends-sales/trends-sales.component';
import { ViewListingComponent } from './listings/view-listing/view-listing.component';
import { NewListingComponent } from './listings/new-listing/new-listing.component';
import { SearchByCriteriaComponent } from './search/search-by-criteria/search-by-criteria.component';
import { EditListingComponent } from './listings/edit-listing/edit-listing.component';
import { AdvertisedListComponent } from './listings/advertised-list/advertised-list.component';
import { EditProfileComponent } from './user/edit-profile/edit-profile.component';
import { CreatedByMeComponent } from './search/created-by-me/created-by-me.component';
import { ClientNewListingsComponent } from './search/client-new-listings/client-new-listings.component';
import { ClientListComponent } from './client/client-list/client-list.component';
import { ClientDetailsComponent } from './client/client-details/client-details.component';
import { RecentListingComponent } from './listings/recent-listing/recent-listing.component';
import { ApplicationListComponent } from './application/application-list/application-list.component';
import { ApplicationDetailsComponent } from './application/application-details/application-details.component';
import { BuildingListComponent } from './building/building-list/building-list.component';
import { BuildingDetailsComponent } from './building/building-details/building-details.component';
import { AgentListComponent } from './agent/agent-list/agent-list.component';
import { RoleGuardService } from './authentication/services/role-guard.service';
import { EventLogComponent } from './agent/event-log/event-log.component';
import { PaymentsListComponent } from './payments/payments-list/payments-list.component';
import { DealsListComponent } from './deals/deals-list/deals-list.component';
import { DealDetailsComponent } from './deals/deal-details/deal-details.component';
import { EditDealComponent } from './deals/edit-deal/edit-deal.component';
import { NewDealComponent } from './deals/new-deal/new-deal.component';
import { DosListComponent } from './dos/dos-list/dos-list.component';
import { NewDosFormComponent } from './dos/new-dos-form/new-dos-form.component';
import { EditDosComponent } from './dos/edit-dos/edit-dos.component';
import { DosDetailsComponent } from './dos/dos-details/dos-details.component';
import { InvoiceListComponent } from './invoices/invoice-list/invoice-list.component';
import { InvoiceDetailsComponent } from './invoices/invoice-details/invoice-details.component';
import { EditInvoiceComponent } from './invoices/edit-invoice/edit-invoice.component';
import { CreateInvoiceComponent } from './invoices/create-invoice/create-invoice.component';
import { RestoreSessionComponent } from './authentication/restore-session/restore-session.component';
import { PerformanceComponent } from "./performance/overview/performance.component";
import { CompsComponent } from "./comps/comps.component";
import { AgentDetailsComponent } from "./agent/agent-details/agent-details.component";


const routes: Routes = [
  { path: '', redirectTo: '/app/dashboard', pathMatch: 'full' },
  {
    path: 'app',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: '/app/dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard' } },
    ]
  },
  {
    path: 'search',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: '/search/rental', pathMatch: 'full' },
      { path: 'rental', component: RentalsComponent, data: { ad_type: 'rental' } },
      { path: 'sale', component: SalesComponent, data: { ad_type: 'sale' } },
      { path: 'criteria/:search_id', component: SearchByCriteriaComponent },
      { path: 'advertised', component: AdvertisedListComponent },
      { path: 'created-by-me', component: CreatedByMeComponent },
      { path: 'clients-new-listings/:client_id', component: ClientNewListingsComponent },
    ]
  },
  {
    path: 'stats',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: '/stats/rental', pathMatch: 'full' },
      { path: 'rental', component: TrendsRentalsComponent, data: { ad_type: 'rental' } },
      { path: 'sale', component: TrendsSalesComponent, data: { ad_type: 'sale' } }
    ]
  },
  {
    path: 'listing',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: '/search/rentals', pathMatch: 'full' },
      { path: ':listing_id/details', component: ViewListingComponent },
      { path: ':listing_id/edit', component: EditListingComponent },
      { path: 'new', component: NewListingComponent },
      { path: 'recent', component: RecentListingComponent },
      { path: 'advertised', component: AdvertisedListComponent },
      { path: 'comps', component: CompsComponent }
    ]
  },
  {
    path: 'mailbox',
    component: MailLayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', component: InboxComponent, pathMatch: 'full' },
      { path: 'send', component: SendComponent },
      { path: ':folder', component: InboxComponent },
      { path: '**', redirectTo: '/mailbox', pathMatch: 'full' },
    ]
  },
  {
    path: 'user',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: '/user/profile', pathMatch: 'full' },
      { path: 'profile', component: EditProfileComponent },
    ]
  },
  {
    path: 'client',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: '/client/list', pathMatch: 'full' },
      { path: 'list', component: ClientListComponent },
      { path: ':client_id/details', component: ClientDetailsComponent },
      { path: 'deals', redirectTo: '/client/deals/list', pathMatch: 'full' },
      { path: 'deals/list', component: DealsListComponent },
      { path: 'deals/new', component: NewDealComponent },
      { path: 'deals/:deal_sheet_id/details', component: DealDetailsComponent },
      { path: 'deals/:deal_sheet_id/edit', component: EditDealComponent },
      { path: 'dos', redirectTo: '/client/dos/list', pathMatch: 'full' },
      { path: 'dos/list', component: DosListComponent },
      { path: 'dos/new', component: NewDosFormComponent },
      { path: 'dos/:dos_form_id/details', component: DosDetailsComponent },
      { path: 'dos/:dos_form_id/edit', component: EditDosComponent }
    ]
  },
  {
    path: 'application',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: '/application/list', pathMatch: 'full' },
      { path: 'list', component: ApplicationListComponent },
      { path: ':application_id/details', component: ApplicationDetailsComponent },
    ]
  },
  {
    path: 'building',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: '/building/list', pathMatch: 'full' },
      { path: 'list', component: BuildingListComponent },
      { path: ':building_id/details', component: BuildingDetailsComponent },
    ]
  },
  {
    path: 'payments',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: 'payments/list', pathMatch: 'full' },
      { path: 'list', component: PaymentsListComponent }
    ]
  },
  {
    path: 'invoices',
    component: LayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: '/invoices/list/rental', pathMatch: 'full' },
      { path: 'list/:type', component: InvoiceListComponent },
      { path: 'new', component: CreateInvoiceComponent },
      { path: ':invoice_id/details', component: InvoiceDetailsComponent },
      { path: ':invoice_id/edit', component: EditInvoiceComponent }
    ]
  },
  {
    path: 'agent',
    component: LayoutComponent,
    canActivate: [AuthGuardService, RoleGuardService],
    children: [
      { path: '', redirectTo: '/agent/list', pathMatch: 'full' },
      { path: 'list', component: AgentListComponent },
      { path: ':user_id/event-log', component: EventLogComponent },
      { path: ':user_id/details', component: AgentDetailsComponent }
    ],
    data: {
      expectedRole: 'SUPERVISOR'
    }
  },
  {
    path: 'performance',
    component: LayoutComponent,
    canActivate: [AuthGuardService, RoleGuardService],
    children: [
      { path: '', redirectTo: '/performance/rental', pathMatch: 'full' },
      { path: ':type', component: PerformanceComponent }
    ]
  },
  {
    path: 'auth',
    component: AuthLayoutComponent,
    children: [
      { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'logout', component: LogoutComponent },
      { path: 'restore-session', component: RestoreSessionComponent },
      { path: 'forgot-password', component: ResetPasswordComponent },
    ]
  },
  { path: '**', redirectTo: '/app/dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
